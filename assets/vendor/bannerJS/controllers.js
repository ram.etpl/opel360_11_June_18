'use strict';


var app = angular


    .module('app', ['angularFileUpload'])

 
    .controller('AppController', ['$scope','$http', 'FileUploader', function($scope,$http, FileUploader) {
        var uploader = $scope.uploader = new FileUploader({ 
            url: base_ourl+'banner/upload_banners'
            
        });

        // FILTERS

        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
				
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            //console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            //console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            //console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            //console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            //console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            //console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            //console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            //console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
            $scope.addController(fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function(fileItem, response, status, headers) {
            //console.log(fileItem);     
    };
    
     $scope.addController = function(fileItem, response, status, headers){
					
					var file_name = fileItem['file']['name'];
					$scope.file = file_name;
					$scope.file_size = fileItem['file']['size'];
					$scope.file_type = fileItem['file']['type'];
					$scope.file_last_modifieddate = fileItem['file']['lastModifiedDate'];
					
					
					
					
					$http({
						  method  : 'POST',
						  url     : base_ourl+"banner/add_banners",
						  data    : { "file_name" : $scope.file, "file_size" : $scope.file_size, "file_type" : $scope.file_type, "file_last_modifieddate" : $scope.file_last_modifieddate}
						  //headers : {'Content-Type': 'application/x-www-form-urlencoded'}

					  }).success(function(data) {
						  
						}).error(function(data) {

						});
		}
            
				

        console.info('uploader', uploader);
    }]);
    
    
    app.controller('myCtrl', function($scope) {
    
		$scope.getbanners = function(){
				//getBannersList();
				location.reload();

			}
		

	});
    
    
    
