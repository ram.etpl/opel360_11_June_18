/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.removeButtons = 'Save,Form,Checkbox,Radio,TextField,Textarea,Select,Button,HiddenField,ImageButton';
	
	config.extraPlugins = 'font,tableresize';
	//config.filebrowserUploadUrl = '../../assets/vendor/editors/ckeditor/ckupload.php';
	config.filebrowserUploadUrl = '../assets/vendor/editors/ckeditor2/ckupload.php';
};
