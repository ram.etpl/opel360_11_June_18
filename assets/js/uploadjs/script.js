$(function(){

    var ul = $('#upload ul');
	//var base_url = 'http://192.168.100.14/opel/';
    $('#drop a').click(function(){
        // Simulate a click on the file input button
        // to show the file browser dialog
        $(this).parent().find('input').click();
    });

    // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({

        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
			
			var ext = getFileExtension(data.files[0].name);
			
			var ext = ext.toLowerCase();
			flag=1;
			if(ext != 'png' && ext != 'jpg' && ext != 'jpeg'){
				alert('Invalid File Extension for '+data.files[0].name);
				flag=0;
			}
			if(flag==1){
			//alert(JSON.stringify(data.files));
				var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
					' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

				// Append the file name and file size
				//alert(data.status);
				tpl.find('p').text(data.files[0].name)
							 .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

				// Add the HTML to the UL element
				//alert(data.files[0].name);
				 
				data.context = tpl.appendTo(ul);

				// Initialize the knob plugin
				tpl.find('input').knob();

				// Listen for clicks on the cancel icon
				tpl.find('span').click(function(){

					if(tpl.hasClass('working')){
						jqXHR.abort();
					}

					tpl.fadeOut(function(){
						tpl.remove();
					});

				});

				// Automatically upload the file once it is added to the queue
				var jqXHR = data.submit();
			}
            
            
        },

        progress: function(e, data){
			
			console.log(data);
			
            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
                var car_model_id = $("#car_model_id").val();
                getcatlogimages(car_model_id);
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });


    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function getFileExtension(name)
	{
		return name.split('.').pop();
	}
    function getcatlogimages(car_model_id) {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: base_ourl+"catalog/getcarcatelogimages",
			data: {"model_id":car_model_id},
		}).success(function (json) { 
			if(json.status == 200)
			{		
				 $("#all_car_images").html(json.data);
			}
			//showMessagesStatus(json); 
		});
		
	}
    
    
    
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
    
    

});
