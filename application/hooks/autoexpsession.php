<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autoexpsession extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("administration_model");
		$this->load->library("form_validation");
	}
	
	
	public function index()
	{ 
	
			$userData[0] = $this->session->userdata("auth_densouser");
			$userstat = $this->session->userdata("stat_id");
			//print_r($userData[0]);
			//print_r($userstat);die;
			
			$cond = array("user_id"=>$userData[0]["user_id"]); 
			$data_update = array("session_id"=>"");
			$this->common_model->update(TB_USERS,$cond,$data_update);	
			
			$cond_stat = array("user_id" => $userData[0]["user_id"],"stat_id"=>$userstat); 
			 
			$data_update1 = array("logout_time" => date("Y-m-d H:i:s"),"date_modified" => date("Y-m-d H:i:s"));
			$this->common_model->update(TB_USER_STAT,$cond_stat,$data_update1);
			
			$this->session->unset_userdata("auth_densouser"); 
			$this->session->unset_userdata("stat_id");
	}
	
}
?>
