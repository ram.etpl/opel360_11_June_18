<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "frontcontroller";
//$route['404_override'] = '';
$route['404_override'] = 'my404/index';

$route["adminlogin"] = "login";
$route["users/index"] = "login";
$route["logout"] = "login/logout";


//fronend
/*AMW urls*/
$route["register/(:any)"] 	= "api/registerUser/$1";
$route["index"] 	= "frontcontroller/index";
$route["logoutuser"] = "login/logoutuser";
$route["models"] = "frontcontroller/models";
$route["models/(:num)"] = "frontcontroller/models/$1";
$route["contact-us"] = "frontcontroller/contact_us";
$route["about-us"] = "frontcontroller/about_us";
$route["accessories"] = "frontcontroller/accessories";
$route["news"] = "frontcontroller/news_and_events";
$route["events-and-promotion"] = "frontcontroller/events_and_promotion";
$route["forum-and-feedback"] = "frontcontroller/forum_and_feedback";
$route["forum-topic-details"] = "frontcontroller/forum_topic_details";
$route["car-onwer-events"] = "frontcontroller/car_onwer_events";
$route["gallery"] = "frontcontroller/gallery";
$route["profile"] = "frontcontroller/profile";
$route["survey"] = "frontcontroller/survey";
$route["pdf-manuals"] = "frontcontroller/pdf_manuals";
$route["service-costs"] = "frontcontroller/service_costs";
$route["rent-an-opel"] = "frontcontroller/rent_an_opel";
$route["rent-confirm-booking"] = "frontcontroller/rent_car_confirm";
$route["rent-next-confirm-booking"] = "frontcontroller/rent_car_next";
$route["service-record"] = "frontcontroller/service_records";
$route["pay-now"] = "frontcontroller/rent_pay_now";


$route["dashboard"] = "frontcontroller/dashboard";
$route["gallery"] = "frontcontroller/gallery";
$route["verifyaccount"] = "frontcontroller/verifyAccount";
$route["service-booking"] = "frontcontroller/service_booking";
$route["getgallery"] = "frontcontroller/getGallery";
$route["get-accesseries-gallery"] = "frontcontroller/getAccesseriesGallery";
$route["get-Pdf"] = "frontcontroller/getPdf";
$route["booking-action"] = "frontcontroller/bookingAction";
$route["booking-confirm"] = "frontcontroller/bookingConfirm";
$route["addsurvey"] = "frontcontroller/add_survey";
$route["postticket"] = "frontcontroller/postTickets";
$route["tickets"] = "frontcontroller/Tickets";
$route["upload_files"] = "frontcontroller/upload_files";
$route["getcarcatelogimages"] = "frontcontroller/getcarcatelogimages";
$route["changeEventStatus"] = "frontcontroller/changeEventStatus";
$route["forms/test-drive"] = "frontcontroller/testDrive";
$route["request-test-drive"] = "frontcontroller/request_test_drive";
$route["forms/upgrade-trade"] = "frontcontroller/upgradeTrade";
$route["upgrade-post"] = "frontcontroller/upgrade_post";
$route["booking-confirm-next"] = "frontcontroller/bookingSubmitNext";

$route["forms/feedback"] = "frontcontroller/feedbackForm";
$route["feedback-post"] = "frontcontroller/feedback_post";
$route["save-topic"] = "frontcontroller/save_topic";
$route["community-gallery"] = "frontcontroller/communitygallery";
$route["get-com-gallery"] = "frontcontroller/get_community_gallery";
$route["del-com-gallery"] = "frontcontroller/delete_community_gallery";
$route["vote-vehicle"] = "frontcontroller/voteToCommunityGallery";
$route["service-exclusive"] = "frontcontroller/service_exclusive";
$route["change-password"] = "frontcontroller/change_password";
$route["my-rewards"] = "frontcontroller/my_rewards";
$route["view-user-rewards"] = "frontcontroller/my_rewards_details";
$route["terms-and-conditions"] = "frontcontroller/term_and_conditions";
$route["terms"] = "frontcontroller/terms_api";
$route["terms-gallery"] = "frontcontroller/terms_gallery_api";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
