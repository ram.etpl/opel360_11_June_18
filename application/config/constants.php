<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/***************** Database Tables **********************/
define("TB_USERS","opl_users");
define("TB_USERS_PROFILE","opl_users_profile");
define("TB_CAR_CATALOG","opl_car_catalog");
define("TB_CAR_GALLERY","opl_catalog_images");
define("TB_SERVICE_COST","opl_service_cost");
define("TB_SERVICE_EXCLUSIVE","opl_service_exclusive");
define("TB_PDF_MANUAL","opl_pdf_manual");
//define("TB_USER_ACCESS","opl_user_roles");
define("TB_USER_ACCESS","opel_admin_access");
define("TB_EVENTS","opl_events");
define("TB_ACCESSORIES","opl_accessories");
define("TB_ACCESSORIES_TYPE","opl_accessories_type");
define("TB_VEHICLE_OWNER_DETAILS","opl_vehicle_owner_details");
define("TB_VEHICLE_DETAILS","opl_vehicle_details");
define("TB_VEHICLE_SERVICE_ORDER","opl_vehicle_service_order");
define("TB_VEHICLE_SERVICE_ORDER_PROCESS","opl_vehicle_service_orderprocess");
define("TB_SURVEY","opl_survey");
define("TB_SURVEY_QUESTIONS","opl_survey_questions");
define("TB_SURVEY_USER_RATING","opl_survey_userrating");
define("TB_SURVEY_USER_ANSWER","opl_survey_useranswer");
define("TB_SURVEY_USED_REWARD","opl_used_reward");

define("TB_REQUEST_TEST_DRIVE","opl_request_test_drive");
define("TB_REQUEST_UPGRADE_TRADE","opl_request_updgra_trade");
define("TB_FEEDBACK","opl_feedback"); 
define("TB_RSVP_USER","opl_rsvp_user");
define("TB_BANNERS","opl_banners");
define("TB_COMMUNITY","opl_community_gallery");
define("TB_COMMUNITY_LIKE","opl_comgallery_likes");
define("TB_USER_GAINED_REWARDS","opl_user_gained_rewards");
define("TB_SURVEY_NOTIFICATION","opl_survey_notification");
define("TB_FORUM_TOPIC_DISCUSSION","opl_forum_topic_discussion");
define("TB_FORUM_TOPIC_VIEWS","opl_forum_topic_views");
define("TB_DISCUSSION_FORUM","opl_discussion_forums");  
define("TB_NOTIFICATION","opl_notification");

define("PER_PAGE",10);
define("PER_PAGE_IMAGE",9);
define("PER_PAGE_OPTION",100);

//admin email setting
define("SERVER_ENVIRONMENT","STAGING");
//define("EMAIL_FROM","mat@fynch.com"); 
define("IS_WEB","web"); 
define("EMAIL_FROM","administrator@opel360.sg"); 
//define("EMAIL_TO","administrator@opel360.sg"); 
define("EMAIL_TO","assist@opel360.sg"); 
// define("EMAIL_TO","ram.k@exceptionaire.co"); //12 Jan 2018 
define("EMAIL_TO_TEST_DRIVE","willy@alpinemotors.sg,beiru.ng@alpinemotors.sg,keith.pang@autogermany.sg");
define("EMAIL_TO_FEEDBACK","willy@alpinemotors.sg,beiru.ng@alpinemotors.sg,keith.pang@autogermany.sg,clara.chai@autogermany.sg,davidpang@alpinecredit.com.sg");
define("EMAIL_TO_UPGRADE_TRADE","willy@alpinemotors.sg,beiru.ng@alpinemotors.sg,keith.pang@autogermany.sg");
define("REGARDS","Regards<br/>Opel360"); 

/***************** Database Tables **********************/

//7FGhTdJ63rLMnpUfn/V3vWUBdDPtYwCQdz/OEJPadf91fwP/
define("API_KEY","7FGhTdJ63rLMnpUfn/V3vWUBdDPtYwCQdz/OEJPadf91fwP/");
//define("API_KEY","eExCN+NqbQTK8NrusdGabFnTDCWxxMUNAz9sKfZTWAUNucoV");
//define("API_SERVICE_URL","http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx?WSDL");
define("API_SERVICE_URL","http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx?WSDL");
/* End of file constants.php */
/* Location: ./application/config/constants.php */
