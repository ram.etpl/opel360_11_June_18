<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

		function checkIsExist($userId)
		{
			$CI = & get_instance();
			$cond = array("user_id"=>$userId);
			$result = $CI->common_model->select("user_nickname",TB_USERS,$cond);
			return $result;
		}

		function checkIsPostExist($userId)
		{
			$CI = & get_instance();
			$cond = array("post_user_id"=>$userId);
			$result = $CI->common_model->select("*",TB_FORUM_TOPIC_DISCUSSION,$cond);
			return $result;
		}

		function getAllTopic($topicId)
		{
			$CI = & get_instance();
			$cond = array("topic_id"=>$topicId);
			$result = $CI->common_model->select("*",TB_DISCUSSION_FORUM,$cond);
			return $result;
		}


?>
