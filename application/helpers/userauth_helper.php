<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_user_logged_in()
{
	$CI = & get_instance();
	if($CI->session->userdata("auth_user"))
	{
		return true;
	}
	else
	{
		return false;
	}
}
 

function is_opeluser_logged_in()
{
	$CI = & get_instance();
	if($CI->session->userdata("auth_opeluser"))
	{ 
		return true;
	}
	else
	{
		$userData[0] = $CI->session->userdata("auth_opeluser");
		
		return false;
	}
}


function get_user_data($var="")
{
	
	if($var == "")
	{
		return false;
	}
	else
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user"); 
			return $sessionArr[$var];
		}
		else
		{
			return false;
		}
	}
}


function get_opeluser_data($var="")
{
	if($var == "")
	{
		return false;
	}
	else
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_opeluser"))
		{
			$sessionArr = $CI->session->userdata("auth_opeluser");
			return $sessionArr[$var];
		}
		else
		{
			return false;
		}
	}
}

function is_ajax_request()
{
	$CI = & get_instance();
	if(!$CI->input->is_ajax_request()) {
	  exit('No direct script access allowed');
	}
	else
	{
		return true;
	}
	
}

function set_user_logged_in($user)
{
	$CI = & get_instance();
	$CI->session->set_userdata("auth_opeluser", $user);
}

function get_user_permission($userId,$module)
{
	if(is_numeric($userId))
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user");
			
			if($sessionArr['user_type'] == 'ADMIN'){
				//echo $module; die;
				$CI = get_instance();
				$CI->load->model("login_model");
				$res_per = $CI->login_model->getModulePermission($userId,$module);
				if($res_per != ""){
					return 1;
				}
				else{
					return 0;
				}
			}
			else{
				return 1;
			}
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 1;
	}
}

function CheckAlphineService($postdata,$service){ 
	    
	  $post_array_string = "";
		foreach($postdata as $key=>$value) 
		{ 
		    $post_array_string .= $key.'='.$value.'&'; 
		}
		$post_array_string = rtrim($post_array_string,'&');
	  $curl = curl_init();  
	  //echo "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx/".$service."";die;
	  curl_setopt($curl, CURLOPT_POST, 1); //Choosing the POST method
	  curl_setopt($curl, CURLOPT_URL, "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx/".$service."");  // Set the url path we want to call
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  // Make it so the data coming back is put into a string
	  curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0); 
	 
	  curl_setopt($curl, CURLOPT_POSTFIELDS ,$post_array_string);  // Insert the data
		
	  // Send the request 
	  $result = curl_exec($curl); 
	   
	  
	  //print_r($result);
	  // Free up the resources $curl is using 
	  curl_close($curl); 
	 
	  return $result;
	  //echo json_encode(array($result)); exit;
	}
	
	function ordinal($number) {
		$ends = array('th','st','nd','rd','th','th','th','th','th','th');
		if ((($number % 100) >= 11) && (($number%100) <= 13))
			return $number. 'th';
		else
			return $number. $ends[$number % 10];
	}


?>
