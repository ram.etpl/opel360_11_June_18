<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_user_logged_in()
{
	$CI = & get_instance();
	if($CI->session->userdata("auth_user"))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_user_data($var="")
{
	if($var == "")
	{
		return false;
	}
	else
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user");
			return $sessionArr[$var];
		}
		else
		{
			return false;
		}
	}
}

function get_user_permission($moduleId)
{
	if(is_numeric($moduleId))
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_user"))
		{
			$sessionArr = $CI->session->userdata("auth_user");
			$CI = get_instance();
			$CI->load->model("permission_model");
			$arrResult = $CI->permission_model->getUserPermission($sessionArr["company_id"],$sessionArr["role"],$moduleId);
			return $arrResult[0];
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function getChildModules($parent)
{
	$CI = get_instance();
	$CI->load->model("administration_model");
	$cond = array("parentmodulename" => $parent);
	$arrResult = $CI->administration_model->getAllModules($cond);
	return $arrResult;
}

function getAccessPermission($moduleId,$role)
{
	$CI = & get_instance();
	$sessionArr = $CI->session->userdata("auth_user");
	$CI = get_instance();
	$CI->load->model("permission_model");
	$arrResult = $CI->permission_model->getUserPermission($sessionArr["company_id"],$role,$moduleId);
	return $arrResult[0];
}

function is_ajax_request()
{
	$CI = & get_instance();
	if(!$CI->input->is_ajax_request()) {
	  exit('No direct script access allowed');
	}
	else
	{
		return true;
	}
	
}

?>
