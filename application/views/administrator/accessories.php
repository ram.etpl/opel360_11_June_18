<?php  
$this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Accessories</li>
											<li class="crumb-trail">Manage Accessories</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#userModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteUsers()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													
													<option value="model_name">Model Name</option>
												</select>
												</div>
											</div>
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
                                                </div>
											</div>
                                            
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Accessories Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			
			<div class="section row mbn">
			 <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Model <span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<select class="form-control" name="model" id="model">
						<option value="0">Select Model</option>
						<?php echo $models; ?>
					</select>	
					<span id="modelNameInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>	
			</div>
			  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload Accessories<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="file" id="accessories_file" name="accessories_file" class="">
					<input type="hidden" id="pdf_manual_file_exist" name="pdf_manual_file_exist" class="">
					<label class="note"><strong>Note:</strong>Please ensure that you upload images with min 100X100 px size.</label>
					<span id="AccessoriesFILEInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Accessories Description<span class="validationerror"></span></label>
				  <div class="col-lg-8 text_inpu_new">
					<textarea id="accessories_manual_desc" name="accessories_manual_desc" class="form-control input-sm"></textarea>
					<span id="PDFDESCINFO"  class="text-danger"></span>
				  </div>
				</div> 
			  </div>
			 </div>  
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="manual_id" id="manual_id" class="gui-input">
			<div class="load_gif"></div>
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div></div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	

<script> 
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
 
$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
}); 	
   

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'user_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}


function changeStatus(uid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>users/change_user_status",
			data: {"userId":uid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#row_"+json.id).html(json.row);
					}
			}
			showMessagesStatus(json); 
		});
}

function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getUserList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New Accessories"); 
	//$("#pdf_manual_file_exist").val("");
	//document.getElementById("formUser").reset();
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#manual_id").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>accessories/getAccessoriesById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#model").val(json.model_id);
				$("#txt_accessories_name").val(json.accessories_name);
				
				$("#pdf_manual_file_exist").val(json.accessories_pic);
				$("#accessories_manual_desc").val(json.accessories_desc);
				 
		
				//$("#txt_city").val(json.city); 
				 
				$("#add").trigger("click"); 
				$("#userModel .panel-title").html("Edit Accessories");
				
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#userModel .mfp-close",function() {
	$("#manual_id").val("");
	$("#PDFFILEInfo").html("");
	$("#PDFInfo").html("");
	$("#modelNameInfo").html("");
	$("#AccessoriesInfo").html("");
	$("#AccessoriesFILEInfo").html("");
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formUser").reset();
	//location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'manual_id','DESC'); 	
}
function loadmore(){
	getUserList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getUserList();

function getUserList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>accessories/list_accessories",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
$("#accessories_file").change(function (){
	
   var imgName = $('#accessories_file').val();
   $("#pdf_manual_file_exist").val(imgName);
   
});

$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'manual_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		
		
		var userId = $("#userId"); 
		
		var modelName = $("#model");
			var modelNameInfo = $("#modelNameInfo"); 
			
		
		var AccessoriesName = $("#txt_accessories_name");
		var AccessoriesErrorInfo = $("#AccessoriesInfo");  	
		
		
		var AccessoriesFile = $("#accessories_file");
		var AccessoriesFileErrorInfo = $("#AccessoriesFILEInfo");  	
		
			
		/*var PDFManualDesc = $("#pdf_manual_desc");
		var PDFManualDescErrorInfo = $("#PDFDESCINFO"); */
							
		var flag=1;
		
		
		if(!checkCombo(modelName, modelNameInfo, "model")){
			flag = 0;
		}
		
		if(!checkCombo(AccessoriesName, AccessoriesErrorInfo, "accessories type")){
			
			flag = 0;
		}
		 	
		
		var pdf_manual_file_exist = $("#pdf_manual_file_exist").val();
		if(pdf_manual_file_exist == '') {
			AccessoriesFileErrorInfo.html("Please select a file to upload.");
			flag = 0;
			
		}
		
		
		if($("#pdf_manual_file_exist").val() != ""){
			//$(".loading-progress").show();
			var fname = $("#pdf_manual_file_exist").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
			
			fname_ext = fname_e.split('.');  
			var lastVar = fname_ext.pop();
			
			if(lastVar != 'png' && lastVar != 'PNG' && lastVar != 'jpg' && lastVar != 'JPEG' && lastVar != 'jpeg'){ 
				AccessoriesFileErrorInfo.html("This file type is not allowed.");
				flag = 0;
			}
		}
		
		
		var formData = new FormData($('#formUser')[0]); 
		if(flag)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>accessories/save_accessories",
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				//$(".load_gif").html("");
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});


function deleteUsers()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected accessories?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>accessories/delete_accessories",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getUserList();
				alert((json.ids).length+" accessories has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one accessories.');
	}
}
</script>
<?php $this->load->view("administrator/incls/footer"); ?>
