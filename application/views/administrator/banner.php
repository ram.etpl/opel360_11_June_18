<?php  
$this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Banner Management</li>
											<li class="crumb-trail">Manage Banners</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
								
								
								   
									
									<a id="add" class="model-open" title="Add New Record"><i class="fa fa-plus" data-target="#myModal" data-toggle="modal" ></i></a>
									<a class="" href="javascript:void(0)" onclick="deleteUsers()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		
		<form class="form-horizontal" method="post" action="" id="formSurvey" name="formSurvey" enctype="multipart/form-data">
		
		<!-- Banner Modal Start-->
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog modal-lg">
					  <div class="modal-content" id="ng-app" ng-app="app" ng-cloak>
						<div class="modal-header" ng-controller="myCtrl">
						  <button type="button" class="close" ng-click="getbanners()" data-dismiss="modal">&times;</button>
						  <h4 class="modal-title">Upload Banners</h4>
						</div>
						<div class="modal-body">
						  
						  
						  <div>
								<div ng-controller="AppController" nv-file-drop="" uploader="uploader">
									<div class="row">
										<div class="col-md-3">
											<h3>Select files</h3>
											<div ng-show="uploader.isHTML5">
												<!-- 3. nv-file-over uploader="link" over-class="className" -->
												<div class="well my-drop-zone" nv-file-over="" uploader="uploader">
													Base drop zone
												</div>
											</div>
											<input type="file" nv-file-select="" uploader="uploader" multiple  /><br/>
										</div>
										<div class="col-md-9" style="margin-bottom: 40px">
											<h2>Upload Banners</h2>
											<h3>The queue</h3>
											<p>Queue length: {{ uploader.queue.length }}</p>

											<table class="table ang_table">
												<thead>
													<tr>
														<th width="50%">Name</th>
														<th ng-show="uploader.isHTML5">Size</th>
														<th ng-show="uploader.isHTML5">Progress</th>
														<th>Status</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
												<tr ng-repeat="item in uploader.queue">
													<td>
														<strong>{{ item.file.name }}</strong>
														<!-- Image preview -->
														<!--auto height-->
														<!--<div ng-thumb="{ file: item.file, width: 100 }"></div>-->
														<!--auto width-->
														<div class="loader_ang" ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 100 }"></div>
														<!--fixed width and height -->
														<!--<div ng-thumb="{ file: item.file, width: 100, height: 100 }"></div>-->
													</td>
													<td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
													<td ng-show="uploader.isHTML5">
														<div class="progress" style="margin-bottom: 0;">
															<div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
														</div>
													</td>
													<td class="text-center">
														<span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
														<span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
														<span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
													</td>
													<td nowrap>
														<button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
															<span class="glyphicon glyphicon-upload"></span> Upload
														</button>
														<button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
															<span class="glyphicon glyphicon-ban-circle"></span> Cancel
														</button>
														<button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
															<span class="glyphicon glyphicon-trash"></span> Remove
														</button>
												</td>
											</tr>
										</tbody>
									</table>

									<div>
										<div>
											Queue progress:
											<div class="progress" style="">
												<div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
											</div>
										</div>
										<button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
											<span class="glyphicon glyphicon-upload"></span> Upload all
										</button>
										<button type="button" class="btn btn-warning btn-s" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
											<span class="glyphicon glyphicon-ban-circle"></span> Cancel all
										</button>
										<button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
											<span class="glyphicon glyphicon-trash"></span> Remove all
										</button>
									</div>

                </div>
				
            </div>

        </div>
        
	 </div>
						  
						  
						  
						</div>
					  </div>
					</div>
				  </div>
		   <!-- Banner Modal End --> 	
		
		
		
		</form>
  <!-- end: .panel -->
  
</div> 	



<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/moment.js"></script> 	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script> 	
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css"> </link>
<style>
	
.table tr td:first-child div canvas{
    height: 100px;
    width: 160px;
}
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}
.nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
.another-file-over-class { border: dotted 3px green; }
</style>

<!-- Banner css start -->
<!--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" /></link>-->
<!-- Banner css end -->

<!-- Banner js start -->
<script src="https://nervgh.github.io/js/es5-shim.min.js"></script>
<script src="https://nervgh.github.io/js/es5-sham.min.js"></script>
<!--<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/console-sham.js"></script>
<!--<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>-->
<script src="https://code.angularjs.org/1.1.5/angular.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/angular-file-upload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/controllers.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/directives.js"></script>
<!-- Banner js end -->




<!-- Inline Banner style !-->	
	
	<style>
            .my-drop-zone { border: dotted 3px lightgray; }
            .nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
            .another-file-over-class { border: dotted 3px green; }

            /*html, body { height: 100%; }*/

            canvas {
                background-color: #f3f3f3;
                -webkit-box-shadow: 3px 3px 3px 0 #e3e3e3;
                -moz-box-shadow: 3px 3px 3px 0 #e3e3e3;
                box-shadow: 3px 3px 3px 0 #e3e3e3;
                border: 1px solid #c3c3c3;
                
                /*height: 100px;*/
                margin: 6px 0 0 6px;
               /* width: 100%;*/
            }
            
            .progress-bar {
				background-color: #428bca;
				box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.15) inset;
				color: #fff;
				float: left;
				font-size: 12px;
				height: 100%;
				text-align: center;
				transition: width 0.6s ease 0s;
				width: 0;
			}
            
      </style>
	
	
<!-- Inline Banner style -->

<script> 

$('#start_date,#date_to').datetimepicker( {pickTime: false,minDate: new Date(),format: 'YYYY-MM-DD'}); 	

$(document).ready(function($){

	$('body').on('click','#add_more',function(e){
		
		
        var n = $('.plus_resp').length+1;
        if( 5 < n ) {
            alert('Stop it!');
            return false;
        }

        //var box_html = $('<p><textarea type="text" id="survey_question' + n + '" name="survey_question[]" class="form-control input-sm" placeholder="Question to ask"></textarea><span id="QuestionInfo"  class="text-danger marg"></span><span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle"><a id="remove_div"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></span></p>');
         
		  var box_html = $('<p><textarea type="text" id="survey_question' + n + '" name="survey_question[]" class="form-control input-sm" placeholder="Question to ask"></textarea><span id="QuestionInfo"  class="text-danger marg"></span><span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle"><a id="remove_div"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></span></p>');
        
        
        box_html.hide();
        $('.add_new_row:last').after(box_html);
        box_html.fadeIn('slow');
        return false;
		
    });
    
	$('body').on('click','#remove_div',function(e){
		//alert('remove');
      
            $(this).parent().parent().remove();
            $('.box-number').each(function(index){
                $(this).text( index + 1 );
            });
        
        return false;
    });
    
    
});





$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
 
$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
}); 	
   

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'user_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}




function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getBannersList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New Banner"); 
	//$("#pdf_manual_file_exist").val("");
	//document.getElementById("formSurvey").reset();
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#survey_id").val(id);
	
	
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>survey/getSurveyById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json.html);
				$("#start_date").val(json.survey.survey_from_date);
				$("#date_to").val(json.survey.survey_to_date);
				$("#survey").val(json.survey.survey_type); 
				$("#award_points").val(json.survey.award_points); 
				$("#survey_name").val(json.survey.survey_name); 
				 
				$("#survey_questions").html(json.questions); 
				 
				 
				$("#add").trigger("click"); 
				$("#userModel .panel-title").html("Edit Survey");
				
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#userModel .mfp-close",function() {
	$("#manual_id").val("");
	$("#PDFFILEInfo").html("");
	$("#PDFInfo").html("");
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formSurvey").reset();
	//location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'survey_id','DESC'); 	
}
function loadmore(){
	getBannersList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getBannersList();

function getBannersList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>banner/list_banners",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			
			
			if(json.start != 0)
				{
					
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
$("#pdf_manual_file").change(function (){
	
   var imgName = $('#pdf_manual_file').val();
   $("#pdf_manual_file_exist").val(imgName);
   
});


$(document).ready(function(){
	$("#txt_search").val("");
	changePaginate(0,'survey_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		
		var start_date = $("#start_date");
		var SurveyFromInfo = $("#SurveyFromInfo");  	
		
		
		var date_to = $("#date_to");
		var SurveyToInfo = $("#SurveyToInfo");
		
		var survey = $("#survey");
		var Surveytypeinfo = $("#Surveytypeinfo");  	
		
		var survey_name = $("#survey_name");
		var Surveynameinfo = $("#Surveynameinfo");	
		
		var award_points = $("#award_points");
		var AwardPointsinfo = $("#AwardPointsinfo");
		
							
		var flag=1;
		
		if(!checkCombo(survey, Surveytypeinfo, "Survey")){
			flag = 0;
		}
		
		if(!validateEmpty(survey_name, Surveynameinfo, "Survey name")){
			flag = 0;
		} 
		
		if(!validateEmpty(award_points, AwardPointsinfo, "Award points")){
			flag = 0;
		} 
		
		if(!validateEmpty(start_date, SurveyFromInfo, "Date from")){
			flag = 0;
		} 
		
		
		if(!validateEmpty(date_to, SurveyToInfo, "Date to")){
			flag = 0;
		}
		
		var formData = new FormData($('#formSurvey')[0]); 
		if(flag)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>survey/save_survey",
				data: formData
			}).success(function (json) {
				
				
				
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				$(".load_gif").html("");
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});


function deleteUsers()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected Banner(s)?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>banner/delete_banner",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					
					var result = paginate.split(",");
					
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getBannersList();
				alert((json.ids).length+" banner(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one banner.');
	}
}
</script>
  
<?php $this->load->view("administrator/incls/footer"); ?>
