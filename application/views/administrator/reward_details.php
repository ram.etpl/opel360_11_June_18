<?php  
$this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Survey</li>
											<li class="crumb-trail">Manage Survey Rewards</li>
										</ol>
								</div>
								
								<span class="panel-controls Users">  
									 <a style="display:none" id="add" class="model-open" href="#userModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									 <a style="display:none" id="deduct" class="model-open" href="#rewardModel" title="Add New Record"><i class="fa fa-minus" data-toggle="modal" ></i></a> 
									
								</span>
								
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="c_name">User Name</option>
												</select>
												</div>
											</div>
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
                                                </div>
											</div>
                                            
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
									
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Reward Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formSurvey" name="formSurvey" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
		<div class="section row mbn">
				
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">User Name<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="user_name" readonly name="user_name" class="form-control input-sm" placeholder="User Name" value=""  title="User Name">
					<span id="UserFromInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>

			
		</div>
		
		
		<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Reward Point To Be Deducted<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="reward_point" name="reward_point" class="numeric form-control input-sm" placeholder="Reward Point To Be Deducted" required>
					<span id="RewardPointinfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			  
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Reason<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					  <textarea id="reson" name="reson" class="form-control input-sm" placeholder="Reason for Deduction" required maxlength="500"></textarea>
					<span id="Resoninfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
		</div>
		
		
		</div>
		

			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="survey_id" id="survey_id" class="gui-input">
			<input type="hidden" name="user_id" id="user_id" class="gui-input">
			<input type="hidden" name="total_adward" id="total_adward" class="gui-input">
			<div class="load_gif"></div>
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div></div>
		  <!-- end .form-footer section -->
		  
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	


<!-- Reward Details Model Start-->

<div id="rewardModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Reward Details</span> </div>
		<!-- end .panel-heading section -->
		<div class="model-body">
			
		</div>
		
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	


<!-- Reward Details Model End -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/moment.js"></script> 	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script> 	
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css"> 
<script src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script> 

$(".numeric").numeric({decimal:'.',negative:false});	
$('#start_date,#date_to').datetimepicker( {pickTime: false,minDate: new Date(),format: 'YYYY-MM-DD'}); 	

$(document).ready(function($){

	$('body').on('click','#add_more',function(e){
		
		
        var n = $('.plus_resp').length+1;
        
        if( 5 < n ) {
            alert('Maximum 5 questions are allowed to enter!');
            return false;
        }
		
		var box_html = '<p class="add_new_row"><textarea type="text" id="survey_question' + n + '" name="survey_question[]" class="form-control input-sm" placeholder="Question to ask"></textarea><span id="QuestionInfo"  class="text-danger marg"></span><span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle"><a id="remove_div"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></span></p>';
        
        //box_html.hide();
        
        $('.add_new_row:last').after(box_html);
        box_html.fadeIn('slow');
        
        return false;
		
    });
    
	$('body').on('click','#remove_div',function(e){
		//alert('remove');
      
            $(this).parent().parent().remove();
            $('.box-number').each(function(index){
                $(this).text( index + 1 );
            });
        
        return false;
    });
    
    
});





$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
 
$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
}); 	
   

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'user_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}




function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getSurveyList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New Survey"); 
	//$("#pdf_manual_file_exist").val("");
	//document.getElementById("formSurvey").reset();
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	var uid = $(this).attr('data-id');
	var sname = $(this).attr('data-sname'); 
	var cname = $(this).attr('data-name');
	var adw = $(this).attr('data-adward');
	//alert(sname);
	//alert(cname);
	//return false;
	$("#survey_id").val(id);
	$("#user_id").val(uid);
	$("#user_name").val(cname);
	$("#survey_name").val(sname);
	$("#total_adward").val(adw);
	
	$("#add").trigger("click"); 
	$("#userModel .panel-title").html("Deduct Rewards");
	
});
$(document).on("click","#userModel .mfp-close",function() {
	
	$('#formSurvey')[0].reset();
	$("#Surveynameinfo").html('');
	$("#SurveyFromInfo").html('');
	$("#SurveyToInfo").html('');
	$("#Surveytypeinfo").html('');
	$("#AwardPointsinfo").html('');
	$(".first_que").html('<p class="add_new_row"><textarea type="text" id="survey_question0" name="survey_question[]" class="form-control input-sm" placeholder="Question to ask"></textarea><span id="QuestionInfo"  class="text-danger marg"></span><span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle">\
							<a href="javascript:void(0);" id="add_more" ><i class="fa fa-plus-circle" aria-hidden="true"></i>\
								</a>\
							</span>\
							</p>');
	$("#manual_id").val("");
	$("#PDFFILEInfo").html("");
	$("#PDFInfo").html("");
	$("#survey_question").html("");
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formSurvey").reset();
});


/*********************** Reward Model Details Start ****************************/

$(document).on("click","#view",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	var uid = $(this).attr('data-id');
	var sname = $(this).attr('data-sname');
	//alert(id);
	//return false;
	$("#survey_id").val(id);
	
	
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>survey/getRewardDetails",
				data: {id:id, uid:uid, sname:sname},
			}).success(function (json) {
				$(".model-body").empty();
				$(".model-body").append(json.table); 
								 
				$("#deduct").trigger("click"); 
				if(json.username!=""){
					username = json.username;
				}
				else{
					username = 'OPEL user';
				}
				$("#rewardModel .panel-title").html("Rewards Deduction Details for "+username);
				
			}); 
	} 
	else{
				$("#deduct").trigger("click");
	}
});
$(document).on("click","#rewardModel .mfp-close",function() {
	
	$('#formSurvey')[0].reset();
	$("#Surveynameinfo").html('');
	$("#SurveyFromInfo").html('');
	$("#SurveyToInfo").html('');
	$("#Surveytypeinfo").html('');
	$("#AwardPointsinfo").html('');
	$(".first_que").html('<p class="add_new_row"><textarea type="text" id="survey_question0" name="survey_question[]" class="form-control input-sm" placeholder="Question to ask"></textarea><span id="QuestionInfo"  class="text-danger marg"></span><span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle">\
							<a href="javascript:void(0);" id="add_more" ><i class="fa fa-plus-circle" aria-hidden="true"></i>\
								</a>\
							</span>\
							</p>');
	$("#manual_id").val("");
	$("#PDFFILEInfo").html("");
	$("#PDFInfo").html("");
	$("#survey_question").html("");
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formSurvey").reset();
});

/********************** Reward Model Dtails End    *****************************/






function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'survey_id','DESC'); 	
}
function loadmore(){
	getSurveyList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getSurveyList();

function getSurveyList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>survey/rewardsurvey",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
$("#pdf_manual_file").change(function (){
	
   var imgName = $('#pdf_manual_file').val();
   $("#pdf_manual_file_exist").val(imgName);
   
});

$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'survey_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		
		var reward = $("#reward_point");
		var RewardPointinfo  = $("#RewardPointinfo");
		var reson = $("#reson");  
		var Resoninfo = $("#Resoninfo");
		
		var flag=1;	
		 
		if(!validateEmpty(reward, RewardPointinfo, "reward points to be deducted."))
		{ 
			flag=0;
		}
		 
		if(!validateEmpty(reson, Resoninfo, "reason"))
		{
			flag=0;
		}
	   
		var formData = new FormData($('#formSurvey')[0]); 
		if(flag)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			//$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>survey/save_used_reward",
				data: formData
			}).success(function (json) {
				
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				$(".load_gif").html("");
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
		
	});		
});


function deleteUsers()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected survey?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>survey/delete_survey",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getSurveyList();
				alert((json.ids).length+" survey(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one survey.');
	}
}
</script>

<script>
		
		$("#award_points").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
});	
		
</script>

<?php $this->load->view("administrator/incls/footer"); ?>
