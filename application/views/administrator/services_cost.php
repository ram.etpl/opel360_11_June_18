<?php  
$this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Services</li>
											<li class="crumb-trail">Manage Services Cost</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#costModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="userServiceCost()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="service_name">Services Name</option>
												</select>
												</div>
											</div>
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
                                                </div>
											</div>
                                            
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="costModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Services Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			
			<div class="section row mbn">
			 <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Model <span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<select class="form-control" name="model" id="model">
						<option value="0">Select Model</option>
						<?php echo $models; ?>
					</select>	
					<span id="modelNameInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>	
			</div>
			
			<div class="section row mbn">
				
			
				
				
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Service Name<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_service_name" name="txt_service_name" class="form-control input-sm" placeholder="Service Name">
					<span id="serviceNameInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Service Cost(In S$)<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_service_cost" name="txt_service_cost" class="numeric form-control input-sm" placeholder="Service Cost" maxlength="12">
					<span id="serviceCostInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div>   
			  <!-- end section -->
			</div>
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="sId" id="sId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div></div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 
<script src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script> 

$(".numeric").numeric({decimal:'.',negative:false});	
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	


$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});


$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'s_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}

function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#costModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getServiceCostList();
} 
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Add New Service Cost");  
});



$(document).on("click","#edit",function() { 
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#sId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>services/getServicesCostById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				
				$("#model").val(json.model_id);
				$("#txt_service_name").val(json.service_name);
				$("#txt_service_cost").val(json.service_cost);
				$("#add").trigger("click"); 
				$("#labelpassword").html("");
				$("#costModel .panel-title").html("Edit Service Cost");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#costModel .mfp-close",function() {
	$("#sId").val("");
	$("#serviceNameInfo").html("");  	
	$("#serviceCostInfo").html("");  	
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formUser").reset();
	//location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'s_id','DESC'); 	
}
function loadmore(){
	getServiceCostList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getServiceCostList();

function getServiceCostList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>services/list_service_cost",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'user_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var sId = $("#sId"); 
		
		
		
		var modelName = $("#model");
			var modelNameInfo = $("#modelNameInfo"); 
		
		var serviceName = $("#txt_service_name");
			var serviceNameInfo = $("#serviceNameInfo"); 
			
		var serviceCost = $("#txt_service_cost");
			var serviceCostInfo = $("#serviceCostInfo"); 	 	 
							
		var flag=1;
		
		
		if(!checkCombo(modelName, modelNameInfo, "model")){
			flag = 0;
		}
		
		
		if(!validateEmpty(serviceName, serviceNameInfo, "services name")){
			flag = 0;
		} 
		
		
		if(!validateEmpty(serviceName, serviceNameInfo, "services name")){
			flag = 0;
		} 	
		
		if(!validateEmpty(serviceCost, serviceCostInfo, "services cost")){
			flag = 0;
		}
		
		if(serviceName.val()!=""){	 
			if(!CheckAlphanumericWithSomeChars(serviceName, serviceNameInfo)){
				flag = 0;
			} 	 
		}
		
		
		
		var formData = new FormData($('#formUser')[0]); 
		if(flag)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>services/save_services_cost",
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function userServiceCost()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected services?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>services/delete_service_cost",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getUserList();
				alert((json.ids).length+" service cost(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one service.');
	}
}
</script>
<?php $this->load->view("administrator/incls/footer"); ?>
