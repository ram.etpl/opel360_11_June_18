<?php  
$this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Survey</li>
											<li class="crumb-trail">Manage Survey</li>
										</ol>
								</div>
								
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#userModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteUsers()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									<!--<a href="javascript:void(0)" onclick="refreshTable();" alt="Refresh" title="Refresh"><i class="fa fa-refresh"></i></a>-->
								</span>
								
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="survey_name">Survey Name</option>
												</select>
												</div>
											</div>
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
                                                </div>
											</div>
                                            
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,user_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Survey Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formSurvey" name="formSurvey" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			<div class="section row mbn">
					
				  <div class="col-sm-6">
					<div class="form-group">
					  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">From Date<span class="validationerror">*</span></label>
					  <div class="col-lg-8 text_inpu_new">
						<input type="text" id="start_date" readonly name="start_date" class="form-control input-sm" placeholder="From Date" value=""  title="From Date">
						<span id="SurveyFromInfo"  class="text-danger marg"></span>
					  </div>
					</div>
				  </div>

				<div class="col-sm-6">
					<div class="form-group">
					  <label for="inputStandard"  class="col-lg-4 pn mt5 control-label new_first">To Date<!-- <span class="validationerror">*</span> --></label>
					  <div class="col-lg-8 text_inpu_new">
						<input type="text" id="date_to" readonly name="date_to" class="form-control input-sm" placeholder="To Date" value=""  title="To Date">
						<span id="SurveyToInfo"  class="text-danger marg"></span>
					  </div>
					</div>
				 </div>
			</div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Survey<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<select id="survey" class="form-control" name="survey">
						<option value="">Select survey</option>
						<!-- <option value="workshop survey">Workshop survey</option> -->
						<option value="general survey">General survey</option>
					</select>
					<span id="Surveytypeinfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>			  
			</div>
		
		
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Survey Name<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="survey_name" name="survey_name" class="form-control input-sm" placeholder="Name of the Survey">
					<span id="Surveynameinfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div>
		
		
		<div id="survey_questions">
		
			<div class="add_new_row">
				<div class="section row mbn">

					
					<div class="col-sm-6">
						<div class="form-group ">
						  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Question to ask<span class="validationerror">*</span></label>
						  <div class="col-lg-8 text_inpu_new first_que"> 
							  
								<textarea type="text" id="survey_question0" name="survey_question[]" class="form-control input-sm" placeholder="Question to Ask"></textarea>
								<span id="QuestionInfo0"  class="text-danger marg"></span>
								
							</div> 
						</div>
					</div>
					<div class="add_new_column">
						<div class="col-sm-6">
							<div class="form-group">
							  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Survey Type<span class="validationerror">*</span></label>
							  <div class="col-lg-8 text_inpu_new add_btn">
								<select id="question_type0" class="form-control" name="question_type[]">
									<option value="">Select question type</option>
									<option value="ratings">1 – 5 questions with star ratings</option>
									<option value="discriptive">Free text</option>
									<option value="Yes/No">Yes/ No</option>
									<option value="Multiple Choice">Multiple Choice</option>
								</select>
								<span id="Questiontypeinfo0" class="text-danger marg"></span>

								<span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle add_btn_icon">
										<a href="javascript:void(0);" id="add_more" ><i class="fa fa-plus-circle" aria-hidden="true"></i>
										</a>
								</span>

							  </div>
							</div>
							<div class="form-group" id="multipleoption0">
								<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">A.<span class="validationerror">*</span></label>
								<div class="col-lg-8 text_inpu_new">					
									<input type="text" id="multiple_choice0" name="multiple_choice[]" class="form-control input-sm multi" placeholder="Option A.">
								</div>
								<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">B.<span class="validationerror">*</span></label>
								<div class="col-lg-8 text_inpu_new">					
									<input type="text" id="multiple_choice0" name="multiple_choice[]" class="form-control input-sm multi" placeholder="Option B.">
								</div>
								<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">C.<span class="validationerror">*</span></label>
								<div class="col-lg-8 text_inpu_new">					
									<input type="text" id="multiple_choice0" name="multiple_choice[]" class="form-control input-sm multi" placeholder="Option C.">
								</div>
								<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">D.<span class="validationerror">*</span></label>
								<div class="col-lg-8 text_inpu_new">					
									<input type="text" id="multiple_choice0" name="multiple_choice[]" class="form-control input-sm multi" placeholder="Option D.">
								</div>
								<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Answer<span class="validationerror">*</span></label>
								<div class="col-lg-8 text_inpu_new">					
									<!-- <input type="text" id="multiChoice_ans0" name="multiChoice_ans[]" class="form-control input-sm" placeholder="Answer"> -->
									<select id="multiChoice_ans0" class="form-control" name="multiChoice_ans[]">
										<option value="">Select Answer</option>
										<option value="A">A</option>
										<option value="B">B</option>
										<option value="C">C</option>
										<option value="D">D</option>
									</select>
								</div>
							</div>
					  </div>
				  	</div>
				</div>
			</div>
		</div>
		<div class="add_question_div"></div>	
		</div>
		

			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="survey_id" id="survey_id" class="gui-input">
			<input type="hidden" name="qid" id="qid" class="gui-input">
			<input type="hidden" name="cnt_plus" id="cnt_plus" class="gui-input">
			<div class="load_gif"></div>
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div></div>
		  <!-- end .form-footer section -->
		  <input type="hidden" name="cnti" id="cnti" class="gui-input" value="1">
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	

<!-- Reward Details Model Start-->

<div id="rewardModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Survey Ratings Details</span> </div>
		<!-- end .panel-heading section -->
		<div class="model-body">
			
		</div>
		
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 	


<!-- Reward Details Model End -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/moment.js"></script> 	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script> 	
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css"> 

<script> 
$('#start_date,#date_to').datetimepicker( {pickTime: false,minDate: new Date(),format: 'YYYY-MM-DD'}); 	
$(document).ready(function($){
	
	//checksurvey();
	var n = $("#cnt_plus").val();
	if(n==""){
		$("#cnt_plus").val(1);
	}
	$('body').on('click','#add_more',function(e){
		 
		var n = $("#cnt_plus").val();
		
        if( 5 <= n ) {
            alert('Maximum 5 questions are allowed to enter!');
            return false;
        }
		
		var box_html = '<div class="add_new_row"><div class="section row mbn"><div class="col-sm-6"><div class="form-group "><label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Question to ask<span class="validationerror">*</span></label><div class="col-lg-8 text_inpu_new first_que"><textarea type="text" id="survey_question' + n + '" name="survey_question[]" class="form-control input-sm" placeholder="Question to Ask"></textarea><span id="QuestionInfo'+n+'"  class="text-danger marg"></span></div></div></div><div class="add_new_column"><div class="col-sm-6 add_btn"><div class="form-group"><label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Survey Type<span class="validationerror">*</span></label><div class="col-lg-8 text_inpu_new"><select id="question_type' + n + '" class="form-control" name="question_type[]"><option value="">Select question type</option><option value="ratings">1 – 5 questions with star ratings</option><option value="discriptive">Free text</option><option value="Yes/No">Yes/ No</option><option value="Multiple Choice">Multiple Choice</option></select><span id="Questiontypeinfo'+n+'" class="text-danger marg"></span></div></div><div class="form-group" id="multipleoption' + n + '"><label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">A.<span class="validationerror">*</span></label><div class="col-lg-8 text_inpu_new"><input type="text" id="multiple_choice' + n + '" name="multiple_choice[]" required class="form-control input-sm" placeholder="Option A."></div><label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">B.<span class="validationerror">*</span></label><div class="col-lg-8 text_inpu_new"><input type="text" id="multiple_choice' + n + '" name="multiple_choice[]" required class="form-control input-sm" placeholder="Option B."></div><label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">C.<span class="validationerror">*</span></label><div class="col-lg-8 text_inpu_new"><input type="text" id="multiple_choice' + n + '" name="multiple_choice[]" required class="form-control input-sm" placeholder="Option C."></div><label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">D.<span class="validationerror">*</span></label><div class="col-lg-8 text_inpu_new"><input type="text" id="multiple_choice' + n + '" name="multiple_choice[]" required class="form-control input-sm" placeholder="Option D."><span id="multipleInfo"  class="text-danger marg"></span></div><label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Answer<span class="validationerror">*</span></label><div class="col-lg-8 text_inpu_new"><select id="multiChoice_ans'+n+'" class="form-control" name="multiChoice_ans[]"><option value="">Select Answer</option><option value="A">A</option><option value="B">B</option><option value="C">C</option><option value="D">D</option></select></div></div></div></div><span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle add_btn_icon2"><a id="remove_div" data-qno='+n+'><i class="fa fa-minus-circle" aria-hidden="true"></i></a></span></div></div>';
                
        $('.add_new_row:last').after(box_html);
        $('#multipleoption'+n+'').hide(); 
	    $('#question_type'+n+'').change(function(){
	        if($('#question_type'+n+'').val() == 'Multiple Choice') {
	            $('#multipleoption'+n+'').show(); 
	        } else {
	            $('#multipleoption'+n+'').hide(); 
	        } 
	    });
   
		$("#cnt_plus").val(parseInt(n)+1);
        
        return false;
		
    });    
    
	$('body').on('click','#remove_div',function(e){
		//alert('remove');
		var qno = $(this).attr('data-qno');
		var id = $(this).attr('data-id'+qno);
		var qno_val = $("survey_question"+qno).val();
		if(id != "" && id !=undefined && qno_val != ""){
			if(confirm("Are you sure want to delete selected survey question?") == true)
			{
					$(this).parent().parent().remove();
					$('.box-number').each(function(index){
						$(this).text( index + 1 );
					});
					
						$.ajax({
							type: "POST",
							dataType: "json",
							//fileElementId :'user_photo',
							url: "<?php echo base_url(); ?>survey/delete_survey_question",
							data: {"id":id},
						}).success(function (json) {
							//console.log(json.html);
							var n = $("#cnt_plus").val();
							$("#cnt_plus").val(parseInt(n)-1);
							
							$(this).parent().parent().remove();
							
						}); 
			}
		}
		else{
			$(this).parent().parent().remove();
			$('.box-number').each(function(index){
				$(this).text( index + 1 );
				
			});
			var n = $("#cnt_plus").val();
			$("#cnt_plus").val(parseInt(n)-1);
		}
        return false;
    });
    
    
});

$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	
 
$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
}); 	
   

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'user_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}




function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getSurveyList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New Survey"); 
	
	//$("#pdf_manual_file_exist").val("");
	//document.getElementById("formSurvey").reset();
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	$("#survey_id").val(id);
	
	if(id != ""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>survey/getSurveyById",
				data: {"id":id},
			}).success(function (json) {
				$("#start_date").val(json.survey.survey_from_date);
				$("#date_to").val(json.survey.survey_to_date);
				$("#survey").val(json.survey.survey_type);  
				$("#survey_name").val(json.survey.survey_name); 
				$("#survey_questions").html(json.questions); 
				// $("#questions_type0").val('');
				//alert(json.total_questions);
				$("#cnt_plus").val(json.total_questions);
				$("#add").trigger("click"); 
				$("#userModel .panel-title").html("Edit Survey");
				$("#qid").val(json.qid);
				var n = 1;
				
			$('#multipleoption0').hide(); 
	    	//$('#question_type0').change(function(){
	        if($('#question_type0').val() == 'Multiple Choice') {
	            $('#multipleoption0').show(); 
	        } else {
	            $('#multipleoption0').hide(); 
	        } 
	    //});
   

    		$('#multipleoption1').hide(); 
	    //$('#question_type1').change(function(){
	        if($('#question_type1').val() == 'Multiple Choice') {
	            $('#multipleoption1').show(); 
	        } else {
	            $('#multipleoption1').hide(); 
	        } 
	    //});
  

         $('#multipleoption2').hide(); 
	    //$('#question_type2').change(function(){
	        if($('#question_type2').val() == 'Multiple Choice') {
	            $('#multipleoption2').show(); 
	        } else {
	            $('#multipleoption2').hide(); 
	        } 
	   // });
   

     $('#multipleoption3').hide(); 
	  //  $('#question_type3').change(function(){
	        if($('#question_type3').val() == 'Multiple Choice') {
	            $('#multipleoption3').show(); 
	        } else {
	            $('#multipleoption3').hide(); 
	        } 
	   // });
   


     		$('#multipleoption4').hide(); 
	   // $('#question_type4').change(function(){
	        if($('#question_type4').val() == 'Multiple Choice') {
	            $('#multipleoption4').show(); 
	        } else {
	            $('#multipleoption4').hide(); 
	        } 
	    //});
   


     //$('#multipleoption0').hide(); 
	    	$('#question_type0').change(function(){
	        if($('#question_type0').val() == 'Multiple Choice') {
	            $('#multipleoption0').show();
	            // $('.multi').val('');
	        } else {
	            $('#multipleoption0').hide(); 
	        } 
	    });
 

    		//$('#multipleoption1').hide(); 
	    $('#question_type1').change(function(){
	        if($('#question_type1').val() == 'Multiple Choice') {
	            $('#multipleoption1').show();
	           // $('.multi').val('');
	        } else {
	            $('#multipleoption1').hide(); 
	        } 
	    });
  

         //$('#multipleoption2').hide(); 
	    $('#question_type2').change(function(){
	        if($('#question_type2').val() == 'Multiple Choice') {
	            $('#multipleoption2').show();
	            // $('.multi').val('');
	        } else {
	            $('#multipleoption2').hide(); 
	        } 
	    });
   

     //$('#multipleoption3').hide(); 
	    $('#question_type3').change(function(){
	        if($('#question_type3').val() == 'Multiple Choice') {
	            $('#multipleoption3').show(); 
	            // $('.multi').val('');
	        } else {
	            $('#multipleoption3').hide(); 
	        } 
	    });
    


     		//$('#multipleoption4').hide(); 
	    $('#question_type4').change(function(){
	        if($('#question_type4').val() == 'Multiple Choice') {
	            $('#multipleoption4').show();
				// $('.multi').val('');
	        } else {
	            $('#multipleoption4').hide(); 
	        } 
	    });
   
    
				// checksurvey();
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});

$(document).on("click","#userModel .mfp-close",function() {
	$("#survey_id").val("");
	$("#cnt_plus").val(1);
	$('#formSurvey')[0].reset();
	$("#Surveynameinfo").html('');
	$("#SurveyFromInfo").html('');
	$("#SurveyToInfo").html('');
	$("#Surveytypeinfo").html('');
	$("#Questiontypeinfo").html('');
	$("#AwardPointsinfo").html('');
	$("#survey_questions").html("");
	$("#question_type0").html("");
	$("#survey_questions").html('<div class="add_new_row">\
		<div class="section row mbn">\
			<div class="col-sm-6">\
				<div class="form-group ">\
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Question to ask<span class="validationerror">*</span></label>\
				  <div class="col-lg-8 text_inpu_new first_que">\
						<textarea type="text" id="survey_question0" name="survey_question[]" class="form-control input-sm" placeholder="Question to Ask"></textarea>\
						<span id="QuestionInfo"  class="text-danger marg"></span>\
					</div>\
				</div>\
			</div>\
			<div class="add_new_column">\
			<div class="col-sm-6">\
				<div class="form-group">\
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Survey Type<span class="validationerror">*</span></label>\
				  <div class="col-lg-8 text_inpu_new add_btn">\
					<select id="question_type0" class="form-control" name="question_type[]">\
						<option value="">Select question type</option>\
						<option value="ratings">1 – 5 questions with star ratings</option>\
						<option value="discriptive">Free text</option>\
						<option value="Yes/No">Yes/ No</option>\
						<option value="Multiple Choice">Multiple Choice</option>\
					</select>\
					<span id="Questiontypeinfo" class="text-danger marg"></span>\
					<span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle add_btn_icon">\
							<a href="javascript:void(0);" id="add_more" ><i class="fa fa-plus-circle" aria-hidden="true"></i>\
							</a>\
					</span>\
				  </div>\
				</div>\
				<div class="form-group" id="multipleoption0">\
					<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">A.<span class="validationerror">*</span></label>\
					<div class="col-lg-8 text_inpu_new">\
						<input type="text" id="multiple_choice0" name="multiple_choice[]" required class="form-control input-sm multi" placeholder="Option A.">\
					</div>\
					<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">B.<span class="validationerror">*</span></label>\
					<div class="col-lg-8 text_inpu_new">\
						<input type="text" id="multiple_choice0" name="multiple_choice[]" required class="form-control input-sm multi" placeholder="Option B.">\
					</div>\
					<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">C.<span class="validationerror">*</span></label>\
					<div class="col-lg-8 text_inpu_new">\
						<input type="text" id="multiple_choice0" name="multiple_choice[]" required class="form-control input-sm multi" placeholder="Option C.">\
					</div>\
					<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">D.<span class="validationerror">*</span></label>\
					<div class="col-lg-8 text_inpu_new">\
						<input type="text" id="multiple_choice0" name="multiple_choice[]" required class="form-control input-sm multi" placeholder="Option D.">\
					</div>\
					<span id="multipleInfo"  class="text-danger marg"></span>\
					<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Answer<span class="validationerror">*</span></label>\
					<div class="col-lg-8 text_inpu_new">\
						<!-- <input type="text" id="multiChoice_ans0" name="multiChoice_ans[]" class="form-control input-sm" placeholder="Answer"> -->\
						<select id="multiChoice_ans0" class="form-control" name="multiChoice_ans[]">\
							<option value="">Select Answer</option>\
							<option value="A">A</option>\
							<option value="B">B</option>\
							<option value="C">C</option>\
							<option value="D">D</option>\
						</select>\
					</div>\
				</div>\
		  </div>\
		  </div>\
		</div>\
		</div>');
	$("#manual_id").val("");
	$("#PDFFILEInfo").html("");
	$("#PDFInfo").html("");
	$('#multipleoption0').hide(); 
	    $('#question_type0').change(function(){
	        if($('#question_type0').val() == 'Multiple Choice') {
	            $('#multipleoption0').show(); 
	        } else {
	            $('#multipleoption0').hide(); 
	        } 
	    });    
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formSurvey").reset();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'survey_id','DESC'); 	
}
function loadmore(){
	getSurveyList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getSurveyList();

function getSurveyList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>survey/list_survey",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
$("#pdf_manual_file").change(function (){
	
   var imgName = $('#pdf_manual_file').val();
   $("#pdf_manual_file_exist").val(imgName);
   
});

$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'survey_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		
		var start_date = $("#start_date");
		var SurveyFromInfo = $("#SurveyFromInfo");  	
		
		
		var date_to = $("#date_to");
		var SurveyToInfo = $("#SurveyToInfo");
		
		var survey = $("#survey");
		var Surveytypeinfo = $("#Surveytypeinfo");  	
		
		var survey_name = $("#survey_name");
		var question_type = $("#question_type0");
		var Questionstypeinfo = $("#Questiontypeinfo0");
		var Surveynameinfo = $("#Surveynameinfo");	
		
		//var award_points = $("#award_points");
		var AwardPointsinfo = $("#AwardPointsinfo");
		
		var survey_question = $("#survey_question0");
		var QuestionInfo = $("#QuestionInfo0");
		
							
		var flag=1;
		
		if(!checkCombo(survey, Surveytypeinfo, "Survey")){
			flag = 0;
		}
		
		if(!validateEmpty(survey_name, Surveynameinfo, "Survey name")){
			flag = 0;
		} 
		
		if(!validateEmpty(question_type, Questionstypeinfo, "Question type")){
			flag = 0;
		}else{
			if($('#question_type0').val() == 'Multiple Choice') {
					var question_type0 = [];
		        $('#multipleoption0 :input:not(:button)').each(function (index, value) { 
		        	
		         	if($(this).val() == "" && index <= 3)
		         	{
		         		flag = 0; 
		         		
		         	}else{
		         		question_type0.push($(this).val());
		         	}

		         });
		        if(flag == 0)
		        {   alert('Please enter the data in the options field.');     	
		        	flag = 0; 
		        }
		        if($('#multiChoice_ans0 option').filter(':selected').val() == "")
		         	{
		         		flag = 0; 
		         		alert('Please select the Answer.');
		         	}
		     	} 
		}
		
		if(!validateEmpty(start_date, SurveyFromInfo, "Date from")){
			flag = 0;
		} 
		
		if(!validateEmpty(survey_question, QuestionInfo, "Survey questions")){
			flag = 0;
		} 	
		
		if($('#survey_question1').val() == "")
         	{
         		flag = 0; 
         		$("#QuestionInfo1").html("Please Enter Question");
         		return flag;
         	}

         	if($('#survey_question2').val() == "")
         	{
         		flag = 0; 
         		$("#QuestionInfo2").html("Please Enter Question");
         		return flag;
         	}
         	if($('#survey_question3').val() == "")
         	{
         		flag = 0; 
         		$("#QuestionInfo3").html("Please Enter Question");
         		return flag;
         	}
         	if($('#survey_question4').val() == "")
         	{
         		flag = 0;
         		$("#QuestionInfo4").html("Please Enter Question");
         		return flag;
         	}

  			if($('#question_type1').val() == "")
         	{
         		flag = 0; 
         		$("#Questiontypeinfo1").html("Please Select Question Type");
         		return flag;
         	}else{
         		if($('#question_type1').val() == 'Multiple Choice') {
		     		var question_type1 = []; 
		        $('#multipleoption1').find('input').each(function (index, value) {

		        	if($(this).val() == "" && index <= 3)
		         	{
		         		flag = 0;		         		
		         	}else{
		         		question_type1.push($(this).val());
		         	}
		         });
		        if(flag == 0)
		        {
		        	flag = 0;
		        	alert('Please enter the data in the options field.');		        	 
		        	return flag;
		        }
		        if($('#multiChoice_ans1 option').filter(':selected').val() == "")
		         	{
		         		flag = 0; 
		         		alert('Please select the Answer.');
		         		return flag;
		         	}
		     	}
         	}

         	if($('#question_type2').val() == "")
         	{
         		flag = 0; 
         		$("#Questiontypeinfo2").html("Please Select Question Type");
         		return flag;
         	}else{
         		if($('#question_type2').val() == 'Multiple Choice') {
		     		var question_type2 = [];

		         $('#multipleoption2').find('input').each(function (index, value) { 
		         	if($(this).val() == "" && index <= 3)
		         	{
		         		flag = 0; 
		         		
		         	}else{
		         		question_type2.push($(this).val());
		         	}
		         });
		         if(flag == 0)
		        {
		        	flag = 0;
		        	alert('Please enter the data in the options field.');
		        	 return flag;
		        }
		         if($('#multiChoice_ans2 option').filter(':selected').val() == "")
		         	{
		         		flag = 0; 
		         		alert('Please select the Answer.');
		         		return flag;
		         	}
		     	}
         	}

         	if($('#question_type3').val() == "")
         	{
         		flag = 0; 
         		$("#Questiontypeinfo3").html("Please Select Question Type");
         		return flag;
         	}else{
         		if($('#question_type3').val() == 'Multiple Choice') {
		     		var question_type3 = [];
		        $('#multipleoption3 :input:not(:button)').each(function (index, value) { 
		         	if($(this).val() == "" && index <= 3)
		         	{
		         		flag = 0; 
		         		
		         	}else{
		         		question_type3.push($(this).val());
		         	}
		         });
		        if(flag == 0)
		        {
		        	flag = 0;
		        	alert('Please enter the data in the options field.');
		        	 return flag;
		        }
		        if($('#multiChoice_ans3 option').filter(':selected').val() == "")
		         	{
		         		flag = 0; 
		         		alert('Please select the Answer.');
		         		return flag;
		         	}
		     	}
         	}



         	if($('#question_type4').val() == "")
         	{
         		flag = 0; 
         		$("#Questiontypeinfo4").html("Please Select Question Type");
         		return flag;
         	}else{
         		if($('#question_type4').val() == 'Multiple Choice') {
		     		var question_type4 = [];
		         $('#multipleoption4 :input:not(:button)').each(function (index, value) { 
		         	if($(this).val() == "" && index <= 3)
		         	{
		         		flag = 0; 
		         		
		         	}else{
		         		question_type4.push($(this).val());
		         	}
		         });
		         if(flag == 0)
		        {
		        	flag = 0;
		        	alert('Please enter the data in the options field.');
		        	 return flag;
		        }
		         if($('#multiChoice_ans4 option').filter(':selected').val() == "")
		         	{
		         		flag = 0; 
		         		alert('Please select the Answer.');
		         		return flag;
		         	}
		     	}
		     }
     	 
		var formData = new FormData($('#formSurvey')[0]); 
		if(question_type0 !='')
		{
			formData.append('multiple_choice0[]', question_type0);
		}
		if(question_type1 !='')
		{
			formData.append('multiple_choice1[]', question_type1);
		}
		if(question_type2 !='')
		{
			formData.append('multiple_choice2[]', question_type2);
		}
		if(question_type3 !='')
		{
			
			formData.append('multiple_choice3[]', question_type3);
		}
		if(question_type4 !='')
		{
			formData.append('multiple_choice4[]', question_type4);
		} 
		if(flag ==1)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				// mimeType: "multipart/form-data",
				contentType: false,
                // cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>survey/save_survey",
				data: formData
			}).success(function (json) {
				
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				
				$("#row_"+json.id).find('td:last').html(json.available_rewards);
				
				showMessages(json);
				$(".load_gif").html("");
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
				setTimeout(function(){  location.reload(); }, 5000);
				
			});
		} 
	});		
});


function deleteUsers()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected survey?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>survey/delete_survey",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getSurveyList();
				alert((json.ids).length+" survey(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one survey.');
	}
}
</script>

<script>
		
// 		$("#award_points").keydown(function (e) {
//         // Allow: backspace, delete, tab, escape, enter and .
//         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
//              // Allow: Ctrl+A, Command+A
//             (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
//              // Allow: home, end, left, right, down, up
//             (e.keyCode >= 35 && e.keyCode <= 40)) {
//                  // let it happen, don't do anything
//                  return;
//         }
//         // Ensure that it is a number and stop the keypress
//         if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
//             e.preventDefault();
//         }
// });	
// $(function() {
//     $('#question_type').change(function(){
//         $('#multipleoption')[ ($("option[value='Multiple Choice']").is(":checked"))? "show" : "hide" ]();  
//     }

    $(function() {
    $('#multipleoption0').hide(); 
	    $('#question_type0').change(function(){
	        if($('#question_type0').val() == 'Multiple Choice') {
	            $('#multipleoption0').show(); 
	        } else {
	            $('#multipleoption0').hide(); 
	        } 
	    });
    $('#yes_no0').hide(); 
    $('#question_type0').change(function(){
        if($('#question_type0').val() == 'Yes/No') {
            $('#yes_no0').show(); 
        } else {
            $('#yes_no0').hide(); 
        } 
    });
});

    
</script>



<?php $this->load->view("administrator/incls/footer"); ?>
