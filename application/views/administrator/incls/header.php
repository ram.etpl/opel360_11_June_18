<?php
$users =  ($this->uri->segment(2) == "UserAccess" || $this->uri->segment(2) == "getUsers" || $this->uri->segment(2) == "ImportOpelUsers") ? 'accordion-toggle menu-open':'accordion-toggle';
$catalog =  ($this->uri->segment(2) == "getCatalogModel" ) ? 'accordion-toggle menu-open':'accordion-toggle';
$pdf =  ($this->uri->segment(2) == "getPdf") ? 'accordion-toggle menu-open':'accordion-toggle';
$services =  ($this->uri->segment(2) == "getServicesCost" || $this->uri->segment(2) == "getServicesExclusiveCost") ? 'accordion-toggle menu-open':'accordion-toggle';
$accessories = ($this->uri->segment(2) == "getAccessories" || $this->uri->segment(2) == "getCatalogModelGallery") ? 'accordion-toggle menu-open':'accordion-toggle';
$events = ($this->uri->segment(2) == "getEvents") ? 'accordion-toggle menu-open':'accordion-toggle';
$banners = ($this->uri->segment(2) == "getBanners") ? 'accordion-toggle menu-open':'accordion-toggle';
$survey  = ($this->uri->segment(2) == "getSurvey" || $this->uri->segment(2) == "reward_survey") ? 'accordion-toggle menu-open':'accordion-toggle';

$forums = ($this->uri->segment(2) == "getForum") ? 'accordion-toggle menu-open':'accordion-toggle';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>OPEL360 Admin</title>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="keywords" content="OPEL360" />
    <meta name="description" content="OPEL360">
    <meta name="author" content="OPEL360">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
    <!-- Font CSS (Via CDN) -->
    <!--<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700'>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
-->
    <!-- Vendor CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/magnific/magnific-popup.css">

     <!-- Datatables CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datatables/media/css/dataTables.bootstrap.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/theme.css">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/loader.css">
     <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/skin/default_skin/css/media.css">

    <!-- Admin Panels CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-plugins/admin-panels/adminpanels.css">

   <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Admin Modals CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admin-tools/admin-plugins/admin-modal/adminmodal.css">
    
   
    
    
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.png">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
     
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/magnific/jquery.magnific-popup.js"></script>
    <!-- Datatables -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

    <!-- Datatables Tabletools addon -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.clickout.js"></script>
    
   
	
    <script type="text/javascript">
		var base_ourl = "<?php echo base_url();?>";
	</script>
	<style>
		.paginate_button:hover{cursor:pointer;}
		table.dataTable thead > tr > th{ padding-left:10px !important;}
		table.dataTable thead th:first-child{background:transparent !important;}
		div.dataTables_length select{ width: 175px !important;}
		.dt-panelfooter{ display: none;}
		#edit1{display:none}

    .table-responsive{overflow-y:auto; /*height:460px;*/}



	</style>
	<script>
            
            $(function(){ 
			var pathname = (window.location.href);
			$("ul.sidebar-menu ul.sub-nav .menu-open ul li a").removeClass("active");
			$(".sidebar-menu ul ul li a[href='" + pathname  + "']").addClass("active");
			}); 
        </script>
</head>

<body class="dashboard-page datatables-page sb-l-o sb-r-c onload-check">
	<div id="loading">
		<div id="loading-center">
		<div id="loading-center-absolute">
		</div>
		</div>
	</div>

<!-- Start: Main -->
    <div id="main">

        <!-- Start: Header -->
        <header class="navbar bg-light navbar-fixed-top">
            <div class="navbar-branding">
                <a class="navbar-brand" href="#"> <img src="<?php echo base_url(); ?>assets/img/logos/logo.png" class="img-responsive pull-left" />
					<span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines pull-left"></span>
                </a>
                
                <ul class="nav navbar-nav pull-right hidden">
                    <li>
                        <a href="#" class="sidebar-menu-toggle">
                            <span class="octicon octicon-ruby fs20 mr10 pull-right "></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="nav navbar-nav navbar-left pt20 text-logo"></div> 
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown"> 
						<i class="fa fa-user"></i>
                        <span>Administrator Settings</span>
                        <span class="caret caret-tp hidden-xs"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-persist pn w250 bg-white" role="menu">
                        <li class="of-h">
                            <a href="#changepassword" class="fw600 p12 animated animated-short fadeInUp model-open">
                                <span class="fa fa-user pr5"></span> Change Password
                                
                            </a>
                        </li>
                        <li class="br-t of-h">
                            <a href="<?php echo base_url(); ?>logout" class="fw600 p12 animated animated-short fadeInDown">
                                <span class="fa fa-power-off pr5"></span> Logout </a>
                        </li>
                    </ul>
                </li>
            </ul>

        </header>
        <!-- End: Header -->

        <!-- Start: Sidebar -->
        <aside id="sidebar_left" class="nano nano-primary affix has-scrollbar">
            <div class="nano-content">

                <!-- Start: Sidebar Header -->
                
                <!-- End: Sidebar Header -->

                <!-- sidebar menu -->
                <ul class="nav sidebar-menu">
                 <!-- sidebar resources -->
                    <li>
                        <a class="accordion-toggle menu-open" href="#">
                            <span class="fa fa-user"></span>
                            <span class="sidebar-title">Administration</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" >
							<?php $userdata_header = $this->session->userdata("auth_user");
							
							 if(get_user_permission($userdata_header['user_id'],'user_management')){?>
							<li>
                                <a class="<?php echo $users; ?>" href="#">
                                    <span class="glyphicons glyphicons-user"></span>Users
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a  href="<?php echo base_url(); ?>users/getUsers">Manage Users</a> </li>
                                    <li><a href="<?php echo base_url(); ?>users/ImportOpelUsers">Import Opel Users</a> </li>
                                </ul>
                            </li>
                            <?php } ?>
                            <?php if(get_user_permission($userdata_header['user_id'],'car_catalogue_management')){?>
                            <li>
                                <a class="<?php echo $catalog; ?>" href="#">
                                    <span class="fa fa-car"></span>Catalog
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav"> 
                                    <li><a href="<?php echo base_url(); ?>catalog/getCatalogModel">Manage Car Model</a> </li>
                                    
                                 </ul>
                            </li>
                            <?php } ?>
                             <?php if(get_user_permission($userdata_header['user_id'],"pdf_manuals_management")){?>
                            <li>
                                <a class="<?php echo $pdf; ?>" href="#">
                                    <span class="fa fa-file-pdf-o fa-1"></span>PDF Manual
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>pdf/getPdf">Manage PDF</a> </li>
                                 </ul>
                            </li>
                            <?php } ?>
                            
                            <?php if(get_user_permission($userdata_header['user_id'],'service_cost_management')){?>
                            <li>
                                <a class="<?php echo $services; ?>" href="#">
                                    <!--<span class="glyphicons glyphicons-user"></span>-->
									<span class="serv_icon"><img src="<?php echo base_url(); ?>img/service.png" /></span>Services
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>services/getServicesCost">Manage Services Cost</a> </li>
                                    <li><a href="<?php echo base_url(); ?>services/getServicesExclusiveCost">Manage Services Exclusive</a> </li>
                                 </ul>
                            </li>
                            <?php } ?> 
                            
                            
                             <?php if(get_user_permission($userdata_header['user_id'],'news_and_events_management')){ ?>
                            <li>
                                <a class="<?php echo $events; ?>" href="#">
                                    <span><i class="fa fa-calendar" aria-hidden="true"></i></span>Events & Promotions
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>events/getEvents">Manage Events & promotions</a> </li>
                                    <li><a href="<?php echo base_url(); ?>events/getEventsHistory">Events History</a> </li>
                                 </ul>
                            </li>
                            <?php } ?> 
                            
                            
                            <?php if(get_user_permission($userdata_header['user_id'],'accessories_catalogue_management')){ ?>
                            <li>
                                <a class="<?php echo $accessories; ?>" href="#">
                                    <span class="fa fa-car"></span>Catalog Accessories 
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
									<!--<li><a href="<?php echo base_url(); ?>accessories/getAccessoriesType">Manage Accessories Types</a></li>-->
                                    <li><a href="<?php echo base_url(); ?>accessories/getAccessories">Manage Accessories</a> </li>
                                    <li><a href="<?php echo base_url(); ?>catalog/getCatalogModelGallery">Manage Community Gallery</a></li>
                                </ul>
                            </li>
                            <?php } ?> 
                            
                            <?php if(get_user_permission($userdata_header['user_id'],'survey_management')){?>
                            <li>
                                <a class="<?php echo $survey; ?>" href="#">
                                    <span class="serv_icon"><img src="<?php echo base_url(); ?>img/survey.png" /></span>Survey Management
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>survey/getSurvey">Manage Survey</a> </li>
                                    <!-- <li><a href="<?php //echo base_url(); ?>survey/reward_survey">Manage Rewards</a> </li> -->
                                    
                                 </ul>
                            </li>
                            <?php } ?>
                            
                            <?php if(get_user_permission($userdata_header['user_id'],'banner_management')){?>
                            <li>
                                <a class="<?php echo $banners; ?>" href="#">
                                    <span><i class="fa fa-file-image-o" aria-hidden="true"></i></span>Banner Management
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>banner/getBanners">Manage Banners</a> </li>
                                 </ul>
                            </li>
                            <?php } ?>
                            
                            <?php if(get_user_permission($userdata_header['user_id'],'forum_management')){?>
                            <li>
                                <a class="<?php echo $forums; ?>" href="#">
                                    <span><i class="fa fa-tasks" aria-hidden="true"></i></span>Forum Management
                                    <span class="caret"></span> </a>
                                <ul class="nav sub-nav">
                                    <li><a href="<?php echo base_url(); ?>Forum/getForum">Manage Forum</a> </li>
                                 </ul>
                            </li>
                            <?php } ?>
                            
                            
                            
                        </ul>
                    </li>
                 <!-- sidebar bullets -->   
              </ul>
            </div>
      </aside>

    <!-- End: Main -->
