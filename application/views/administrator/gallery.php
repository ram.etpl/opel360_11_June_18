<?php  
$this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Community Gallery</li>
											<li class="crumb-trail">Manage Community Gallery</li>
										</ol>
								</div>
								<span class="panel-controls Users"> 
									<a id="view_panel" class="model-open" style="display:none;" href="#viewuserModel" title="View Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a id="add" class="model-open" href="#userModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a>
									
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="c_name">Name</option> 
													<option value="email_address">Email Address</option>
													<option value="user_type">User Type</option>
												</select>
												</div>
											</div>
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
													
												</div>
											</div>
                                            
											<div id="message"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,com_gal_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
	 
	 
  
</div> 
</div>
<script> 
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	

$(document).click(function(e) {  
    if(e.target.id !== "select-category" && !$("#vehicle-categories").find(e.target).length)
    {
     $("#vehicle-categories").hide(); 
    }
});

$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
jQuery("#txt_contact_number").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	
 
 

$( document ).ready(function(){ 
	$('#sel_user_type').change( function(){
		var user_type = $(this).val(); 
		
		if( user_type == 'ADMIN' ){
			$(".user_acc_section").show();
		}
		else{
			$(".user_acc_section").hide();
		}
	});
	$(document).on("click",".chk_role",function() {
		
        if($(this).is(":checked")) {
			
			$(this).val(1);
		}
		else{
			$(this).val(0);
		}
	});
});

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'user_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}


function changeStatus(uid,status)
{
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>users/change_user_status",
			data: {"userId":uid,"status":status},
		}).success(function (json) { 
			if(json.status == 1)
			{		
				if(json.action == "add")
					{   $("#row_"+json.id).html(json.row);
					}
			}
			showMessagesStatus(json); 
		});
}

function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'user_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getUserList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New User"); 
	 $(".user_acc_section").hide();
	 $('#sel_user_type').trigger('change');
	//$("#userId").val("");
	//document.getElementById("formUser").reset();
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#userId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>users/getUserById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_first_name").val(json.users.c_name);  
				$("#txt_email_address").val(json.users.email_address);
				$("#txt_contact_number").val(json.users.contact_details);
				$("#sel_user_type").val(json.users.user_type);
				$(".user_acc_section").show(); 
				$(".dataUserAccess").html(json.user_access); 
				$("#password_status").val(1);
				//$("#txt_city").val(json.city); 
				 
				$("#add").trigger("click"); 
				$("#labelpassword").html("");
				$("#userModel .panel-title").html("Edit User");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#userModel .mfp-close",function() {
	$("#userId").val("");
	$("#labelpassword").html("");
	$("#fInfo").html("");  	
	$("#lInfo").html("");  	
	$("#emailAddressInfo").html("");
	$("#userTypeInfo").html("");  
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formUser").reset();
	//location.reload();
});

function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'user_id','DESC'); 	
}
function loadmore(){
	getUserList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getUserList();

function getUserList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>users/list_users",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'user_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var userId = $("#userId"); 
		var fName = $("#txt_first_name");
			var fInfo = $("#fInfo");  	
		/*var lName = $("#txt_last_name");
			var lInfo = $("#lInfo");  	*/
		var emailAddress = $("#txt_email_address");
			var emailInfo = $("#emailAddressInfo"); 
			 			
		var upassword = $("#txt_password");
			var pwdInfo = $("#pwdInfo");
			  		
		
		var userType = $("#sel_user_type");
			var userTypeInfo = $("#userTypeInfo");	
							
		var flag=1;
		
		if(!validateEmpty(fName, fInfo, "name")){
			flag = 0;
		} 	
		
		/*if(!validateEmpty(lName, lInfo, "last name")){
			flag = 0;
		}*/
		
		if(fName.val()!=""){	 
			if(!CheckAlphabates(fName, fInfo)){
				flag = 0;
			} 	 
		}
		
		/*if(lName.val()!=""){	 
			if(!CheckAlphabates(lName, lInfo)){
				flag = 0;
			} 	 
		}*/ 
		
		if(!validateEmpty(emailAddress, emailInfo, "email address")){
			flag = 0;
		}
		
		if($("#password_status").val()==""){
			if(!validateEmpty(upassword, pwdInfo, "the password")){
				flag = 0;
			}
			else if(CheckPasswordText(upassword, pwdInfo)){
				
			} 
			else{
				flag=0;
			}
		}
		
		if($("#password_status").val()!=""){
			if(CheckPasswordText(upassword, pwdInfo)){
				
			} 
			else{
				flag=0;
			}
		}
		 
		if(emailAddress.val()!=""){
			if(!validateEmail(emailAddress, emailInfo)){
				flag = 0;
			}
		}
				
		
		if(!checkCombo(userType, userTypeInfo, "user type")){
			flag = 0;
		}
		
		
		var formData = new FormData($('#formUser')[0]); 
		if(flag)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>users/save_users",
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					changePaginate('','add');
				}
				showMessages(json);
				$(".load_gif").html("");
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});

 $(document).on("click","#view",function() {
	var i = 0; 
	var id = $(this).attr('data-option');
	$("#userId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>users/getUserDetailsById",
				data: {"id":id},
			}).success(function (json) {	
				$("#user_details").html(json.user_details);
				$("#view_panel").trigger("click");  
				 
			}); 
	} 
	else{
				$("#view_panel").trigger("click");
	}
});


function deleteUsers()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected users?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>users/delete_users",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getUserList();
				alert((json.ids).length+" user(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one user.');
	}
}
</script>
<?php $this->load->view("administrator/incls/footer"); ?>
