<?php $this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
										<li class="crumb-link">Users</li>
										<li class="crumb-trail">User Access</li>
									</ol>
								</div>
								<span class="panel-controls UserRoles"> 
									
								</span>
							</div>
							<div id="messages"></div>
							<form method="post" action="" id="formUserAccess" name="formUserAccess">
							<div class="panel-body pn of-a">
							<div id="table" class="table-responsive">
								<div class="user_acc_section">
									<div class="dataUserAccess">
										<?php echo $userAccess; ?>
									</div>
									<div class="col-lg-12 save_set"> 
										<a href="javascript:void(0)" id="save" class="button table-submitbtn btn-info btn-xs">Save Settings</a>
												<!--<input type="submit" class="button table-submitbtn btn-info btn-xs" id="btn_save" value="Save Setting">-->
									</div>
								</div>
							</div>
                        </form>
						<div class="rolepop-mainrow"></div>
					</div>
				</div>
            </section>
        </section>
		</div>
<script>  
/*$("input[type=checkbox]").change(function() {
	   if($(this).is(':checked')){
		   $(this).val(1);
	   }
	   else{
		   $(this).val(0);
	   }
});*/

$( document ).ready(function() {
	var height = $( window ).height() - 200;
	$("#animation-switcher .user_acc_section").css('height',height);
});

$(document).on("click","input:checkbox",function() {
		
        if($(this).is(":checked")) {
			
			$(this).val(1);
		}
		else{
			$(this).val(0);
		}
	});


$(document).on("click","#save",function(e) {
	var frm_val = $("#formUserAccess").serialize();
	
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>users/SaveUserAccessSettings",
			data: frm_val,
		}).success(function (data) {   
			if(data['msg']){
					
					$("#messages").html(data['msg']);
					$("#messages div").fadeOut(3000);
			}
			$(".dataUserAccess").html(data['userAccess']);
		});
}); 


</script>
<?php $this->load->view("administrator/incls/footer"); ?>
