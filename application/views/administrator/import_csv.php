<?php $this->load->view("administrator/incls/header"); ?>
<section id="content_wrapper">
	<!-- Begin: Content -->
	<section id="content" class="p15 pbn">
		<div class="row">
				<!-- Three panes -->
			<div class="col-md-12 admin-grid" id="animation-switcher">
				<div class="panel panel-info sort-disable" id="p0">
					<div class="panel-heading">
						<div class="topbar-left pull-left">
								<ol class="breadcrumb"> 
								<li class="crumb-link">Import Opel Users</li>
								<li class="crumb-trail">Import Opel via XML</li>
							</ol>
						</div>
						 
					</div> 
					<div class="panel-body mnw700 pn of-a">
						<div class="row mn">
							<div class="col-md-12 pn">
								<div class="dt-panelmenu clearfix">
									<?php if(!empty($validation_message)){?> 
									<div class="form-group col-md-12">
										<div class="alert alert-dismissable alert-danger" role="alert"><?php echo $validation_message; ?></div>
									</div>
									<?php }?>
									<?php if(!empty($message)) { ?>
									<div class="form-group col-md-12">
										<div class="alert alert-dismissable alert-success" role="alert"><?php echo $message; ?></div>
									</div>
									<?php } ?>
									
									<div id="already-exist-msg"></div>
									
									<div id="new-opel-msg"></div>
									
									<div id="blank-email-msg"></div>
									
									<div class="form-group uplo">
										<div class="bg-primary importcv-bgprimary"><b>Note :</b> 1. Maximum allowed size for XML file is 25 mb
										<span>2. XML file is allowed to upload.</span>										
										</div>
									</div>
									
									<div class="dataTables_filter pull-left">
										<form class="form-horizontal" action="javascript:void(0)" method="post" id="formImport" name="formImport" enctype="multipart/form-data">
										
										<div class="section row mbn">
										  <div class="col-sm-12 xml_file">
											<div class="form-group">
											  <label for="inputStandard" class="col-sm-3"><label>Upload XML File:</label></label>
											  <div class="col-sm-9"> 
												<input type="file" id="XML" name="XML" />
												<input type="hidden" id="XML_hidden"/>
											  </div>
											</div>
											<span id="csvInfo" style="margin-right:70px;"  class="text-danger"></span>
										  </div> 
										</div>
										<div class="col-lg-12 sumbit_xml"> 
											<input type="submit" class="button table-submitbtn btn-info btn-xs" id="frm-import1" name="frm-import1" value="Submit">
											<span id="loader_reg" class="loader_reg"></span>
										</div>
										</form>
										</div>
										
										  
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div> 
	</section>
</section> 

<script>

$(document).ready(function () {
	
	$('#frm-import1').on('click', function (e) {
		var formData = new FormData($('#formImport')[0]); 
		var ajaxRunning = false;
		
		var flag=1;
		
		var fileInfo = $("#csvInfo");
		
		if($("#XML_hidden").val() == ""){
			fileInfo.html("Please select the XML to upload.");
			flag = 0;
		}
		
		
		if($("#XML_hidden").val() != ""){
			
			var fname = $("#XML_hidden").val(); 
			var fname_e = fname.split('/').pop().split('\\').pop();  
			
			fname_ext = fname_e.split('.');  
			var lastVar = fname_ext.pop();
			
			if(lastVar != 'xml' && lastVar != 'XML'){ 
				fileInfo.html("Please upload xml file only.<br/>"+fname_e+" is not a xml file.");
				flag = 0;
			}
		}
		
		
		
		
		if(!ajaxRunning){
				ajaxRunning = true;
				
				 
		 
		if(flag)
		{
			
				
				$("#loader_reg").show();
				$("#loader_reg").attr('disabled','disabled');
				$("#loader_reg").html('<img width="20%" src="<?php echo base_url();?>img/gif-load.gif" >');	
				$("#already-exist-msg").show();
				$("#already-exist-msg").html('');
				
				$("#new-opel-msg").show();
				$("#new-opel-msg").html('');
				
				$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					url: "<?php echo base_url(); ?>users/ImportOpelUsersfromXML",
					data: formData
				}).success(function (json) {
					
					if(json.New_Opel_Users>0){
						$("#new-opel-msg").html("<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.New_Opel_Users+" Users successfully inserted.</div>");
						
					}
					
					if(json.Already_exist!=''){
						$("#already-exist-msg").html("<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.Already_exist+" Users are duplicate</div>");
						
					}
					if(json.blank_email!=''){
						$("#blank-email-msg").html("<div class='alert alert-info'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.blank_email+" Users are having blank email address</div>");
						
					}
					
					$("#loader_reg").hide();
				});
				
			}	
				
				
			}
		
		
		
	});
	
});



function showMessages(json)
{
	var class_label = "avadhut";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(5000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'news_id','ASC');
		}
		setTimeout(function(){  $("#importCSVModel .mfp-close").trigger( "click" ); }, 3000);
	}
}
   
  
$(document).on("click","#add",function() {
	$(".panel-title").html("Import CSV Product"); 
}); 
$("#XML").change(function (){
	
   var csv = $('#XML').val();
   $("#XML_hidden").val(csv);
}); 


</script>
<?php $this->load->view("administrator/incls/footer"); ?>
