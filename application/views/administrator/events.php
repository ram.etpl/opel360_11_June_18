<?php
$this->load->view("administrator/incls/header"); ?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
                        <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> 
											<li class="crumb-link">Events & Promotions Mangement</li>
											<li class="crumb-trail">Manage Events & Promotions</li>
										</ol>
								</div>
								<span class="panel-controls Users">
									<a id="add" class="model-open" href="#userModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a id="view_panel" class="model-open" style="display:none;" href="#viewuserModel" title="View Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteContest()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection">
												<select name="dd_searchBy" id="dd_searchBy"  aria-controls="datatable2" class="form-control input-sm">
													<option value="event_name">Event Title</option>
													<option value="start_date">Event Date</option>
												</select>
												</div>
											</div>
											
                                            <div class="dataTables_filter pull-left">
													<div id="contestrow1" class="row">
														<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
														<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
														<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
														<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
													</div>
													<div id="contestrow2" class="row" style="display: none;">
														<div class="col-xs-6 col-sm-4 top-serchbar2 displayFromDate"><div class="input-group date"><input type="text" id="start_date" readonly="readonly" name="start_date" class="form-control input-sm" placeholder="Start Date" value=""  title="Start Date"></div></div>
														<div class="col-xs-6 col-sm-4 top-serchbar2 pn displayToDate"> <div class="input-group date"><input type="text" id="end_date" readonly="readonly" name="end_date" class="form-control input-sm" placeholder="End Date" value=""  title="End Date"></div></div>
														<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
														<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
													</div>

											</div>
                                            
											<div id="message"></div>
										</div>
									</div>
								</div>
								<div class="loading-data" style="text-align:center;"></div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,contest_id,DESC"/>
								<div id="table" class="table-responsive">
									
								</div>
								<div id="links"></div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
        <!-- <div id="loading" style="text-align:center;"><img src="<?php echo base_url();?>assets/skin/default_skin/css/preloader.gif"></div> -->
	
		<div id="userModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Contest</span> </div>
		<!-- end .panel-heading section -->
		
		<form class="form-horizontal" method="post" action="" id="formContest" name="formContest" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
		  	<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Type<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					 <label><input type="radio" id="event_val" name="event_promo" value="Event" checked> Event</label>
		 			 <label><input type="radio" id="promotion_val" name="event_promo" value="Promotion"> Promotion</label>
					<span id="typeInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div> 
			<div id="message"></div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" id="eventpromotitle" class="col-lg-4 pn mt5 control-label new_first">Title<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_title" name="txt_title" class="form-control input-sm" placeholder="Enter Title" value="">
					<span id="titleInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div>  
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" id="startDatetitle" class="col-lg-4 pn mt5 control-label new_first">From Date<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new show-calend" >
					<input type="text" id="txt_from_date" name="txt_from_date" class="form-control input-sm" placeholder="Select Start Date" readonly="readonly" style="background:white;">
					<span class="add-on"><i class="icon-th"></i></span>
					<span id="fromdateInfo"  class="text-danger"></span>
					<span class="glyphicon glyphicon-calendar"></span>
				  </div>
				</div>
				
			  </div>
			  
			  
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" id="endDatetitle" class="col-lg-4 pn mt5 control-label new_first">To Date<span class="validationerror">*</span></label>
				 <div class="col-lg-8 text_inpu_new  show-calend">
					<input type="text" id="txt_to_date" name="txt_to_date" class="form-control input-sm" placeholder="Select End Date" readonly="readonly" style="background:white;">
					<span id="todateInfo"  class="text-danger"></span>
					<span class="glyphicon glyphicon-calendar"></span>
				  </div>
				</div>
			  </div>
			</div>
			
			
			<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" id="eventpromopic" class="col-lg-4 pn mt5 control-label new_first">Event Picture<span class="validationerror"></span></label>
				  
				  <div class="col-lg-8 text_inpu_new">
					<input type="file" id="file_event_pic" name="file_event_pic">
					<label class="note"><strong>Note:</strong>Please ensure that you upload images with min 100X100 px size.</label>
					<input type="hidden" id="hidden_file_event_pic" name="hidden_file_event_pic"> 
					<a href="javascript:void(0);" onclick="deleteEventDelete()" id="event_pic_delete" data-id="">Delete Event Picture</a>
					<img style="width:50%" src=""  id="event_pic_display" name="event_pic_display">
				  </div>
				</div>
			 </div>
			 </div>
			 
			 
			 <div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first"><span class="validationerror"></span></label>
				  <div class="col-lg-8 text_inpu_new" id="event_pic">
					  
					<input type="hidden" id="file_event_pic1" name="file_event_pic1">
				  </div>
				</div>
			 </div>
			 </div>
			 
			 
			 
			<div class="section row mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">User Type<span class="validationerror">*</span></label>
				  <div class="col-lg-10 text_inpu_new">
					<div class="own_check event">
						<span>Opel Owners<input type="radio" id="event_shown" name="event_shown[]" value="0"></span>
						<span>Non Opel Owners<input type="radio" id="event_shown" name="event_shown[]" value="1"></span>
						<span>All<input type="radio" id="event_shown" name="event_shown[]" value="2"></span>
					</div>
					<!-- <div class="own_check promo">
						<span>Opel Owners<input type="radio" id="event_shown" name="event_shown[]" value="0"></span>
						<span>Non Opel Owners<input type="radio" id="event_shown" name="event_shown[]" value="1"></span>
						<span>All<input type="radio" id="event_shown" name="event_shown[]" value="2"></span>
						<span>Public<input type="radio" id="event_shown" name="event_shown[]" value="3"></span>
					</div> -->
					<span id="frmshowInfo"  class="text-danger"></span>	
				  </div>
				</div>
			 </div>		 
			
			
			<div class="section mbn">
			  <div class="col-sm-12">
				<div class="form-group">
				  <label for="inputStandard" id="eventpromodesc" class="col-lg-2 pn mt5 control-label new_first" >Event Description<span class="validationerror"></span></label>
				  <div class="col-lg-12 event_inpu_new">
					<textarea  id="txt_description" name="txt_description" maxlength="500" rows="5" cols="60" placeholder="Enter short description"></textarea>
					<br/><span id="descriptionInfo"  class="text-danger"></span>
				  </div>
				</div>
			  </div>  
			</div>

			<div class="col-sm-12 rsvp">
				<div class="form-group">
				  <label for="" class="col-lg-2 pn mt5 control-label"></label>
				  <div class="col-lg-10 text_inpu_new">
					<div class="own_check col-lg-10">						
						<span><input type="checkbox" id="rsvp" name="rsvp" checked>RSVP</span>
					</div>
					<span id=""  class="text-danger"></span>	
				  </div>
				</div>
			 </div>
			
			<!-- end section row section -->
		  </div>
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="contestId" id="contestId" class="gui-input">
			<div class="load_gif"></div>
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
  <!-- end: .panel -->
  
</div> 

<div id="viewuserModel" class="popup-basiclg taxdetails-modaledit admin-form mfp-with-anim modal-lg mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span id="property_title" class="panel-title">View Topic Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div> 
				
			<div id="user_details">
			  
			</div>	 
			<!-- end section row section -->
		  </div>
		  <!-- end .form-body section -->
		  <div class="panel-footer">
			<input type="hidden" name="topicId" id="topicId" class="gui-input">
			<!--<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Update</button>-->
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  <!-- end .form-footer section -->
		</form>
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
	  </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/moment.js"></script> 	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script> 	
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css"> 

<script>
$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
}); 	
   
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});
$('#start_date,#end_date').datetimepicker( {pickTime: false,format: 'YYYY-MM-DD'}); 	
$('#txt_from_date,#txt_to_date').datetimepicker( {pickTime: false,minDate: '<?php echo date("Y-m-d"); ?>',format: 'YYYY-MM-DD'}); 	

$('#selecctall1').click(function(event) {
	if(this.checked) {
		$('.chk1').each(function() {
			this.checked = true;           
		});
	}else{
		$('.chk1').each(function() {
			this.checked = false;                    
		});         
	}
});

$(document).ready(function(){ 
		var chks = $("input[type='radio']:checked").val();
		if(chks !='end_date' ||chks !='start_date' || chks !='' )
		{	
			$('#contestrow1').show();
			$('#contestrow2').hide();
		}
		
		$("#event_pic_delete").hide();
});
$('#event_pic_display').attr('src','');
		$("#event_pic_delete").hide();
$(document).on("click","#search_btn",function() {
	var chks = $("input[type='radio']:checked").val();
	flag=1;
	
	if(chks =='title'){
		
		if($("#txt_search").val()==""){
			alert("Please enter the search term");
			flag=0;
			return false;
		}
	}  
	if(chks =='title'){
		if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
				//alert("Spaces are not allowed");
				flag = 0;
				return false;
		}
	}
	if(flag){
		changePaginate(0,'contest_id','DESC');
	}
	else{
		return false;
	}
});

$('#dd_searchBy').change(function () { 
		var chks = $("input[type='radio']:checked").val();
		
		if(chks =='end_date' ||chks =='start_date')
		{	
			$("#txt_search").val("");
			$('#contestrow1').hide();
			$('#contestrow2').show();
			$("#start_date").val("");
			$("#end_date").val("");			
		}else
		{	
			$("#txt_search").val("");
			$('#contestrow1').show();
			$('#contestrow2').hide();
			//$("#txt_search").datepicker("destroy");
		}
	});  


function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 3px 0;'>"+json.msg+"</div>");

$("#message div").fadeOut(10000);
}



function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'contest_id','DESC');
		}
		setTimeout(function(){  $("#userModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	
	$("#txt_paginate").val(start+","+column+","+order);
	getContestList();
} 

$(document).on("click","#add",function() {
	$(".panel-title").html("Create New Event & Promotion");
	var radioValue = $("input[name='event_promo']:checked").val();
	if(radioValue == "Event")
	{	$(".rsvp").show();
		$("#eventpromotitle").html("Event title<span class='validationerror'>*</span>");
		$("#startDatetitle").html("Start Date<span class='validationerror'>*</span>");
		$("#endDatetitle").html("End Date<span class='validationerror'>*</span>");
		$("#eventpromopic").html("Event Picture");
		$("#eventpromodesc").html("Event Description");
	}
});



$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	//alert(id);
	//return false;
	$("#contestId").val(id);
	if(id != ""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>events/getEventById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json.contest); 
				$("#txt_title").val(json.contest['event_name']);
				$("#txt_description").val(json.contest['event_description']); 
				$("#txt_from_date").val(json.contest['start_date']); 
				$("#txt_to_date").val(json.contest['end_date']);
				//$("#event_promo").val(json.contest['type']);
				//$("#rsvp").val(json.contest['is_rsvp']);
				// if(json.contest['is_rsvp'] != null && json.contest['is_rsvp']!="")
				// {
				// 	$('#rsvp').attr('checked', 'checked');
				// }else{
				// 	$('#rsvp').removeAttr('checked');
				// }
				//alert(json.contest['event_pic']);
				/*if(json.contest['event_pic'] != null && json.contest['event_pic']!="") {
					var event_pic = '<?php echo base_url(); ?>images/Events/'+json.contest['event_pic'];
					$("#event_pic").html('<img width="80%" src="'+event_pic+'" >');
				}
				else {
					$("#event_pic").html('');
				}*/
				if(json.contest['event_pic'] != null && json.contest['event_pic']!="") {
					
					$("#event_pic_display").show();
					$("#event_pic_delete").show();
					$("#event_pic_delete").attr('data-id',json.contest['id']);
					var srcimg='<?php echo base_url(); ?>images/Events/'+json.contest['event_pic'];	
					$('#event_pic_display').attr('src',srcimg);
					$("#hidden_file_event_pic").val(json.contest['event_pic']);
				}else{
					$("#event_pic_display").hide();
					$("#event_pic_delete").hide();
					var srcimg='';
					$('#event_pic_display').attr('src',srcimg);
					$("#hidden_file_event_pic").val('');
				}
				

				if(json.contest['type'] == 'Event')
				{
					$('input[name="event_promo"][value="' + json.contest['type'] + '"]').prop('checked', true);
					// $('input[name=event_promo]').attr('checked', true);
					var show_to = json.contest['event_show_to'];
					var shown_to = show_to.split(',');
					// alert(show_to);
					$('.event').find(':radio[name^="event_shown"]').each(function () {
	                    $(this).prop("checked", ($.inArray($(this).val(), shown_to) != -1));
					});
					$("#event_val").prop("disabled", false);
					$("#promotion_val").prop("disabled", true);
					$(".rsvp").show();
					if(json.contest['is_rsvp'] != null && json.contest['is_rsvp']!="")
					{
						$('#rsvp').attr('checked', 'checked');
					}else{
						$('#rsvp').removeAttr('checked');
					}
						$("#eventpromotitle").html("Event title<span class='validationerror'>*</span>");
						$("#startDatetitle").html("Start Date<span class='validationerror'>*</span>");
						$("#endDatetitle").html("End Date<span class='validationerror'>*</span>");
						$("#eventpromopic").html("Event Picture");
						$("#eventpromodesc").html("Event Description");
					
				}
				if(json.contest['type'] == 'Promotion')
				{
					$('input[name="event_promo"][value="' + json.contest['type'] + '"]').prop('checked', true);
					var show_to = json.contest['event_show_to'];				
					var shown_to = show_to.split(',');

					$('.event').find(':radio[name^="event_shown"]').each(function () {
	                    $(this).prop("checked", ($.inArray($(this).val(), shown_to) != -1));
					});
					$("#promotion_val").prop("disabled", false);
					$("#event_val").prop("disabled", true);
						$(".rsvp").hide();
			   //      	$(".promo").show();
			   //      	$(".event").hide();	
			   			$("#eventpromotitle").html("Promotion title<span class='validationerror'>*</span>");
						$("#startDatetitle").html("Start Date<span class='validationerror'>*</span>");
						$("#endDatetitle").html("End Date<span class='validationerror'>*</span>");
						$("#eventpromopic").html("Promotion Picture");
						$("#eventpromodesc").html("Promotion Description");		      
				}
				
				
				
				
				
				if(json.contest['status']!='Upcoming') {

				$("#txt_from_date").attr("disabled","disabled"); 
				$("#txt_to_date").attr("disabled","disabled"); 
				
			}
				$("#selecctall1").attr("disabled","disabled"); 
								
				$("#add").trigger("click"); 
				$("#userModel .panel-title").html("Edit Contest");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});

$(document).on("click","#userModel .mfp-close",function() {
	$("#contestId").val("");
	$("#titleInfo").html("");  	
	$("#descriptionInfo").html("");  	
	$("#fromdateInfo").html(""); 
	$("#todateInfo").html(""); 
	$("#frmshowInfo").html("");
	$("#checkedInfo").html("");
	$("#event_pic").html("");
	$("#btn_save").removeAttr('disabled','disabled');
	$("#txt_from_date").removeAttr("disabled","disabled"); 
	$("#txt_to_date").removeAttr("disabled","disabled"); 
	$("input[name='ids[]']").removeAttr("disabled","disabled"); 
	$("#selecctall1").removeAttr("disabled","disabled"); 
	$('#event_pic_display').hide();
	$("#event_pic_delete").hide();
	$("input[name='ids[]']").removeAttr("checked", true);
	$('#rsvp').attr('checked', 'checked');
	$("#promotion_val").prop("disabled", false);
	$("#event_val").prop("disabled", false);
	document.getElementById("formContest").reset();
	
});

function refreshTable()
{
	$("#txt_search").val("");
	$("#dd_searchBy").val(""); 
	$('#contestrow1').show();
	$('#contestrow2').hide();
	
	$(".multiselect-container .active input:checked").removeAttr("checked","checked");
	$(".multiselect-container li").removeClass("active");
	//$(".multiselect-container li:first-child").addClass("active");
	//$(".multiselect-container .active input").attr("checked","checked");
	
	$(".multiselect-container li:first-child").find("input").each(function () {
		$(this).trigger("click");
	});
	
	changePaginate(0,'contest_id','DESC'); 	
	
}

function loadmore(){
	getContestList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getContestList();

function getContestList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	
	if(searchBy =='start_date'){
			var fromDate = $("#start_date").val();
			var toDate = $("#end_date").val();
			if(fromDate =="" || toDate =="")
			{
				alert("Start date and end date are required");
				return false;
			}
			if(new Date(fromDate) >new Date(toDate))
			{
				alert("Start date should not be greater than end date");
				return false;
			}
	}
	
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>events/list_events",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy,"fromdate":fromDate,"todate":toDate},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}


$(document).ready(function(){
	$("#txt_search").val("");
	changePaginate(0,'contest_id','DESC'); 
	// if ($('#rsvp').is(':checked')){
	// 	$('#rsvp').val('0');
	// }
	// if ($('#rsvp').is(':unchecked')){
	// 	$('#rsvp').val('');
	// }


	$('#btn_save').on('click', function (e) {
		
		e.preventDefault();
		var contestId = $("#contestId"); 
		var titleName = $("#txt_title");
			var titleInfo = $("#titleInfo");  	
			
		var fromDate = $("#txt_from_date");
			var fromdateInfo = $("#fromdateInfo"); 
			 	
		var toDate = $("#txt_to_date");
			var todateInfo = $("#todateInfo"); 
			 			
		var description = $("#txt_description");
			var descriptionInfo = $("#descriptionInfo");
		var frmshowInfo = $("#frmshowInfo");
							
		var flag=1;
		
		if(!validateEmpty(titleName, titleInfo, "title")){
			flag = 0;
		} 	
		
		if(!checkCombo(fromDate, fromdateInfo, "start date")){
			flag = 0;
		}
		
		if(!checkCombo(toDate, todateInfo, "end date")){
			flag = 0;
		}
		
		if(titleName.val()!=""){	 
			if(!CheckAlphanumeric(titleName, titleInfo)){
				flag = 0;
			} 	 
		}
		
		if(fromDate.val()>toDate.val()){	
			fromdateInfo.html('The Start date should not be greater than End date');
			flag=0;
		}
		
		/*if(!validateEmpty(description, descriptionInfo, "short description of the event")){
			flag = 0;
		}*/
		
		var count_checked = $("[name='event_shown[]']:checked").length;
		if(count_checked==0) {
			frmshowInfo.html('Please select user type');
			flag = 0;
		}else{
			frmshowInfo.html('');
		}
		
		var formData = new FormData($('#formContest')[0]); 
		
		if(flag)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				//mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>events/save_event",
				data: formData
			}).success(function (json) {
				if(json.action == "modify")
				{
					$("#loading").hide();
					$("#row_"+json.id).html(json.row);
				}else{
					$("#loading").hide();
					changePaginate('','add');
				}
				if(json.status ==2)
				{
					$("#loading").hide();
					showMessages(json);
					$("#btn_save").removeAttr('disabled','disabled');
					// $(".panel-footer img:last-child").remove();
				}else{
					$("#loading").hide();
					showMessages(json);
					$("#btn_save").removeAttr('disabled','disabled');
					$(".panel-footer img:last-child").remove();
					setTimeout(function() {
			               window.location.reload();
			          },5000);
				}
				

				 
				
			});
		} 
	});		
});


function deleteContest()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		  //alert(chkId);
		  
		
		if(confirm('Are you sure want to delete?') == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>events/delete_event",
				data: {"ids":chkId},
			}).success(function (json) {
				
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getContestList();
				alert((json.ids).length+" row(s) has been deleted successfully.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one event.');
	}
}

function deleteEventDelete()
{
		var dataid = $("#event_pic_delete").attr('data-id');
		if(confirm("Are you sure want to delete event picture?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>events/delete_event_pic",
				data: {"id":dataid},
			}).success(function (json) {
				if(json.status='success'){
					alert("Event picture has been deleted.");
					$("#event_pic_delete").hide();
					$("#event_pic_display").hide();
					var srcimg='';	
					$('#event_pic_display').attr('src',srcimg);
					$("#hidden_file_event_pic").val('');
				}
				else{
					alert("Something went wrong.");
				}
				//location.reload();
			});
		}  
	 
}

$(document).ready(function(){
	
	$('input[name="event_promo"]').click(function(){
        var inputValue = $(this).attr("value");        
        if(inputValue == 'Promotion')
        {
        	$(".rsvp").hide();
        	$(".event").show();
        	//$(".event").hide();

				$("#eventpromotitle").html("Promotion title<span class='validationerror'>*</span>");
				$("#startDatetitle").html("Start Date<span class='validationerror'>*</span>");
				$("#endDatetitle").html("End Date<span class='validationerror'>*</span>");
				$("#eventpromopic").html("Promotion Picture");
				$("#eventpromodesc").html("Promotion Description");
			
        }else{
		    $(".rsvp").show();        	
        	$(".event").show();
        	//$(".event").show();
        	$("#eventpromotitle").html("Event title<span class='validationerror'>*</span>");
			$("#startDatetitle").html("Start Date<span class='validationerror'>*</span>");
			$("#endDatetitle").html("End Date<span class='validationerror'>*</span>");
			$("#eventpromopic").html("Event Picture");
			$("#eventpromodesc").html("Event Description");
        }
        
    });
    $('input:radio[name="event_shown[]"]').change(function(){
		    if($('input[name="event_promo"]:checked').val() == 'Event'){
		      $(".rsvp").show();
		    }else{
		    	$(".rsvp").hide();
		    }
		});
	$('input:radio[name="event_promo"]').change(function(){
		    if($('input[name="event_promo"]:checked').val() == 'Event'){
		      $(".rsvp").show();
		    }else{
		    	$(".rsvp").hide();
		    }
		});
});
$(document).on("click","#view",function() {
	var i = 0; 
	var id = $(this).attr('data-option');
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>events/getEventByIdDetials",
				data: {"id":id},
			}).success(function (json) {
				$("#user_details").html(json.user_details);
				$("#view_panel").trigger("click");  
				$("#viewuserModel .panel-title").html(json.types+" Details"); 
			}); 
	} 
	else{
				$("#view_panel").trigger("click");
	}
});
</script>
<?php $this->load->view("administrator/incls/footer"); ?>
