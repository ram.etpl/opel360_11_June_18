<?php  
$this->load->view("administrator/incls/header"); 
?>
        <!-- Start: Content-Wrapper -->
        <section id="content_wrapper">
            <!-- Begin: Content -->
            <section id="content" class="p15 pbn">
                <div class="row">
            <!-- Three panes -->
					<div class="col-md-12 admin-grid" id="animation-switcher">
						<div class="panel panel-info sort-disable" id="p0">
							<div class="panel-heading">
								<div class="topbar-left pull-left">
										<ol class="breadcrumb"> <li class="crumb-link">Catalog</li>
											<li class="crumb-trail">Manage Car Model</li>
										</ol>
								</div>
								<span class="panel-controls Users">  
									<a id="add" class="model-open" href="#catalogModel" title="Add New Record"><i class="fa fa-plus" data-toggle="modal" ></i></a> 
									<a class="" href="javascript:void(0)" onclick="deleteCatalogModel()" title="Delete Record"><i class="fa fa-times-circle text-white"></i></a>
									
								</span>
							</div>
							<div class="panel-body mnw700 pn of-a">
								<div class="row mn">
									<div class="col-md-12 pn">
										<div class="dt-panelmenu clearfix">
											<div class="dataTables_length">
												<div class="multiple-selection mr5">
												<select name="dd_searchBy" id="dd_searchBy" multiple="multiple" aria-controls="datatable2" class="form-control input-sm">
													<option value="model_name">Model Name</option>
													<option value="propellant">Propellant</option> 
												</select>
												</div>
											</div>
                                            <div class="dataTables_filter pull-left">
												<div class="row">
													<div class="col-xs-4 col-sm-2 top-serchtitle1 pt5"><label>Search:</label></div>
													<div class="col-xs-8 col-sm-6 top-serchbar2 pn"><input type="text" id="txt_search" name="txt_search" class="form-control input-sm" placeholder="Search Terms" aria-controls="datatable2"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar3"><input type="button" id="search_btn" class="button table-submitbtn btn-info btn-xs" value="Search"></div>
													<div class="col-xs-6 col-sm-2 top-serchbar4 button-submit col-sm-3"><input type="button" class="button table-submitbtn btn-info btn-xs" onclick="refreshTable();" value="Refresh"></div>
                                                </div>
											</div>
                                            
											<div id="message1"></div>
										</div>
									</div>
								</div>
								<input type="hidden" name="txt_paginate" id="txt_paginate" value="0,model_id,DESC"/>
								<div class="loading-data" style="text-align:center;"></div>
								<div id="table" class="table-responsive">
								</div>
								<div id="paginate"></div>
							</div>
						</div>
						</div>
					</div>
				</div>
            </section>
        </section>
        
        
			
			<!-- Modal -->
<div id="myModal" class="modal fade catlog_section" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Gallery</h4>
      </div>
      <div class="modal-body">
		
		 <!-- Upload file form start -->
  
			<form id="upload" method="post" action="<?php echo base_url();?>upload_files" enctype="multipart/form-data">
				<div id="drop">Drop Here
					<a>Browse</a>
					<input type="file" name="upl" multiple />
					<input type="text" name="car_model_id" id="car_model_id"/>
				</div>
				<ul>
					<!-- The file uploads will be shown here -->
				</ul>
				
			</form>
  
	   <!-- Upload file form end -->	
						
						<ul class="lb-album" id="all_car_images">
							
							<!--<li>
								<a href="#image-1">
									<img src="<?php echo base_url(); ?>car_gallery/a.jpeg" alt="image01">
									<span></span>
								</a>
								<div class="lb-overlay" id="image-1">
									
									<img src="<?php echo base_url(); ?>car_gallery/img_2016-04-26-16-08-36_571f7664e5823.jpg" alt="image01" />
									<div>
										<h3> <span></h3>
										<p></p>
									</div>
									<a href="#page" class="lb-close">x Close</a>
								</div>
							</li>-->
						</ul>	
						
      </div>
      
    </div>

  </div>
</div>
			
			
		
        
        
        
		<div id="catalogModel" class="popup-basiclg popup-full taxdetails-modaledit admin-form mfp-with-anim mfp-hide">
	  <div class="panel">
		<div class="panel-heading p15"> <span class="panel-title">Car Model Details</span> </div>
		<!-- end .panel-heading section -->
		<form class="form-horizontal" method="post" action="" id="formUser" name="formUser" enctype="multipart/form-data">
		 
		  <div class="panel-body p15">
			<div id="message"></div>
			
			<div><strong>All fields are required</strong></div>  
			<div class="section row mbn">
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Model Name</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_m_name" name="txt_m_name" class="form-control input-sm" placeholder="Model Name" />
					<span id="modelInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Weight - Without Driver KG</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_weight" name="txt_weight" class="form-control input-sm numeric" placeholder="Weight - Without Driver KG" />
					<span id="weightInfo" class="text-danger marg"></span> 
				  </div>
				</div>
			  </div>
			</div>   
			<div class="section row mbn">
			   
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Propellant</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_propellant" name="txt_propellant" class="form-control input-sm" placeholder="Propellant">
					<span id="propellanInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">No. Valves per cylinder</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_no_of_valve_cylinder" name="txt_no_of_valve_cylinder" class="form-control input-sm numeric" placeholder="No. Valves per cylinder"></textarea>
					<span id="noValveCylinderInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div> 
			
			<div class="section row mbn">
				<div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Engine Capacity (cc)</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_engine_capacity" name="txt_engine_capacity" class="form-control input-sm numeric" placeholder="Engine Capacity (cc)" />
					<span id="engineCapacityInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Number of Cylinder</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_no_of_cylinder" name="txt_no_of_cylinder" class="form-control input-sm numeric" placeholder="Number of Cylinder" />
					<span id="noOfCylinderInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div> 
			<div class="section row mbn">
			    <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Valve Train</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_valve_train" name="txt_valve_train" class="form-control input-sm" placeholder="Valve Train"/>
					<span id="valveTrainInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Cylinder Configuration</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_cylinder_config" name="txt_cylinder_config" class="form-control input-sm" placeholder="Cylinder Configuration" />
					<span id="cylinderConfigInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			</div>
			<div class="section row mbn">
				<div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Max output (Kw/rpm)</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_max_output" name="txt_max_output" class="form-control input-sm" placeholder="Max output (Kw/rpm)" />
					<span id="maxOutputInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			   <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Cooling</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_cooling" name="txt_cooling" class="form-control input-sm" placeholder="Cooling" />
					<span id="CoolingInfo" class="text-danger marg"></span>
				  </div>
				</div>
			   </div>
			</div>
			<div class="section row mbn">
			  <div class="col-sm-6">
				 <div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Acceleration 0-100 kmh</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_acceleration" name="txt_acceleration" class="form-control input-sm" placeholder="Acceleration 0-100 kmh" cols="40" rows="6"></textarea>
					<span id="accelerationInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			  <div class="col-sm-6">
				<div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Top speed (km/h)</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text" id="txt_top_speed" name="txt_top_speed" class="form-control input-sm numeric" placeholder="Top speed (km/h)" cols="40" rows="6"></textarea>
					<span id="topSpeedInfo" class="text-danger marg"></span>
				  </div>
				</div>
			  </div> 
			</div>
			<div class="section row mbn">
				<div class="col-sm-6">
				 <div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Fuel Consumption - Combined</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="text"  id="txt_fuel" name="txt_fuel" class="form-control input-sm" placeholder="Fuel Consumption - Combined" cols="40" rows="6"></textarea>
					<span id="fuelInfo" class="text-danger marg"></span>
				  </div>
				 </div>
			    </div>
			    <div class="col-sm-6">
					<div class="form-group">
					  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">CO2 - Combined	</label>
					  <div class="col-lg-8 text_inpu_new">
						<input type="text" id="txt_co2" name="txt_co2" class="form-control input-sm" placeholder="CO2 - Combined" cols="40" rows="6"></textarea>
						<span id="co2Info" class="text-danger marg"></span>
					  </div>
					</div>
				</div>
			</div>
			 <div class="section row mbn">
				<div class="col-sm-6">
				 <div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Upload Car Thumbnail</label>
				  <div class="col-lg-8 text_inpu_new">
					<input type="file"  id="car_thumb" name="car_thumb">
					<span id="carthumbInfo" class="text-danger marg"></span>
					<a href="javascript:void(0);" onclick="deleteCarThumb()" id="car_thumb_delete" data-id="">Delete Car Thumbnail</a>
				  </div>
				 </div>
			    </div>
			 </div>
			 <div class="section row mbn">
				<div class="col-sm-6">
				 <div class="form-group">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first"></label>
				  <div class="col-lg-8 text_inpu_new img_data">
					<img src=""  id="old_car_thumb" name="old_car_thumb" alt="No Image">
					<span id="oldcarthumbInfo" class="text-danger marg"></span>
				  </div>
				 </div>
			    </div>
			 </div>
			
			  <!-- end section -->
			
			<!-- end section row section -->
		  
		  <!-- end .form-body section -->
		  </div>
		  <div class="panel-footer">
			<input type="hidden" name="modelId" id="modelId" class="gui-input">
			<button type="submit" class="button btn-info btn-xs" name="btn_save" id="btn_save">Submit</button>
			<button type="button" class="button btn-info btn-xs mfp-close">Close</button>
		  </div>
		  
		  <!-- end .form-footer section -->
		</form>
		
		
		
		
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		
		
		
		
	  </div>
  <!-- end: .panel -->
  
	  
  
</div> 	

	
	<!-- Car Gallery css file start-->
	
	<!-- Car Gallery css file end-->


	<!-- The main CSS file -->
	
	<!-- Google web fonts -->
	<link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />
	<link href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/style.css" rel="stylesheet" /> 

 <!-- JavaScript Includes For Upload Car Gallery-->
    <script src="<?php echo base_url(); ?>assets/js/uploadjs/jquery.knob.js"></script>
    <!-- jQuery File Upload Dependencies -->
    <script src="<?php echo base_url(); ?>assets/js/uploadjs/jquery.ui.widget.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/uploadjs/jquery.iframe-transport.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/uploadjs/jquery.fileupload.js"></script>
	
	<!-- Our main JS file -->
	<script src="<?php echo base_url(); ?>assets/js/uploadjs/script.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script> 
$("#car_thumb_delete").hide();
$(".numeric").numeric({decimal:'.',negative:false});	
$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	

$('#txt_search').keydown(function (e){
    if(e.keyCode == 13){
        $("#search_btn").trigger('click');
    }
});	 
function loadeditor(){ 
	
    CKEDITOR.disableAutoInline = true;
			CKEDITOR.replace('txt_description', {
				width: 450,
				height: 250,
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
					
				}
			}); 		
									
}  


 

$( document ).ready(function() {
	var height = $( window ).height() - 230;
	$("#animation-switcher .table-responsive").css('height',height);
});
jQuery("#txt_contact_number").keypress(function (e) {  
	if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
			   return false;
	}
	else {
		return true;
	}
}); 	
 
 

$(document).on("click","#search_btn",function() {
	var searchby = $(".multiselect-container input:checked").length;
	flag=1;
	
	if($("#txt_search").val()==""){
		alert("Please enter the search term");
		flag=0;
		return false;
	} 
	if(searchby==0){
		alert("Please select at least one search by field");
		flag=0;
		return false;
	}
	
	if(!CheckSearchText($("#txt_search"),$("#searchInfo"))){
			//alert("Spaces are not allowed");
			flag = 0;
			return false;
	}
	
	if(flag){
		changePaginate(0,'model_id','DESC');
	}
	else{
		return false;
	}
});

function showMessagesStatus(json)
{

$("#message").html("<div class='pull-right makeText  alert alert-success' style='padding:0 0 2px 0;'> <button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");

//$("#message div").fadeOut(10000);
}
 
function showMessages(json)
{
	var class_label = "";
	switch (json.status) {
		case 0:
			class_label = "";
			break;
		case 1:
			class_label = "success";
			break;
		case 2:
			class_label = "text-danger";
			break;
	}
	$("#message").html("<div class='p5 mbn alert-dismissable pln "+class_label+"'>"+json.msg+"</div>");
	$("#message div").fadeOut(10000);
	if(json.status == 1)
	{	if(json.action == "add")
		{
			refreshTable(0,'model_id','DESC');
		}
		setTimeout(function(){  $("#catalogModel .mfp-close").trigger( "click" ); }, 5000);
	}
}
	 
function changePaginate(start,column,order)
{
	$("#txt_paginate").val(start+","+column+","+order);
	getCatalogModelList();
} 
  
$(document).on("click","#add",function() {
	$("#labelpassword").html("*");
	$(".panel-title").html("Add New Car Model"); 
	//loadeditor();
});


$(document).on("click","#close_gallery",function() { 
	var id = "";
	var img_id = $(this).attr('data-option');
	var model_id = $("#car_model_id").val();
	
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>catalog/deletecarcatelogimages",
			data: {"model_image_id":img_id,"model_id":model_id},
		}).success(function (json) { 
			if(json.status == 200)
			{		
				 $("#all_car_images").html(json.data);
			}
			//showMessagesStatus(json); 
		});
	
});


$(document).on("click","#add_car",function() { 
	var id = "";
	var model_id = $(this).attr('data-option');
	$("#car_model_id").val(model_id);
	
	//Get previous car images
	
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url();?>catalog/getcarcatelogimages",
			data: {"model_id":model_id},
		}).success(function (json) { 
			if(json.status == 200)
			{		
				 $("#all_car_images").html(json.data);
			}
			//showMessagesStatus(json); 
		});
	
});

$(document).on("click","#edit",function() { 
	$(".pic1 img:last-child").remove();
	var id = "";
	var i = 0; 
	var id = $(this).attr('data-option'); 
	var imgsrc="<?php echo base_url(); ?>car_gallery/thumb/actual_thumb/";
	//alert(id);
	//return false;
	$("#modelId").val(id);
	if(id!=""){
		$.ajax({
				type: "POST",
				dataType: "json",
				//fileElementId :'user_photo',
				url: "<?php echo base_url(); ?>catalog/getCatalogModelById",
				data: {"id":id},
			}).success(function (json) {
				//console.log(json);
				$("#txt_model_name").val(json.model_name);
				$("#txt_m_name").val(json.m_name);
				$("#txt_chasis_no").val(json.chasis_no);
				$("#txt_make").val(json.make);
				$("#txt_propellant").val(json.propellant);
				$("#txt_no_of_valve_cylinder").val(json.no_valves_per_cylinder);
				$("#txt_engine_capacity").val(json.engine_capacity);
				$("#txt_no_of_cylinder").val(json.number_of_cylinder);
				$("#txt_valve_train").val(json.valve_train);
				$("#txt_cylinder_config").val(json.cylinder_config);
				$("#txt_max_output").val(json.max_output);
				$("#txt_cooling").val(json.cooling);
				$("#txt_acceleration").val(json.acceleration);
				$("#txt_top_speed").val(json.top_speed);
				$("#txt_weight").val(json.weight);
				$("#txt_fuel").val(json.fuel_consumption);
				$("#txt_co2").val(json.co2_combined);
				if(json.car_thumb == ""){
						$("#car_thumb_delete").hide();
				}
				else{
						$("#car_thumb_delete").show();
				}
				$('#car_thumb_delete').attr('data-id',json.id);
				var srcimg=imgsrc+json.car_thumb;	
				$('#old_car_thumb').attr('src',srcimg);
				/*$("#txt_description").html(json.model_description);*/
				$("#add").trigger("click");  
				$("#catalogModel .panel-title").html("Edit Car Model");
			}); 
	} 
	else{
				$("#add").trigger("click");
	}
});
$(document).on("click","#catalogModel .mfp-close",function() {
	$("#upload ul li").remove();
	$("#modelId").val(""); 
	$("#modelInfo").html("");  	
	$("#chasisNoInfo").html("");
	$("#btn_save").removeAttr('disabled','disabled');
	document.getElementById("formUser").reset();
	//location.reload();
	$('#old_car_thumb').attr('src','');
	$('#old_car_thumb').attr('alt','');
	$("#carthumbInfo").html("");
});

$(document).on("click","#myModal .close",function() {
	$("#upload ul li").remove();
	$("#carthumbInfo").html("");
});


function refreshTable()
{
	$("#txt_search").val("");
	$(".multiselect-container .active input:checked").removeAttr("checked");
	$(".multiselect-container li").removeClass("active");
	$("#dd_searchBy").val(''); 
	changePaginate(0,'model_id','DESC'); 	
}



function loadmore(){
	getCatalogModelList();
	$("#table tr:last").remove();
	$('.moredata').animate({ scrollTop: $('#table table').height() }, 800); 
}
getCatalogModelList();

function getCatalogModelList()
{
	var paginate = $("#txt_paginate").val();
	var result = paginate.split(",");
	var searchBy = $("#dd_searchBy").val();
	var search = $("#txt_search").val();
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>catalog/list_catalog_model",
			data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
		}).success(function (json) {
			if(json.start != 0)
				{
					$("#table tbody").append(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				}
				else
				{
					$("#table").html(json.table);
					if(json.totalrec > 0 && json.totalrec >= json.limit){
						$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
					}
				} 
			$("#txt_paginate").val(json.paginate);
			checkAll();
			$("#loading").hide();
			$(".loading-data").html('');
		});
}


$(document).ready(function(){     
	$("#txt_search").val("");
	changePaginate(0,'model_id','DESC'); 
	/*$("#table").scroll(function(){
			if  ($("#table").scrollTop() == $("#table table").height() - $("#table").height()){
				getUserList();
			}
		});	*/
	$('#btn_save').on('click', function (e) {
		e.preventDefault();
		var modelId = $("#modelId");  
		var modelName = $("#txt_m_name");
			var modelInfo = $("#modelInfo");  	
		var txt_propellant = $("#txt_propellant");
			var propellanInfo = $("#propellanInfo");  	
		var txt_engine_capacity = $("#txt_engine_capacity");
			var engineCapacityInfo =$("#engineCapacityInfo");
		var txt_valve_train = $("#txt_valve_train");
			var valveTrainInfo =$("#valveTrainInfo");			
		var txt_max_output = $("#txt_max_output");
			var maxOutputInfo =$("#maxOutputInfo");	
		var txt_acceleration = $("#txt_acceleration");
			var accelerationInfo = $("#accelerationInfo");
		var txt_fuel = $("#txt_fuel");
			var fuelInfo =$("#fuelInfo");
		var txt_weight = $("#txt_weight");
			var weightInfo =$("#weightInfo");
		var txt_no_of_valve_cylinder = $("#txt_no_of_valve_cylinder");
			var noValveCylinderInfo =$("#noValveCylinderInfo");	
			
		var txt_cylinder_config = $("#txt_cylinder_config")	;
			var cylinderConfigInfo = $("#cylinderConfigInfo");
		var txt_no_of_cylinder = $("#txt_no_of_cylinder");
			var noOfCylinderInfo =$("#noOfCylinderInfo");
		var txt_cooling = $("#txt_cooling");
			var CoolingInfo =$("#CoolingInfo");
		var txt_top_speed= $("#txt_top_speed");
			var topSpeedInfo =$("#topSpeedInfo");
		var txt_co2= $("#txt_co2");
			var co2Info =$("#co2Info");			
		var flag=1;
		
		if(!validateEmpty(modelName, modelInfo, "model name")){
			flag = 0;
		} 	
		
		if(modelName.val() != "") {	 
			if(!CheckAlphanumericWithSomeChars(modelName, modelInfo)){
				flag = 0;
			} 	 
		}
		
		if(!validateEmpty(txt_propellant, propellanInfo, "propellant")){
			flag = 0;
		} 
		if(!validateEmpty(txt_engine_capacity, engineCapacityInfo, "engine capacity")){
			flag = 0;
		} 
		if(!validateEmpty(txt_valve_train, valveTrainInfo, "valve train")){
			flag = 0;
		} 
		if(!validateEmpty(txt_max_output, maxOutputInfo, "max output")){
			flag = 0;
		} 
		if(!validateEmpty(txt_acceleration, accelerationInfo, "acceleration")){
			flag = 0;
		} 
		if(!validateEmpty(txt_fuel, fuelInfo, "fuel consumption")){
			flag = 0;
		} 
		if(!validateEmpty(txt_weight, weightInfo, "weight")){
			flag = 0;
		} 
		if(!validateEmpty(txt_no_of_valve_cylinder, noValveCylinderInfo, "no. valves per cylinder")){
			flag = 0;
		} 
		if(!validateEmpty(txt_no_of_cylinder, noOfCylinderInfo, "no. of cylinder")){
			flag = 0;
		} 
		if(!validateEmpty(txt_cylinder_config, cylinderConfigInfo, "cylinder configuration")){
			flag = 0;
		} 
		
		if(!validateEmpty(txt_cooling, CoolingInfo, "cooling")){
			flag = 0;
		} 
		if(!validateEmpty(txt_top_speed, topSpeedInfo, "top speed")){
			flag = 0;
		} 
		if(!validateEmpty(txt_co2, co2Info, "co2")){
			flag = 0;
		} 
		
		/*if(!validateEmpty(description, descriptionInfo, "description")){
			flag = 0;
		}*/		
		
		var formData = new FormData($('#formUser')[0]); 
		//formData.append("txt_description", CKEDITOR.instances['txt_description'].getData());
		if(flag)
		{
			$(".load_gif").html("<img src='<?php echo base_url();?>img/gif-load.gif' />");
			$("#btn_save").attr('disabled','disabled');
			$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>catalog/save_catalog_model",
				data: formData
			}).success(function (json) {
				
				if(json.status == '0') {
					
					$("#carthumbInfo").html(json.error);
					return false;
				}
				
				if(json.action == "modify")
				{   $("#row_"+json.id).html(json.row);
				}else{
					
					 $("#message").html(json.msg);
					
					changePaginate('','add');
				}
				showMessages(json);
				$(".load_gif").html("");
				$("#btn_save").removeAttr('disabled','disabled');
				$(".panel-footer img:last-child").remove();
			});
		} 
	});		
});
function deleteCatalogModel()
{
	if($('.chk:checked').length) {
		  chkId = new Array();
		  var i = 0;
		  $('.chk:checked').each(function () {
			chkId[i] = $(this).val();
			i++;
		  });
		
		if(confirm("Are you sure want to delete selected model?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>catalog/delete_catalog_model",
				data: {"ids":chkId},
			}).success(function (json) {
				for (i = 0; i < (json.ids).length; i++) { 
					$("#row_"+json.ids[i]).remove();
					var paginate = $("#txt_paginate").val();
					var result = paginate.split(",");
					result[0] = result[0] - (json.ids).length;
					$("#txt_paginate").val(result[0]+","+result[1]+","+result[2]);
				}
				getCatalogModelList();
				alert((json.ids).length+" catalog(s) has been deleted.");
				//location.reload();
			});
		}  
	}
	else {
	  alert('Please select at least one car model.');
	}
}

function deleteCarThumb()
{
		var dataid = $("#car_thumb_delete").attr('data-id');
		if(confirm("Are you sure want to delete car picture?") == true)
		{
		  $.ajax({
				type: "POST",
				dataType: "json",
				cache:false,
				url: "<?php echo base_url(); ?>catalog/delete_catalog_car_thumb",
				data: {"id":dataid},
			}).success(function (json) {
				if(json.status='success'){
					alert("car picture has been deleted."); 
					$("#car_thumb_delete").hide();
					var srcimg='';	
					$('#old_car_thumb').attr('src',srcimg);
				}
				else{
					alert("Something went wrong.");
				}
				//location.reload();
			});
		}  
	 
}

</script>
<link href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/demo.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/admin-tools/admin-forms/css/style2.css" rel="stylesheet" />
<?php $this->load->view("administrator/incls/footer"); ?>
