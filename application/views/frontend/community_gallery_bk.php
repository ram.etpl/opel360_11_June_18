<!--all profile page section -->
<div class="gallery_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Community Gallery</div>
				</div>
			</div>
		</div>	
	</div>
	<?php //$this->load->view("frontend/incls/dashboard_menu");?>
<!--customer profile section-->	
	<div class="service_wrapper">
		<div class="container">
			<div class="col-lg-12">
				<div class="road_wrapper"> 
					<form class="form-horizontal" method="post" action="" id="formSurvey" name="formSurvey" enctype="multipart/form-data">
		
		
		<!-- Banner Modal Start-->
				    <div  id="ng-app" ng-app="app">
						<div ng-controller="myCtrl">
						  <!--<button type="button" class="close" ng-click="getcomgallery()" data-dismiss="modal">&times;</button>-->
						  
						</div>
					    <div>
								<div ng-controller="AppController" nv-file-drop="" uploader="uploader">
									<div class="row">
										<div class="col-md-3">
											<h3>Select files</h3>
											<div ng-show="uploader.isHTML5">
												<!-- 3. nv-file-over uploader="link" over-class="className" -->
												<div class="well my-drop-zone" nv-file-over="" uploader="uploader">
													Base drop zone
												</div>
											</div>
											<input type="file" nv-file-select="" uploader="uploader" multiple  /><br/>
										</div>
										<div class="col-md-9" style="margin-bottom: 40px">
											<h2>Upload Community Images</h2>
											<h3>The queue</h3>
											<p>Queue length: {{ uploader.queue.length }}</p>

											<table class="table">
												<thead>
													<tr>
														<th width="50%">Name</th>
														<th ng-show="uploader.isHTML5">Size</th>
														<th ng-show="uploader.isHTML5">Progress</th>
														<th>Status</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
												<tr ng-repeat="item in uploader.queue">
													<td>
														<strong>{{ item.file.name }}</strong>
														<!-- Image preview -->
														<!--auto height-->
														<!--<div ng-thumb="{ file: item.file, width: 100 }"></div>-->
														<!--auto width-->
														<div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 100 }"></div>
														<!--fixed width and height -->
														<!--<div ng-thumb="{ file: item.file, width: 100, height: 100 }"></div>-->
													</td>
													<td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
													<td ng-show="uploader.isHTML5">
														<div class="progress" style="margin-bottom: 0;">
															<div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
														</div>
													</td>
													<td class="text-center">
														<span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
														<span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
														<span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
													</td>
													<td nowrap>
														<button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
															<span class="glyphicon glyphicon-upload"></span> Upload
														</button>
														<button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
															<span class="glyphicon glyphicon-ban-circle"></span> Cancel
														</button>
														<button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
															<span class="glyphicon glyphicon-trash"></span> Remove
														</button>
												</td>
											</tr>
										</tbody>
									</table>

									<div>
										<div>
											Queue progress:
											<div class="progress" style="">
												<div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
											</div>
										</div>
										<button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
											<span class="glyphicon glyphicon-upload"></span> Upload all
										</button>
										<button type="button" class="btn btn-warning btn-s" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
											<span class="glyphicon glyphicon-ban-circle"></span> Cancel all
										</button>
										<button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
											<span class="glyphicon glyphicon-trash"></span> Remove all
										</button>
									</div>

                </div>

            </div>

        </div>
        
	 </div>
					</div>
					
		   <!-- Banner Modal End --> 	
		
		
		</form>
				
				</div>
				
			</div>			
			<div class="col-md-12">			
				<div id="no_record"></div>
				<div id="com_gallery"></div>
			</div>
		</div>
	</div>
<!--customer profile section-->	

</div>
<!--all profile page section -->
<style>
            .my-drop-zone { border: dotted 3px lightgray; }
            .nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
            .another-file-over-class { border: dotted 3px green; }

            html, body { height: 100%; }

            canvas {
                background-color: #f3f3f3;
                -webkit-box-shadow: 3px 3px 3px 0 #e3e3e3;
                -moz-box-shadow: 3px 3px 3px 0 #e3e3e3;
                box-shadow: 3px 3px 3px 0 #e3e3e3;
                border: 1px solid #c3c3c3;
                height: 100px;
                margin: 6px 0 0 6px;
                width: 100%;
            }
            
            .progress-bar {
				background-color: #428bca;
				box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.15) inset;
				color: #fff;
				float: left;
				font-size: 12px;
				height: 100%;
				text-align: center;
				transition: width 0.6s ease 0s;
				width: 0;
			}
            
      </style>
<!-- Banner js start -->
<script src="https://nervgh.github.io/js/es5-shim.min.js"></script>
<script src="https://nervgh.github.io/js/es5-sham.min.js"></script>
<!--<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/console-sham.js"></script>
<!--<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>-->
<script src="https://code.angularjs.org/1.1.5/angular.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/angular-file-upload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/communitycontroller.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/directives.js"></script>
<!-- Banner js end -->


<script type="text/javascript">
	getCommunityGallery();
	function getCommunityGallery()
	{
		$(".loading-data").html('<b>Please wait while loading data</b>');
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>get-com-gallery",
				data: {},
			}).success(function (json) {
				$("#com_gallery").html(json.html);
				
			});
	}
	
	function deleteCommunityGalleryImage(img_id)
	{
		if(img_id != ''){ 
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>del-com-gallery",
				data: {img_id:img_id},
			}).success(function (json) {
				getCommunityGallery();
				
			});
		}
		else{
			alert('Image not exist');
		}
	}

</script>
