<!--about page section -->
<div class="serexclu_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/models_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">	
			<div class="row">
				<div class="col-lg-12">
					<div class="about_title">Opel Service Exclusive</div>
				</div>
			</div>	
		</div>	
	</div>
	
	<div class="service_wrapper">
		<div class="container">
		  <div class="row">
			<div class="col-md-12">
	
				<div class="serexc_head">COPY FOR OPEL SERVICE EXCLUSIVE PAGE :</div>
					<p>Buying an Opel is always a good choice, and here you will find more reasons why. As an Opel owner, we make sure we look after you and your car.
					</p>
					<p>
					It is our personal commitment to you, your car and your safety on the road. Building a great vehicle is just first step – maintaining it for optimal performance is what makes a truly enjoyable driving experience. Plus, our pricing is always fair and transparent for any work we perform.
					</p>
				<div class="serexc_head">OPEL SERVICE EXLUSIVE ENTITLEMENTS :</div>
					<ul>
						<li>Servicing costs will be capped at $1,000 for 3 years (6 scheduled servicing appointments) with option to extend 4th and 5th year servicing at $800</li>
						<li>Complimentary transport back to your office or home after dropping off your vehicle for servicing</li>
						<li>Exclusive programmes and activities for customers held annually</li>
						<li>24-hour roadside assistance to our service centre</li>
					</ul>				
				<div class="serexc_head">SERVICING AND OWNERSHIP :</div>
					<p>As part of our Opel Service Exclusive, Opel is proud to offer a service experience carefully designed to guarantee transparency and total peace-of-mind concerning all aspects of your vehicle's maintenance and costs. Experienced, profession staff trained by Opel and Genuine Opel Parts ensure that the quality of our work is never compromised.</p>
					<p>We provide you with a low cost of ownership by offering cost efficiencies and competitive pricing. In addition, you will always be made aware of the cost of any scheduled maintenance, repairs of menu-priced items, thanks to transparent cost estimates, based on clearly communicated prices even before you purchase and Opel.</p>
					<div class="serexc_head">WARRANTY:</div>
					<p>With a 5 year warranty, Opel Service Exclusive gives you peace of mind right from the start. The warranty comes with the assurance of trained service technicians, quality workmanship and genuine Opel parts.
					</p>
					<div class="serexc_head">HIGH QUALITY OPEL ORIGINAL PARTS :</div>
					<p>No matter how much you drive your car you know that in time certain parts become worn and eventually need replacing. But you can be sure the original manufacturer makes the best replacement parts.
					</p>
					<ul>
						<li>Consistent high quality</li>
						<li>Best fit (less fitting time, no reworking costs)</li>
						<li>Fully tested for function and safety </li>
						<li>Excellent availability (for quick replacement)</li>
						<li>Best value at a fair price</li>
					</ul>
					<div class="serexc_head">24/7 ROADSIDE ASSISTANCE :</div>
					<p>Opel Roadside Assistance lets you travel with complete peace of mind. A stress-free motoring experience like having one's own personal assistant, a mere phone call away:
						<ul>
							<li>24-hour Roadside Assistance: 9423 0863</li>
							<li>24-hour Accident Hotline: 9247 8842</li>
							<li>24-hour Towing Service: 9729 7337</li>
						</ul>
					</p>	
			</div>
			<div class="col-md-12">
				<div class="serexcl_table">
					<div class="table-responsive">				
						<table class="table table-bordered" id="services_tb">
						<?php if(!empty($service_data)){						
						$model_id = "";
						$i=1;
						$prv_model_id=0;	
						$prev_cost="";	
						$j=0;		
						
						
								  
						foreach($service_data as $service)
						{


						if($prv_model_id!=$service['model_id'])
						{
						$prv_model_id=$service['model_id'];
						?>
						
						<thead>
							<tr>
								<th colspan="3" class="mod_service"><i class="fa fa-car" aria-hidden="true"></i><?php echo $service['model_name']; ?>(for vehicles registered from 23 June 2016 onwards)</th>
							</tr>
							<tr>
								<th class="text-center">Year</th>
								<th class="text-center">Milege(KM)</th>
								<th class="text-center">Opel Service Exclusive</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-center pr_col" width="30%"><?php echo ordinal($service["year"])." Year"; ?></td>
								<td class="text-center" width="40%"><?php echo number_format($service["milage"]); ?></td>
								<td class="text-center pr_col" width="30%"><?php echo "S$".$service["cost"]; ?></td> 
							</tr>
							<?php
							}
							else
							{
							$prv_model_id=$service['model_id'];
							$prev_cost = $service['cost'];
							?>
							<tr>
								
								<td class="text-center pr_col" width="30%"><?php echo ordinal($service["year"])." Year"; ?></td>
								<td class="text-center"><?php echo number_format($service["milage"]); ?></td>
								<td  class="text-center pr_col"><?php echo "S$".$service["cost"]; ?></td> 
							</tr>
							<?php 
							}
							$i++;
							}

							}
							else{
							?>
							<tr>
								<td colspan="3">No Records Available</td>
							</tr>
							<?php	
							} ?>
						</tbody>	
					  </table>
					  <div></div>
					  
					</div>
				</div>	
			</div>	
			
	   </div>		
	 </div>
 </div>
	
	
	   
<script type="text/javascript"> 
    $(document).ready(function () {
		
        $('#services_tb').each(function () {
            var Column_number_to_Merge = 3;
			var Column_number_to_Merge_Year = 1;
            // Previous_TD holds the first instance of same td. Initially first TD=null.
            var Previous_TD = null;
            var Previous_TD_Year = null;
            var i = 1;
            var j = 1;
            $("tbody",this).find('tr').each(function () {
                // find the correct td of the correct column
                // we are considering the table column 1, You can apply on any table column
                var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');
                 
                if (Previous_TD == null) {
                    // for first row
                    Previous_TD = Current_td;
                    i = 1;
                }
                else if (Current_td.text() == Previous_TD.text()) {
					//alert('tet');
                    // the current td is identical to the previous row td
                    // remove the current td
                    Current_td.remove();
                    // increment the rowspan attribute of the first row td instance
                    Previous_TD.attr('rowspan', i + 1);
                    i = i + 1;
                }
                else {
                    // means new value found in current td. So initialize counter variable i
                    Previous_TD = Current_td;
                    i = 1;
                }
                
                var Current_td_year = $(this).find('td:nth-child(' + Column_number_to_Merge_Year + ')');
                //alert(Current_td_year.text());
                if (Previous_TD_Year == null) {
                    // for first row 
                    Previous_TD_Year = Current_td_year;
                    j = 1; 
                }
                else if (Current_td_year.text() == Previous_TD_Year.text()) {
					
                    // the current td is identical to the previous row td
                    // remove the current td
                    
						Current_td_year.remove();
						// increment the rowspan attribute of the first row td instance
						Previous_TD_Year.attr('rowspan', j + 1);
						j = j+ 1;
					
                }
                else { 
                    // means new value found in current td. So initialize counter variable i
                    Previous_TD_Year = Current_td_year;
                    j = 1;
                }
            });
        });
    });
</script>    
</div>
<!-- about page section -->
