<!--all rent section -->
<div class="rent_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Pay for Car Booking</div>
				</div>
			</div>
		</div>	
	</div>	
<!--customer profile section-->	
	<div class="news_wrapper_all">
		<div class="container">
			<div class="col-md-12">
				
				<div class="cars_total_wrapper"><span class="col-md-6 car_count">Order Summary :</span><a href="#" title="Back"><span class="col-md-6 car_count_back"><i class="fa fa-hand-o-left" aria-hidden="true"></i></span></a></div>
				<div class="rent_next_conf_wrapper car_pay_wrapper">					
					<div class="rent_next">
						<div class="list_car_name"><?php echo $booking_data['Model'];?></div>
						<div class="li_decar_wra">
							<div class="col-md-7 date_wrapp">
								<div class="from_date"><span class="fr_text">From - </span><?php echo $booking_data['pickup_date_time'];?>/div>
								<div class="from_date"><span class="fr_text">To - </span><?php echo $booking_data['return_date_time'];?></div>
								<div class="locat_wrapper">
									<div class="loc_inner"><span class="loc_text">Pickup Location - </span>7 Ubi Close</div>
									<div class="loc_inner"><span class="loc_text">Dropoff Location - </span>7 Ubi Close</div>
								</div>
							</div>
							<div class="col-md-5">
							  <div class="lt_img_car_pay">
									<img alt="" src="images/car_rent_opel.jpg">
								</div>
							</div>
						</div>
					</div>	

					<div class="order_pay_table">
						<div class="table-responsive">				
							<table class="table table-bordered" id="">
							<thead>
								<tr>							
									<th>Car Details</th>
									<th class="text-center">Days</th>
									<th class="text-center">Net Price</th>
									<th class="text-center">Tax</th>
									<th class="text-center">Total Price</th>	
								</tr>
							</thead>
							<tbody>						
								<tr>
									<td>Astra Gtc 1.4 A</br>Standard Insurance</br>Kms Included: Unlimited</td>
									<td class="text-center">1</td> 
									<td class="text-center">$&nbsp;<?php echo $booking_data['Startprice'];?></td>
									<td class="text-center">$&nbsp;<?php echo $booking_data['Tax'];?></td>
									<td class="text-center">$&nbsp;<?php echo ($booking_data['Startprice']+$booking_data['Tax']);?></td>
								</tr>
								<tr class="total_wrapper">
									<td class="tot_amt">Total</td>
									<td class="text-center">1</td> 
									<td class="text-center">$&nbsp;<?php echo $booking_data['Startprice'];?></td>
									<td class="text-center">$&nbsp;<?php echo $booking_data['Tax'];?></td>
									<td class="text-center">$&nbsp;<?php echo ($booking_data['Startprice']+$booking_data['Tax']);?></td>
								</tr>	
							</tbody>
							</table>
						</div>		
					</div>
					<form action="<?php echo base_url();?>pay-now" method="post" id="frmRentCar" name="frmRentCar">
					<div class="driver_wrapper">						
							<div class="driver_head">Driver's Informations</div>
							<div class="driver_contct_wrapper">							
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-user"></i>First Name
											<span class="aster">*</span>
										</label>
										<input id="FirstName" class="form-control" type="text" placeholder="Enter First Name" name="FirstName">
									</div>
								</div>
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-user"></i>Last Name
											<span class="aster">*</span>
										</label>
										<input id="LastName" class="form-control" type="text" placeholder="Enter Last Name" name="LastName">
									</div>
								</div>
								<div class="col-md-4 rt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-location-arrow" aria-hidden="true"></i>Address
											<span class="aster">*</span>
										</label>
										<input id="Street" class="form-control" type="text" placeholder="Enter Address" name="Street">
									</div>
								</div>
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-phone"></i>Mobile Number
											<span class="aster">*</span>
										</label>
										<input id="MobileNo" class="form-control" type="text" placeholder="Enter Mobile Number" name="MobileNo">
									</div>
								</div>
								
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-thumb-tack" aria-hidden="true"></i>Zip Code
											<span class="aster">*</span>
										</label>
										<input id="PostalCode" class="form-control" type="text" placeholder="Enter Zip Code" name="PostalCode">
									</div>
								</div>
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-globe" aria-hidden="true"></i>Country
											<span class="aster">*</span>
										</label>
										<input id="Country" class="form-control" type="text" placeholder="Enter Country" name=""Country>
									</div>
								</div>
								<div class="col-md-4 rt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa fa-envelope"></i>Email Address
											<span class="aster">*</span>
										</label>
										<input id="CustomerEmail" class="form-control" type="text" placeholder="Enter Email Address" name="CustomerEmail">
									</div>
								</div>
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-calendar" aria-hidden="true"></i>Date of Birth
											<span class="aster">*</span>
										</label>
										<input id="DateOfBirth" class="form-control" type="text" placeholder="Enter Date of Birth" name="DateOfBirth">
									</div>
								</div>
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-file-text-o" aria-hidden="true"></i>Nric/Fin/Passport
											<span class="aster">*</span>
										</label>
										<input id="" class="form-control" type="text" placeholder="Enter Nric/Fin/Passport" name="">
									</div>
								</div>
								<div class="col-md-4 rt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-calendar" aria-hidden="true"></i><i class="fa fa-user"></i>Motorcar Licence Pass Date
											<span class="aster">*</span>
										</label>
										<input id="" class="form-control" type="text" placeholder="Enter Motorcar Licence Pass Date" name="">
									</div>
								</div>
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-sticky-note-o" aria-hidden="true"></i>Notes
										</label>
										<textarea id="" class="form-control" placeholder="Enter Notes" name="" type="text"></textarea>
									</div>
								</div>								
							</div>

						<div class="driver_head">Credit Card Details</div>
							<div class="driver_contct_wrapper">							
								<!--<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-credit-card" aria-hidden="true"></i>Name on Card
											<span class="aster">*</span>
										</label>
										<input id="" class="form-control" type="text" placeholder="Enter Name on Card" name="">
									</div>
								</div>-->
								<div class="col-md-4 lt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-credit-card-alt" aria-hidden="true"></i>Credit Card No
											<span class="aster">*</span>
										</label>
										<input id="CrCardNo" class="form-control" type="text" placeholder="Enter Credit Card No" name="CrCardNo">
									</div>
								</div>
								<div class="col-md-4 rt_re">
									<div class="form-group">
										<label for="usrname">
											<i class="fa fa-calendar" aria-hidden="true"></i>Expiry Date (MM/YY)
											<span class="aster">*</span>
										</label>
										<input id="CrCardDate" class="form-control" type="text" placeholder="Enter Expiry Date" name="CrCardDate">
									</div>
								</div>
							</div>
							
							<div class="conf_order_btn">
								<button id="" type="submit" name="">Confirm Order</button>
							</div>	
				</div>
					</form>
			</div>
		</div>
	</div>	
<!--customer profile section-->	

</div>
