<!-- start Banner section -->
			<div class="banner_wrapper">
				<div class="banner">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<?php echo $banners; ?>
						</div>
					</div>
				</div>
			</div>
		<!-- end Banner section -->
		
<!-- start body section -->
		
		<div class="bottom_line">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 opel_wrapper">
						<div class="col-lg-10 be_wra">
							<div class="opel_title">Be An Opel Member And <span class="enjoy_title">Enjoy Great benefits</span></div>
						</div>
						 
					</div>
				</div>
			</div>	
		</div>		
		
		<div class="white_wrapper">	
			<div class="container">
					 
					<div class="col-lg-12 search_car_wrapper">
						<div class="search_title">Search car Models</div>
						<div class="search_text">Lorem Ipsum is simply dummy text of the printing.</div>
						<div class="circle_img"><img src="images/circle.png" alt=""/></div>
						<div class="input_box_wrapper">
							
							<div class="col-lg-4 box">
								<div class="form-group">
									<label>Select A Manufacturer :</label>
									<select class="form-control">
										<option>select</option>
									</select>									
								</div>
							</div>
							<div class="col-lg-4 box">
								<div class="form-group">
									<label>Select Model :</label>
									<select class="form-control">
										<option>select</option>
									</select>									
								</div>
							</div>
							<div class="col-lg-4 box">
								<div class="form-group">
									<label>Price Filter :</label>
									<select class="form-control">
										<option>select</option>
									</select>									
								</div>
							</div>
							<div class="col-lg-4 box">
								<div class="form-group">
									<label>Vehicle Type :</label>
									<select class="form-control">
										<option>select</option>
									</select>									
								</div>
							</div>
							<div class="col-lg-4 box">
								<div class="form-group">
									<label>Vehicle Status :</label>
									<select class="form-control">
										<option>select</option>
									</select>									
								</div>
							</div>
							<div class="col-lg-4 filter_wrap">
								<div class="form-group">
									<label>&nbsp;</label>
									<div class="filte_btn"><a href="#">Filter vehicles &raquo;</a></div>							
								</div>
							</div>
							
						</div>
					</div>					
					<div class="car_det_wrapper">
						<div class="col-lg-12 car_rent">Car for rental</div>
						<div class="col-lg-4 car_wra">
							<div class="img_rent"><img src="images/car1.jpg" alt=""/><div class="mask"></div></div>
							<div class="price_car">30,000</div>
							<div class="car_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
						</div>
						<div class="col-lg-4 car_wra">
							<div class="img_rent"><img src="images/car2.jpg" alt=""/><div class="mask"></div></div>
							<div class="price_car">30,000</div>
							<div class="car_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
						</div>
						<div class="col-lg-4 car_wra">
							<div class="img_rent"><img src="images/car3.jpg" alt=""/><div class="mask"></div></div>
							<div class="price_car">30,000</div>
							<div class="car_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
						</div>
					</div>
			</div>	
		</div>
		
		<!-- start background image section -->
			
		<!-- end background image section -->
		
		<!-- start newsletter section -->
		<div class="newlet_wrapper">
			<div class="col-lg-8 lt_news_wrapper">
				 
			</div>
			<div class="col-lg-4 social_wrapper">
				<ul>
					<li class="face"><a href="https://www.facebook.com/OpelSingapore/" title="Facebook"><i class="fa fa-facebook"></i></a></li>
				</ul>
			</div>
		</div>	
		<!-- end newsletter section -->
<!-- end body section -->
