<!--all profile page section -->
<div class="gallery_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Survey</div>
				</div>
			</div>
		</div>	
	</div>
	<?php $this->load->view("frontend/incls/dashboard_menu");?>
	
	<!--customer profile section-->	
	<div class="profile_wrapper">
		<div class="container">
			<div class="col-lg-12 cus_name">
				<img src="<?php echo base_Url();?>images/survey_ico.png"/>Survey				
			</div>
			<div class="col-lg-12 survey_wrapper">
				<div class="all_sur">
				
					<div class="sur_inner">
						<div class="lt_survey">
							<div class="how_head">How Was Your Wait</div>
						</div>
						<div class="rt_survey">
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
						</div>
					</div>
					<div class="sur_inner">
						<div class="lt_survey">
							<div class="how_head">How Was Your Wait</div>
						</div>
						<div class="rt_survey">
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
						</div>
					</div>
					<div class="sur_inner">
						<div class="lt_survey">
							<div class="how_head">How Was Your Wait</div>
						</div>
						<div class="rt_survey">
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
						</div>
					</div>
					<div class="sur_inner">
						<div class="lt_survey">
							<div class="how_head">How Was Your Wait</div>
						</div>
						<div class="rt_survey">
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
							 <div class="star small"></div>
						</div>
					</div>
					
				</div>				
			</div>				
		</div>
	</div>
<!--customer profile section-->	

</div>
<!--all profile page section -->

<script type="text/javascript">
	jQuery("#edit-profile-contact").keypress(function (e) {  
		if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
				   return false;
		}
		else {
			return true;
		}
	}); 
	$(document).ready(function () {
		
		
		$("#edit-profile-name").keypress(function(e) {
    if(e.which == 13) {
		$("#editprofile").click();
    }
});

		$("#edit-profile-contact").keypress(function(e) {
    if(e.which == 13) {
		$("#editprofile").click();
    }
});

		$("#edit-profile-email").keypress(function(e) {
    if(e.which == 13) {
		$("#editprofile").click();
    }
});
		
		
		$('#editprofile').on('click', function (e) {
			var fName = $("#edit-profile-name");
			var fInfo = $("#EditProfileNameInfo");
			
			var contact_number = $("#edit-profile-contact");
			var ContactNoInfo = $("#EditContactNoInfo");
			
			var email_address = $("#edit-profile-email");
			var EmailAddressInfo = $("#EditProfileEmailAddressInfo");
			
			var flag=1;
				
				if(!validateEmpty(fName, fInfo, "name")){
				flag = 0;
				}
				
				if(fName.val() != ""){	 
					if(!CheckAlphabates(fName, fInfo)){
						flag = 0;
					}
				}
				
				if(!validateEmpty(email_address, EmailAddressInfo, "email")){
					flag = 0;
				}
		
				if(email_address.val()!=""){
					if(!validateEmail(email_address, EmailAddressInfo)){
						flag = 0;
					}
				}
				
				/*if(!validateEmpty(contact_number, ContactNoInfo, "contact no")){
					flag = 0;
				}*/
				
				var formData = new FormData($('#frmeditprofile')[0]); 
				var ajaxRunning = false;
				
				if(flag)
		{
			if(!ajaxRunning){
				ajaxRunning = true;
				
				//$("#loader_reg").show();
				//$("#loader_reg").attr('disabled','disabled');
				//$("#loader_reg").html('<img width="20%" src="<?php echo base_url();?>img/gif-load.gif" >');	
				$("#edit-profile-msg").show();
				$("#edit-profile-msg").html('');
				$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					url: "<?php echo base_url(); ?>api/edit_profile",
					data: formData
				}).success(function (json) {
					
					if(json.status=='success'){
					
						$("#edit-profile-msg").html("<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Profile update successfully.</div>");
						$("#edit-profile-msg").fadeOut(6000);
						setTimeout(function(){  window.location="<?php echo base_url();?>profile"; }, 5000);
					}
					
					if(json.status=='error'){
						$("#edit-profile-msg").html("<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>Something went wrong.</div>");
						$("#edit-profile-msg").fadeOut(6000);
					}
				});
			}
		}
			
		});
	});
</script>		


<script type="text/javascript">
	jQuery('#carousel ul li').click(function () {
	jQuery(this).closest('li').removeClass('selected');
	jQuery(this).closest('li').addClass('activeation');
	});
</script>
