<!--all accesories section -->
<div class="access_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Accessories</div>
				</div>
			</div>
		</div>	
	</div>
	
<!--accesss section-->	
	<div class="access_wrapper">
		<div class="container">
			<div class="col-md-12 dro_wra">
				<?php if($models){?>
				<div class="sel_model"><i class="fa fa-car" aria-hidden="true"></i>Select Car Model</div>
				
				<div class="fleft">
					<select id="cd-dropdown" class="cd-select">
						<option value="-1" selected>Select Model</option>
						<?php foreach($models as $model):?>	
								<option value="<?php echo $model['id']?>"><?php echo $model['m_name']."-".$model['propellant']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>	
				<?php } ?> 
				<?php if($acctype){?>
				<div class="sel_model"><i class="fa fa-car" aria-hidden="true"></i>Select Accessories Type</div>
				
				<div class="fleft">
					<select id="acc-dropdown" class="cd-select">
						<option value="-1" selected>Select Accessories Type</option>
						<?php foreach($acctype as $acc):?>	
								<option value="<?php echo $acc['id']?>"><?php echo $acc['acc_type']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>	
				<?php } ?>		
				<div class="yellow-btn">
					<a name="editprofile" id="editprofile" href="javascript:void(0);"><span><i class="fa fa-paper-plane"></i></span>Sumbit</a>
				</div>	
			</div>
			<div class="slider_section col-md-12">
				<div id="main" role="main">
					<section class="slider">			
						<div id="carousel" class="flexslider">
							<ul class="slides">
								<li class="selected"><img src="images/gal_1.jpg" /></li>
								<li class="selected"><img src="images/gal_2.jpg" /></li>
								<li class="selected"><img src="images/gal_3.jpg" /></li>
								<li class="selected"><img src="images/gal_4.jpg" /></li>
								<li class="selected"><img src="images/gal_5.jpg" /></li>
							</ul>
						</div>				
						<div id="slider" class="flexslider">
							<ul class="slides">
								<li><img src="images/gal_large_1.jpg" /></li>
								<li><img src="images/gal_large_2.jpg" /></li>
								<li><img src="images/gal_large_3.jpg" /></li>
								<li><img src="images/gal_large_4.jpg" /></li>
								<li><img src="images/gal_large_1.jpg" /></li>
							</ul>
						</div>				
					</section>
				</div>			
			</div>
			<div class="car_mod_text col-md-12">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
		</div>
	</div>
<!--accesss section-->	
</div>

<!--all accesories page section -->
<script type="text/javascript">
	jQuery('#carousel ul li').click(function () {
		jQuery(this).closest('li').removeClass('selected');
		jQuery(this).closest('li').addClass('activeation');
	});
</script>
<script> 

	$("#cd-dropdown").on("change","#search_btn",function() {
			
	});
	function getAccessoriesDetails(){
		var acc_type = <?php echo $acc_type;?>;
		var model = $("#txt_search").val();
		$("#loading").show();
		$(".loading-data").html('<b>Please wait while loading data</b>');
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>pdf/list_pdf_manual",
				data: {"start":result[0],"column":result[1],"order":result[2],"search":search,"searchBy":searchBy},
			}).success(function (json) {
				if(json.start != 0)
					{
						$("#table tbody").append(json.table);
						if(json.totalrec > 0 && json.totalrec >= json.limit){
							$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
						}
					}
					else
					{
						$("#table").html(json.table);
						if(json.totalrec > 0 && json.totalrec >= json.limit){
							$("#table tr:last").after('<tr><td colspan="6" align="center"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></td></tr>');
						}
					} 
				$("#txt_paginate").val(json.paginate);
				checkAll();
				$("#loading").hide();
				$(".loading-data").html('');
			});
	}
</script>
