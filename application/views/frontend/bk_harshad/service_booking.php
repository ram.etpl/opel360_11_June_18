
<!--about page section -->
<div class="service_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url(); ?>images/service_cost_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">			
			<div class="col-lg-12">
				<div class="about_title">Service Booking</div>
			</div>			
		</div>	
	</div>
	
	<div class="service_wrapper serv_booking">
		<div class="container">
			<div class="col-md-12">
			
				<ul class="tabs">
					<li class="tab col"><a class="active" href="#stp1">Step-1</a></li>
					<li class="tab col"><a href="#stp2">Step-2</a></li>
					<li class="tab col"><a href="#stp3">Step-3</a></li>
				</ul>
				</div>
				<div class="all_tabs_wrapper col-md-12">
					<div id="stp1" class="step1_wrapper">						
						<div class="form-group select_drop">
							<label class="col-md-2">Select Service-<span class="">*</span></label>
							<div class="col-md-3">
								<select id="" name="" class="select-sm form-control">
									<option value="">service1</option>
									<option value="">service2</option>
								</select>
							</div>
						</div>
						<div class="form-group select_drop">
							<label class="col-md-2">Select Service-<span class="">*</span></label>
							<div class="col-md-3">
								<select id="" name="" class="select-sm form-control">
									<option value="">service1</option>
									<option value="">service2</option>
								</select>
							</div>
						</div>
						<div class="col-md-2">
							&nbsp;
						</div>
						<div class="next_btn col-md-3">
							<a id="" name="" href="javascript:void(0);">Next</a>
						</div>
					</div>
					
					<div id="stp2" class="step2_wrapper">
						<div class="col-md-6 fist_colu">
							<div class="cust_tab_wrap">
								<div class="form-group select_drop">
									<label class="col-md-5">Customer Name-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">harshad</div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Vehicle No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">62061545</div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Customer HP No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">5566</div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Reference No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">3964799564</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-6 second_colu">		
							<div class="form-group select_drop">
								<label class="col-md-5">Select Booking Date-<span class="">*</span></label>
								<div class="col-md-6">
									<select id="" name="" class="select-sm form-control">
										<option value="">service1</option>
										<option value="">service2</option>
									</select>
								</div>
							</div>
							<div class="form-group select_drop">
								<label class="col-md-5">Select Booking Milage-<span class="">*</span></label>
								<div class="col-md-6">
									<select id="" name="" class="select-sm form-control">
										<option value="">10k</option>
										<option value="">20k</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5">Remarks-</label>
								<div class="col-md-6">
									<textarea id=""  name=""></textarea>
								</div>
							</div>							
						</div>
						<div class="next_btn col-md-12">
							<a id="" name="" href="javascript:void(0);">Sumbit</a>
							<a id="" name="" href="javascript:void(0);" class="previ">Previous</a>
						</div>
					</div>
					
					<div id="stp3" class="step3_wrapper">
						<div class="col-md-6 fist_colu">
							<div class="cust_tab_wrap">
								<div class="cus_head_book">Customer Details-</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Customer Name-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">harshad</div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Vehicle No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">62061545</div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Customer HP No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">5566</div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Reference No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">3964799564</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-6 second_colu">		
							<div class="cus_head_book">Booking Details</div>
							<div class="form-group select_drop">
									<label class="col-md-5">Booking Date & Time-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">harshad</div>
									</div>
							</div>
							<div class="form-group select_drop">
									<label class="col-md-5">Booking Milage-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">62061545</div>
									</div>
							</div>
							<div class="form-group select_drop">
									<label class="col-md-5">Remarks-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text">5566</div>
									</div>
							</div>							
						</div>
						<div class="next_btn col-md-12">
							<a id="" name="" href="javascript:void(0);">Confirm & Sumbit</a>
							<a id="" name="" href="javascript:void(0);" class="previ">Previous</a>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
		
</div>
<!--about page section-->
<!--material js & css-->
<link rel="stylesheet" href="<?php echo base_url();?>css/materialize.min.css">
<script src="<?php echo base_url();?>js/materialize.min.js"></script>