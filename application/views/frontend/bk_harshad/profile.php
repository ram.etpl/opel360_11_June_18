<!--all profile page section -->
<div class="gallery_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Profile</div>
				</div>
			</div>
		</div>	
	</div>
	<?php $this->load->view("frontend/incls/dashboard_menu");?>
<!--customer profile section-->	
	<div class="profile_wrapper">
		<div class="container">
									<?php
										$Login_user_data = $this->session->userdata("auth_opeluser");
										$login_user_id = $Login_user_data['id'];
										$cond = array("user_id"=>$login_user_id);
										$getAllVehicleOwnerUsersData = $this->users_model->getCustomerVehicleById($cond);
										$users = $this->users_model->getAllUsers($cond);
									?>	
			<div class="col-lg-12 cus_name">
				Customer profile
				<?php
								$user_type = $Login_user_data['user_type'];
								if($user_type=='NONOPEL') {
								?>
				<span class="edit_link"><a href="#" data-toggle="modal" data-target="#editmodel" title="Edit Profile"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>
				<?php
			}
				?>
			</div>
			<div class="col-lg-12">
				<div class="road_wrapper">
					<div class="col-lg-12 left_sect">
						<div class="lt_pro_wrapper">
							<div class="rt_det_wrapper">
								<div class="cus_details_all">								
																
									<div class="lt_title">Name :</div>
									<div class="rt_title"><?php echo $users[0]['c_name']; ?></div>
								</div>
								<div class="cus_details_all">
									<div class="lt_title">Contact No :</div>
									<div class="rt_title"><?php echo ($users[0]['contact_number']?$users[0]['contact_number']:'-'); ?></div>
								</div>
								<div class="cus_details_all">
									<div class="lt_title">Email :</div>
									<div class="rt_title"><?php echo $users[0]['email_address']; ?></div>
								</div>							
								<?php
								$user_type = $Login_user_data['user_type'];
								if($user_type=='OPEL') {
								?>
								<div class="cus_details_all">
									<div class="lt_title">Opel Model :</div>
									<div class="rt_title"><?php echo $getAllVehicleOwnerUsersData[0]['vehicle_group']; ?></div>
								</div>
								<div class="cus_details_all last_det">
									<div class="lt_title">Regsister No :</div>
									<div class="rt_title"><?php echo $getAllVehicleOwnerUsersData[0]['registration_no']; ?></div>
								</div>
								<?php
								}
								?>							
							</div>
						</div>
					</div>	
				</div>
			</div>			
		<!--slider section-->	
		<div class="slider_section">
			<div id="main" role="main">
				<section class="slider">			
					<div id="carousel" class="flexslider">
						<ul class="slides">
							<li class="selected"><img src="images/gal_1.jpg" /></li>
							<li class="selected"><img src="images/gal_2.jpg" /></li>
							<li class="selected"><img src="images/gal_3.jpg" /></li>
							<li class="selected"><img src="images/gal_4.jpg" /></li>
							<li class="selected"><img src="images/gal_5.jpg" /></li>
						</ul>
					</div>				
					<div id="slider" class="flexslider">
						<ul class="slides">
							<li><img src="images/gal_large_1.jpg" /></li>
							<li><img src="images/gal_large_2.jpg" /></li>
							<li><img src="images/gal_large_3.jpg" /></li>
							<li><img src="images/gal_large_4.jpg" /></li>
							<li><img src="images/gal_large_1.jpg" /></li>
						</ul>
					</div>				
				</section>
			</div>			
		</div>
		<!--slider section-->	
		</div>
	</div>
<!--customer profile section-->	

<!--start edit profile popup-->		
			<div id="editmodel" class="modal fade forgot_popup" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
							<h4 class="modal-title">Edit Profile</h4>
						</div>
						<div class="modal-body">
							
							<div id="edit-profile-msg" class="edit-profile-msg"></div>
							
							
							<form role="form" action="javascript:void(0);" name="frmeditprofile" id="frmeditprofile">

								<input type="hidden" id="login-user-id" name="user_id" value="<?php echo $login_user_id; ?>" />
								<div class="col-md-12 forgot_box">
									<div class="form-group">
										<label for="usrname"><i class="fa fa-user"></i>Name<span class="aster">*</span></label>
										<input type="text" placeholder="Enter name" value="<?php echo $users[0]['c_name']; ?>" id="edit-profile-name" name="fname" class="form-control">
										<span id="EditProfileNameInfo" class="validation"></span>
									</div>
									 
									<div class="form-group">
										<label for="usrname"><i class="fa fa-envelope"></i>Email<span class="aster">*</span></label>
										<input type="text" placeholder="Enter email" value="<?php echo $users[0]['email_address']; ?>" id="edit-profile-email" name="email" class="form-control">
										<span id="EditProfileEmailAddressInfo" class="validation"></span>
									</div>
									
									
									<div class="form-group">
										<label for="usrname"><i class="fa fa-phone"></i>Contact No</label>
										<span class="plus_text">+65</span><input type="text" value="<?php echo $users[0]['contact_number']; ?>" placeholder="Enter contact no" id="edit-profile-contact" name="contact_no" class="form-control">
										<span id="EditContactNoInfo" class="validation"></span>
									</div>
									
									<div class="forgot_btn pro_btn"><a href="javascript:void(0);" id="editprofile" name="editprofile"><span><i class="fa fa-paper-plane"></i></span>Sumbit</a></div>
								</div>
							</form>
							<div class="clearfix">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
<!--end edit profile popup-->	


</div>
<!--all profile page section -->

<script type="text/javascript">
	jQuery("#edit-profile-contact").keypress(function (e) {  
		if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
				   return false;
		}
		else {
			return true;
		}
	}); 
	$(document).ready(function () {
		
		
		$("#edit-profile-name").keypress(function(e) {
    if(e.which == 13) {
		$("#editprofile").click();
    }
});

		$("#edit-profile-contact").keypress(function(e) {
    if(e.which == 13) {
		$("#editprofile").click();
    }
});

		$("#edit-profile-email").keypress(function(e) {
    if(e.which == 13) {
		$("#editprofile").click();
    }
});
		
		
		$('#editprofile').on('click', function (e) {
			var fName = $("#edit-profile-name");
			var fInfo = $("#EditProfileNameInfo");
			
			var contact_number = $("#edit-profile-contact");
			var ContactNoInfo = $("#EditContactNoInfo");
			
			var email_address = $("#edit-profile-email");
			var EmailAddressInfo = $("#EditProfileEmailAddressInfo");
			
			var flag=1;
				
				if(!validateEmpty(fName, fInfo, "name")){
				flag = 0;
				}
				
				if(fName.val() != ""){	 
					if(!CheckAlphabates(fName, fInfo)){
						flag = 0;
					}
				}
				
				if(!validateEmpty(email_address, EmailAddressInfo, "email")){
					flag = 0;
				}
		
				if(email_address.val()!=""){
					if(!validateEmail(email_address, EmailAddressInfo)){
						flag = 0;
					}
				}
				
				/*if(!validateEmpty(contact_number, ContactNoInfo, "contact no")){
					flag = 0;
				}*/
				
				var formData = new FormData($('#frmeditprofile')[0]); 
				var ajaxRunning = false;
				
				if(flag)
		{
			if(!ajaxRunning){
				ajaxRunning = true;
				
				//$("#loader_reg").show();
				//$("#loader_reg").attr('disabled','disabled');
				//$("#loader_reg").html('<img width="20%" src="<?php echo base_url();?>img/gif-load.gif" >');	
				$("#edit-profile-msg").show();
				$("#edit-profile-msg").html('');
				$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					url: "<?php echo base_url(); ?>api/edit_profile",
					data: formData
				}).success(function (json) {
					
					if(json.status=='success'){
					
						$("#edit-profile-msg").html("<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Profile update successfully.</div>");
						$("#edit-profile-msg").fadeOut(6000);
						setTimeout(function(){  window.location="<?php echo base_url();?>profile"; }, 5000);
					}
					
					if(json.status=='error'){
						$("#edit-profile-msg").html("<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>Something went wrong.</div>");
						$("#edit-profile-msg").fadeOut(6000);
					}
				});
			}
		}
			
		});
	});
</script>		


<script type="text/javascript">
	jQuery('#carousel ul li').click(function () {
	jQuery(this).closest('li').removeClass('selected');
	jQuery(this).closest('li').addClass('activeation');
	});
</script>
