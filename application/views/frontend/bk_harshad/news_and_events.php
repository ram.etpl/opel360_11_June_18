<!--all news section -->
<div class="news_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Accessories</div>
				</div>
			</div>
		</div>	
	</div>
	
<!--customer profile section-->	
	<div class="news_wrapper_all">
		<div class="container">
			<div class="col-md-12">
				<div class="news_wrapper">
					<div class="news_head"><img src="images/news_icon.png" />Latest News</div>
					<div id="nt-example1-container">
						<ul id="nt-example1">							
							<li>
								<span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
								<span class="read_news"><a href="javascript:;">Read More...</a></span>
							</li>
							<li>
								<span>Etiam imperdiet volutpat libero eu tristique.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
								<span class="read_news"><a href="javascript:;">Read more...</a></span>
							</li>
							<li>
								<span>Etiam imperdiet volutpat libero eu tristique. Aenean,Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
								<span class="read_news"><a href="javascript:;">Read more...</a></span>
							</li>
							<li>
								<span>Etiam imperdiet volutpat libero eu tristique. Aenean,Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
								<span class="read_news"><a href="javascript:;">Read more...</a></span>
							</li>
							<li>
								<span>Etiam imperdiet volutpat libero eu tristique. Aenean, rutrum felis in..</span>
								<span class="read_news"><a href="javascript:;">Read more...</a></span>
							</li>							
						</ul>
					 </div>
				 </div>			
				<div class="events_wrapper">
					<div class="news_head"><img src="images/event_icon.png" />Events</div>					
					<!--Running section-->
					<div class="events_inner">
						<div class="running_head">Running Event</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--Running section-->
					<!--upcoming section-->
					<div class="events_inner upcom_inner">
						<div class="running_head">Upcoming Event</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
								<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="images/event.jpg" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">Running Test1</div>
										<div class="start_date">Start Date :&nbsp;2015-09-15</div>
										<div class="start_date end_da">End Date :&nbsp;2015-09-15</div>
										<div class="ru_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--upcoming section-->
				</div>
			</div>
		</div>
	</div>
<!--customer profile section-->	

<script src="<?php echo base_url();?>js/jquery.newsTicker.js"></script>
<script type="text/javascript">
var nt_example1 = $('#nt-example1').newsTicker({
	row_height: 95,
	//max_rows: 10,
	duration: 3000,
	prevButton: $('#nt-example1-prev'),
	nextButton: $('#nt-example1-next')
});
</script>
<style>
.mCSB_scrollTools {
    display: block !important;
}
</style>
</div>
