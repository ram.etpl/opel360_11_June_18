
<!--gallery page section -->
<div class="gallery_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url(); ?>images/service_cost_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">			
			<div class="col-lg-12">
				<div class="about_title">
				
					<div class="col-md-8 gal_head">Gallery</div>					
					<div class="col-md-4 dropdwn_gallery">
						<div class="sel_gall"><i class="fa fa-car" aria-hidden="true"></i>Sort By <b>-</b></div>				
						<div class="fleft">
							<select id="cd-dropdown" class="cd-select">
								<option value="-1" selected>Select Sort By</option>
								<option value="">All Car</option>
								<option value="">Latest Car</option>										
							</select>
						</div>	
					</div>
					
				</div>
			</div>			
		</div>	
	</div>
	
	<div class="gallery_wrapper">
		<div class="container">
			<!--<div class="col-md-12 top_title">Top Posts</div>-->
			<div class="col-md-12">				
				<div class="grid">
				
					<div class="first_gal">
						<a href="images/gallery/galllary_display.jpg" class="lightbox">
							<figure class="effect-phoebe">
								<img src="images/gallery/gallery1.jpg" alt="img03"/>
							</figure>
						</a>		
					</div>					
					<div class="first_gal">
						<a href="images/gallery/galllary_display1.jpg" class="lightbox">
							<figure class="effect-phoebe">
								<img src="images/gallery/gallery1.jpg" alt="img03"/>
							</figure>
						</a>		
					</div>					
					<div class="first_gal last_gal">
						<a href="images/gallery/galllary_display1.jpg" class="lightbox">
							<figure class="effect-phoebe">
								<img src="images/gallery/gallery6.jpg" alt="img03"/>
							</figure>
						</a>		
					</div>
					
					<div class="first_gal">
						<a href="images/gallery/galllary_display.jpg" class="lightbox">
							<figure class="effect-phoebe">
								<img src="images/gallery/gallery1.jpg" alt="img03"/>
							</figure>
						</a>		
					</div>					
					<div class="first_gal">
						<a href="images/gallery/galllary_display1.jpg" class="lightbox">
							<figure class="effect-phoebe">
								<img src="images/gallery/gallery1.jpg" alt="img03"/>
							</figure>
						</a>		
					</div>					
					<div class="first_gal last_gal">
						<a href="images/gallery/galllary_display1.jpg" class="lightbox">
							<figure class="effect-phoebe">
								<img src="images/gallery/gallery6.jpg" alt="img03"/>
							</figure>
						</a>		
					</div>
					<!--<div class="first_gal">
					<figure class="effect-phoebe">
						<img src="images/gallery/gallery3.jpg" alt="img03"/>
						<figcaption>
							<p>
								<a href="#"><i class="fa fa-fw fa-user"></i></a>
								<a href="#"><i class="fa fa-fw fa-heart"></i></a>
							</p>
						</figcaption>	
										
					</figure>
					</div>-->
				</div>
			</div>
			
		</div>
	</div>
		
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/simple-lightbox.js"></script>
<script>
	$(function(){
		var $gallery = $('.lightbox').simpleLightbox();
		
		$gallery.on('show.simplelightbox', function(){
			console.log('Requested for showing');
		})
		.on('shown.simplelightbox', function(){
			console.log('Shown');
		})
		.on('close.simplelightbox', function(){
			console.log('Requested for closing');
		})
		.on('closed.simplelightbox', function(){
			console.log('Closed');
		})
		.on('change.simplelightbox', function(){
			console.log('Requested for change');
		})
		.on('next.simplelightbox', function(){
			console.log('Requested for next');
		})
		.on('prev.simplelightbox', function(){
			console.log('Requested for prev');
		})
		.on('nextImageLoaded.simplelightbox', function(){
			console.log('Next image loaded');
		})
		.on('prevImageLoaded.simplelightbox', function(){
			console.log('Prev image loaded');
		})
		.on('changed.simplelightbox', function(){
			console.log('Image changed');
		})
		.on('nextDone.simplelightbox', function(){
			console.log('Image changed to next');
		})
		.on('prevDone.simplelightbox', function(){
			console.log('Image changed to prev');
		})
		.on('error.simplelightbox', function(e){
			console.log('No image found, go to the next/prev');
			console.log(e);
		});
	});
</script>
<!-- gallery page section -->
