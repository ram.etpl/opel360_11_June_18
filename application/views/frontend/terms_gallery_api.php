<!DOCTYPE html >
<html lang="en">
<head> 
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
<title>Terms and Conditions</title>
<meta name="description" content="">
<meta name="keywords" content="">
<!--Favicon Icon-->
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>favicon.ico">
<link rel="icon" type="image/png" href="<?php echo base_url();?>favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo base_url();?>favicon-16x16.png" sizes="16x16" />
<meta name="msapplication-TileColor" content="#000">
<!--Favicon Icon-->
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/media.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>fonts/font.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/demo.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/flexslider.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/lightbox.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/loader.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/animate.css">

<!-- scroll css in model page-->
<link rel="stylesheet" href="<?php echo base_url();?>css/style_scroll.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.mCustomScrollbar.css">
<!-- responsive menu css-->
<link rel="stylesheet" href="<?php echo base_url();?>css/responsee.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/responsive_menu.css">
<!-- end css -->

<body>
<div class="service_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!--<div class="serexc_head">TERMS & CONDITIONS FOR COMMUNITY GALLERY:</div>
					<p>Your uploads should not in any way be violent or pornographic, or promote any religion, and will not harm the dignity or any other rights of any third party. Please do not feature other car brands in your posts. When you upload the images to the page, you hereby grant the Company (and its licensees, advertising agencies and promotion agencies) and the employees, agencies and authorized representatives of each and all of them (collectively, "Authorized Persons"), the unrestricted, perpetual, worldwide, non-transferable, royalty-free right and license to display, exhibit, transmit, reproduce, record, digitize, modify, alter, adapt, create derivative works, exploit and otherwise use and permit others to use the Images (including, all copyrights in the User Content) in connection with the Company's marketing, advertising and promotions. We reserve the right to remove any information / images which we consider to be unlawful, offensive, threatening, libellous, defamatory, pornographic or otherwise objectionable.
					</p>
					<p>-->
					 
				<div class="serexc_head">TERMS & CONDITIONS FOR COMMUNITY GALLERY:</div>
							<p>Your uploads should not in any way be violent or pornographic, or promote any religion, and will not harm the dignity or any other rights of any third party. Please do not feature other car brands in your posts. When you upload the images to the page, you hereby grant the Company (and its licensees, advertising agencies and promotion agencies) and the employees, agencies and authorized representatives of each and all of them (collectively, "Authorized Persons"), the unrestricted, perpetual, worldwide, non-transferable, royalty-free right and license to display, exhibit, transmit, reproduce, record, digitize, modify, alter, adapt, create derivative works, exploit and otherwise use and permit others to use the Images (including, all copyrights in the User Content) in connection with the Company's marketing, advertising and promotions. We reserve the right to remove any information / images which we consider to be unlawful, offensive, threatening, libellous, defamatory, pornographic or otherwise objectionable.
							</p>
							<p>
					 </div>		
			 </div>					
				
			</div>		
		</div>
	</div>  
</div>
</body>
</html>
