<style>
.next_btn a:not(.disabled):before {
  content: '';
  position: absolute;
  right: 10px;
  top: 10px;
  width: 16px;
  height: 16px;
  border: 2px solid;
  border-left-color: transparent;
  border-radius: 50%;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-transition-duration: 0.5s;
  -o-transition-duration: 0.5s;
  -webkit-transition-duration: 0.5s;
  transition-duration: 0.5s;
  -moz-transition-property: opacity;
  -o-transition-property: opacity;
  -webkit-transition-property: opacity;
  transition-property: opacity;
  -moz-animation-duration: 1s;
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -moz-animation-iteration-count: infinite;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -moz-animation-name: rotate;
  -webkit-animation-name: rotate;
  animation-name: rotate;
  -moz-animation-timing-function: linear;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
}
.next_btn a:not(.disabled):after {
  content: '';
  display: inline-block;
  height: 100%;
  width: 0px;
  -moz-transition-delay: 0.5s;
  -o-transition-delay: 0.5s;
  -webkit-transition-delay: 0.5s;
  transition-delay: 0.5s;
  -moz-transition-duration: 0.75s;
  -o-transition-duration: 0.75s;
  -webkit-transition-duration: 0.75s;
  transition-duration: 0.75s;
  -moz-transition-property: width;
  -o-transition-property: width;
  -webkit-transition-property: width;
  transition-property: width;
}
.next_btn a:not(.disabled).sending {
  pointer-events: none;
  cursor: not-allowed;
}
.next_btn a:not(.disabled).sending:before {
  -moz-transition-delay: 0.5s;
  -o-transition-delay: 0.5s;
  -webkit-transition-delay: 0.5s;
  transition-delay: 0.5s;
  -moz-transition-duration: 1s;
  -o-transition-duration: 1s;
  -webkit-transition-duration: 1s;
  transition-duration: 1s;
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
.next_btn a:not(.disabled).sending:after {
  -moz-transition-delay: 0s;
  -o-transition-delay: 0s;
  -webkit-transition-delay: 0s;
  transition-delay: 0s;
  width: 20px;
}

@keyframes rotate {
  0% {
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<!--all rent section -->
<div class="rent_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/profile_banner.jpg" alt=""/>
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Rent An Opel</div>
				</div>
			</div>
		</div>	
	</div>	
	<!--customer profile section-->	
	<div class="service_wrapper serv_booking rent_opel">
		<div class="container">
			<div class="col-md-12 rent_respon">
				<ul class="tabs">
					<li class="tab col" id="tab_step1"><a class="active" href="#stp1">Step 1</a></li>
					<li class="tab col" id="tab_step2"><a href="#stp2">Step 2</a></li>
					<li class="tab col" id="tab_step3"><a href="#stp3">Step 3</a></li>
				</ul>
				<div class="all_tabs_wrapper col-md-12">
					<!--<form action="< ?php echo base_url();?>booking-action" id="frmBookingAction" name="frmBookingAction" method="post">-->
					<div id="stp1" class="step1_wrapper">						
						<div class="col-md-12 step1_error_msg">
							<div id="errmsg"></div>  
						</div>
						
						<!-- <div class="add_rent_wrapp col-md-12">
							<div class="form-group select_drop">
								<label class="col-md-2">Choose Action<span class="aster">*</span></label>
								<div class="col-md-3">
									<select id="book_action" name="book_action" class="select-sm form-control">
										<option value="I">Add</option>
										<option value="U">Update</option>
									</select>
								</div>
							</div>
							<div class="form-group" id="booking_no" style="display: none;">
								<label class="col-md-2 booking_no">Booking Number<span class="aster">*</span></label>
								<div class="col-md-3">
									<input class="form-control" name="txt_booking_no" id="txt_booking_no" type="text">
									<span id="bookingInfo" class="validation"></span>
								</div>
							</div>
							<div class="col-md-12">							
								<div class="next_btn">
									<a id="search_submit" name="search_submit" href="javascript:void(0);">Submit</a>
								</div>							
							</div>
						</div>  -->
						
						<div class="pick_wrapper">
							<div class="rent_sec_wrapper rent_drop">
								<div class="book_rent_wrapp">
									<div class="col-md-6 lt_book">
										<div class="pk_wrapper">						
											<div class="pk_top">
												<div class="form-group date_full">
													<label>
														<i class="fa fa-map-marker" aria-hidden="true"></i>
														Pickup Location
													</label>
													<div class="pi_input">
														<select class="select-sm form-control" name="pickup_location" id="pickup_location">
															<option value="1" selected="selected">7 Ubi Close, Alpine Centre - S408604</option>
														</select>
													</div>
												</div>
											</div>
											<div class="pk_date_wrap">
												<div class="date_input" id="datePicker">
													<input onkeydown="return false" type="text" class="form-control" name="pickup_date" id="pickup_date" placeholder="Pickup Date"/>
													<i class="fa fa-calendar"></i>
													<span class="validation" id="pickupDateInfo"></span>
												</div>
												<div class="time_wrapper">
													<label for="pickup_hour">Pickup Time</label>
													<div class="select_loc_drop">
														<div id="datePicker" class="">	
															<select class="select-sm form-control" name="pickup_time_hr" id="pickup_time_hr">
																<option value="09" selected="selected">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
																<option value="16">16</option>
															</select>
														</div>
													</div>
													<div class="select_loc_drop">
														<div class="" id="datePicker">
															<select id="pickup_time_min" name="pickup_time_min" class="select-sm form-control">
																<option selected="selected" value="00">00</option>
																<option value="15">15</option>
																<option value="30">30</option>
																<option value="45">45</option>
															</select>
														</div>
													</div>
												</div>
											</div>										
										</div>									
									</div>
									<div class="col-md-6 rt_book">
										<div class="pk_wrapper">						
											<div class="pk_top">
												<div class="form-group date_full">
													<label>
														<i class="fa fa-map-marker" aria-hidden="true"></i>Dropoff Location
													</label>
													<div class="pi_input">
														<select class="select-sm form-control" name="dropoff_location" id="dropoff_location">
															<option value="1" selected="selected">7 Ubi Close, Alpine Centre - S408604</option>
														</select>
													</div>
												</div>
											</div>
											<div class="pk_date_wrap">
												<div class="date_input" id="datePicker">
													<input onkeydown="return false" type="text" class="form-control" id="return_date" name="return_date" placeholder="Dropoff Date" />
													<i class="fa fa-calendar"></i>
													<span id="returnDateInfo" class="validation"></span>
												</div>
												<div class="time_wrapper">
													<label for="pickup_hour">Dropoff Time</label>
													<div class="select_loc_drop">
														<div id="datePicker" class="">	
															<select class="select-sm form-control" name="return_time_hr" id="return_time_hr">
																<option value="09" selected="selected">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
																<option value="16">16</option>
															</select>
														</div>
													</div>
													<div class="select_loc_drop">
														<div class="" id="datePicker">
															<select id="return_time_min" name="return_time_min" class="select-sm form-control">
																<option selected="selected" value="00">00</option>
																<option value="15">15</option>
																<option value="30">30</option>
																<option value="45">45</option>
															</select>
														</div>
													</div>
												</div>
											</div>										
										</div>									
									</div>
								</div>								
							</div>

							<div class="rent_sec_wrapper rent_drop">
								<div class="col-md-6 carcat_rent">
									<div class="car_catego">
										<div class="form-group">
											<label><i aria-hidden="true" class="fa fa-car"></i>Car Model</label>
											<?php echo $cars;?>							
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 rent_nt_btn">							
							<div class="next_btn">
								<a id="step_1" name="step_1" href="javascript:void(0);" >Next</a>
								<div id="loading1"></div>
							</div>							
							
						</div>
					</div>

					<div id="stp2" class="rent_step2_wrapper">
						<div class="step2_html">Please complete the step 1</div>
					</div>

					<div id="stp3" class="rent_step3_wrapper">
						<div class="pick_wrapper step3_html">
							<div class="client_info" id="clientInfo">
								<div class="sum_head">Client Information</div>
								<form name="client_info" method="post" id="client_info"> 
									<div class="client_wra">
										<div class="upg_wrapper">
											<div class="col-md-6 lt_re">	
												<div class="form-group">
													<label for="first name"><i class="fa fa-user"></i>First Name<span class="aster">*</span></label>
													<input class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" type="text" value="">
													<span id="firstNameInfo" class="validation"></span>
												</div>
											</div>
											<div class="col-md-6 rt_re">
												<div class="form-group">
													<label for="last name"><i class="fa fa-user"></i>Last Name<span class="aster">*</span></label>
													<input class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" type="text" value="">
													<span id="lastNameInfo" class="validation"></span>
												</div>
											</div>
										</div>
										<div class="upg_wrapper">
											<div class="col-md-6 lt_re">	
												<div class="form-group">
													<label for="first name"><i class="fa fa-user"></i>Customer Type</label>
													<select class="form-control" name="customer_type" id="customer_type"  type="text">
														<option value="">Select Customer Type</option>
														<option value="P"> Passenger</option>
														<option value="C"> Commercial</option>
													</select>
												</div>
											</div>
											<div class="col-md-6 rt_re">
												<div class="form-group">
													<label for="last name"><i class="fa fa-user"></i>Gender<span class="aster">*</span></label>
													<div class="form-control radio_sect">
														<label class="radio-inline"><input type="radio" name="user_gender" id="user_gender">Male</label>
														<label class="radio-inline"><input type="radio" name="user_gender" id="user_gender">Female</label>
													</div>
													<span id="userGenderInfo" class="validation"></span>
												</div>
											</div>
										</div>
										<div class="upg_wrapper">
											<div class="col-md-6 lt_re">	
												<div class="form-group">
													<label for="address"><i class="fa fa-location-arrow"></i>Address<span class="aster">*</span></label>
													<input class="form-control" name="user_address" id="user_address" placeholder="Enter Address" type="text" value="">
													<span id="userAddressInfo" class="validation"></span>
												</div>
											</div>
											<div class="col-md-6 rt_re plus_rent">
												<div class="form-group">
													<label for="mobno"><i class="fa fa-phone"></i>Contact No.</label>
													<span class="plus_text">+65</span>
													<input type="text" class="form-control"  maxlength="14" name="user_phoneNumber" id="user_phoneNumber" placeholder="Enter Contact No.">
													<span id="userphoneNumberInfo" class="validation"></span>
												</div>
											</div>
										</div>
										<div class="upg_wrapper">
											<div class="col-md-6 lt_re">	
												<div class="form-group">
													<label for="first name"><i class="fa fa-globe"></i>City<span class="aster">*</span></label>
													<input class="form-control" name="user_city" id="user_city" placeholder="Enter City" type="text" value="">
													<span id="userCityInfo" class="validation"></span>
												</div>
											</div>
											<div class="col-md-6 rt_re">
												<div class="form-group">
													<label for="last name"><i class="fa fa-globe"></i>State<span class="aster">*</span></label>
													<input class="form-control" name="user_state" id="user_state" placeholder="Enter State" type="text" value="">
													<span id="userStateInfo" class="validation"></span>
												</div>
											</div>
										</div>
										<div class="upg_wrapper">
											<div class="col-md-6 lt_re">	
												<div class="form-group">
													<label for="first name"><i class="fa fa-file-archive-o"></i>Zip Code<span class="aster">*</span></label>
													<input class="form-control" name="user_zip" id="user_zip" placeholder="Enter Zip Code" type="text" value="">
													<span id="userZipInfo" class="validation"></span>
												</div>
											</div>
											<div class="col-md-6 rt_re">
												<div class="form-group">
													<label for="last name"><i class="fa fa-globe"></i>Country<span class="aster">*</span></label>
													<select class="form-control" name="user_country" id="user_country" placeholder="Enter Country" type="text">
														<option>Singapore</option>
													</select>
													<span id="userCountryInfo" class="validation"></span>
												</div>
											</div>
										</div>
										<div class="upg_wrapper">
											<div class="col-md-6 lt_re">	
												<div class="form-group">
													<label for="first name"><i class="fa fa-envelope"></i>Email Address	<span class="aster">*</span></label>
													<input class="form-control" name="user_email" id="user_email" placeholder="Enter Email Address" type="text" value="">
													<span id="userEmailInfo" class="validation"></span>
												</div>
											</div>
											<div class="col-md-6 rt_re">
												<div class="form-group">
													<label for="last name"><i class="fa fa-calendar"></i>Date of Birth<span class="aster">*</span></label>
													<input class="form-control" name="user_dob" id="user_dob" placeholder="Enter Date of Birth" type="text" value="">
													<span id="userDobInfo" class="validation"></span>
												</div>
											</div>
										</div>
										
										<!--div class="upg_wrapper">
											<div class="col-md-6 lt_re">	
												<div class="form-group">
													<label for="first name"><i class="fa fa-address-card" aria-hidden="true"></i></i>Nric / Fin / Passport<span class="aster">*</span></label>
													<input class="form-control" name="user_passport" id="user_passport" placeholder="Enter Nric / Fin / Passport" type="text" value="">
													<span id="userPassportInfo" class="validation"></span>
												</div>
											</div>
											<div class="col-md-6 rt_re">
												<div class="form-group">
													<label for="last name"><i class="fa fa-calendar"></i>Motorcar License Pass Date<span class="aster">*</span></label>
													<input class="form-control" name="user_license" id="user_license" placeholder="Enter Motorcar License Pass Date" type="text" value="">
													<span id="userLicenseInfo" class="validation"></span>
												</div>
											</div>
										</div>-->
										
										<div class="upg_wrapper">
											<div class="col-md-12 lt_re rt_re">
												<div class="form-group">
													<label for="Notes"><i class="fa fa-comment"></i>Notes<span class="aster">*</span></label>
													<textarea class="form-control" name="user_notes" id="user_notes" placeholder="Enter Notes"></textarea>
													<span id="userNotesInfo" class="validation"></span>
												</div>
											</div>
										</div>								
										<div class="book_btn_wrap">
											<div class="send_btn">
												<a href="javascript:void(0)" onclick="saveCustomerInfo()"><span><i class="fa fa-car" aria-hidden="true"></i></span>Book Now</a>
												<a href=""><span><i class="fa fa-trash-o" aria-hidden="true"></i></span>Cancel</a>
											</div>
										</div>								
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

</div>
</div>	
<!--customer profile section-->	
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/moment.js"></script> 	
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script> 	
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css"> 
<link rel="stylesheet" href="<?php echo base_url();?>css/materialize.min.css">
<script src="<?php echo base_url();?>js/materialize.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.numeric.js"></script>

<script type="text/javascript">
	$(function () {
		$('#pickup_date,#return_date,#user_dob').datetimepicker( {pickTime: false,minDate: new Date(),format: 'YYYY-MM-DD',daysOfWeekDisabled: [0]}); 	

		/*$('#book_action').on('click',function(){
			var book_action = $('#book_action').val();
			if(book_action == 'U'){
				$('#booking_no').css({'display':'block'});
			}else{
				$('#booking_no').css({'display':'none'});
			}
		});*/

		
	});

	$(".numeric").numeric({decimal:false,negative:false});
	jQuery("#user_phoneNumber").keypress(function (e) { 
		if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
				   return false;
		}
		else {
			return true;
		}
	});	

	$("#tab_step2").addClass("disabled","disabled");
	$("#tab_step3").addClass("disabled","disabled");
	$("#tab_step4").addClass("disabled","disabled");
	$("#ref_no").hide();
	
	$('#search_submit').on('click',function(){
		var booking_no = $('#txt_booking_no').val();
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>rent-get-booking",
			data: {"booking_no":booking_no},
		}).success(function (json) {
			if(json.status == 'failed'){
				$("#errmsg").show();
				$("#loading1").html("");
				$("#errmsg").html(json.message);
				$("#bexistsinfo").html(json.booking_info);
			}
			else if(json.booking_action == "new_step2" && json.message == ""){
				$(".step2_html").html(json.html_next); 
				$("#errmsg").html("");  
				setTimeout(function(){
					$("#tab_step2 a").click();
				}, 200);
				
				$("#tab_step1").addClass("disabled","disabled");
				$("#tab_step2").removeClass("disabled","disabled");
				$("#tab_step3").addClass("disabled","disabled");
				$("#tab_step4").addClass("disabled","disabled");
			}
		});
	});

	$('#step_1').on('click', function () { 
		var pickup_location = $("#pickup_location option:selected").text(); 
		var pickup_date  = $("#pickup_date").val();
		var pickup_time_hr = $("#pickup_time_hr").val();
		var pickup_time_min = $("#pickup_time_min").val();
		var dropoff_location = $("#dropoff_location option:selected").text(); 
		var return_date  = $("#return_date").val();
		var return_time_hr = $("#return_time_hr").val();
		var return_time_min = $("#return_time_min").val();
		var sel_model = $("#sel_model").val();
		//alert(veh_no);
		$("#loading1").show();
		
		var flag = 1;
	 	
		if(pickup_date == ""){
			$("#pickupDateInfo").html("Please select a pickup date.");
			flag=0;
		}
		if(return_date == ""){
			$("#returnDateInfo").html("Please select a return date.");
			flag=0;
		}
		 
		if(flag){
		$("#pickupDateInfo").html("");	  
		$("#returnDateInfo").html("");	  	
		$(this).addClass("sending");
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>rent-confirm-booking",
				data: {"pickup_location":pickup_location,"pickup_date":pickup_date,"pickup_time_hr":pickup_time_hr,"pickup_time_min":pickup_time_min,"dropoff_location":dropoff_location,"return_date":return_date,"return_time_hr":return_time_hr,"return_time_min":return_time_min,"sel_model":sel_model},
			}).success(function (json) {
				$("#step_1").removeClass("sending");
				if(json.status == 'failed'){
					$("#errmsg").show();
					$("#loading1").html("");
					$("#errmsg").html(json.message);
					$("#bexistsinfo").html(json.booking_info);
				}
				else if(json.booking_action == "new_step2" && json.message == ""){
					$(".step2_html").html(json.html_next); 
					$("#errmsg").html("");  
					setTimeout(function(){
						$("#tab_step2 a").click();
					}, 200);
					
					//$('ul.tabs').tabs('tab', 'tab_step2');
					$("#tab_step1").addClass("disabled","disabled");
					$("#tab_step2").removeClass("disabled","disabled");
					$("#tab_step3").addClass("disabled","disabled");
					$("#tab_step4").addClass("disabled","disabled");
				}
				else if(json.message != "" && json.booking_action != "new_step2"){
					$("#errmsg").show();
					$("#errmsg").html(json.message);
					$("#loading1").html(""); 
					
				}
				   
			});
		}
	});

	function displayCheckoutPage(){
		$("#loading2").show();
		$('#continue').addClass("sending");
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>frontcontroller/rent_car_checkout",
		}).success(function (json) {
			$("#continue").removeClass("sending");
			if(json.status == 'failed'){
				$("#errmsg").show();
				$("#loading1").html("");
				$("#errmsg").html(json.message);
				$("#bexistsinfo").html(json.booking_info);
			}
			else if(json.booking_action == "new_step3" && json.message == ""){
				$(json.html_next).insertBefore("#clientInfo");
				$("#errmsg").html("");  
				setTimeout(function(){
					$("#tab_step3 a").click();
				}, 200);
				
				//$('ul.tabs').tabs('tab', 'tab_step2');
				$("#tab_step1").addClass("disabled","disabled");
				$("#tab_step2").addClass("disabled","disabled");
				$("#tab_step3").removeClass("disabled","disabled");
			}
			else if(json.message != "" && json.booking_action != "new_step2"){
				$("#errmsg").show();
				$("#errmsg").html(json.message);
				$("#loading1").html(""); 
				
			}
			   
		});
	}

	function saveCustomerInfo()
	{
		var first_name = $("#first_name").val(); 
		var last_name  = $("#last_name").val();
		var user_address = $("#user_address").val();
		var user_phoneNumber = $("#user_phoneNumber").val();
		var user_city = $("#user_city").val(); 
		var user_state  = $("#user_state").val();
		var user_zip = $("#user_zip").val();
		var user_country = $("#user_country").val();
		var user_email = $("#user_email").val();
		var user_dob = $("#user_dob").val();
		var user_notes = $("#user_notes").val();
		
		var flag = 1;
	 	
		if(first_name == ""){
			$("#firstNameInfo").html("Please Enter First Name.");
			flag=0;
		}

		if(last_name == ""){
			$("#lastNameInfo").html("Please Enter Last Name.");
			flag=0;
		}

		if(user_address == ""){
			$("#userAddressInfo").html("Please Enter Address.");
			flag=0;
		}

		if(user_phoneNumber == ""){
			$("#userphoneNumberInfo").html("Please Enter Contact Number.");
			flag=0;
		}

		if(user_city == ""){
			$("#userCityInfo").html("Please Enter City.");
			flag=0;
		}

		if(user_state == ""){
			$("#userStateInfo").html("Please Enter State.");
			flag=0;
		}

		if(user_zip == ""){
			$("#userZipInfo").html("Please Enter Zip.");
			flag=0;
		}

		if(user_country == ""){
			$("#userCountryInfo").html("Please Select Country.");
			flag=0;
		}

		if(!validateEmpty($('#user_email'), $('#userEmailInfo'), "email address.")) {
			flag = 0;
			//$("#userEmailInfo").html("Please Enter Email.");
		}

		if(user_email !=""){
			if(!validateEmail($('#user_email'), $('#userEmailInfo'))){
				flag = 0;
				//$("#userEmailInfo").html("Please Enter Valid Email.");
			}
		}	

		if(user_dob == ""){
			$("#userDobInfo").html("Please Select Date of Birth.");
			flag=0;
		}

		if(user_notes == ""){
			$("#userNotesInfo").html("Please Enter Note.");
			flag=0;
		}

		if(user_gender == ""){
			$("#userGenderInfo").html("Please Select Gender.");
			flag=0;
		}

		if(flag){

			$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>frontcontroller/rent_car_booking",
				data:$('#client_info').serialize(),
			}).success(function (json) {
				if(json.status == 'danger'){
					$("#errmsg").addClass('alert alert-danger');
				}
				else{
					$("#errmsg").addClass('alert alert-success');
				}

				$("#errmsg").html(json.message);  
				setTimeout(function(){
					$("#tab_step1 a").click();
				}, 200);

				$("#tab_step1").removeClass("disabled","disabled");	
				$("#tab_step2").addClass("disabled","disabled");	
				$("#tab_step3").addClass("disabled","disabled");

				$('#stp1').css({'display':'block'});
				$('#stp2').css({'display':'none'});
				$('#stp3').css({'display':'none'});
			});

		}
	}

	function stepPrevious(id){
		if(id == '2'){
			setTimeout(function(){
				$("#tab_step1 a").click();
			}, 200);

			$("#tab_step1").removeClass("disabled","disabled");	
			$("#tab_step2").addClass("disabled","disabled");	
			$("#tab_step3").addClass("disabled","disabled");

			$('#stp1').css({'display':'block'});
			$('#stp2').css({'display':'none'});
			$('#stp3').css({'display':'none'});
		}else if(id == '3'){
			setTimeout(function(){
				$("#tab_step2 a").click();
			}, 200);

			//$('ul.tabs').tabs('tab', 'tab_step2');
			$("#tab_step3").addClass("disabled","disabled");
			$("#tab_step2").removeClass("disabled","disabled");
			$("#tab_step1").addClass("disabled","disabled");	

			$('#stp1').css({'display':'none'});
			$('#stp2').css({'display':'block'});
			$('#stp3').css({'display':'none'});
		}
	}

</script>
