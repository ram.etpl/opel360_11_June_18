<!--models page section -->
<div class="models_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/models_banner.jpg" alt=""/>
	</div>
	<!--inner banner section-->
		<div class="model_car_section">			
			<div class="container">
				<div class="row">
				<div class="car_center_sec">					
					
					<div class="col-md-12 car_heading"><?php echo $car_catalog['m_name'];?> <span class="car_year"><?php echo $car_catalog['weight'];?></span>
						<div class="drpmodels"> 
							<div onclick="myFunction()" class="dropbtn "><?php echo ($selected_car?$selected_car:"Select Model");?></div>							
							<ul id="myDropdown" class="dropdown-content ">								
							<?php if($car_gallery){
								foreach($car_gallery as $gal):
								?>
								<li><a href="<?php echo base_url();?>models/<?php echo $gal['id'];?>"><?php echo $gal['model_name'];?></a></li>
								<?php endforeach;?>
								 
								<?php } ?>
							</ul>							
						</div>
					</div>				
					<?php //if($car_gallery['img_file']!=""){?>
					<!--<div class="col-md-12 car_model_no"><img src="<?php //echo base_url();?>car_gallery/<?php //echo $car_gallery['img_file'];?>"/></div>-->
					<?php //} ?>				
					
				<!--slider section-->	
				<div class="col-md-12">
					
					<?php if(!empty($cgallery)){?>
					<div class="slider_section">
						<div id="main" role="main">
							<section class="slider">
								<h6>&nbsp;</h6>
								<div id="slider" class="flexslider">
									<ul class="slides">
										<?php foreach($cgallery as $gallery):
										
										?>
											<li><img src="<?php echo base_url();?>car_gallery/<?php echo $gallery['img_file'];?>" alt=""/></li>
										<?php endforeach; ?>
									</ul>
								</div>	
								<div id="carousel" class="flexslider">
									<ul class="slides">
										<?php foreach($cgallery as $gallery):
										
										?>
										<li class="selected"><img src="<?php echo base_url();?>car_gallery/<?php echo $gallery['img_file'];?>" alt=""/></li>
										<?php endforeach; ?>
									</ul>
								</div>				
							</section>
						</div>			
					</div>
					<?php } ?>
				</div>
				<!--slider section-->
					
					
					<div class="col-md-12 specif_name">Specifications</div>
					<!-- specification scroll table -->
					<div class="col-md-12">
						<div class="ide_wrapper">
							
							<div id="demo" class="showcase scrollTo-demo">
								<section id="examples">
									<h6>&nbsp;</h6>
									<div class="content demo-x">										
										<div class="ident_all_wrapper">
											<!--First Column-->
											<div class="ident_inner_wrapper">
												<div class="inner_box">
													<div class="inner_wrap">
														<div class="make_head">Model Name</div>
														<div class="make_name"><?php echo ($car_catalog['model_name']?$car_catalog['model_name']:"-"); ?></div>
													</div>
																				
													<div class="inner_wrap">
														<div class="make_head">Propellant</div>
														<div class="make_name"><?php echo ($car_catalog['propellant']?$car_catalog['propellant']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Engine Capacity(cc)</div>
														<div class="make_name"><?php echo ($car_catalog['engine_capacity']?$car_catalog['engine_capacity']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Valve Train</div>
														<div class="make_name"><?php echo ($car_catalog['valve_train']?$car_catalog['valve_train']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Max Output</div>
														<div class="make_name"><?php echo ($car_catalog['max_output']?$car_catalog['max_output']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Acceleration</div>
														<div class="make_name"><?php echo ($car_catalog['acceleration']?$car_catalog['acceleration']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Fuel Consumption - Combined</div>
														<div class="make_name"><?php echo ($car_catalog['fuel_consumption']?$car_catalog['fuel_consumption']:"-"); ?></div>
													</div>
													
														
																									
												</div>
																							
											</div>
											<!--First Column-->
											
											<!--Second Column-->
											<div class="ident_inner_wrapper">
												<div class="inner_box">
												 
												 	<div class="inner_wrap">
														<div class="make_head">Weight - Without Driver KG</div>
														<div class="make_name"><?php echo ($car_catalog['weight']?$car_catalog['weight']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">No Valves Per Cylinder</div>
														<div class="make_name"><?php echo ($car_catalog['no_valves_per_cylinder']?$car_catalog['no_valves_per_cylinder']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Number of Cylinder</div>
														<div class="make_name"><?php echo ($car_catalog['number_of_cylinder']?$car_catalog['number_of_cylinder']:"-"); ?></div>
													</div>
													
													<div class="inner_wrap">
														<div class="make_head">Cylinder Configuration</div>
														<div class="make_name"><?php echo ($car_catalog['cylinder_config']?$car_catalog['cylinder_config']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Cooling</div>
														<div class="make_name"><?php echo ($car_catalog['cooling']?$car_catalog['cooling']:"-"); ?></div>
													</div>
																									
													<div class="inner_wrap">
														<div class="make_head">Top Speed</div>
														<div class="make_name"><?php echo ($car_catalog['top_speed']?$car_catalog['top_speed']:"-"); ?></div>
													</div>
													
													
													<div class="inner_wrap">
														<div class="make_head">CO2 - Combined</div>
														<div class="make_name"><?php echo ($car_catalog['co2_combined']?$car_catalog['co2_combined']:"-"); ?></div>
													</div>
												</div>
											</div>
											<!--Second Column-->
										</div>
									</div>
								</section>
							</div>
							
						</div>	
					</div>
					<!-- specification scroll table -->
				</div>
				</div>
			</div>
		</div>
	<!--inner banner section-->
</div>
<!-- models page section -->

<script>
$(".dropbtn").addClass("close_drp");
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
    
}
window.onclick = function(event) {
 
  /*if (!event.target.matches('.dropbtn')) {
	  
	  
 $(".dropdown-content").addClass('open_drp');
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
        
      }
      
    }
  }*/
  
  var classes = event.target.className.split(' ');
  var found = false; var i = 0;
  while (i < classes.length && !found) {
      if (classes[i]=='dropbtn') found = true;
      else ++i;
  }
  if (!found) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
  if (event.target.matches('.dropbtn') && $(".dropbtn").hasClass('close_drp')) {
	 $(".dropbtn").removeClass("close_drp");
	 $(".dropbtn").addClass("open_drp");
  }
  else{
	  $(".dropbtn").addClass("close_drp");
	  $(".dropbtn").removeClass("open_drp");
  }
}
</script>
<script src="<?php echo base_url();?>js/jquery.ddslick.min.js"></script>
<script type="text/javascript">
	jQuery('#carousel ul li').click(function () {
	jQuery(this).closest('li').removeClass('selected');
	jQuery(this).closest('li').addClass('activeation');
	});
</script>
