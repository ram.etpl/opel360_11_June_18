
<!--about page section -->
<div class="about_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url(); ?>images/models_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">			
			<div class="col-lg-12">
				<div class="about_title">Activate Account</div>
			</div>			
		</div>	
	</div>
	
	<div class="about_wrapper">
		<div class="container">
			<div class="col-md-12 about_details">
				
				<input type="hidden" name="key" value="<?php echo $_GET['key']; ?>" />
            <input type="hidden" name="email" value="<?php echo $_GET['email']; ?>" />
				<?php  if(isset($message) && $message!=""){?>
			<div class="alert alert-success"><?php echo $message;?></div>
			<?php } ?>
			<?php  if(isset($validation_message) && $validation_message!=""){?>
			<div class="alert alert-danger"><?php echo $validation_message;?></div>
			<?php } ?>
				
				</div>
		</div>
	</div>
		
</div>
<!-- about page section -->
