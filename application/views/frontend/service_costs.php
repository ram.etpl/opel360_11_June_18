<!--about page section -->
<div class="service_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url(); ?>images/service_cost_banner.jpg" alt=""/>
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="about_title">Service Booking Cost</div>
				</div>
			</div>	
		</div>	
	</div>	
	<div class="service_wrapper">
		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">				
				<table class="table table-bordered" id="services_tb">
					<thead>
						<tr>							
							<th width="50%">DESCRIPTION</th>
							<th width="50%" class="text-center">PRICE(SGD)</th>							
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($services_cost)){						
						 $model_id = "";
						 $i=1;						  
						 foreach($services_cost as $cost):	
						?>
						<tr>
							<td><strong><?php echo $cost["model_name"]; ?></strong> - <?php echo $cost["service_name"]; ?></td>
							<td class="text-center"><?php echo "S$".$cost["service_cost"]; ?></td> 
						</tr>
						<?php $i++; 
							endforeach;
						}
						else{
						?>
						<tr>
							<td colspan="2">No Records Available</td>
						</tr>
						<?php	
						} ?>
					</tbody>
				</table>
				</div>
			</div>
		  </div>
		</div>
	</div>		
</div>
<!-- about page section -->
