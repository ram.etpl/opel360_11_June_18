<!--all news section -->
<div class="news_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/profile_banner.jpg" alt=""/>
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Carowner Events</div>
				</div>
			</div>
		</div>	
	</div>
	
<!--customer profile section-->	
	<div class="news_wrapper_all ownner_wrapp">
		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<div class="events_wrapper">
					<div class="news_head"><img src="images/event_icon.png" alt=""/>Events</div>					
					<!--Running section-->
					<?php if($Running_events!=""){?>
					<div class="events_inner">
						<div class="running_head">Running Events</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">								
								<?php
									echo $Running_events;
								?> 
							</div>
						</div>
					</div>
					<?php } ?>
					<!--Running section-->
					<!--upcoming section-->
					<?php if($upcoming_events!=""){?>
					<div class="events_inner upcom_inner">
						<div class="running_head">Upcoming Events</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">								
								<?php
									echo $upcoming_events;
								?>		
							</div>
						</div>
					</div>
					<?php } ?>
					<!--upcoming section-->
				</div>

				<div class="events_wrapper">
					<div class="news_head"><img src="images/event_icon.png" alt=""/>Promotion</div>					
					<!--Running section-->
					<?php if($Running_promotion!=""){?>
					<div class="events_inner">
						<div class="running_head">Running Promotion</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">
								
								<?php
									echo $Running_promotion;
								?> 
							</div>
						</div>
					</div>
					<?php } ?>
					<!--Running section-->
					<!--upcoming section-->
					<?php if($upcoming_promotion!=""){?>
					<div class="events_inner upcom_inner">
						<div class="running_head">Upcoming Promotion</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">								
								<?php
									echo $upcoming_promotion;
								?>		
							</div>
						</div>
					</div>
					<?php } ?>
					<!--upcoming section-->
				</div>
			</div>
			</div>
		</div>
	</div>
<!--customer profile section-->	

<script src="<?php echo base_url();?>js/jquery.newsTicker.js"></script>
<script type="text/javascript">
var nt_example1 = $('#nt-example1').newsTicker({
	row_height: 95,
	//max_rows: 10,	
	duration: 3000,
	prevButton: $('#nt-example1-prev'),
	nextButton: $('#nt-example1-next')
});

$( document ).ready(function() {
	 
	$(".events_inner .scrollTo-demo .demo-y").each(function( index ) {
		var height = $(this).height();
		if(height < 475){
			$(this).removeClass("demo-y");
		}
	});
	$('.wlink').hide();
});



function storeRsvp(event_id,id)
{
	$.ajax({
		type: "POST",
		dataType: "json",
        cache: false,
		url: "<?php echo base_url(); ?>frontcontroller/save_rsvp",
		data: {'event_id':event_id}
	}).success(function (json) {
		if(json.id == id)
		{	
			$('#test1'+id).show();
			$('#test'+id).hide();
		}
	});
}
</script>
</div>