
<!--models page section -->
<div class="models_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/models_banner.jpg" />
	</div>
	<!--inner banner section-->
		<div class="model_car_section">
			<div class="container">
				<div class="car_center_sec">
					<div class="col-md-12 car_heading"><?php echo $car_catalog['m_name'];?> <span class="car_year"><?php echo $car_catalog['weight'];?></span></div>
					<?php if($car_gallery['img_file']!=""){?>
					<div class="col-md-12 car_model_no"><img src="<?php echo base_url();?>car_gallery/<?php echo $car_gallery['img_file'];?>"/></div>
					<?php } ?>
					<div class="col-md-12 specif_name">Specifications</div>
					<!-- specification scroll table -->
					<div class="col-md-12">
						<div class="ide_wrapper">
							
							<div id="demo" class="showcase scrollTo-demo">
								<section id="examples">
									<div class="content demo-x">										
										<div class="ident_all_wrapper">
											<!--First Column-->
											<div class="ident_inner_wrapper">
												<div class="inner_box">
													<div class="inner_wrap">
														<div class="make_head">Model Name</div>
														<div class="make_name"><?php echo ($car_catalog['model_name']?$car_catalog['model_name']:"-"); ?></div>
													</div>
																				
													<div class="inner_wrap">
														<div class="make_head">Propellant</div>
														<div class="make_name"><?php echo ($car_catalog['propellant']?$car_catalog['propellant']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">No Valves Per Cylinder</div>
														<div class="make_name"><?php echo ($car_catalog['no_valves_per_cylinder']?$car_catalog['no_valves_per_cylinder']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Engine Capacity</div>
														<div class="make_name"><?php echo ($car_catalog['engine_capacity']?$car_catalog['engine_capacity']:"-"); ?></div>
													</div>	
													<div class="inner_wrap">
														<div class="make_head">Number of Cylinder</div>
														<div class="make_name"><?php echo ($car_catalog['number_of_cylinder']?$car_catalog['number_of_cylinder']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Valve Train</div>
														<div class="make_name"><?php echo ($car_catalog['valve_train']?$car_catalog['valve_train']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Cylinder Configuration</div>
														<div class="make_name"><?php echo ($car_catalog['cylinder_config']?$car_catalog['cylinder_config']:"-"); ?></div>
													</div>												
												</div>
																							
											</div>
											<!--First Column-->
											
											<!--Second Column-->
											<div class="ident_inner_wrapper">
												<div class="inner_box">
												 
													<div class="inner_wrap">
														<div class="make_head">Max Output</div>
														<div class="make_name"><?php echo ($car_catalog['max_output']?$car_catalog['max_output']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Cooling</div>
														<div class="make_name"><?php echo ($car_catalog['cooling']?$car_catalog['cooling']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Aceleration</div>
														<div class="make_name"><?php echo ($car_catalog['acceleration']?$car_catalog['acceleration']:"-"); ?></div>
													</div>												
													<div class="inner_wrap">
														<div class="make_head">Top Speed</div>
														<div class="make_name"><?php echo ($car_catalog['top_speed']?$car_catalog['top_speed']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Weight - Without Driver KG</div>
														<div class="make_name"><?php echo ($car_catalog['weight']?$car_catalog['weight']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">Fuel Consumption - Combined</div>
														<div class="make_name"><?php echo ($car_catalog['fuel_consumption']?$car_catalog['fuel_consumption']:"-"); ?></div>
													</div>
													<div class="inner_wrap">
														<div class="make_head">CO2 - Combined</div>
														<div class="make_name"><?php echo ($car_catalog['co2_combined']?$car_catalog['co2_combined']:"-"); ?></div>
													</div>
												</div>
											</div>
											<!--Second Column-->
										</div>
									</div>
								</section>
							</div>
							
						</div>	
					</div>
					<!-- specification scroll table -->
				</div>
			</div>
		</div>
	<!--inner banner section-->
</div>
<!-- models page section -->
