
<!--about page section -->
<div class="pdf_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url(); ?>images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="about_title">Pdf Manuals</div>
				</div>
			</div>	
		</div>	
	</div>
	<?php $this->load->view("frontend/incls/dashboard_menu");?>
	<div class="profile_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 cus_name pdf_cust">
					<img src="<?php echo base_Url();?>images/pdf_manuals_ico.png"/>Pdf Manuals				
				</div>
			
				<div class="col-md-4 dropdwn_gallery pdf_drop">
					<?php if($models){?>
					<div class="sel_gall"><i class="fa fa-car" aria-hidden="true"></i>Select Model <b>-</b></div>				
					<div class="fleft">
						<select id="cd-dropdown" class="cd-select sel-gmodel">
							<?php $i=1;
								foreach($models as $model):
								$selected="";
								if($i==1){
									$selected = 'selected';
								}
								?>
								<option <?php echo $selected;?> value="<?php echo $model['id']; ?>"><?php echo $model['m_name']."-".$model['propellant']; ?></option>
							<?php $i++;
							endforeach; ?>
						</select>
					</div>	
					<?php } ?>
				</div> 
				<div class="col-md-12" id="pdf-listing">
				
			</div>	
			</div>
		</div>
	</div>
		
</div>
<script>
	
	
	$( window ).load(function() {
		//var active_val = $('.cd-dropdown .cd-active ul li').trigger('click');
		
		setTimeout(function() {
			$('.cd-dropdown ul li').trigger('click');
		},10);
		
		$('.cd-dropdown ul li').on('click', function () { 
			
				var m_id = $(this).attr('data-value'); 
				$("#loading").show();
				$(".loading-data").html('<b>Please wait while loading data</b>');
				$.ajax({
						type: "POST",
						dataType: "json",
						url: "<?php echo base_url(); ?>get-Pdf",
						data: {"m_id":m_id},
					}).success(function (json) {
						$("#pdf-listing").html(json.html);
						
						$("#loading").hide();
						$(".loading-data").html('');
					});
		});
	});
</script>
<!-- about page section -->
