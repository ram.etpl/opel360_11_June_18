<!--upgrade page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_contact.jpg" alt=""/>
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="contact_title">Feedback</div>
				</div>
			</div>	
		</div>	
	</div>	
	<div class="contact_wrapper">
		<div class="container">	
			<div class="row">
			<div class="contact_inner">			
				<div class="col-md-12">
					<div class="cont_box_wra upgrade_box">
						<form id="frmticket" name="frmticket" method="post">
							
							<div class="col-md-6 lt_re">
								<div class="form-group">
									<label for="name"><i class="fa fa-user"></i>Name<span class="aster">*</span></label>
									<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
									<span id="infoName" class="validation"></span>
								</div>
							</div>							 
							<div class="col-md-6 rt_re">
								<div class="form-group">
									<label for="email"><i class="fa fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
									<input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address">
									<span id="infoEmail" class="validation"></span>
								</div>
							</div>
							<div class="col-md-6 lt_re">
								<div class="form-group">
									<label for="sel_department"><i class="fa fa-briefcase" aria-hidden="true"></i>Department</label>
									<select class="form-control" name="sel_department" id="sel_department">
										<option value="Sales">Sales</option>
										<option value="Aftersales">Aftersales</option>
										<option value="Parts and accessories">Parts and Accessories</option>
										<option value="Leasing">Leasing</option>
										<option value="Marketing">Marketing</option>
										<option value="Others">Others</option>
									</select>
									<span id="selDepartmentAction" class="validation"></span>
								</div>
							</div>							
							<div class="col-md-6 rt_re">
								<div class="form-group">
									<label for="text_msg"><i class="fa fa-comment" aria-hidden="true"></i>Comments</label>
									<textarea class="form-control" name="text_msg" id="text_msg" placeholder="Enter Comments"></textarea>
									<span id="infoMsg" class="validation"></span>
								</div>
							</div> 				
							
							<div class="send_btn">
								<a href="javascript:void(0);" id="ticket_btn"><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Submit</a>
								<!--<a href="javascript:history.go(-1);"><span><i class="fa fa-trash-o" aria-hidden="true"></i></span>Cancel</a>-->
							</div>
							<div id="message_result"></div>
						</form>
					</div>
				</div>				 			
			</div>
			</div>
		</div>
	</div>
		
</div>
<!-- upgrade page section -->

<script>
$('#ticket_btn').on('click', function () {
	
	var sel_department = $("#sel_department");
	var selDepartmentAction = $("#selDepartmentAction");
	
	var name = $("#name");
	var infoName = $("#infoName");
	
	var email = $("#email");
	var infoEmail = $("#infoEmail");
	
	var text_msg = $("#text_msg");
	var infoMsg = $("#infoMsg");
	
	 
	var flag = 1;
	
	if(!validateEmpty(name, infoName, "name")) {
				flag = 0;
	}
	
	if(!validateEmpty(email, infoEmail, "email address")) {
				flag = 0;
	}
	if(email.val()!=""){
				if(!validateEmail(email, infoEmail)){
						flag = 0;
				}
		}
	
	if(!checkCombo(sel_department, selDepartmentAction, "department")){
			flag = 0;
	}
	
	if(!validateEmpty(text_msg, infoMsg, "comments")) {
				flag = 0;
	}
	var formData = new FormData($('#frmticket')[0]); 
	if(flag){
		 $("#ticket_btn").addClass("sending");
		$.ajax({
				type: "POST",
				dataType: "json",		
				//mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>feedback-post",
				data: formData 
				
		}).success(function (json) {
			
			$("#ticket_btn").removeClass("sending");
			if(json.status=='error'){
				$("#message_result").html("<div class='alert alert-danger'>"+json.msg+"</div>");
			}
			else{
				$("#message_result").html(json.msg);
				$("#name").val('');
				$("#email").val('');
				$("#sel_department").val('');
				$("#text_msg").val('');
			}
		});
	}
});
</script>
