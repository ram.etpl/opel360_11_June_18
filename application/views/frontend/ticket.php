
<!--contact page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/models_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">			
			<div class="col-lg-12">
				<div class="contact_title">Raise a Ticket</div>
			</div>			
		</div>	
	</div>
	
	<div class="contact_wrapper">
		<div class="container">
			 
			<div class="contact_inner">
			
				<div class="col-md-5">
					<div class="cont_box_wra">
						<form action="" id="frmticket" name="frmticket" method="post">
							<div class="form-group">
								<label for="usrname"><i class="fa fa-user"></i>Your Name</label>
								<input type="text" class="form-control" name="user_firstname" id="user_firstname" placeholder="Enter your name">
								<span id="firstname" class="validation"></span>
							</div>
							<div class="form-group">
								<label for="usrname"><i class="fa fa fa-envelope"></i>Email Address</label>
								<input type="text" class="form-control" name="user_email" id="user_email" placeholder="Enter email address">
								<span id="err_email_address" class="validation"></span>
							</div>
							
							<div class="form-group">
								<label for="usrname">Priority</label>
								<select class="form-control" name="priority" id="priority">
									<option value="">select priority</option>
									<option value="1">Low</option>
									<option value="2">Medium</option>
									<option value="3">High</option>
									<option value="4">Critical</option>
								</select>
								<span id="err_priority" class="validation"></span>
							</div>
							
							<div class="form-group">
								<label for="usrname">Subject</label>
								<input type="text" class="form-control" name="subject" id="subject" placeholder="Enter Subject">
								<span id="err_subject_info" class="validation"></span>
							</div>
							
							<div class="form-group">
								<label for="usrname">Message</label>
								<textarea type="text" class="form-control" name="text_msg" id="text_msg" placeholder="Enter Message"></textarea>
								<span id="err_massage_info" class="validation"></span>
							</div>
							 
							<div class="send_btn">
								<a href="javascript:void(0);" id="ticket_btn"><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Submit</a>
								<a href="javascript:history.go(-1);"><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Cancel</a>
							</div>
							<div style="margin-top:75px;" id="message_result"></div>
						</form>
					</div>
				</div>
				 			
			</div>
		</div>
	</div>
		
</div>
<!-- contact page section -->
<script>
$('#ticket_btn').on('click', function () {
	
	var first_name = $("#user_firstname");
	var login_FirstName_info = $("#firstname");
	
	var email_address = $("#user_email");
	var email_address_info = $("#err_email_address");
	
	var priority = $("#priority");
	var priority_info = $("#err_priority");
	
	var subject = $("#subject");
	var subject_err_info = $("#err_subject_info");
	
	var msg = $("#text_msg");
	var err_massage_info = $("#err_massage_info");
	
	
	var flag = 1;
	
	if(!validateEmpty(first_name, login_FirstName_info, "Name")) {
				flag = 0;
	}
	
	if(!validateEmpty(email_address, email_address_info, "Email address")) {
				flag = 0;
	}
	
	if(email_address.val()!=""){
				if(!validateEmail(email_address, email_address_info)){
						flag = 0;
				}
		}
	
	if(!checkCombo(priority, priority_info, "Priority")){
			flag = 0;
	}
	

	if(!validateEmpty(subject, subject_err_info, "Subject")) {
				flag = 0;
	}
	
	if(!validateEmpty(msg, err_massage_info, "Message")) {
				flag = 0;
	}

	
	
	
	var formData = new FormData($('#frmticket')[0]); 
	if(flag){
		 
		$.ajax({
				type: "POST",
				dataType: "json",		
				mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>postticket",
				data: formData 
				
		}).success(function (json) {
			
			
			if(json.status='error'){
				$("#message_result").html("<div class='alert alert-danger'>"+json.message+"</div>");
			}
			else{
				$("#message_result").html("<div class='alert alert-success'>"+json.message+"</div>");
			}
		});
	}
});
</script>
