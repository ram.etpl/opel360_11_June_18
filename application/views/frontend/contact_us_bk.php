
<!--contact page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/models_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">			
			<div class="col-lg-12">
				<div class="contact_title">Contact Us</div>
			</div>			
		</div>	
	</div>
	
	<div class="contact_wrapper">
		<div class="container">
			<div class="col-md-12 contact_details">
				Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.
			</div>
			<div class="contact_inner">
			
				<div class="col-md-5">
					<div class="cont_box_wra">
						<div class="form-group">
							<label for="usrname"><i class="fa fa-user"></i>Your Name</label>
							<input type="text" class="form-control" name="ur_name" id="ur_name" placeholder="Enter your name">
						</div>
						<div class="form-group">
							<label for="usrname"><i class="fa fa fa-envelope"></i>Email Address</label>
							<input type="text" class="form-control" name="email" id="email" placeholder="Enter email address">
						</div>
						<div class="form-group">
							<label for="usrname"><i class="fa fa-phone"></i>Phone No.</label>
							<input type="text" class="form-control" name="phone" id="phone" placeholder="Enter phone no">
						</div>
						<div class="form-group">
							<label for="usrname"><i class="fa fa-comment" aria-hidden="true"></i>Comment</label>
							<input type="text" class="form-control" name="comment" id="comment" placeholder="Enter comment">
						</div>
						<div class="send_btn">
							<a href="javascript:;"><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Sumbit</a>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="contact_map_section">
						<img src="images/Google_map.png" />
					</div>
				</div>			
			</div>
		</div>
	</div>
		
</div>
<!-- contact page section -->
