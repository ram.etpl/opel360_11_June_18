<!-- images section-->	
	<div class="img_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php $dashboard_user_data = $this->session->userdata("auth_opeluser");
					
					if($dashboard_user_data['user_type'] == 'OPEL'){
						$class= "reco_wrapper";
					}
					else{
						$class= "reco_wrapper_nonopel";
					}
					?>
					<div class="<?php echo $class;?>">
						<div class="cus_wrapper">
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_Url();?>profile"><img src="<?php echo base_Url();?>images/cust_prof.png" /></a></div>
								<div class="img_name">Customer profile</div>
							</div>
							
							<?php
							if($dashboard_user_data['user_type'] == 'OPEL'){?>
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_url();?>service-record"><img src="<?php echo base_Url();?>images/service_record.png" /></a></div>
								<div class="img_name">Service records</div>
							</div>							
							
							<?php } ?>
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_Url();?>survey"><img src="<?php echo base_Url();?>images/survey.png" /></a></div>
								<div class="img_name">Survey</div>
							</div>
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_Url();?>car-onwer-events"><img src="<?php echo base_Url();?>images/car_owner.png" /></a></div>
								<div class="img_name">Carowner Events</div>
							</div>
						
							<?php
							 if($dashboard_user_data['user_type'] == 'NONOPEL'){
							?>
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_Url();?>pdf-manuals"><img src="<?php echo base_Url();?>images/pdf_manuals.png" /></a></div>
								<div class="img_name">Pdf manuals</div>
							</div>
							<?php } ?>
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_Url();?>community-gallery"><img src="<?php echo base_Url();?>images/community_gallery.png" /></a></div>
								<div class="img_name">Community Gallery</div>
							</div>
							
							<div class="cus_inner">
								<div class="top_img"><a href="<?php  echo base_Url(); ?>my-rewards"><img src="<?php echo base_Url();?>images/my_rew.png" /></a></div>
								<div class="img_name">My rewards</div>
							</div>
							<?php
							 if($dashboard_user_data['user_type'] == 'OPEL'){
							?>
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_url();?>service-booking"><img src="<?php echo base_Url();?>images/book_ser.png" /></a></div>
								<!-- <div class="top_img"><a href="https://www.myopelservice.com/SG/Login.aspx" target="_blank"><img src="<?php echo base_Url();?>images/book_ser.png" /></a></div> -->
								<div class="img_name">Book a service</div>
							</div>
							
							
							<?php } ?>
							<div class="cus_inner">
								<div class="top_img"><a href="<?php echo base_url();?>rent-an-opel"><img src="<?php echo base_Url();?>images/book_ser.png" /></a></div>
								<div class="img_name">Rent an Opel</div>
							</div>
							<div class="cus_inner">
								<!-- <div class="top_img"><a href="<?php echo base_url();?>service-booking"><img src="<?php echo base_Url();?>images/book_ser.png" /></a></div> -->
								<div class="top_img"><a href="<?php echo base_url();?>forum-and-feedback"><img src="<?php echo base_Url();?>images/feedback-icon.png" /></a></div>
								<div class="img_name">Forum</div>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>	
	</div>
<!-- images section-->	
