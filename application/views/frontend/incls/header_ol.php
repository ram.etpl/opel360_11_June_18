<?php
$active_models =  ($this->uri->segment(1) == "models")?'class="active"':'';
$active_dashboard =  ($this->uri->segment(1) == "" || $this->uri->segment(1) == "index" || $this->uri->segment(1) == "profile" || $this->uri->segment(1) == "dashboard" || $this->uri->segment(1) == "pdf-manuals")?'class="active"':'';
$active_service_costs =  ($this->uri->segment(1) == "service-costs")?'class="active"':'';
$active_rent_opel =  ($this->uri->segment(1) == "rent-an-opel")?'class="active"':'';
?>

<!-- start header section -->
			<header class="header_section">			
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="col-lg-2 top_logo">
								<h1>
									<a href="<?php echo base_url();?>index" title="Opel360">
										<img src="<?php echo base_url();?>images/logo.png"  alt="Opel360" title="Opel360"/>
									</a>
								</h1>
								<img src="<?php echo base_url();?>images/lt_strip.png" class="srp" alt="">
								<div class="blck_overlay"></div>
							</div>
							<div class="col-lg-10 rt_usre_wrapper">
								<?php
								
								$Login_user_data = $this->session->userdata("auth_opeluser");
								$cond = array("user_id"=>$Login_user_data['id']);
								$users_header_data = $this->users_model->getAllUsers($cond);
								if($Login_user_data!='') {
									
									
								?>
									<div class="after_log_wrapper">
										<div class="logout_btn">
											<button type="button" id="frm-logout" name="frm-logout" class="btn-yellow"><span class="fa fa-chevron-right"></span>Logout</button>
										</div>
										<div class="wel_msg_wrapper">
											<span class="fafa_icon"><i class="fa fa-user" aria-hidden="true"></i></span>
											<span id="welcome-msg" class="welcome-msg">Welcome</span>
											<span id="welcome-msg-user" class="welcome-msg"><?php echo $users_header_data[0]['c_name']; ?></span>
										</div>
									</div>	
								<?php
							}
							else {
								?>
								<div class="head_rt">
									<div class="for_got">
										<a href="#" data-toggle="modal" data-target="#forgotmodel">Forgot Password?</a>
									</div>
									<div class="login">										
										<button type="button" class="btn-yellow" data-toggle="modal" data-target="#loginmodel"><span class="fa fa-chevron-right"></span>Login</button>
									</div>
									<div class="register">
										<button type="button" class="btn-yellow" data-toggle="modal" data-target="#registermodel"><span class="fa fa-chevron-right"></span>Register</button>
									</div>
									<div class="head_mobil_wrapper">
										<div class="opel_mobile_app">			
											<ul>
												<li><a href="#"><img src="<?php echo base_url();?>images/andriod.png" title="OPEL360 Android App" alt="OPEL360 Android App" target="_blank"></a></li>
												<li><a href="#"><img src="<?php echo base_url();?>images/iphone.png" title="OPEL360 iPhone App" alt="OPEL360 iPhone App" target="_blank"></a></li>
											</ul>						
										</div>
									</div>
								</div>
							 <?php 
							 } 
							 ?>
							</div>
							
						</div>
					</div>	
				</div>
				<div class="yell_bg">
					<div class="black_portion">
						<div class="container hover_manage">
							<div class="row">
								<!--Desktop menu-->
								<nav class="menu_wrapper">
									<ul>
										<?php
								
								$Login_user_data = $this->session->userdata("auth_opeluser");
								if($Login_user_data!='') {
								?>
										<li <?php echo $active_dashboard; ?>><a href="<?php echo base_url();?>profile">Dashboard</a></li> 
										<?php }else{ ?>
										<li <?php echo $active_dashboard; ?>><a href="<?php echo base_url();?>index">Home</a></li> 
										<?php } ?>
										<?php 
												$cond_model_header = array(); 
												$car_model_header = $this->catalog_model->getCatalogModelById($cond_model_header);
												if(count($car_model_header)>0){
											?>	
												<li <?php echo $active_models;?>>	
												<a href="<?php echo base_url();?>models">Cars</a>
													<img class="top_arow_hover" src="<?php echo base_Url();?>images/top_arrow.png" alt="">
													
													<ul class="sub_menu">
														<?php foreach($car_model_header as $car_model):
														
															  $cond_img_header = array("model_id" => $car_model['id']); 
															  $car_img_header = $this->catalog_model->getCatalogModelById($cond_img_header);
															  //$car_img = $car_img_header[0]['car_thumb'];
															  if($car_img_header[0]['car_thumb'] !=""){
																  $car_img = $car_img_header[0]['car_thumb'];
															  }
															  else{
																  $car_img = 'car-generic.png';
															  }
															  
														?>
															<li><a href="<?php echo base_url();?>models/<?php echo $car_model['id'];?>"><img src="<?php echo base_url(); ?>car_gallery/thumb/actual_thumb/<?php echo $car_img; ?>" alt=""/><?php echo $car_model['m_name']; ?></a></li>
														<?php endforeach;?>
													</ul> 
												</li>
										<?php } ?>
										<!--<li <?php echo $active_service_costs;?>><a href="<?php echo base_url();?>service-costs">Cars</a>
											<ul class="sub_menu contact_menu">
												<li><a href="<?php echo base_url();?>models">Models</a></li>
												<li><a href="<?php echo base_url();?>forms/test-drive">Test Drive</a></li>
												<li><a href="<?php echo base_url();?>forms/upgrade-trade">Upgrade / Trade</a></li>
												<li><a href="javascript:;">Feedback</a></li>
											</ul>
										</li>-->
										<li <?php echo $active_service_costs;?>><a href="javascript:void(0);">Aftersales</a>
											<ul class="contact_menu">
												<li><a href="<?php echo base_url();?>service-exclusive">Service Exclusive</a></li>
												<!--<li><a href="<?php echo base_url();?>service-costs">Service Cost</a></li> -->
											</ul>
										</li>
										<li><a href="<?php echo base_url();?>accessories">Accessories</a></li>
										<li><a href="<?php echo base_url();?>news-and-events">News & Events</a></li>
										<li><a href="<?php echo base_url();?>gallery">Community Gallery</a></li> 
										<?php if($Login_user_data!='') {?>
										<!--<li <?php echo $active_rent_opel; ?>><a href="<?php echo base_url();?>rent-an-opel">Rent An Opel</a></li>-->
										<?php } ?>
										<li><a href="<?php echo base_url();?>contact-us">Contact Us</a>		
											<ul class="contact_rt_menu">
												<li><a href="<?php echo base_url();?>forms/test-drive">Test Drive</a></li>
												<li><a href="<?php echo base_url();?>forms/upgrade-trade">Upgrade / Trade</a></li>
												<li><a href="<?php echo base_url();?>forms/feedback">Feedback</a></li>
											</ul>
										
										</li>
										<?php 
											if($this->session->userdata("auth_opeluser")) { 
										?>
										
										<?php 
											}
										?>
									</ul>
								</nav>
								<!--Desktop menu-->								
								<!--resposive menu-->
								<nav class="res_menu">									
										<div class="top-nav">
											<div class="nav-text"><i class="fa fa-bars" aria-hidden="true"></i></div>
											<ul class="right">
												<?php 
												 
												if($Login_user_data!='') {
												?>
												<li <?php echo $active_dashboard; ?>><a href="<?php echo base_url();?>profile">Dashboard</a></li> 
												<?php }else{ ?>
												<li <?php echo $active_dashboard; ?>><a href="<?php echo base_url();?>index">Home</a></li> 
												<?php } ?>
												<!--<li><a href="<?php //echo base_url();?>models">Cars</a>-->
												<li><a href="javascript:void(0);">Cars</a>
												<?php 
												$cond_model_header2 = array(); 
												$car_model_header2 = $this->catalog_model->getCatalogModelById($cond_model_header2);
												if(count($car_model_header2)>0){
											?>	
													<ul class="">
														<?php foreach($car_model_header2 as $car_model):?>													
															<li><a href="<?php echo base_url();?>models/<?php echo $car_model['id'];?>"><?php echo $car_model['m_name']; ?></a></li>
														<?php endforeach; ?>												
													</ul>													
											<?php } ?>		
												</li>
												<li><a href="javascript:void(0);">Aftersales</a>
													<ul class="">														
														<li><a href="<?php echo base_url();?>service-exclusive">Service Exclusive</a></li>
														<!--<li><a href="<?php echo base_url();?>service-costs">Service Cost</a></li>-->
													</ul>		
												</li>	
												<li><a href="<?php echo base_url();?>accessories">Accessories</a></li>
												<li><a href="<?php echo base_url();?>news-and-events">News & Events</a></li>
												<li><a href="<?php echo base_url();?>gallery">Gallery</a></li>
												
												<!--<li><a href="<?php echo base_url();?>rent-an-opel">Rent An Opel</a></li>-->
												<li><a href="javascript:void(0);">Contact Us</a>
													<ul class="">
														<li><a href="<?php echo base_url();?>contact-us">Contact Us</a></li>
														<li><a href="<?php echo base_url();?>forms/test-drive">Test Drive</a></li>
														<li><a href="<?php echo base_url();?>forms/upgrade-trade">Upgrade / Trade</a></li>
														<li><a href="<?php echo base_url();?>forms/feedback">Feedback</a></li>
													</ul>
												</li>
												
											</ul>
										</div>							
								</nav>
								<!--resposive menu-->								
							</div>
						</div>
					</div>
				</div>
			</header>
		<!-- end header section -->
		<!--start Register popup-->		
			<div id="registermodel" class="modal fade register_popup" role="dialog">
				<div class="modal-dialog">				
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close mfp-register-close" id="btn-register"><i class="fa fa-times"></i></button>
						<h4 class="modal-title">Register</h4>
					</div>					
					<form  name="frmUserRegister" id="frmUserRegister">					
						<div class="modal-body">
							<div id="reg-msg"></div>
							<div class="col-md-12 register_box">
								<div class="form-group">
									<label for="c_name"><i class="fa fa-user"></i>Name<span class="aster">*</span></label>
									<input type="text" placeholder="Enter the Name" id="c_name" name="c_name" class="form-control">
									<span id="FirstNameInfo" class="validation"></span>
								</div>
								 
								<div class="form-group">
									<label for="email_address"><i class="fa fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
									<input type="text" placeholder="Enter Email Address" id="email_address" name="email_address" class="form-control">
									<span id="EmailAddressInfo" class="validation"></span>
								</div>
								<div class="form-group">
									<label for="password"><i class="fa fa-unlock-alt"></i>Password<span class="aster">*</span></label>
									<input type="password" placeholder="Enter Password" id="password" name="password" class="form-control">
									<span id="PasswordInfo" class="validation"></span>
								</div>
								
								<div class="form-group">
									<label for="contact_number"><i class="fa fa-phone"></i>Contact No</label>
									<span class="plus_text">+65</span><input type="text" placeholder="Enter Contact No" maxlength="8" id="contact_number" name="contact_number" class="form-control">
									<span id="ContactNoInfo" class="validation"></span>
								</div>
								<div class="form-group terms_cond"> 
									<span class="i_agree">I Agree<a target="_blank" href="<?php echo base_url();?>terms-and-conditions" class=""> Terms and Conditions</a></span>
									<input type="checkbox" id="terms_and_cond" name="terms_and_cond" class="">
									<span id="termsInfo" class="validation"></span>
								</div>
								<div class="register_btn"><a href="javascript:void(0);" id="frm-register"><span><i class="fa fa-sign-in"></i></span>Register</a></div>
							</div>
							<div class="clearfix">&nbsp;</div>
						</div>
					</form>					
				</div>
				</div>
			</div>
	<!--end register popup-->	
	<!--start login popup-->		
			<div id="loginmodel" class="modal fade login_popup" role="dialog">
				<div class="modal-dialog">					
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close mfp-login-close" id="btn-login" ><i class="fa fa-times"></i></button>
							<h4 class="modal-title">Login</h4>
						</div>
						<div class="modal-body">
							<form id="frmlogin" name="frmlogin">
								<div class="col-md-12 login_box">
									<div id="login-reg-msg"></div>
									<div id="login_success_msg_for_activation_link"></div>
									<div class="form-group">
										<label for="email-id"><i class="fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
										<input type="text" placeholder="Enter Email Address" id="email-id" name="email_address" class="form-control">
										<span id="loginEmailAddressInfo" class="validation"></span>
									</div>
									<div class="form-group">
										<label for="pwd"><i class="fa fa-unlock-alt"></i>Password<span class="aster">*</span></label>
										<input type="password" placeholder="Enter Password" id="pwd" name="password" class="form-control">
										<span id="loginPasswordInfo" class="validation"></span>
									</div>
									<div class="login_btn"><a href="javascript:void(0);" id="frm-login" name="frm-login"><span><i class="fa fa-sign-in"></i></span>Login</a></div>
								</div>
							</form>
						</div>
						<div class="clearfix">&nbsp;</div>
					</div>
				</div>
			</div>
	<!--end login popup-->	
	<!--start forgot popup-->		
			<div id="forgotmodel" class="modal fade forgot_popup" role="dialog">
				<div class="modal-dialog">					
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close mfp-forgot-close" data-dismiss="modal"><i class="fa fa-times"></i></button>
							<h4 class="modal-title">Forgot Password</h4>
						</div>
						<div class="modal-body">
							<form action="javascript:void(0);" name="frmUserforgotPassword" id="frmUserforgotPassword">
								<div class="col-md-12 forgot_box">
									<div id="forgot-err-msg"></div>
									<div class="form-group">
										<label for="forgot_email"><i class="fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
										<input type="text" placeholder="Enter Email Address" id="forgot_email" name="email" class="form-control">
										<span id="forgotEmailAddressInfo" class="validation"></span>
									</div>
									<div class="forgot_btn"><a href="javascript:void(0);" id="frm-forgot" name="frm-forgot"><span><i class="fa fa-paper-plane"></i></span>Submit</a></div>
								</div>
							</form>
							<div class="clearfix">&nbsp;</div>
						</div>
					</div>
				</div>
			</div>
	<!--end forgot popup-->
<script type="text/javascript">
	
	$(document.body).delegate('.mfp-login-close','click');
	$(document).ready(function () {

$("#forgot_email").keypress(function(e) {
    if(e.which == 13) {
		$("#frm-forgot").click();
		
    }
});

	$("#c_name").keypress(function(e) {
    if(e.which == 13) {
		$("#frm-register").click();
    }
});

	$("#email_address").keypress(function(e) {
    if(e.which == 13) {
		$("#frm-register").click();
    }
});

	$("#password").keypress(function(e) {
    if(e.which == 13) {
		$("#frm-register").click();
    }
});

	$("#contact_number").keypress(function(e) {
    if(e.which == 13) {
		$("#frm-register").click();
    }
});

	$("#email-id").keypress(function(e) {
    if(e.which == 13) {
		$("#frm-login").click();
    }
});

	$("#pwd").keypress(function(e) {
    if(e.which == 13) {
		$("#frm-login").click();
    }
});

	$('#contact_number').keyup(function() {
        var $th = $(this);
        $th.val( $th.val().replace(/[^0-9]/g, function(str) { $("#contact_number").html('Please enter numbers only.'); return ''; } ) );
    });
	
	$('.mfp-login-close').on('click', function (e) {
		
		
		$("#email-id").val('');
		$("#pwd").val('');
		$("#c_name").val(''); 
		$("#email_address").val('');
		$("#password").val('');
		$("#contact_number").val('');
		$("#FirstNameInfo").html('');
		$("#EmailAddressInfo").html('');
		$("#PasswordInfo").html('');
		$("#loginEmailAddressInfo").html('');
		$("#loginPasswordInfo").html('');
		$('#btn-login').attr('data-dismiss', 'modal');
		$("#login-reg-msg").html('');
		//$('#loginmodel').modal('hide');
	});
	
	$('.mfp-register-close').on('click', function (e) {
		$("#c_name").val(''); 
		$("#email_address").val('');
		$("#password").val('');
		$("#contact_number").val('');
		$("#FirstNameInfo").html('');
		$("#EmailAddressInfo").html('');
		$("#PasswordInfo").html('');
		$('#btn-register').attr('data-dismiss', 'modal');
	});
	
	$('.mfp-forgot-close').on('click', function (e) {
		$("#forgot_email").val(''); 
		$("#forgotEmailAddressInfo").html(''); 
		$('#btn-login').attr('data-dismiss', 'modal');
	});
	
		$('#frm-logout').on('click', function (e) {
			
			$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					url: "<?php echo base_url(); ?>frontcontroller/logout",
					//data: formData
				}).success(function (json) { 
					if(json.status=='success'){
						window.location="<?php echo base_url();?>";
					}
				});
			
		});
	
		$('#frm-forgot').on('click', function (e) {
			
			var forgot_email_id = $("#forgot_email");
			var forgotEmailAddressInfo = $("#forgotEmailAddressInfo");
			
			var flag=1;
			if(!validateEmpty(forgot_email_id, forgotEmailAddressInfo, "email address")){
				flag = 0;
			}
		
			if(forgot_email_id.val()!=""){
				if(!validateEmail(forgot_email_id, forgotEmailAddressInfo)){
						flag = 0;
				}
			}
			
			var formData = new FormData($('#frmUserforgotPassword')[0]); 
			var ajaxRunning = false;
			//FORGOT API CALL			
			
			if(flag)
		{
			if(!ajaxRunning){
				ajaxRunning = true;
				
				//$("#loader_reg").show();
				//$("#loader_reg").attr('disabled','disabled');
				//$("#loader_reg").html('<img width="20%" src="<?php echo base_url();?>img/gif-load.gif" alt="">');	
				$("#forgot-err-msg").show();
				$("#forgot-err-msg").html('');
				$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					url: "<?php echo base_url(); ?>api/forgot_password",
					data: formData
				}).success(function (json) {
					
					if(json.status=='success'){
						$("#forgot-err-msg").html("<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");
						$("#forgot-err-msg").fadeOut(10000);
						window.location="<?php echo base_url();?>index";
					}
					
					if(json.status=='error'){
						$("#forgot-err-msg").html("<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");
						$("#forgot-err-msg").fadeOut(6000);

					}
				});
			}
		}
	});

		$('#frm-login').on('click', function (e) {
			
			var email_id = $("#email-id");
			var loginEmailAddressInfo = $("#loginEmailAddressInfo");
			
			var pwd = $("#pwd");
			var loginPasswordInfo = $("#loginPasswordInfo");
			
			var flag=1;
			
			if(!validateEmpty(email_id, loginEmailAddressInfo, "email address")){
				flag = 0;
			}
		
			if(email_id.val()!=""){
				if(!validateEmail(email_id, loginEmailAddressInfo)){
						flag = 0;
				}
			}
			
			if(!validateEmpty(pwd, loginPasswordInfo, "password")){
				flag = 0;
			}
			
			var formData = new FormData($('#frmlogin')[0]); 
			var ajaxRunning = false;
		
		
		if(flag)
		{
			if(!ajaxRunning){
				ajaxRunning = true;
				
				//$("#loader_reg").show();
				//$("#loader_reg").attr('disabled','disabled');
				//$("#loader_reg").html('<img width="20%" src="<?php echo base_url();?>img/gif-load.gif" alt="" >');	
				$("#login-reg-msg").show();
				$("#login-reg-msg").html('');
				$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					url: "<?php echo base_url(); ?>api/loginweb",
					data: formData
				}).success(function (json) {
					
					if(json.status=='success'){
						$("#login-reg-msg").html("<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");
						$("#login-reg-msg").fadeOut(6000);
						window.location="<?php echo base_url();?>profile";
					}
					
					if(json.status=='error'){
						$("#login-reg-msg").html("<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");
						//$("#login-reg-msg").fadeOut(6000);
					}
				});
			}
		}
});

		$('#frm-register').on('click', function (e) {
				var fName = $("#c_name");
				var fInfo = $("#FirstNameInfo"); 
				
				var password = $("#password");
				var passwordInfo = $("#PasswordInfo");
				
				
				var email_address = $("#email_address");
				var EmailAddressInfo = $("#EmailAddressInfo");
				
				var contact_number = $("#contact_number");
				var ContactNoInfo = $("#ContactNoInfo");
				
				var terms_and_cond = $("#terms_and_cond");
				var termsInfo = $("#termsInfo");
				
				var flag=1;
				
				if(!validateEmpty(fName, fInfo, "name")){
				flag = 0;
				}
				
				if(fName.val() != ""){	 
					if(!CheckAlphabates(fName, fInfo)){
						flag = 0;
					}
				}			
				
				
				if(!validateEmpty(password, passwordInfo, "password")){
				flag = 0;
				}
				else if(CheckPasswordText(password, passwordInfo)){
			
				} 
				else{
					flag=0;
				}
				
				if(!validateEmpty(email_address, EmailAddressInfo, "email address")){
					flag = 0;
				}
		
				if(email_address.val()!=""){
					if(!validateEmail(email_address, EmailAddressInfo)){
						flag = 0;
					}
				}
				 
				
				if(!terms_and_cond.is(':checked')){ 
					termsInfo.html("Please tick the checkbox if you agree terms and conditions.");
					flag = 0;
				}
				else{
					termsInfo.html('');
				}
				
				
				var formData = new FormData($('#frmUserRegister')[0]); 
				var ajaxRunning = false;
				
				if(flag)
				{
					if(!ajaxRunning){
						ajaxRunning = true;
						
						//$("#loader_reg").show();
						//$("#loader_reg").attr('disabled','disabled');
						//$("#loader_reg").html('<img width="20%" src="<?php echo base_url();?>img/gif-load.gif" alt="" >');	
						$("#reg-msg").show();
						$("#reg-msg").html('');
						$.ajax({
							type: "POST",
							dataType: "json",		
							mimeType: "multipart/form-data",
							contentType: false,
							cache: false,
							processData: false,
							url: "<?php echo base_url(); ?>api/registerUser",
							data: formData
						}).success(function (json) {
							
							if(json.status == 'success'){
								$("#registermodel").hide(); 
								$('#registermodel').modal('hide');
								$('#btn-register').attr('data-dismiss', 'modal');
								
								$("#loginmodel").show();
								//$("#loginmodel .modal").click(); 
								//$("#loginmodel").addClass('in'); 
								$('#loginmodel').modal('show');
								$("#login-reg-msg").html("<div class='alert alert-success'>"+json.msg+"</div>");
								//$("#reg-msg").fadeOut(6000);
							}
							
							if(json.status=='error'){
								$("#reg-msg").html("<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>"+json.msg+"</div>");
								$("#reg-msg").fadeOut(6000);
							}
						});
					}
		}
	});
	
});

function make_login(){
	$('#loginmodel').modal('show'); 
	
}
function sendactivationlink(email) {
		
		//Resend OTP
		var user_id = 1;
		$("#login_success_msg_for_activation_link").show();
		$("#login_success_msg_for_activation_link").html('');
		$.ajax({
					type: "POST",
					dataType: "json",		
					//mimeType: "multipart/form-data",
					//contentType: false,
					//cache: false,
					//processData: false,
					url: "<?php echo base_url(); ?>frontcontroller/resend_activation_link_on_deactivation",
					data: {'email':email}
				}).success(function (json) {
					
						if(json.status==1) {
							
							$("#login_success_msg_for_activation_link").html(json.msg);
							$("#login_success_msg_for_activation_link").fadeOut(6000);
							$("#login-reg-msg").hide();
						}
				});		
	}

function resend() {
		
		var formData = new FormData($('#frm-login')[0]);
		//Resend OTP
		var user_id = 1;
		$("#login_success_msg_for_activation_link").show();
		$("#login_success_msg_for_activation_link").html('');
		$.ajax({
					type: "POST",
					dataType: "json",		
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					url: "<?php echo base_url(); ?>frontcontroller/resend_activation_link",
					data: formData
				}).success(function (json) {
					
						if(json.status==1) {
							
							$("#login_success_msg_for_activation_link").html(json.msg);
							$("#login_success_msg_for_activation_link").fadeOut(6000);
						}
					
					
				});
		
	}	
</script>		
<script type="text/javascript">
$('.menu_wrapper ul li a').removeClass('active');
    $(this).addClass('active');
</script>
