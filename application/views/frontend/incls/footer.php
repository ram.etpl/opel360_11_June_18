<!-- start footer section -->
		<footer class="footer_wrapper">
			<div class="footer_center">
				<div class="col-lg-4">
					<div class="footer_logo"><a href="<?php echo base_url();?>index"><img src="<?php echo base_url();?>images/footer_logo.png" alt="Opel360" title="Opel360"/></a></div>
				</div>
				<div class="col-lg-4">
					<div class="abou_name">About Us</div>
					<div class="abou_text">Auto Germany Pte Ltd is the Sole Dealer for Opel passenger and commercial vehicles in Singapore. We're committed to bringing a high level of service quality to all our customers.</div>
					<div class="term_foot"><a href="<?php echo base_url();?>terms-and-conditions">Terms And Conditions</a></div>
					<div class="opel_mobile_app_footer">			
						<ul>
							<li><a target="_blank" href="https://itunes.apple.com/us/app/opel360/id1162749031?ls=1&mt=8"><img src="<?php echo base_url();?>images/iphone.png" title="OPEL360 iPhone App" alt="OPEL360 iPhone App" ></a></li>
							<li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.autogermany.opel360"><img src="<?php echo base_url();?>images/andriod.png" title="OPEL360 Android App" alt="OPEL360 Android App"></a></li>
						</ul>						
					</div>
				</div>
				<div class="col-lg-4 map_section">
					<div class="phone_wrap">
						<div class="confooter_wra">
							<div class="con_fot_title"><i class="fa fa-phone-square"></i>Contact No</div>
							<ul class="sal_no">
								<li>&raquo;<span>Sales </span>- <a href="tel:69223288">6922 3288</a></li>
								<li>&raquo;<span>Service </span>- <a href="tel:69223282">6922 3282</a></li>
								<li>&raquo;<span>Parts </span>- <a href="tel:69223285">6922 3285</a></li>
							</ul>
						</div>
						<div class="confooter_wra site">
							<div class="con_fot_title"><i class="fa fa-sitemap" aria-hidden="true"></i>Sites</div>
							<ul class="sal_no">
								<li>&raquo;<span><a href="javascript:;" target="_blank">opel.com.sg</a></span></li>
								<li>&raquo;<span><a href="javascript:;" target="_blank">autogermany.sg/support</a></span></li>
							</ul>
						</div>
						<div class="add_wra">
							<div class="con_fot_title"><i class="fa fa-map-marker" aria-hidden="true"></i>Address</div>
							<ul class="sal_no">
								<li>&raquo;<span>7 Ubi Close, Singapore 408604</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>
<!-- end footer section -->
<!--horizontal and vertical scroll-->
	<script>window.jQuery || document.write('<script src="<?php echo base_url();?>js/minified/jquery-1.11.0.min.js"><\/script>')</script>
	<script src="<?php echo base_url();?>js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		(function($){
			$(window).load(function(){				
				$.mCustomScrollbar.defaults.theme="light-2"; //set "light-2" as the default theme				
				$(".demo-x").mCustomScrollbar({
					axis:"x",
					advanced:{autoExpandHorizontalScroll:true}
				});
				$(".demo-y").mCustomScrollbar();
			});
		})(jQuery);
	</script>
<!--dropdown js-->	
<script src="<?php echo base_url();?>js/drop_down_js/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/drop_down_js/jquery.dropdown.js"></script>
<script type="text/javascript">	
	$( function() {		
		$( '#cd-dropdown' ).dropdown( {
			gutter : 5,
			stack : false,
			delay : 100,
			slidingIn : 100
		} );
		$( '#acc-dropdown' ).dropdown( {
			gutter : 5,
			stack : false,
			delay : 100,
			slidingIn : 100
		} );
	});
</script>


