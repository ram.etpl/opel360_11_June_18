<!DOCTYPE html >
<html lang="en">
<head> 
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
<title><?php echo $title;?></title>
<meta name="description" content="<?php echo $meta_description;?>">
<meta name="keywords" content="<?php echo $meta_keyword;?>">
<!--Favicon Icon-->
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>favicon.ico">
<link rel="icon" type="image/png" href="<?php echo base_url();?>favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo base_url();?>favicon-16x16.png" sizes="16x16" />
<meta name="msapplication-TileColor" content="#000">
<!--Favicon Icon-->
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/lightbox.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/media.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>fonts/font.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/demo.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/flexslider.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>css/loader.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/animate.css">

<!-- scroll css in model page-->
<link rel="stylesheet" href="<?php echo base_url();?>css/style_scroll.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.mCustomScrollbar.css">
<!-- responsive menu css-->
<link rel="stylesheet" href="<?php echo base_url();?>css/responsee.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/responsive_menu.css">
<!-- end css -->

<!-- start js -->
<script src="<?php echo base_url();?>js/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script defer src="<?php echo base_url();?>js/jquery.flexslider.js"></script>
<script src="<?php echo base_url();?>js/validate.js" type="text/javascript"></script>
<!-- responsive menu js -->
<script defer type="text/javascript" src="<?php echo base_url();?>js/jquery-ui.min.js"></script>    
<script defer type="text/javascript" src="<?php echo base_url();?>js/responsee.js"></script>
<!-- responsive menu js -->
<script type="text/javascript">
	
	var base_ourl = '<?php echo base_url();?>';
	$(function(){
	  //SyntaxHighlighter.all();
	});
	$(window).load(function(){
	  $('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 224,
		itemMargin: 5,
		asNavFor: '#slider'
	  });

	  $('#slider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel",
		start: function(slider){
		  $('body').removeClass('loading');
		}
	  });
	});
</script>
<!-- end js -->
<script type="text/javascript">
$(document).ready(function(){
     $("#myCarousel").carousel({
         interval : 4000,
         pause: false
     });
});
</script>
<body>
	
<div class="wrapper">
 <!--container main_section end here--> 			
		<?php $this->load->view("frontend/incls/header");?>
		<?php $this->load->view($main_content);?>
		<?php $this->load->view("frontend/incls/footer");?>
 <!--Footer section Close here-->
</div>
</body>
</html> 
