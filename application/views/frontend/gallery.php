<!--gallery page section -->
<div class="gallery_section gall_new">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_gallery.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">			
			<div class="row">
				<div class="col-lg-12">
					<div class="about_title">				
						<div class="col-md-8 gal_head">Community Gallery</div>					
										
						<?php if($logged_in ==1){?>
							<div class="col-md-4 dropdwn_gallery"><div class="upload_btn"><a class="btn-yellow" href="<?php echo base_url();?>community-gallery">Upload</a></div></div>	
						<?php } ?>
					</div>
				</div>	
			</div>
		</div>	
	</div>	
	<?php //if($logged_in ==1){?>
	<div class="service_wrapper">
		<div class="container">
		  <div class="row">
			<div class="col-md-12">
			<div class="serexc_head">TERMS & CONDITIONS FOR COMMUNITY GALLERY:</div>
							<p>Your uploads should not in any way be violent or pornographic, or promote any religion, and will not harm the dignity or any other rights of any third party. Please do not feature other car brands in your posts. When you upload the images to the page, you hereby grant the Company (and its licensees, advertising agencies and promotion agencies) and the employees, agencies and authorized representatives of each and all of them (collectively, "Authorized Persons"), the unrestricted, perpetual, worldwide, non-transferable, royalty-free right and license to display, exhibit, transmit, reproduce, record, digitize, modify, alter, adapt, create derivative works, exploit and otherwise use and permit others to use the Images (including, all copyrights in the User Content) in connection with the Company's marketing, advertising and promotions. We reserve the right to remove any information / images which we consider to be unlawful, offensive, threatening, libellous, defamatory, pornographic or otherwise objectionable.
							</p>
							<p>
					 </div>		
			 </div>
	</div>
	<?php //} ?>
	<div class="gallery_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">			
					<div id="no_record"></div>
					<div class="grid" style="display: none;">
					 <div id="loading"></div>
					</div>
				</div>
			</div>	
		</div>
	</div>		
</div>
<input type="hidden" id="start" name="start"/>
<script type="text/javascript" src="<?php echo base_url();?>js/simple-lightbox.js"></script>
<script>
	 $(document).delegate(".veh_vote","click",function(e){
			
			var vote = $(this).attr('data-attr'); 
			var vid =  $(this).attr('data-cat');
			var uid =  $(this).attr('data-uid'); 
			$.ajax({
						type: "POST",
						dataType: "json",
						url: "<?php echo base_url(); ?>vote-vehicle",
						data: {"vote":vote,"vid":vid,"uid":uid},
					}).success(function (json) {
						
						if(json.status == 'like'){
							$("#like_"+json.id).html(json.html); 
							$("#like_"+json.id).attr('data-attr','dislike');
							
						}
						if(json.status == 'dislike'){
							$("#like_"+json.id).html(json.html); 
							$("#like_"+json.id).attr('data-attr','like');
						}
						
						//setTimeout(function(){
							$("#cnt_"+json.id).html(json.heart_cnt);
						//}, 100);
						
					});
		});
	$( window ).load(function() {
		
		var win = $(window); 
		win.scroll(function() {
			if ($(document).height() - win.height() == win.scrollTop()) {
				getgallery();
			}
		});
		//$('.veh_vote').on('click', function (e) {
		
		//$('.cd-dropdown ul li').on('click', function () { 
			 
		getgallery();
		function getgallery(){	
				
				if($("#start").val() == ""){
					start =  $("#start").val(0); 
					var start =  $("#start").val(); 
				}else{
					var start =  $("#start").val(); 
				}
				
				$("#loading").show();
				$(".loading-data").html('<b>Please wait while loading data</b>');
				$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>getgallery",
				data: {"start":start},
				beforeSend: function(){
					$('.loading').show(); 
					$('.loading').html("Please wait while loading images..."); 
				},
				complete: function(){
					$(".grid").show("");
					$('.loading').html("");
					var $gallery = $('.lightbox').simpleLightbox();
				},
				success: function(json){
					/*if(json.no_records=='0'){
						$(".grid").html("");
						$("#no_record").html(json.html);
						
						$(".grid").hide("");
					}
					else{
						$("#no_record").html("");
						$("#no_records").html("");
					}
					$(".dis_like_wrapper").hide();
					$("#loading").hide();
					$(".loading-data").html('');*/
					$('.no-data-found').hide(); 
					if(json.start != 0){  
						
						$(".grid").append(json.html); 
						$("#start").val(json.paginate); 
						if(json.totalrec > 0 && json.totalrec >= json.limit){
							
							//$(".grid .first_gal:last").after('<div class="view_more_video"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></div>');
						}
						
						$('.ajax-loading').html(''); 
					}
					else{ 
						$(".grid").html(json.html); 
						$("#start").val(json.paginate);
						if(json.totalrec > 0 && json.totalrec >= json.limit){
							//$(".grid .first_gal:last").after('<div class="view_more_video"><a class="moredata" href="javascript:void(0);" onclick="loadmore();">Load More Data</a></div>');
						}
						$('.ajax-loading').html('');  
					}
				},
				error: function(){} 
		});
	}
	});
	
	$(function(){		
		var $gallery = $('.lightbox').simpleLightbox();
		
		$gallery.on('show.simplelightbox', function(){
			console.log('Requested for showing');
		})
		.on('shown.simplelightbox', function(){
			console.log('Shown');
		})
		.on('close.simplelightbox', function(){
			console.log('Requested for closing');
		})
		.on('closed.simplelightbox', function(){
			console.log('Closed');
		})
		.on('change.simplelightbox', function(){
			console.log('Requested for change');
		})
		.on('next.simplelightbox', function(){
			console.log('Requested for next');
		})
		.on('prev.simplelightbox', function(){
			console.log('Requested for prev');
		})
		.on('nextImageLoaded.simplelightbox', function(){
			console.log('Next image loaded');
		})
		.on('prevImageLoaded.simplelightbox', function(){
			console.log('Prev image loaded');
		})
		.on('changed.simplelightbox', function(){
			console.log('Image changed');
		})
		.on('nextDone.simplelightbox', function(){
			console.log('Image changed to next');
		})
		.on('prevDone.simplelightbox', function(){
			console.log('Image changed to prev');
		})
		.on('error.simplelightbox', function(e){
			console.log('No image found, go to the next/prev');
			console.log(e);
		});
	});	
	
</script>
<!-- gallery page section -->
