<!--all news section -->
<div class="news_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_event.jpg" alt=""/>
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">News And Events</div>
				</div>
			</div>
		</div>	
	</div>	
<!--customer profile section-->	
	<div class="news_wrapper_all">
		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<div class="news_wrapper">
					<div class="news_head"><img src="images/news_icon.png" alt=""/>Latest News</div>
					<div id="nt-example1-container">
						<ul id="nt-example1">							
							<?php echo $news;?>						
						</ul>
					 </div>
				 </div>			
				<div class="events_wrapper">
					<div class="news_head"><img src="images/event_icon.png" alt=""/>Events</div>					
					<!--Running section-->
					<div class="events_inner">
						<div class="running_head">Running Events</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">								
								<?php
									echo $Running_events;
								?>				
							</div>
						</div>
					</div>
					<!--Running section-->
					<!--upcoming section-->
					<div class="events_inner upcom_inner">
						<div class="running_head">Upcoming Events</div>					
						<div id="demo" class="showcase scrollTo-demo">
							<div class="content demo-y">								
								<?php
									echo $upcoming_events;
								?>								
							</div>
						</div>
					</div>
					<!--upcoming section-->
				</div>
			</div>
			</div>
		</div>
	</div>
<!--customer profile section-->	
<script src="<?php echo base_url();?>js/jquery.newsTicker.js"></script>
<script type="text/javascript">
var nt_example1 = $('#nt-example1').newsTicker({
	row_height: 95,
	//max_rows: 10,
	duration: 3000,
	prevButton: $('#nt-example1-prev'),
	nextButton: $('#nt-example1-next')
});
$( document ).ready(function() {
	 
	$(".events_inner .scrollTo-demo .demo-y").each(function( index ) {
		var height = $(this).height();
		if(height < 545){
			$(this).removeClass("demo-y");
		}
	});
});
</script>
</div>
