<!--all rent section -->
<div class="rent_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/profile_banner.jpg" alt=""/>
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Confirm Booking</div>
				</div>
			</div>
		</div>	
	</div>	
<!--customer profile section-->	
	<div class="news_wrapper_all">
		<div class="container">
			<div class="col-md-12">
				<?php if($error_message){ ?><div class='alert alert-danger'><?php echo $error_message;?></div><?php }?>
				<div class="cars_total_wrapper"><span class="col-md-6 car_count">Cars Found - <?php echo ($cars_found!=0?$cars_found:0);?></span><a href="javascript:;" title="Back"><span class="col-md-6 car_count_back"><i class="fa fa-hand-o-left" aria-hidden="true"></i></span></a></div>
				<div class="rent_next_conf_wrapper">					
				<?php if($cars_array!=""){
				foreach($cars_array  as $car):
				?>
					<div class="rent_next">
						<div class="list_car_name"><?php echo $car['Model'];?></div>
						<div class="li_decar_wra">
							<div class="col-md-7 car_next">
								<div class="lt_img_car">
									<img alt="" src="<?php echo base_url();?>images/car_rent_opel.jpg">
								</div>
								
								<div class="rt_car_det">
									<div class="rt_car_info"><img alt="Air Conditioned" src="images/air_conditioned.png">Air Conditioned</div>
									<div class="rt_car_info"><img alt="Automatic" src="images/automatic.png"><?php echo $car['Transmission']; ?></div>
									<div class="rt_car_info"><img alt="Door" src="images/door.png">4 Door</div>
									<div class="rt_car_info"><img alt="Passenger" src="images/passenger.png"><?php echo $car['Units'];?> Passenger</div>
									<div class="rt_car_info"><img alt="Luggage" src="images/luggage.png">3 Luggage</div>
								</div>
								
							</div>
							<div class="col-md-5 full_pric_wrapp">
							  <div class="submit_select_box"> 	
									<div class="price_wrapper">
										<div class="star_frm">
											Starting From
										</div>
										<div class="euro">
											$ <?php echo $car['StartPrice'];?>
										</div>
										<div class="continue_rent_btn">
											<a href="<?php echo base_url();?>pay-now" id="cmdSubmit" name="cmdSubmit" type="submit">
												Continue
											</a>
										</div>
									</div>
									<div class="price_rt">
										<div class="arow_wra">
											<img alt="Choose Car" src="images/arrow_full.png" alt="">
										</div>
									</div>
							  </div>
							</div>
						</div>
					</div>
					<?php endforeach;
					} 
					?>
					
				</div>
				
			</div>
		</div>
	</div>	
<!--customer profile section-->	

</div>
