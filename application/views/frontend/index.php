<!-- start Banner section -->
			<div class="banner_wrapper">
				<div class="banner">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<?php echo $banners; ?>
						</div>
					</div>
				</div>
			</div>
		<!-- end Banner section -->
		
<!-- start body section -->
		
		<div class="bottom_line">
			<div class="container">
				<div class="row">
					<div class="opel_wrapper">
						<div class="col-lg-12 be_wra">
							<div class="opel_title">OPEL360 <span class="enjoy_title">AT YOUR FINGERTIPS</span></div>
						</div>
						 
					</div>
				</div>
			</div>	
		</div>		
		
		<div class="white_wrapper">	
			<div class="container">
				<div class="row">
					
					<div class=" col-lg-12 opel_app_wrapper">
						<div class="lt_app_wrapp">
							<div class="opel_lt_img"><img src="<?php echo base_url();?>images/app_dashboard_icon.png" alt="" title=""/></div>
						</div>
						<div class="rt_app_wrapp">
							<div class="opel_app_title">Get The New Opel 360 App</div>
							<div class="opel_app_txt">
								Opel360 offers you a brand new way of connecting with your Opel.<br>
								It  is your package of innovative services, products and lifestyle offers from Opel / AutoGermany – including access to your vehicle’s aftersales details via smartphone, of course. Your dashboard on the application, covers the individual topics relevant to you. Registering with OPEL360 gives you access to exclusive life-enhancing offers, updates and promotions, anytime and anywhere!
								<br>
								More features will be progressively added in future updates of the Opel360 app, but here are some of the key features you can now enjoy:
							</div>
							<div class="opel_app_offers">
								<div class="offers_name">WHAT OPEL360 OFFERS :</div>	
								<ul>
									<li>Online Service Request : Arrange for a service appointment online</li>
									<li>Your Opel’s profile : It comprises important data on your car as well as your service check book</li>
									<li>Service History : Never miss a service again – let us remind you of it</li>
									<li>Newsletters with news and offers from Opel</li>
									<li>Custom-made offers for you and your Opel, as well as many attractive promotions and car-specific accessories and services</li>
									<li>And much more …</li>
								</ul>
							</div>
							<div class="opel_mobile_app">			
								<ul>
									<li><a target="_blank" href="https://itunes.apple.com/us/app/opel360/id1162749031?ls=1&mt=8"><img src="<?php echo base_url();?>images/iphone.png" title="OPEL360 iPhone App" alt="OPEL360 iPhone App"></a></li>
									<li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.autogermany.opel360"><img src="<?php echo base_url();?>images/andriod.png" title="OPEL360 Android App" alt="OPEL360 Android App"></a></li>
								</ul>						
							</div>							
						</div>
					</div>
					
					<div class="col-lg-12 news_home_wrapper">
						<div class="news_wrapper">
							<div class="news_head"><img src="images/news_icon.png" alt=""/>Latest News</div>
							<div id="nt-example1-container">
								<ul id="nt-example1">							
									<?php echo $news;?>						
								</ul>
							</div>
						</div>
						<!-- <div class="events_wrapper">						
							<div class="news_head"><img alt="" src="images/event_icon.png">Events</div>
							<div class="events_inner upcom_inner">
								<div class="running_head">Upcoming Events</div>					
								<div id="demo" class="showcase scrollTo-demo">
									<div class="content demo-y">								
										<?php
										echo $upcoming_events;
										?>								
									</div>
								</div>
							</div>						
						</div> -->
						<!-- <div class="events_wrapper">						
							<div class="news_head"><img alt="" src="images/event_icon.png">Promotion</div>
							<div class="events_inner upcom_inner">
								<div class="running_head">Upcoming Promotion</div>					
								<div id="demo" class="showcase scrollTo-demo">
									<div class="content demo-y">								
										<?php
										echo $upcoming_promotion;
										?>								
									</div>
								</div>
							</div>
							<div class="events_inner upcom_inner">
								<div class="running_head">Running Promotion</div>					
								<div id="demo" class="showcase scrollTo-demo">
									<div class="content demo-y">								
										<?php
										echo $running_promotion;
										?>								
									</div>
								</div>
							</div>					
						</div> -->							
					</div>	
					
					<div class="col-lg-12 car_det_wrapper">
						<div class="car_rent">Featured Car</div>
						
						<div class="featu_car_wrapper">
						<?php if(count($featured_cars)){
							foreach($featured_cars as $car):
							?>
							<div class="col-lg-4 car_wra">
								<div class="home_wrapper">
									<div class="img_rent"><a href="<?php echo base_url();?>gallery"><img src="<?php echo base_url();?>community_gallery/thumb/<?php  echo $car['img_thumb']; ?>" alt=""/></a><div class="mask"></div></div>
								</div>
							</div>
							 
						<?php endforeach;}else{ ?>
							<div class="col-lg-4 car_wra">
								<div>No Records Available</div>
							</div>	
						<?php } ?>
						</div>
					</div>
					
				</div>	
			</div>	
		</div>		
		
<!-- end body section -->
<script src="<?php echo base_url();?>js/jquery.newsTicker.js"></script>
<script type="text/javascript">
var nt_example1 = $('#nt-example1').newsTicker({
	row_height: 95,
	//max_rows: 10,
	duration: 3000,
	prevButton: $('#nt-example1-prev'),
	nextButton: $('#nt-example1-next')
});
$( document ).ready(function() {
	 
	$(".events_inner .scrollTo-demo .demo-y").each(function( index ) {
		var height = $(this).height();
		if(height < 545){
			$(this).removeClass("demo-y");
		}
	});
});
</script>
