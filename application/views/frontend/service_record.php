<!--all profile page section -->
<div class="gallery_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Service Records</div>
				</div>
			</div>
		</div>	
	</div>
	<?php $this->load->view("frontend/incls/dashboard_menu");?>
<!--customer profile section-->	
	<div class="service_record_wrapper">
		<div class="container"> 
			<div class="row">
				<div class="col-lg-12">
					<div class="record_wrapper">
						<div id="accordion"  class="record_collapse" role="tablist" aria-multiselectable="true">
						  <?php echo $html;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
<!--end edit profile popup-->	
</div>
<!--all profile page section -->

<script type="text/javascript">
	$('.collapse').on('shown.bs.collapse', function(){
	$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
	}).on('hidden.bs.collapse', function(){
	$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
	});
	
	jQuery('#carousel ul li').click(function () {
	jQuery(this).closest('li').removeClass('selected');
	jQuery(this).closest('li').addClass('activeation');
	
	
	});
</script>
