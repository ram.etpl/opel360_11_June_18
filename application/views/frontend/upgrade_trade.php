<!--upgrade page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_contact.jpg" alt=""/>
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">		
			<div class="row">
				<div class="col-lg-12">
					<div class="contact_title">Upgrade / Trade In</div>
				</div>
			</div>			
		</div>	
	</div>	
	<div class="contact_wrapper">
		<div class="container">		
			<div class="row">
			<div class="contact_inner">			
				<div class="col-md-12">
					<div class="cont_box_wra upgrade_box">
						<form id="frmticket" name="frmticket" method="post">
							<div class="upg_wrapper">
								<div class="col-md-6 lt_re">	
									<div class="form-group">
										<label for="current_model"><i class="fa fa-user"></i>Current Make/Model<span class="aster">*</span></label>
										<input type="text" class="form-control" name="current_model" id="current_model" placeholder="Enter Current Model" maxlength="100">
										<span id="infoCurModel" class="validation"></span>
									</div>
								</div>
								<div class="col-md-6 rt_re">
									<div class="form-group">
										<label for="year_purchased"><i class="fa fa-calendar" aria-hidden="true"></i>Year Purchased</label>
										<select class="form-control" name="year_purchased" id="year_purchased">
											<option value="">Select Year Purchased</option>
											<?php for($i=1800;$i<=date("Y");$i++){?>
											<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										</select>
										<span id="infoYearPurchased" class="validation">&nbsp;</span>
									</div>
								</div>
							</div>
							
							<div class="upg_wrapper">
								<div class="col-md-6 lt_re">	
									<div class="form-group">
										<label for="model_interested"><i class="fa fa-car" aria-hidden="true"></i>Model Interested</label>
										<select class="form-control" name="model_interested" id="model_interested" maxlength="100">
											<?php echo $models; ?>
										</select>
									</div>
								</div>
								<div class="col-md-6 rt_re">
									<div class="form-group">
										<label for="action"><i class="fa fa-paper-plane" aria-hidden="true"></i>Action</label>
										<select class="form-control" name="action" id="action">
											<option value="Upgrade">Upgrade</option>
											<option value="Trade/In">Trade/In</option>
										</select>
									</div>
								</div>
							</div>
							
							<div class="upg_wrapper">
								<div class="col-md-6 lt_re">
									<div class="form-group">
										<label for="name"><i class="fa fa-user"></i>Name<span class="aster">*</span></label>
										<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" maxlength="100">
										<span id="infoName" class="validation"></span>
									</div>
								</div>
								<div class="col-md-6 rt_re valid_mobile">
									<div class="form-group">
										<label for="mobile_number"><i class="fa fa-phone"></i>Contact No.<span class="aster">*</span></label>
										<span class="plus_text">+65</span><input type="text" class="form-control numeric" name="mobile_number" id="mobile_number" placeholder="Enter Contact No." maxlength="10">
										<span id="infoMobileNumber" class="validation"></span>
									</div>
								</div>
							</div>
							
							<div class="upg_wrapper">
								<div class="col-md-6 lt_re">
									<div class="form-group">
										<label for="email"><i class="fa fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
										<input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address">
										<span id="infoEmail" class="validation"></span>
									</div>
								</div>
								<div class="col-md-6 rt_re">
									<div class="form-group">
										<label for="address"><i class="fa fa-location-arrow" aria-hidden="true"></i>Address<span class="aster">*</span></label>
										<input type="text" class="form-control" name="address" id="address" placeholder="Enter Address">
										<span id="infoAddress" class="validation"></span>
									</div>
								</div>
							</div>
							
							<div class="upg_wrapper">
								<div class="col-md-6 lt_re">
									<div class="form-group">
										<label for="text_msg"><i class="fa fa-comment" aria-hidden="true"></i>General Text</label>
										<textarea class="form-control" name="text_msg" id="text_msg" placeholder="Enter General Text"></textarea>
									</div>
								</div> 
							</div>
							<div class="send_btn">
								<a href="javascript:void(0);" id="ticket_btn"><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Submit</a>
								<!--<a href="javascript:history.go(-1);"><span><i class="fa fa-trash-o" aria-hidden="true"></i></span>Cancel</a>-->
							</div>
							<div id="message_result"></div>
						</form>
					</div>
				</div>				 			
			</div>
			</div>
		</div>
	</div>
		
</div>
<!-- upgrade page section -->
<script src="<?php echo base_url();?>js/jquery.numeric.js"></script>
<script>
	
	$(".numeric").numeric({decimal:false,negative:false});	
	jQuery("#mobile_number").keypress(function (e) {  
		if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
				   return false;
		}
		else {
			return true;
		}
	}); 
$('#ticket_btn').on('click', function () {
	
	var current_model = $("#current_model");
	var infoCurModel = $("#infoCurModel");
	
	var model_interested = $("#model_interested");
	var infoSelModel = $("#infoSelModel");
	
	var action = $("#action");
	var infoAction = $("#infoAction");
	
	var name = $("#name");
	var infoName = $("#infoName");
	
	var mobile_number = $("#mobile_number");
	var infoMobileNumber = $("#infoMobileNumber");
	
	var address = $("#address");
	var infoAddress = $("#infoAddress");
	
	var email = $("#email");
	var infoEmail = $("#infoEmail");
	 
	var flag = 1;
	
	if(!validateEmpty(current_model, infoCurModel, "current model")) {
				flag = 0;
	}
	
	if(!checkCombo(model_interested, infoSelModel, "model interested")) {
				flag = 0;
	}
	if(!validateEmpty(email, infoEmail, "email address")) {
				flag = 0;
	}
	if(email.val()!=""){
				if(!validateEmail(email, infoEmail)){
						flag = 0;
				}
		}
	
	if(!checkCombo(action, infoAction, "action")){
			flag = 0;
	}
	

	if(!validateEmpty(name, infoName, "name")) {
				flag = 0;
	}
	
	if(!validateEmpty(mobile_number, infoMobileNumber, "mobile number")) {
				flag = 0;
	}

	if(!validateEmpty(address, infoAddress, "address")) {
				flag = 0;
	}
	
	
	var formData = new FormData($('#frmticket')[0]); 
	if(flag){
		 $("#ticket_btn").addClass("sending");
		$.ajax({
				type: "POST",
				dataType: "json",		
				// mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>upgrade-post",
				data: formData 
				
		}).success(function (json) {
			$("#ticket_btn").removeClass("sending");
			
			if(json.status=='error'){
				$("#message_result").html("<div class='alert alert-danger'>"+json.msg+"</div>");
			}
			else{
				$("#message_result").html(json.msg);
				$("#current_model").val("");
				$("#year_purchased").val("");
				$("#address").val("");
				$("#email").val("");
				$("#name").val("");
				$("#text_msg").val("");
			}
		});
	}
});
</script>
