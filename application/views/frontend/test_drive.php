<!--test page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_contact.jpg" alt=""/>
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="contact_title">Request a Test drive</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="contact_wrapper">
		<div class="container">
			<div class="row">
			<div class="contact_inner">				
				<div class="col-md-12">	
					<div class="cont_box_wra request_box">
						<form id="frmtestdrive" name="frmtestdrive" method="post">
							<div class="test_wrapper">
								<div class="col-md-6 lt_re">
									<div class="form-group">
										<label for="model"><i class="fa fa-car" aria-hidden="true"></i>Model<span class="aster">*</span></label>
											<select class="form-control" name="model" id="model">
												<option value="">Select model</option>
												<?php
													echo $models;
												?>
											</select>
										<span id="modelInfo" class="validation"></span>
									</div>
								</div>							
								<div class="col-md-6 rt_re">
									<div class="form-group">
										<label for="title"><i class="fa fa-pencil" aria-hidden="true"></i>Title<span class="aster">*</span></label>
										<input type="text" class="form-control" name="title" id="title" placeholder="Enter Title" maxlength="100">
										<span id="titleInfo" class="validation"></span>
									</div>
								</div>
							</div>
							<div class="col-md-6 lt_re">
								<div class="form-group">
									<label for="name"><i class="fa fa-user"></i>Name<span class="aster">*</span></label>
									<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" maxlength="100">
									<span id="nameInfo" class="validation"></span>
								</div>
							</div>							
							<div class="col-md-6 rt_re">							
								<div class="form-group">
									<label for="email"><i class="fa fa fa-envelope"></i>Email Address<span class="aster">*</span></label>
									<input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address">
									<span id="emailInfo" class="validation"></span>
								</div>								
							</div>
							
							<div class="col-md-6 lt_re valid_mobile">
								<div class="form-group">
									<label for="mobno"><i class="fa fa-phone"></i>Contact No.</label>
									<span class="plus_text">+65</span><input type="text" class="form-control"  maxlength="14" name="mobno" id="mobno" placeholder="Enter Contact No.">
									<!--<span id="mobnoInfo" class="validation"></span>-->
								</div>
							</div>
							<div class="col-md-6 rt_re">								
								<div class="form-group ">
									<label for="address"><i class="fa fa-location-arrow" aria-hidden="true"></i>Address</label>
									<input type="text" class="form-control" name="address" id="address" placeholder="Enter Address">
									<span id="addressInfo" class="validation"></span>
								</div>
							</div>
							
							<div class="col-md-6 lt_re">
								<div class="form-group">
									<label for="existing_model"><i class="fa fa-car" aria-hidden="true"></i>Existing model<span class="aster">*</span></label>
									<input type="text" class="form-control" name="existing_model" id="existing_model" placeholder="Enter Existing Model" maxlength="200">
									<span id="exModelInfo" class="validation"></span>
								</div>
							</div>
							<div class="col-md-6 rt_re">
								<div class="form-group mar_botom">
									<label for="year_purchased"><i class="fa fa-calendar" aria-hidden="true"></i>Year purchased</label>
									<input type="text" class="form-control numeric" maxlength="4" name="year_purchased" id="year_purchased" placeholder="Enter Year Purchased">
									<span id="yearInfo" class="validation">&nbsp;</span>
								</div>
							</div>
							
							<div class="col-md-6 lt_re">
								<div class="form-group">
									<label for="general_com"><i class="fa fa-comment" aria-hidden="true"></i>General Comments</label>
									<textarea class="form-control" name="general_com" id="general_com" placeholder="Enter General Comments"></textarea>
								</div>
							</div>
							<div class="send_btn">
								<a href="javascript:void(0);" id="test_drive_btn"><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Submit</a>
								<!--<a href="javascript:history.go(-1);"><span><i class="fa fa-trash-o" aria-hidden="true"></i></span>Cancel</a>-->
							</div>
							<div style="margin-top:8px;" id="loading"></div>
							<div id="message_result"></div>
						</form>
					</div>
				</div>
				 			
			</div>
			</div>
		</div>
	</div>
		
</div>
<!-- test page section -->
<script src="<?php echo base_url();?>js/jquery.numeric.js"></script>

<script>
$(".numeric").numeric({decimal:false,negative:false});	
jQuery("#mobno").keypress(function (e) {  
		if (e.which != 8 && e.which != 0 && e.which != 43  && e.which != 27 && e.which != 45 && (e.which < 48 || e.which > 57) ) { 
				   return false;
		}
		else {
			return true;
		}
}); 

$('#test_drive_btn').on('click', function () {
	
	var model = $("#model");
	var modelInfo = $("#modelInfo");
	
	var title = $("#title");
	var titleInfo = $("#titleInfo");
	
	var name = $("#name");
	var nameInfo = $("#nameInfo");
	
	//var mobno = $("#mobno");
	//var mobnoInfo = $("#mobnoInfo");
	
	//var address = $("#address");
	//var addressInfo = $("#addressInfo");
	
	var email = $("#email");
	var emailInfo = $("#emailInfo");
	
	var existing_model = $("#existing_model");
	var exModelInfo = $("#exModelInfo");
	
	//var year_purchased =$("#year_purchased");
	//var yearInfo = $("#yearInfo");
	
	//var general_com =$("#general_com");
	//var generalInfo = $("#generalInfo");
	
	var flag = 1;
	
	if(!checkCombo(model, modelInfo, "the model")) {
		flag = 0;
	}
	
	if(!validateEmpty(title, titleInfo, "title.")) {
		flag = 0;
	}
	
	if(!validateEmpty(name, nameInfo, "name.")) {
		flag = 0;
	}
	
	/*if(!validateEmpty(mobno, mobnoInfo, "mobile no.")) {
		flag = 0;
	}
	
	if(!validateEmpty(address, addressInfo, "address")) {
		flag = 0;
	}*/
	
	
	if(!validateEmpty(email, emailInfo, "email address.")) {
		flag = 0;
	}
	
	if(!validateEmpty(existing_model, exModelInfo, "existing model.")) {
		flag = 0;
	}
	
	
	if(email.val()!=""){
			if(!validateEmail(email, emailInfo)){
					flag = 0;
			}
	}
	
	
	var formData = new FormData($('#frmtestdrive')[0]); 
	if(flag){
		$("#test_drive_btn").addClass("sending");
		$.ajax({
				type: "POST",
				dataType: "json",		
				//mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>request-test-drive",
				data: formData 
				
		}).success(function (json) {
			$("#test_drive_btn").removeClass("sending");
			if(json.status=='error'){
				$("#message_result").html("<div class='alert alert-danger'>"+json.message+"</div>");
			}
			else{
				$("#message_result").html(json.msg);
				$("#name").val('');
				$("#email").val('');
				$("#model").val('');
				$("#title").val('');
				$("#mobno").val('');
				$("#existing_model").val('');
				$("#address").val('');
				$("#year_purchased").val('');
				$("#general_com").val('');
			}
		});
	}
});
</script>
