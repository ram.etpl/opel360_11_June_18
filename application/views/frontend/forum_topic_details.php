<!--upgrade page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_contact.jpg" alt=""/>
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="contact_title">Forum Topic Details</div>
					<div class="post-update"> 
							<div class="upbtn"><a href="<?php echo base_url();?>forum-and-feedback">Back</a></div>
						</div>
				</div>
			</div>	
		</div>	
	</div>	
	<div class="contact_wrapper">
		<div class="container">	
			<div class="row">
				<div class="contact_inner">			
					<div class="col-md-12">
						<div class="for_deta_wrapp" id="detailsforumsect">
							
						</div>
					</div>				 			
				</div>
			</div>
		</div>
	</div>
	<div id="loading" style="text-align:center;"><img src="<?php echo base_url();?>assets/skin/default_skin/css/preloader.gif"></div>
	<div class="loading-data" style="text-align:center;"></div>	
</div>	
<!-- upgrade page section -->

<!-- reply popup -->
<div id="reply_pop" class="same_pop modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title">Reply Discussion Topic</h4>
      </div>
      <div class="modal-body">
			<form method="post" action="" id="forumreply" name="forumreply" enctype="multipart/form-data">
				<div class="col-md-12 forum_box">
					<?php 
					$isExist = checkIsExist($user_id);
					
					if($isExist[0]['user_nickname'] !='')
					{ ?>
					<div class="form-group" style="display: none;">
						<label for="">Nick Name<span class="aster">*</span></label>
						<input placeholder="Enter Nick Name" maxlength="10" id="nickname" name="nickname" class="form-control" type="text">
						<!-- <span id="nicknameinfo" class="validation"></span> -->
					</div>
					<?php }else{?>
						<div class="form-group">
						<label for="">Nick Name<span class="aster">*</span></label>
						<input placeholder="Enter Nick Name" maxlength="10" id="nickname1" name="nickname" class="form-control" type="text">
						<span id="nicknameinfo" class="validation"></span>
					</div>
					<?php } ?>
					<div class="form-group">
						<label for="">Description<span class="aster">*</span></label>
						<textarea placeholder="Enter Description" id="description" name="description" class="form-control"></textarea>
						<span id="descriptioninfo" class="validation"></span>
					</div>
					<input id="topic_id" name="topic_id" class="form-control" type="hidden" value="<?php echo ($topic_id ? $topic_id : ''); ?>">			
					<!-- <div class="form-group">
						<label for="">Email Id<span class="aster">*</span></label>
						<input placeholder="Enter Email Id" id="" name="" class="form-control" type="text">
						<span id="" class="validation"></span>
					</div> -->
					<div class="login_btn"><a href="javascript:void(0);" id="comment_btn" name="comment_btn">Submit</a></div>
				</div>
			</form>
      </div>
	  <div class="clearfix">&nbsp;</div>
    </div>
  </div>
</div>

<script>
$(document).on("click","#reply_pop .close",function() {
	$("#topic_id").val("");
	$("#nickname").val("");
	$("#nicknameinfo").html("");  	
	$("#description").html("");  	
	$("#descriptioninfo").html("");	

	document.getElementById("forumreply").reset();

});
$('#comment_btn').on('click', function () {
	
	var nickname = $("#nickname1");
	var nicknameinfo = $("#nicknameinfo");
	
	var description = $("#description");
	var descriptioninfo = $("#descriptioninfo");
	
	 
	var flag = 1;
	
	
	if(!validateEmpty(description, descriptioninfo, "Description")) {
				flag = 0;
	}
	if(nickname.val() == ""){
				if(!validateEmpty(nickname, nicknameinfo,"nickname")){
						flag = 0;
				}else{
					flag = 1;
				}

		}
		
	var formData = new FormData($('#forumreply')[0]); 
	if(flag){
		 $("#comment_btn").addClass("sending");
		$.ajax({
				type: "POST",
				dataType: "json",		
				//mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>frontcontroller/save_comment",
				data: formData 
				
		}).success(function (json) {
			
			$("#comment_btn").removeClass("sending");
			if(json.status=='error'){
				if(json.result == 'notexist' || json.result == 'inactive')
				{
					$("#message_result").html("<div class='alert alert-danger'>"+json.message+"</div>");	
					$('#reply_pop').modal('hide');
						setTimeout(function() {
			            $('#message_result').fadeOut('slow');
			            // forumTopicListing();
			            window.location.href = '<?php echo base_url(); ?>frontcontroller/forum_and_feedback';
			            }, 5000);


				}else{
					$("#message_result").html("<div class='alert alert-danger'>"+json.message+"</div>");
				}
				
			}
			else{
				$("#message_result").html("<div class='alert alert-success'>"+json.message+"</div>");
				setTimeout(function() {
	            $('#message_result').fadeOut('slow');
	            forumTopicListing();
	            }, 5000);
				$("#nickname").val('');
				$("#description").val('');
				$('#reply_pop').modal('hide');
				
	            
				return false;
			}
		});
	}
});

$('document').ready(function(){
	$(".events_inner .scrollTo-demo .demo-y").each(function( index ) {
		var height = $(this).height();
		if(height < 200){
			$(this).removeClass("demo-y");
		}
	});
	forumTopicListing();
})

function forumTopicListing(){
	var topic_id = '<?php echo $topic_id;?>';
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	//alert(topic_id);
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>frontcontroller/forumTopicListing",
			data: {'topic_id':topic_id}
		}).success(function (json) {
			$('#detailsforumsect').html(json.html);
			$("#loading").hide();
			$(".loading-data").html('');
		});
}
</script>
