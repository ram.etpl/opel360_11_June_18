
<!--contact page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_contact.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">	
			<div class="row">
				<div class="col-lg-12">
					<div class="contact_title">Contact Us</div>
				</div>
			</div>	
		</div>	
	</div>
	
	<div class="contact_wrapper">
		<div class="container">
			<div class="row">
			
				<!--<div class="col-md-12 contact_details">
Auto Germany Pte Ltd is the Sole Dealer for Opel passenger and commercial vehicles in Singapore. We're committed to bringing a high level of service quality to all our customers.

				</div>-->
				<div class="contact_inner">				
					<div class="col-md-6">
						<div class="cont_box_wra">
							<div class="con_pgno_wrapper">
								<div class="con_title"><i class="fa fa-phone-square"></i>Contact No</div>
								<div class="sale_wrapper">
									<div class="col-md-4 lt_sale">
										<div class="sal_name">» sales</div>
										<div class="salno"><a href="tel:69223288">6922 3288</a></div>	
									</div>
									<div class="col-md-4">
										<div class="sal_name">» Service</div>
										<div class="salno"><a href="tel:69223282">6922 3282</a></div>
									</div>
									<div class="col-md-4 rt_sale">
										<div class="sal_name">» Parts</div>
										<div class="salno"><a href="tel:69223285">6922 3285</a></div>
									</div>
								</div>
							</div>
							<div class="con_pgno_wrapper">
								<div class="con_title"><i class="fa fa-sitemap" aria-hidden="true"></i>Sites</div>
								<div class="sale_wrapper">
									<div class="col-md-4 lt_sale">
										<div class="salno">» opel.com.sg</div>
									</div>
									<div class="col-md-8 rt_sale">
										<div class="salno">» autogermany.sg/support</div>
									</div>
								</div>
							</div>
							<div class="con_pgno_wrapper">
								<div class="con_title"><i class="fa fa-map-marker" aria-hidden="true"></i>Address</div>
								<div class="sale_wrapper">
									<div class="col-md-12 lt_sale">
										<div class="salno">» 7 Ubi Close, Singapore 408604</div>
									</div>									
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="contact_map_section">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7359899506487!2d103.89641914911503!3d1.334599061994207!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da17f1c3136021%3A0xd068dae23523e80!2s7+Ubi+Cl%2C+Singapore+408604!5e0!3m2!1sen!2sin!4v1468932070473" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>			
				</div>
				
		    </div>
		</div>
	</div>
		
</div>
<!-- contact page section -->
