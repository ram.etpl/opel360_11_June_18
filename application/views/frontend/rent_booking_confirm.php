<!--all rent section -->
<div class="rent_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/profile_banner.jpg" alt=""/>
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Confirm Booking</div>
				</div>
			</div>
		</div>	
	</div>	
<!--customer profile section-->	
	<div class="news_wrapper_all" id="rent_car_section">
		<div class="container">
			<div class="col-md-12">
				<div class="rent_middle_wrapper conf_rent_book">
					<div class="rent_title">Booking Details</div>
					<div class="pick_wrapper">
						<label for="usrname"><i class="fa fa-map-marker" aria-hidden="true"></i>Pickup</label>
						<div class="pick_text">
								Ang Mo Kio
						</div>
						<div class="da_time_wra">
							<div class="form-group col-md-6 date_full">
								<label class="control-label"><i class="fa fa-calendar" aria-hidden="true"></i>Pickup Date</label>
								<div class="pick_text">
									11-12-2016
								</div>
							</div>
							<div class="form-group col-md-6 time_full">
								<label class="control-label"><i class="fa fa-clock-o" aria-hidden="true"></i>Pickup Time</label>
								<div class="pick_text">
								10.00 Am
								</div>
							</div>
						</div>
					</div>
					<div class="pick_wrapper return_wrapper">
						<label for="usrname"><i class="fa fa-map-marker" aria-hidden="true"></i>Return</label>
						<div class="pick_text">
							Clementi,Singapore
						</div>
						<div class="da_time_wra">
							<div class="form-group col-md-6 date_full">
								<label class="control-label"><i class="fa fa-calendar" aria-hidden="true"></i>Return Date</label>
								<div class="pick_text">
									11-12-2016
								</div>
							</div>
							<div class="form-group col-md-6 time_full">
								<label class="control-label"><i class="fa fa-clock-o" aria-hidden="true"></i>Return Time</label>
								<div class="pick_text">
									12.00 Am
								</div>
							</div>
						</div>
					</div>
					<div class="car_catego">
						<div class="form-group">
							<label for="usrname"><i aria-hidden="true" class="fa fa-car"></i>Car Category</label>
							<div class="pick_text">
								Combo-D Van (N1)
							</div>									
						</div>
					</div>
					<div class="book_car">
						<a id="" href=""><span><i aria-hidden="true" class="fa fa-paper-plane"></i></span>Confirm Booking</a>								
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>	
<!--customer profile section-->	

</div>
