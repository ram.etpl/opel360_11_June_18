<style>
.next_btn a:not(.disabled):before {
  content: '';
  position: absolute;
  right: 10px;
  top: 10px;
  width: 16px;
  height: 16px;
  border: 2px solid;
  border-left-color: transparent;
  border-radius: 50%;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-transition-duration: 0.5s;
  -o-transition-duration: 0.5s;
  -webkit-transition-duration: 0.5s;
  transition-duration: 0.5s;
  -moz-transition-property: opacity;
  -o-transition-property: opacity;
  -webkit-transition-property: opacity;
  transition-property: opacity;
  -moz-animation-duration: 1s;
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -moz-animation-iteration-count: infinite;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -moz-animation-name: rotate;
  -webkit-animation-name: rotate;
  animation-name: rotate;
  -moz-animation-timing-function: linear;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
}
.next_btn a:not(.disabled):after {
  content: '';
  display: inline-block;
  height: 100%;
  width: 0px;
  -moz-transition-delay: 0.5s;
  -o-transition-delay: 0.5s;
  -webkit-transition-delay: 0.5s;
  transition-delay: 0.5s;
  -moz-transition-duration: 0.75s;
  -o-transition-duration: 0.75s;
  -webkit-transition-duration: 0.75s;
  transition-duration: 0.75s;
  -moz-transition-property: width;
  -o-transition-property: width;
  -webkit-transition-property: width;
  transition-property: width;
}
.next_btn a:not(.disabled).sending {
  pointer-events: none;
  cursor: not-allowed;
}
.next_btn a:not(.disabled).sending:before {
  -moz-transition-delay: 0.5s;
  -o-transition-delay: 0.5s;
  -webkit-transition-delay: 0.5s;
  transition-delay: 0.5s;
  -moz-transition-duration: 1s;
  -o-transition-duration: 1s;
  -webkit-transition-duration: 1s;
  transition-duration: 1s;
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
.next_btn a:not(.disabled).sending:after {
  -moz-transition-delay: 0s;
  -o-transition-delay: 0s;
  -webkit-transition-delay: 0s;
  transition-delay: 0s;
  width: 20px;
}

@keyframes rotate {
  0% {
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<!--about page section -->
<div class="service_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url(); ?>images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">	
			<div class="row">
				<div class="col-lg-12">
					<div class="about_title">Service Booking<button type="button" id="view_cur_booking" class="btn-yellow-current" data-toggle="modal" data-target="#currentBookingModal"><span class="info_cir"><i class="fa fa-info-circle" aria-hidden="true"></i></span>Current Bookings</button></div>
				</div>	
				<div class="col-lg-12 current_book_pop">
								
				</div>
			</div>			
			
		</div>	
	</div>
	
	<div class="service_wrapper serv_booking">
		<div class="container">
			<div class="col-md-12">
			
				<ul class="tabs">
					<li class="tab col" id="tab_step1"><a class="active" href="#stp1">Step 1</a></li>
					<li class="tab col" id="tab_step2"><a href="#stp2">Step 2</a></li>
					<li class="tab col" id="tab_step3"><a href="#stp3">Step 3</a></li>
				</ul>
				</div>
				<div class="all_tabs_wrapper col-md-12">
					<!--<form action="<?php echo base_url();?>booking-action" id="frmBookingAction" name="frmBookingAction" method="post">-->
					<div id="stp1" class="step1_wrapper">						
						<div class="form-group select_drop">
							<label class="col-md-2">Vehicle No<span class="aster">*</span></label>
							<div class="col-md-3">
							<?php if($vehicle_list){
								?>
								<select id="veh_reg_no" name="veh_reg_no" class="select-sm form-control">
								<?php
								foreach($vehicle_list as $list):
							?> 
								<option value="<?php echo $list['registration_no']; ?>"><?php echo $list['registration_no']; ?></option>
								
							<?php endforeach;
							?>
							</select>
							<?php
							}
							?>
							</div>
						</div>
						<div class="form-group select_drop">
							<label class="col-md-2">Choose Action<span class="aster">*</span></label>
							<div class="col-md-3">
								<select id="book_action" name="book_action" class="select-sm form-control">
									<option value="I">Add New Booking</option>
									<option value="U">Change Current Booking</option>
									<option value="C">Cancel Booking</option>
								</select>
							</div>
						</div>
						<div class="form-group" id="ref_no">
								<label class="col-md-2 ref_no">Reference Number<span class="aster">*</span></label>
								<div class="col-md-3">
									<input class="form-control" type="text" name="txt_ref_no" id="txt_ref_no" />
									<span id="refInfo" class="validation"></span>
								</div>
						</div>
						<div class="form-group">
								<label class="col-md-2 empty_label">&nbsp;</label>
								<div class="next_btn col-md-3">
									<a id="step_1" name="step_1" href="javascript:void(0);" >Next</a>
									<div id="loading1"></div>
								</div>
						</div>
						<div class="col-md-4 step1_error_msg">
							<div id="errmsg"></div> 
							<div id="bexistsinfo"></div> 
						</div>
						<div class="col-md-12">
							<div id="message_booking"></div> 
						</div>
					</div>
					<!--</form>-->
					
					<div id="stp2" class="step2_wrapper">
						<div class="col-md-6 fist_colu">
						
							<div class="cust_tab_wrap">								
							</div>							
							<div class="cust_tab_wrap_1">
								<div class="form-group select_drop">
									<label class="col-md-5">Select Booking Date -<span class="required">*</span></label>
									<div class="col-md-6">
										<button type="button" class="btn btn-info btn-lg cal_click" data-toggle="modal" data-target="#myModal">Select Date</button>
										<span id='selected_date'></span>
										<span id="bookDateInfo" class="validation"></span>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Select Booking Mileage -<span class="required">*</span></label>
									<div class="col-md-6 re_wid">
										<select id="booking_milage" name="booking_milage" class="select-sm form-control">
											<?php for($i=10;$i<=120;$i++){?>
											<option value="<?php echo $i;?>"><?php echo $i;?>k</option>
											<?php } ?>
										</select>
										<span id="bookMilageInfo" class="validation"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-5">Remarks -</label>
									<div class="col-md-6 re_wid">
										<textarea id="remarks" name="remarks"></textarea>
									</div>
								</div>								
							</div>
							
						</div>						
						<div class="next_btn col-md-12">
							<a id="submit_calender" name="submit_calender" href="javascript:void(0);" >Next</a>
							<a id="step_2_prev" name="step_2_prev" href="javascript:void(0);" class="previ">Previous</a>
						</div>
					</div>
					
					<div id="stp3" class="step3_wrapper">
						<div class="col-md-6 fist_colu">
							
							<div class="cust_tab_wrap">
								<div class="cus_head_book">Customer Details-</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Customer Name-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text"></div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Vehicle No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text"></div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Customer HP No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text"></div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Reference No.-</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text"></div>
									</div>
								</div>
							</div>
							<div class="cust_tab_wrap_1">
								<div class="cus_head_book"><span>Booking Details</span></div>
								<div class="form-group select_drop">
									<label class="col-md-5">Booking Date & Time -</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text" id="booking_date_info"></div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Booking Mileage -</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text" id="milage_info"></div>
									</div>
								</div>
								<div class="form-group select_drop">
									<label class="col-md-5">Remarks -</label>
									<div class="col-md-7 rt_cust">
										<div class="view_text" id="remark_info"></div>
									</div>
								</div>
							 </div>
							 
							<div class="next_btn col-md-12">
								<a id="step_confirm_submit" name="step_confirm_submit" href="javascript:void(0);">Confirm & Submit</a>
								<a id="step_prev_step3" name="step_prev_step3" href="javascript:void(0);" class="previ">Previous</a>
								<div id="loading2"></div>
							</div>
						
						</div>
						
						
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
	
	<!-- servic calender Modal -->
	<div id="myModal" class="modal fade modal-lg" role="dialog">
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
			<h4 class="modal-title">OPEL Service Booking Calender</h4>
		  </div>
		  <div class="modal-body">
			<div class="cale_color">
				<div class="color_wrapper">
					<label>Available -</label>
					<span class="color_box"></span>
				</div>
				<div class="color_wrapper">
					<label>Not-Available -</label>
					<span class="color_box" style="background-color:gray;"></span>
				</div>
				<div class="rt_color_box">
					<div class="color_wrapper">
						<label>Selected -</label>
						<span class="color_box" style="background-color:green;"></span>
					</div>
					<div class="color_wrapper">
						<label>After selecting slot, close the calender to proceed to next</label>
						
						
					</div>
				</div>
			</div>
			<div id="calendar"></div>
		  </div>
		</div>
	  </div>
	</div>
	<div id="currentBookingModal" class="modal fade modal-lg" role="dialog">
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
			<h4 class="modal-title">OPEL Service Current Booking</h4>
		  </div>
		  <div class="modal-body">
			<?php echo $cur_booking_data;?>
		  </div>
		</div>
	  </div>
	</div>
</div>
<div id="available_slots"></div>
<input type="hidden" name="bkt" id="bkt"/>
<input type="hidden" name="ex_ref_no" id="ex_ref_no"/>
<!--about page section-->
<!--material js & css-->

<!--<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/moment.min.js'></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery.min.js'></script> 
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/fullcalendar.min.js'></script>-->

<link rel="stylesheet" href="<?php echo base_url();?>js/fullcalendar-2.5.0/fullcalendar.css">
<script src='<?php echo base_url(); ?>js/fullcalendar-2.5.0/lib/moment.min.js'></script>
<script src='<?php echo base_url(); ?>js/fullcalendar-2.5.0/fullcalendar.js'></script>
<!--<script src='<?php echo base_url(); ?>js/fullcalendar-2.5.0/fullcalendar.min.css'></script>-->

<link rel="stylesheet" href="<?php echo base_url();?>css/materialize.min.css">
<script src="<?php echo base_url();?>js/materialize.min.js"></script>
<style>.fc-other-month,.fc-prev-button{visibility: hidden}</style>
<script>
	 
$("#tab_step2").addClass("disabled","disabled");
$("#tab_step3").addClass("disabled","disabled");
$("#ref_no").hide();

$('#view_cur_booking').on('click', function () {
var veh_no = $("#veh_reg_no").val();

$("#currentBookingModal .modal-body").html("<div style='text-align:center'>Please wait...</div>");
$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>frontcontroller/viewCurrentBooking",
			data: {"veh_no":veh_no},
		}).success(function (json) { 
			if(json.status == "success"){ 
				
				
				$("#currentBookingModal .modal-body").html(json.result_val);
			}
		});

});

$("#book_action").on('change', function () { 	
	var action = $(this).val();
	if(action == 'I'){
		$("#ref_no").hide();
		$("#errmsg").html("");
		
	}
	else{
		$("#ref_no").show();
		$("#errmsg").html("");
		$("#step_1").removeClass("sending");
	}
	
});


$('#step_confirm_submit').on('click', function () { 
	var veh_no = $("#veh_reg_no").val();
	var ex_ref_no = $("#ex_ref_no");
	//alert($("#ex_ref_no").val());
	//return false;
	var book_action = $("#book_action").val();
	var book_slot = $("#book_slot");
	var booking_milage = $("#booking_milage");
	var remarks = $("#remarks");
	var bookMilageInfo = $("#bookMilageInfo");
	//alert(veh_no);
	$("#loading1").show();
	
	var flag = 1;
	if(flag){
	$("#refInfo").html("");	  
	$(this).addClass("sending");
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>booking-confirm",
			data: {"veh_no":veh_no,"ref_no":ex_ref_no.val(), "book_action":book_action, "book_slot":book_slot.val(),"booking_milage":booking_milage.val(),"remarks":remarks.val()},
		}).success(function (json) {
			$('#step_confirm_submit').removeClass("sending");
			if(json.status == "success"){
				$("#errmsg").show();
				
				$("#message_booking").html(json.message_booking);
				$("#loading1").html("");
				//$("#tab_step1 a").click();
				setTimeout(function(){
					$("#tab_step1 a").click();
				}, 200);
				$("#tab_step1").removeClass("disabled","disabled");
				$("#tab_step2").addClass("disabled","disabled");
				$("#tab_step3").addClass("disabled","disabled");
			}
			
			/*if(json.booking_action == "I"){
				
				$(".cust_tab_wrap").html(json.customer_html);
				$("#errmsg").html(""); 
				$("#loading1").html("");
				
				$("#tab_step2 a").click();
				//$('ul.tabs').tabs('tab', 'tab_step2');
				
				$("#tab_step1").addClass("disabled","disabled");
				$("#tab_step2").removeClass("disabled","disabled");
				$("#tab_step3").addClass("disabled","disabled");
				 
			}*/
			//$("#ex_ref_no").val(""); 
		});
	}
});

$('#step_1').on('click', function () { 
	var veh_no = $("#veh_reg_no").val();
	var book_action = $("#book_action").val();
	//alert(veh_no);
	$("#loading1").show();
	
	var flag = 1;
	if(book_action != "I"){
		
		if($("#txt_ref_no").val() == ""){
			$("#refInfo").html("Please enter a reference number.");
			flag=0;
		}
		var txt_ref_no  = $("#txt_ref_no").val();
	}
	else{
		txt_ref_no = "";
		$("#refInfo").html("");
		flag=1;
	}
	if(flag){
	$("#refInfo").html("");	  	
	$(this).addClass("sending");
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>booking-action",
			data: {"veh_no":veh_no,"book_action":book_action,"ref_no":txt_ref_no},
		}).success(function (json) {
			$("#step_1").removeClass("sending");
			if(json.status=='failed'){
				$("#errmsg").show();
				$("#loading1").html("");
				$("#errmsg").html(json.message);
				$("#bexistsinfo").html(json.result_val);
				$("#message_booking").html(""); 
			}
			else if(json.booking_action == "I" && json.message == ""){
				$("#bkt").val("I");
				$(".cust_tab_wrap").html(json.customer_html); 
				$("#errmsg").html(""); 
				$("#loading1").html("");
				setTimeout(function(){
					$("#tab_step2 a").click();
				}, 200);
				
				//$('ul.tabs').tabs('tab', 'tab_step2');
				$("#tab_step1").addClass("disabled","disabled");
				$("#tab_step2").removeClass("disabled","disabled");
				$("#tab_step3").addClass("disabled","disabled");
			}
			else if(json.message != "" && json.booking_action != "I"){
				$("#errmsg").show();
				$("#errmsg").html(json.message);
				$("#loading1").html("");
				$("#bexistsinfo").html("");
				setTimeout(function(){
					$("#tab_step2 a").click();
				}, 200);
			}
			else if(json.booking_action == "U"){
				$("#bkt").val("U");
				$("#bexistsinfo").html("");
				$(".cust_tab_wrap").html(json.customer_html); 
				$("#tab_step1").addClass("disabled","disabled");
				$("#tab_step2").removeClass("disabled","disabled");
				$("#tab_step3").addClass("disabled","disabled");
					$('#remarks').html(json.remark);
					$('#booking_milage').val(parseInt(json.milage));
					$('#booked_time_slot').html(json.booked_date);
					$('#ex_ref_no').val(json.ref_no);
					setTimeout(function(){
						$("#tab_step2 a").click();
					}, 300);
			}
			   
		});
	}
});


$('#submit_calender').on('click', function () { 
	var veh_no = $("#veh_reg_no").val();
	var book_slot = $("#book_slot");
	//alert($("#ex_ref_no").val());
	//var ex_ref_no  = $("#ex_ref_no");
	var from_time = $("#from_time");
	var booking_milage = $("#booking_milage");
	var remarks = $("#remarks");
	var bookMilageInfo = $("#bookMilageInfo");
	var bkt = $("#bkt");
	//alert(veh_no); 
	
	var flag = 1;
	
	if($("#available_slots").html() == ""){
		$("#bookDateInfo").html("Please select a booking date.");
		flag=0;
	}
		
	if(flag){
	$("#submit_calender").addClass("sending");
	$("#bookDateInfo").html("");	 
	//$("#loading1").html('<b>Please Wait...</b>');	
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>booking-confirm-next",
			data: {"bkt":bkt.val(),"veh_no":veh_no, "from_time":from_time.val(),"book_slot":book_slot.val(), "booking_milage":booking_milage.val(), "remarks":remarks.val()},
		}).success(function (json) {
				$("#booking_date_info").html(json.booking_info['booking_date']);
				$("#milage_info").html(json.booking_info['booking_milage']);
				$("#remark_info").html(json.booking_info['remarks']);
				$("#submit_calender").removeClass("sending");
				setTimeout(function(){
					$("#tab_step3 a").click();
				}, 200);
	
				//$('ul.tabs').tabs('tab', 'tab_step2');
				$("#tab_step1").addClass("disabled","disabled");
				$("#tab_step2").addClass("disabled","disabled");
				$("#tab_step3").removeClass("disabled","disabled");	
		});
	}
});

jQuery(document).ready(function($) {
    /* CALENDAR */
    
$("#step_2_prev").on('click', function () { 
	setTimeout(function(){
					$("#tab_step1 a").click();
				}, 200);
	
				//$('ul.tabs').tabs('tab', 'tab_step2');
	$("#tab_step1").removeClass("disabled","disabled");
	$("#tab_step2").addClass("disabled","disabled");
	$("#tab_step3").addClass("disabled","disabled");
	$("#step_1").removeClass("sending");	
});    
$("#step_prev_step3").on('click', function () { 
	$("#submit_calender").removeClass("sending");
	$("#message_booking").html("");
	setTimeout(function(){
					$("#tab_step2 a").click();
				}, 200);
	
				//$('ul.tabs').tabs('tab', 'tab_step2');
	$("#tab_step1").addClass("disabled","disabled");
	$("#tab_step2").removeClass("disabled","disabled");
	$("#tab_step3").addClass("disabled","disabled");
		
});    

$(".cal_click").on('click', function () { 
	 //$('#calendar').fullCalendar('today');
	 setTimeout(function(){
	  $(".fc-today-button").click();
	  $(".fc-view").addClass("table-responsive");
	}, 300);
	 
});



/* START NEW CALENDAR */ 
$('#calendar').fullCalendar({
	  defaultView: 'agendaWeek',
	  allDaySlot: false,
	  header: {
			left: 'prev',
			center: 'today title',
			right: 'next'
	  },
	  axisFormat: 'HH:mm',
	  timeFormat: {
       agenda: 'H:mm{ - h:mm}'
	  },
	  minTime:'08:00',
	  maxTime:'17:00',
	  eventColor: '#ff0000',
	  //timeFormat: 'HH:mm{ - HH:mm}',
	  editable: true,
      selectable: true,
	  slotDuration: "00:15:00",
	  slotLabelInterval : "00:15:00",
	  selectHelper: true,
	  dragScroll:false,
	  //multi:0,
	  select: function (start, end) {
				//$(this).css('background-color', 'green');
				
				/*var humanSelection = true;
				return function(start, end) {
					if(humanSelection) {
						humanSelection = false;
						$('#calendar').fullCalendar('select', start);
						humanSelection = true;
					}
				};*/
				/*if(start.isBefore(moment())) {
					
					$('#calendar').fullCalendar('unselect');
					return false;
				}*/
                //start.backgroundColor =  '#cccccc';
                /*var selectionStart = moment(start);
				//daysToAdd = 2;
				//myDate.setDate(myDate.getDate() + daysToAdd);
				var today = moment(); // passing moment nothing defaults to today
				
				if (selectionStart < today) {
					
					$('#calendar').fullCalendar('unselect');
					return false;
				} 
				
				//var check = $.fullCalendar.formatDate(start,'yyyy-MM-dd');
				//var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
				
				/*var start=moment(start).format('YYYY-MM-DDTHH:mm:ssZ'); 
				var end=moment(end).format('YYYY-MM-DDTHH:mm:ssZ'); 
				
				if(start < end)
				{
					// Previous Day. show message if you want otherwise do nothing.
							// So it will be unselectable
				}
				else
				{
					// Its a right date
							// Do something
							alert("test");
				}*/
				var bookdate = start.toISOString().slice(0, 19); 
				
				var from_time = start.toISOString().slice(11, 16); 
				
				booktime_date = bookdate.slice(0,10);
				booktime_time = bookdate.slice(11,16);
				
				$("#selected_date").html(booktime_date+" at "+booktime_time);
				$('#available_slots').html('<input type="hidden" id="book_slot" name="book_slot" value="' + bookdate + '">');
				$('#available_slots').append('<input type="hidden" id="from_time" name="from_time" value="' + from_time + '">');

      }, 
        
      editable: true,
});
});

$( window ).load(function() {
 var events=new Array();     
	
	$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>frontcontroller/getUnAvailableTimeSlot",
				data: {},
			}).success(function (json) {
				
				$( json.array_block ).each(function( index ) {
					events.push(json.array_block[index]);
					//events.push(selectable: true);
				});
				
				$('#calendar').fullCalendar('addEventSource',events);
				/*$('#calendar').fullCalendar({
					select: (function() {
						var humanSelection = true;
						return function(start, end, jsEvent, view) {
							if(humanSelection) {
								humanSelection = false;
								//$('#calendar').fullCalendar('select', start);
								//humanSelection = true;
							}
						};
})(),
					});*/
				
				
				 
			});
	
	
});
</script>
