<!--about page section -->
<div class="serexclu_section_terms">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/models_banner.jpg" />
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">	
			<div class="row">
				<div class="col-lg-12">
					<div class="about_title">Terms And Conditions</div>
				</div>
			</div>	
		</div>	
	</div>
	
	<div class="service_wrapper">
		<div class="container">
		  <div class="row">
			<div class="col-md-12">
	
				<!--<div class="serexc_head">TERMS & CONDITIONS FOR COMMUNITY GALLERY:</div>
					<p>Your uploads should not in any way be violent or pornographic, or promote any religion, and will not harm the dignity or any other rights of any third party. Please do not feature other car brands in your posts. When you upload the images to the page, you hereby grant the Company (and its licensees, advertising agencies and promotion agencies) and the employees, agencies and authorized representatives of each and all of them (collectively, "Authorized Persons"), the unrestricted, perpetual, worldwide, non-transferable, royalty-free right and license to display, exhibit, transmit, reproduce, record, digitize, modify, alter, adapt, create derivative works, exploit and otherwise use and permit others to use the Images (including, all copyrights in the User Content) in connection with the Company's marketing, advertising and promotions. We reserve the right to remove any information / images which we consider to be unlawful, offensive, threatening, libellous, defamatory, pornographic or otherwise objectionable.
					</p>
					<p>-->
					 
				<div class="serexc_head">TERMS & CONDITIONS FOR SITE / APPLICATION:</div>
					<p>
						Opel will use reasonable efforts to ensure that the contents of this site / application are accurate and up to date but not does accept any liability for any claims or losses arising from a reliance upon the contents of this site / application. Some of the information may not be correct due to product / price / promotional changes which may have occurred since it was launched. Some of the equipment / accessories / prices / products described or shown may only be available in certain countries or may be available only at extra cost. Opel reserves the right to change product specifications at any time. For the actual information / specifications, please consult Opel.
					</p>				
				<div class="serexc_head">INDEMNITY:</div>
					<p>
						You agree to indemnify, defend, hold harmless Auto Germany Pte Ltd, its directors, officers, employees, consultants, agents, and affiliates, from any and all third party claims, liability, damages and / or costs (including but not limited t o legal fees) arising from your use of this Site or your breach of the Terms & Conditions. 
					</p>					
			
			
	   </div>		
	 </div>
 </div>
	
	
	   
<script type="text/javascript"> 
    $(document).ready(function () {
		
        $('#services_tb').each(function () {
            var Column_number_to_Merge = 3;
			var Column_number_to_Merge_Year = 1;
            // Previous_TD holds the first instance of same td. Initially first TD=null.
            var Previous_TD = null;
            var Previous_TD_Year = null;
            var i = 1;
            var j = 1;
            $("tbody",this).find('tr').each(function () {
                // find the correct td of the correct column
                // we are considering the table column 1, You can apply on any table column
                var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');
                 
                if (Previous_TD == null) {
                    // for first row
                    Previous_TD = Current_td;
                    i = 1;
                }
                else if (Current_td.text() == Previous_TD.text()) {
					//alert('tet');
                    // the current td is identical to the previous row td
                    // remove the current td
                    Current_td.remove();
                    // increment the rowspan attribute of the first row td instance
                    Previous_TD.attr('rowspan', i + 1);
                    i = i + 1;
                }
                else {
                    // means new value found in current td. So initialize counter variable i
                    Previous_TD = Current_td;
                    i = 1;
                }
                
                var Current_td_year = $(this).find('td:nth-child(' + Column_number_to_Merge_Year + ')');
                //alert(Current_td_year.text());
                if (Previous_TD_Year == null) {
                    // for first row 
                    Previous_TD_Year = Current_td_year;
                    j = 1; 
                }
                else if (Current_td_year.text() == Previous_TD_Year.text()) {
					
                    // the current td is identical to the previous row td
                    // remove the current td
                    
						Current_td_year.remove();
						// increment the rowspan attribute of the first row td instance
						Previous_TD_Year.attr('rowspan', j + 1);
						j = j+ 1;
					
                }
                else { 
                    // means new value found in current td. So initialize counter variable i
                    Previous_TD_Year = Current_td_year;
                    j = 1;
                }
            });
        });
    });
</script>    
</div>
<!-- about page section -->
