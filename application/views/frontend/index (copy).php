<!-- start Banner section -->
			<div class="banner_wrapper">
				<div class="banner">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<?php echo $banners; ?>
						</div>
					</div>
				</div>
			</div>
		<!-- end Banner section -->
		
<!-- start body section -->
		
		<div class="bottom_line">
			<div class="container">
				<div class="row">
					<div class="opel_wrapper">
						<div class="col-lg-12 be_wra">
							<div class="opel_title">Be An Opel Member And <span class="enjoy_title">Enjoy Great benefits</span></div>
						</div>
						 
					</div>
				</div>
			</div>	
		</div>		
		
		<div class="white_wrapper">	
			<div class="container">
				<div class="row">
					<div class="col-lg-12 news_home_wrapper">						
						
						<div class="news_wrapper">
							<div class="news_head"><img src="images/news_icon.png" alt=""/>Latest News</div>
							<div id="nt-example1-container">
								<ul id="nt-example1">							
									<?php echo $news;?>						
								</ul>
							</div>
						</div>
						<div class="events_wrapper">						
							<div class="news_head"><img alt="" src="images/event_icon.png">Events</div>
							<div class="events_inner upcom_inner">
								<div class="running_head">Upcoming Events</div>					
								<div id="demo" class="showcase scrollTo-demo">
									<div class="content demo-y">								
										<?php
										echo $upcoming_events;
										?>								
									</div>
								</div>
							</div>						
						</div>	
						
					</div>	
					
					<div class="col-lg-12 car_det_wrapper">
						<div class="car_rent">Featured Car</div>
						
						<div class="featu_car_wrapper">
						<?php if(count($featured_cars)){
							foreach($featured_cars as $car):
							?>
							<div class="col-lg-4 car_wra">
								<div class="home_wrapper">
									<div class="img_rent"><img src="<?php echo base_url();?>community_gallery/<?php  echo $car['img_name']; ?>" alt=""/><div class="mask"></div></div>
								</div>
							</div>
							 
						<?php endforeach;}else{ ?>
							<div class="col-lg-4 car_wra">
								<div>No Records Available</div>
							</div>	
						<?php } ?>
						</div>
					</div>
					
				</div>	
			</div>	
		</div>		
		
<!-- end body section -->
<script src="<?php echo base_url();?>js/jquery.newsTicker.js"></script>
<script type="text/javascript">
var nt_example1 = $('#nt-example1').newsTicker({
	row_height: 95,
	//max_rows: 10,
	duration: 3000,
	prevButton: $('#nt-example1-prev'),
	nextButton: $('#nt-example1-next')
});
$( document ).ready(function() {
	 
	$(".events_inner .scrollTo-demo .demo-y").each(function( index ) {
		var height = $(this).height();
		if(height < 545){
			$(this).removeClass("demo-y");
		}
	});
});
</script>
