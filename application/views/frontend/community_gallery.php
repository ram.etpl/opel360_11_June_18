<!--all profile page section -->
<div class="gallery_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Community Gallery</div>
				</div>
			</div>
		</div>	
	</div>
	<?php //$this->load->view("frontend/incls/dashboard_menu");?>
<!--customer profile section-->	
	<div class="service_wrapper">
		<div class="container">
			<div class="row">
			<div class="col-lg-12">
				<div class="road_wrapper"> 
				
					<form class="form-horizontal" method="post" action="" id="formSurvey" name="formSurvey" enctype="multipart/form-data">		
				    <div  id="ng-app" ng-app="app" ng-cloak>
						<div ng-controller="myCtrl">
						</div>
						<div id="load_data"></div>
					    <div class="commu_gall_wrapper">
								<div ng-controller="AppController" nv-file-drop="" uploader="uploader">
								
										<div class="col-md-3 lt_comm_section">
											<div class="sel_files">Select files</div>
											<div class="new_lt_well" ng-show="uploader.isHTML5">
												<!-- 3. nv-file-over uploader="link" over-class="className" -->
												<div class="well my-drop-zone" nv-file-over="" uploader="uploader">
													Base drop zone
												</div>
											</div>
											<input type="file" nv-file-select="" uploader="uploader" multiple  /><br/>
										</div>
										<div class="col-md-9 rt_comm_section">
											<div class="uploa_comm_head">Upload Community Images</div>
											
											<div class="que_leng">Queue length: {{ uploader.queue.length }}</div>
											<div class="table-responsive">
											<table class="table table_community">
												<thead>
													<tr>
														<th width="30%">Name</th>
														<th width="10%" ng-show="uploader.isHTML5" class="text-center">Size</th>
														<th width="19%" ng-show="uploader.isHTML5" class="text-center">Progress</th>
														<th width="10%" class="text-center">Status</th>
														<th width="30%" class="text-center">Actions</th>
													</tr>
												</thead>
												<tbody>
												<tr ng-repeat="item in uploader.queue">
													<td>
														<strong>{{ item.file.name }}</strong>
														<!-- Image preview -->
														<!--auto height-->
														<!--<div ng-thumb="{ file: item.file, width: 100 }"></div>-->
														<!--auto width-->
														<div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 100 }"></div>
														<!--fixed width and height -->
														<!--<div ng-thumb="{ file: item.file, width: 100, height: 100 }"></div>-->
													</td>
													<td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
													<td ng-show="uploader.isHTML5">
														<div class="progress" style="margin-bottom: 0;">
															<div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
														</div>
													</td>
													<td class="text-center">
														<span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
														<span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
														<span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
													</td>
													<td nowrap class="three_btn three_table">
														<button type="button" class="btn-yellow-commu" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
															<span><i class="fa fa-upload" aria-hidden="true"></i></span>Upload
														</button>
														<button type="button" class="btn-yellow-commu" ng-click="item.cancel()" ng-disabled="!item.isUploading">
															<span class="glyphicon glyphicon-ban-circle"></span> Cancel
														</button>
														<button type="button" class="btn-yellow-commu" ng-click="item.remove()">
															<span class="glyphicon glyphicon-trash"></span> Remove
														</button>
													</td>
												</tr>
												</tbody>
											</table>
											</div>

									<div class="qur_progress">
										<div class="pr_title">
											Queue progress:
											<div class="progress" style="">
												<div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
											</div>
										</div>
										<div class="three_btn">
											<button type="button" class="btn-yellow-commu" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
												<span><i class="fa fa-upload" aria-hidden="true"></i></span>Upload all
											</button>
											<button type="button" class="btn-yellow-commu" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
												<span class="glyphicon glyphicon-ban-circle"></span> Cancel all
											</button>
											<button type="button" class="btn-yellow-commu" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
												<span class="glyphicon glyphicon-trash"></span> Remove all
											</button>
										</div>
									</div>
								</div>
							</div>
						 </div>
					</div>	
					</form>
				
				</div>				
			</div>				
			<div class="col-md-12">			
				<div id="no_record"></div>
				<div id="com_gallery"></div>
			</div>
			</div>
		</div>
	</div>
<!--customer profile section-->	
</div>
<!--all profile page section -->

<style>
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}
.nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
.another-file-over-class { border: dotted 3px green; }
</style>
<!-- Banner js start -->
<!-- comment this code 21-07-2016 for loading issue -->

<!-- <script src="https://nervgh.github.io/js/es5-shim.min.js"></script>
<script src="https://nervgh.github.io/js/es5-sham.min.js"></script> -->

<!-- comment this code 21-07-2016 for loading issue -->



<!--<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>-->
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/console-sham.js"></script>
<!--<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>-->
<script src="https://code.angularjs.org/1.1.5/angular.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/angular-file-upload.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/communitycontroller.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/bannerJS/directives.js"></script>
<!-- Banner js end -->

<script type="text/javascript">
	getCommunityGallery();
	
	function getCommunityGallery()
	{
		$(".loading-data").html('<b>Please wait while loading data</b>');
		$.ajax({
				type: "POST",
				dataType: "json",
				url: "<?php echo base_url(); ?>get-com-gallery",
				data: {},
			}).success(function (json) {
				$("#com_gallery").html(json.html);				
			});
	}
	
	function deleteCommunityGalleryImage(img_id)
	{
		var result = confirm("Are you sure you want to delete this image?");
		if(result){
			if(img_id != ''){ 
			$.ajax({
					type: "POST",
					dataType: "json",
					url: "<?php echo base_url(); ?>del-com-gallery",
					data: {img_id:img_id},
				}).success(function (json) {
					getCommunityGallery();
					
				});
			}
			else{
				alert('Image not exist');
			}
		}
	}

</script>
