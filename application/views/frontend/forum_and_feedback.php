<style type="text/css">
table {
    width: 100%;
    border-spacing: 0;
}

thead, tbody, tr, th, td { display: block; }

thead tr {
    /* fallback */
    width: 97%;
    /* minus scroll bar width */
    width: -webkit-calc(100% - 16px);
    width:    -moz-calc(100% - 16px);
    width:         calc(100% - 16px);
}
/* clearing float */
tr:after {  
    content: ' ';
    display: block;
    visibility: hidden;
    clear: both;
}

tbody {
    height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
}
 /* 19% is less than (100% / 5 cols) = 20% */
tbody td, thead th {
    width: 20%; 
    float: left;
}
</style>

<!--upgrade page section -->
<div class="contact_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/opel360_contact.jpg" alt=""/>
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="contact_title">Forum <span class="forum_pop"><button type="button" class="cre_btn" data-toggle="modal" data-target="#forum_pop">Create New Topic</button></span></div>
				</div>
			</div>	
		</div>	
	</div>	
		<?php //echo "<pre>";print_r($forum_topic);die; ?>
	<div class="forum_feed_page">
		<div class="container">
			<div class="row">
			<div class="contact_inner">			
				<div class="col-md-12">
					<div id="message_result"></div>
					<div class="forum_wrapp">
						<div class="table-responsive">
							<div class="forfeed_wrapp" id="forumData">
<!--  <div class="events_inner">
	<div class="running_head">Running Events</div>					
	<div id="demo" class="showcase scrollTo-demo">
		<div class="content demo-y"> -->
							
						<!-- </div>
						</div>
						</div> -->
					    </div>
					</div>
				</div>				 			
			</div>
			</div>
		</div>
	</div>
	<div id="loading" style="text-align:center;"><img src="<?php echo base_url();?>assets/skin/default_skin/css/preloader.gif"></div>
	<div class="loading-data" style="text-align:center;"></div>		
</div>
<!-- upgrade page section -->

<!-- forum popup -->
<div id="forum_pop" class="same_pop modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <h4 class="modal-title">Create New Topic</h4>
      </div>
      <div class="modal-body">
			<form method="post" action="" id="formFeedback" name="formFeedback" enctype="multipart/form-data">
				<div class="col-md-12 forum_box">
					<div class="form-group">
						<label for="">Title<span class="aster">*</span><a href="#" data-toggle="tooltip" data-placement="right" title="Topic name should be max 50 charcters" data-original-title="Topic name should be max 50 charcters">(Topic name guideline)</a></label>
						<input placeholder="Enter Title" maxlength="50" id="topic_name" name="topic_name" class="form-control" type="text">
						<span id="topicNameinfo" class="validation"></span>
					</div>
					<div class="form-group">
						<label for="">Description<span class="aster">*</span></label>
						<textarea placeholder="Enter Description" id="description" name="description" class="form-control"></textarea>
						<span id="descriptioninfo" class="validation"></span>
					</div>
					<?php 
					$isExist = checkIsExist($user_id);
					// echo "<pre>";print_r($isExist); die;
					if($isExist[0]['user_nickname'] !='')
					{ ?>
						<div class="form-group" style="display: none;">
						<label for="">Nick Name<span class="aster">*</span></label>
						<input placeholder="Enter Nick Name" maxlength="10" id="nickname" name="nickname" class="form-control" type="text" required>
						<!-- <span id="nicknameinfo" class="validation"></span> -->
					</div>
				<?php }else{ ?>
						<div class="form-group">
						<label for="">Nick Name<span class="aster">*</span></label>
						<input placeholder="Enter Nick Name" maxlength="10" id="nickname1" name="nickname" class="form-control" type="text" required>
						<span id="nicknameinfo" class="validation"></span>
					</div>
						<?php }	?>
					
					<div class="login_btn"><a href="javascript:void(0);" id="topic_btn"><span><i class="fa fa-paper-plane" aria-hidden="true"></i></span>Submit</a></div>
				</div>

			</form>
      </div>
	  <div class="clearfix">&nbsp;</div>
    </div>
  </div>
</div>

<script>
$(document).on("click","#forum_pop .close",function() {
	$("#topic_name").val("");
	$("#topicNameinfo").html("");
	$("#description").html("");  	
	$("#descriptioninfo").html("");	
	$("#nickname").val("");
	$("#nicknameinfo").html("");	

	document.getElementById("formFeedback").reset();

});
$( document ).ready(function() {
	 
	$(".events_inner .scrollTo-demo .demo-y").each(function( index ) {
		var height = $(this).height();
		if(height < 475){
			$(this).removeClass("demo-y");
		}
	});
	// $('.wlink').hide();
forumlisting();


});

function forumlisting(){
	$("#loading").show();
	$(".loading-data").html('<b>Please wait while loading data</b>');
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "<?php echo base_url(); ?>frontcontroller/forum_datalist",
		}).success(function (json) {
			$('#forumData').html(json.html);
			$("#loading").hide();
			$(".loading-data").html('');
		});
}

$('#topic_btn').on('click', function () {
	
	var topic_name = $("#topic_name");
	var topicNameinfo = $("#topicNameinfo");
	
	var description = $("#description");
	var descriptioninfo = $("#descriptioninfo");
	
	var nickname = $("#nickname1");
	var nicknameinfo = $("#nicknameinfo");
	
	var flag = 1;
	
	if(!validateEmpty(topic_name, topicNameinfo, "topic name")) {
				flag = 0;
	}
	
	if(!validateEmpty(description, descriptioninfo, "description")) {
				flag = 0;
	}
	if(nickname.val() == ""){
				if(!validateEmpty(nickname, nicknameinfo,"nickname")){
						flag = 0;
				}else{
					flag = 1;
				}

		}
	
	
	var formData = new FormData($('#formFeedback')[0]); 
	//alert(formData);
	if(flag){
		 $("#topic_btn").addClass("sending");
		$.ajax({
				type: "POST",
				dataType: "json",		
				//mimeType: "multipart/form-data",
				contentType: false,
                cache: false,
                processData: false,
				url: "<?php echo base_url(); ?>save-topic",
				data: formData 
				
		}).success(function (json) {
			
			$("#topic_btn").removeClass("sending");
			if(json.status=='error'){
				$("#message_result").html("<div class='alert alert-danger'>"+json.msg+"</div>");
			}
			else{
				$("#message_result").html("<div class='alert alert-success'>Topic created successfully.</div>");
				setTimeout(function() {
	            $('#message_result').fadeOut('slow');
	            }, 20000);
				$("#topic_name").val('');
				$("#description").val('');
				$("#nickname").val('');
				// $("#forum_pop").hide();
				$('#forum_pop').modal('hide');
			
		            forumlisting();
    			return false;
			}
		});
	}
});
</script>
