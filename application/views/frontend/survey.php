<?php //echo "<pre>";print_r('test');die; ?>
<!--all profile page section -->
<div class="gallery_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/profile_banner.jpg" alt="" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Survey</div>
				</div>
			</div>
		</div>	
	</div>
	<?php $this->load->view("frontend/incls/dashboard_menu");?>	
	<!--customer profile section-->	
	
	<div class="profile_wrapper survey_all">
		<div class="container">
		  <div class="row">
			<div class="col-lg-12 cus_name">
				<img src="<?php echo base_Url();?>images/survey_ico.png" alt=""/>Survey				
			</div>
			<?php if($access_denied!=""){ echo "<div>".$access_denied."</div>";}else{?>
			<form role="form"  name="frmsurvey" id="frmsurvey">			
				<?php if($survey_questions_data != ""){?>
				<div class="col-lg-12 survey_wrapper">
					<div class="all_sur">					
					
						<?php
							echo $survey_questions_data;
						?>						
						<div class="comm_text">
							<textarea id="comments" name="comments" placeholder="General comments here"></textarea>
						</div>
						<div class="send_btn survey_btn">
							<a href="javascript:;" id="survey-from"><span><i aria-hidden="true" class="fa fa-paper-plane"></i></span>Submit</a>						
							<span id="success-msg" class="sur_msg"></span>							
						</div>	
					</div>				
				</div>
				<?php }else{ ?>
					  <div class="col-lg-12 survey_wrapper"><div class="col-md-12 nosurvey_head">No Survey To Show</div></div>
				<?php } ?>
			</form>
			<?php } ?>
			</div>
		</div>		
	</div>
	<?php // } ?>
<!--customer profile section-->	
</div>
<!--all profile page section -->
<script type="text/javascript">
		arrQidList = new Array;
		$('.ratingstar').click( function () {
			
			var rating = $(this).attr('data-val');
			var qid = $(this).attr('data-qid');
			
			$("#qid"+qid).val(qid);
			
			$("#ratingQ"+qid).val(rating);
			
			//$("#to_list option").each  ( function() {
			   arrQidList.push ( qid);
			//});
		});

		$('.multi_choice').change( function () {
			
			var multi_choice = $(this).val();
			//alert(multi_choice);
			var qid = $(this).attr('data-id');
			//alert(qid);
			$("#qid"+qid).val(qid);
			
			$("#ratingQ"+qid).val(multi_choice);
			
			//$("#to_list option").each  ( function() {
			   arrQidList.push ( qid);
			//});
		});

		// arrYNList = new Array;
		$('.descriptive').change( function () {
			
			var descriptive = $(this).val();
			//alert(descriptive);
			var qid = $(this).attr('data-id');
			//alert(qid);
			$("#qid"+qid).val(qid);
			
			$("#ratingQ"+qid).val(descriptive);
			
			//$("#to_list option").each  ( function() {
			   arrQidList.push ( qid);
			//});
		});
		
		$('#survey-from').on('click', function (e) {
			//alert(arrQidList.length);
			if(arrQidList.length>0){
			var formData = new FormData($('#frmsurvey')[0]); 

			formData.append("qid", arrQidList);
			
			var flag = 1;
			if(flag)
			{
				//$('#survey-from').addClass('sending');
				$("#success-msg").html('');
				$.ajax({
					type: "POST",
					dataType: "json",
					//mimeType: "multipart/form-data",
					contentType: false,
					processData: false,
					url: "<?php echo base_url(); ?>addsurvey",
					data: formData
				}).success(function (json) {
						//$('#survey-from').removeClass('sending');
						$("#success-msg").show();	
						$("#success-msg").html(json.msg);	
						$("#comments").val('');
						arrQidList= [];
						for(i=0;i<json.qids; i++) { 
							if(json.status!=""){
								// $("#rate"+json.status[i]['qid']).html("You have given "+json.status[i]['rating']+" survey.");
								$("#rate"+json.status[i]['qid']).html("Thanks for your survey.");
							}
						}
						$("#success-msg").fadeOut(6000);
				});
			}
			else{
				return false;
			}
		}
		else{
			alert("Please select a survey to submit");
			return false;
		}
			
});
</script>
<script type="text/javascript">
	jQuery('#carousel ul li').click(function () {
		jQuery(this).closest('li').removeClass('selected');
		jQuery(this).closest('li').addClass('activeation');
	});
</script>
