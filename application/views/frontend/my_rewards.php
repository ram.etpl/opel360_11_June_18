<!--about page section -->
<div class="service_section">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url(); ?>images/profile_banner.jpg" alt=""/>
	</div>
	<!--inner banner section-->
	<div class="bottom_line">
		<div class="container">	
			<div class="row">
				<div class="col-lg-12">
					<div class="about_title">My Reward Points</div>
				</div>
			</div>
		</div>	
	</div>	
	<div class="service_wrapper">
		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<div class="table-responsive my_reward_table">				
				<table class="table table-bordered" id="services_tb">
					<thead>
						<tr>					
							<th class="text-center">Rewards Gained</th>
							<th class="text-center">Available Rewards</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$userdata = $this->session->userdata("auth_opeluser");
						$total_rewards = 0;
						 $available_rewards	=0;
						 //print_r($rewards);die;
						if(!empty($rewards)){						
						 $model_id = "";
						 $i=1;		
						 
						 foreach($rewards as $reward):	
						 
						 $cond1 = array("user_id" => $userdata['id']);
						 $rewards = $this->survey_model->getUsedReward($cond1);
						 $used_rewards = ($rewards[0]["reward"]?$rewards[0]["reward"]:0);
						 //echo $this->db->last_query();die;
						 //print_r($rewards);
						 
						 $available_pts = $reward["total_rewards"] - $used_rewards;
						 
						 
						 $total_rewards += $reward["total_rewards"];
						 $available_rewards += $available_pts;
						?>
						<tr>
							<td class="text-center"><?php echo "S$".$reward["total_rewards"]; ?></td> 
							<td class="text-center"><?php echo "S$".$available_pts; ?></td>
							<td class="text-center"><a href="#" data-id="<?php echo $reward['survey_id']; ?>" class="view_rewards" data-toggle="modal" data-target="#rewardsmodel" title="View Rewards Details">View Details</a></td>
							
						</tr>
						<?php $i++; 
							endforeach;
						}
						else{
						?> 
						<tr>
							<td colspan="3" align="center">No Records Available</td>
						</tr>
						<?php	
						} ?>
						
					</tbody>
				</table>
				
				</div>
			</div>
		  </div>
		</div>
	</div>		
</div>
<!-- about page section -->
<!--start change password popup-->		
			<div id="rewardsmodel" class="modal fade forgot_popup" role="dialog">
				<div class="modal-dialog modal-lg">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" id="changepwd_close" data-dismiss="modal"><i class="fa fa-times"></i></button>
							<h4 class="modal-title">Rewards Usage Details</h4>
						</div>
						<div class="modal-body">							
							<div class="table-responsive">
								<table class="table table-striped" id="services_tb">
									<thead>
										<tr>
											<th class="text-center">Rewards Used</th>
											<th class="text-center">Reason</th>
										</tr>
									</thead>
									<tbody id="view_html">										 
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
<!--end change password popup-->
<script>
$( function() {	
$('.view_rewards').on('click', function () {
				var s_id = $(this).attr('data-id'); 
				
				$.ajax({
						type: "POST",
						dataType: "json",
						url: "<?php echo base_url(); ?>view-user-rewards",
						data: {"s_id":s_id},
					}).success(function (json) {
						$("#view_html").html(json.html);
					});
});
});
</script>
