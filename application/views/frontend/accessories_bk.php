<!--all accesories section -->
<div class="access_section acc_new">
	<!--inner banner section-->
	<div class="inner_banner">
		<img src="<?php echo base_url();?>images/profile_banner.jpg" />
	</div>
	<!--inner banner section-->	
	<div class="bottom_line">
		<div class="container">			
				<div class="col-lg-12 gal_wra">
					<div class="gal_title">Accessories</div>
				</div>			
		</div>	
	</div>
	
<!--accesss section-->	
	<div class="access_wrapper">
		<div class="container">
			<div class="col-md-12 dro_wra">
			
				<?php if($models){?>
				<div class="col-md-5 sel_type">
					<div class="sel_model"><i class="fa fa-car" aria-hidden="true"></i>Select Car Model <b>-</b></div>				
					<div class="fleft">
						<select id="cd-dropdown" class="cd-select">
							<option value="-1" selected>Select Model</option>
							<?php foreach($models as $model):?>	
									<option data-model="<?php echo $model['id']?>" value="<?php echo $model['id']?>"><?php echo $model['m_name']."-".$model['propellant']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>	
				</div>
				<?php } ?> 
				<?php if($acctype){?>
				<div class="col-md-5 sel_accses">
					<div class="sel_model"><i class="fa fa-car" aria-hidden="true"></i>Select Accessories <b>-</b></div>
					<div class="fleft">
						<select id="acc-dropdown" class="cd-select">
							<option value="-1" selected>Select Accessories</option>
							<?php foreach($acctype as $acc):?>	
									<option data-acctype="<?php echo $acc['id']?>" value="<?php echo $acc['id']?>"><?php echo $acc['acc_type']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>	
				</div>
				<?php } ?>		
				<div class="search_btn col-md-2">
					<a name="searchmodel" id="searchmodel" href="javascript:void(0);"><i class="fa fa-search" aria-hidden="true"></i>Search</a>
				</div>	
			</div>
			
			<!-- -->
			
			
			<div class="gallery_wrapper">				
				<div class="container">				
					<div class="col-md-12 pding">				
						<div id="no_record"></div>
						<div class="grid">
							<div id="loading"></div>
						</div>
					</div>
				</div>
			</div>
			
			
			<!-- -->
			
		</div>
	</div>
<!--accesss section-->	
</div>

<!--all accesories page section -->
<script src="<?php echo base_url();?>js/drop_down_js/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/drop_down_js/jquery.dropdown.js"></script>
<script type="text/javascript">	
	$( function() {
		
		$( '#cd-dropdown' ).dropdown( {
			gutter : 5,
			stack : false,
			delay : 100,
			slidingIn : 100
		} );
		$( '#acc-dropdown' ).dropdown( {
			gutter : 5,
			stack : false,
			delay : 100,
			slidingIn : 100
		} );
	});
</script>
<script type="text/javascript">
	jQuery('#carousel ul li').click(function () {
		jQuery(this).closest('li').removeClass('selected');
		jQuery(this).closest('li').addClass('activeation');
	});
</script>
<script type="text/javascript" src="<?php echo base_url();?>js/simple-lightbox.js"></script>
<script> 
	$( window ).load(function() {
		
		setTimeout(function() {
			$('#searchmodel').trigger('click');
		},10);
		
		$('#searchmodel').on('click', function () {
				var m_id = $('input[name="cd-dropdown"]').val(); 
				var acc_id = $('input[name="acc-dropdown"]').val();
				$("#loading").show();
				$(".loading-data").html('<b>Please wait while loading data</b>');
				$.ajax({
						type: "POST",
						dataType: "json",
						url: "<?php echo base_url(); ?>get-accesseries-gallery",
						data: {"m_id":m_id,"acc_id":acc_id},
					}).success(function (json) {
						if(json.no_records=='0'){
							$(".grid").html("");
							$("#no_record").html(json.html);
							$(".grid").hide("");
						}
						else{
							$("#no_record").html("");
							$(".grid").show("");
							$(".grid").html(json.html);
							var $gallery = $('.lightbox').simpleLightbox();
						}
						$("#loading").hide();
						$(".loading-data").html('');
					});
		});
	});	
	
	$(function(){		
		var $gallery = $('.lightbox').simpleLightbox();
		
		$gallery.on('show.simplelightbox', function(){
			console.log('Requested for showing');
		})
		.on('shown.simplelightbox', function(){
			console.log('Shown');
		})
		.on('close.simplelightbox', function(){
			console.log('Requested for closing');
		})
		.on('closed.simplelightbox', function(){
			console.log('Closed');
		})
		.on('change.simplelightbox', function(){
			console.log('Requested for change');
		})
		.on('next.simplelightbox', function(){
			console.log('Requested for next');
		})
		.on('prev.simplelightbox', function(){
			console.log('Requested for prev');
		})
		.on('nextImageLoaded.simplelightbox', function(){
			console.log('Next image loaded');
		})
		.on('prevImageLoaded.simplelightbox', function(){
			console.log('Prev image loaded');
		})
		.on('changed.simplelightbox', function(){
			console.log('Image changed');
		})
		.on('nextDone.simplelightbox', function(){
			console.log('Image changed to next');
		})
		.on('prevDone.simplelightbox', function(){
			console.log('Image changed to prev');
		})
		.on('error.simplelightbox', function(e){
			console.log('No image found, go to the next/prev');
			console.log(e);
		});
	});	
</script>
