<?php
class Alpine_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function CheckAlphineService($postdata,$service){ 
	    
	  $post_array_string = "";
		foreach($postdata as $key=>$value) 
		{ 
		    $post_array_string .= $key.'='.$value.'&'; 
		}
	  $post_array_string = rtrim($post_array_string,'&');
	  $curl = curl_init(); 
	  
	  $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SetAlpineBooking xmlns="http://www.starvisionit.com/webservices/">
      <is_current_booking>Y</is_current_booking>
      <action>I</action>
      <info><Data><booking><COMP_CODE>OPEL</COMP_CODE><DOC_TYPE>WSB</DOC_TYPE><DEPT_CODE>ASC</DEPT_CODE><REF_NO>
</REF_NO><VEH_NO>SKG2091G</VEH_NO><CUST_CODE>1200003458</CUST_CODE><CUST_NAME>MOHAMMAD ADIB BIN MOHAMMAD
 HAMBER</CUST_NAME><TXT_MLEG></TXT_MLEG><MLEG_IN>1000</MLEG_IN><PC_IND>N</PC_IND><PYMT_MODE></PYMT_MODE
><VEH_RECV_DATE>2016-05-18T08:15:00</VEH_RECV_DATE><VEH_GRP>ZAFIRA B</VEH_GRP><MAKE_MDL>ZAFIRA 1.4 AUTO
 TURBO PANO</MAKE_MDL><COLR_CODE>GAZ</COLR_CODE><YR_MNFC>2012</YR_MNFC><CHAS_NO>W0LPD9DC4C2103974</CHAS_NO
><ENG_NO>A14NET19AK9206</ENG_NO><REG_DATE>8/8/2012 12:00:00 AM</REG_DATE></booking></Data></info>
    </SetAlpineBooking>
  </soap:Body>
</soap:Envelope>';   // data from the form, e.g. some ID number

	   
	  if($service == 'SetAlpineBooking'){
		  
			$headers = array(
                        "POST: /AG_WS_Test/AlpineCalendarService.asmx HTTP/1.1",
                        "Host: webmail.alpinemotors.sg",
                        "Content-Type: text/xml;charset=\"utf-8\"",
                        "Accept: text/xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "SOAPAction: http://www.starvisionit.com/webservices/".$service, 
                        "Content-length: ".strlen($xml_post_string),
            ); //SOAPAction: your op URL
	  }
	  
	  $soap_url = "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx?op=SetAlpineBooking";
	  //echo "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx/".$service."";die;
	  curl_setopt($curl, CURLOPT_POST, 1); //Choosing the POST method
	  curl_setopt($curl, CURLOPT_URL, $soap_url);  // Set the url path we want to call
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  // Make it so the data coming back is put into a string
	  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0); 
	  if($service == 'SetAlpineBooking'){
		   curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	  }
	  curl_setopt($curl, CURLOPT_POSTFIELDS ,$xml_post_string);  // Insert the data
	  
	  // Send the request 
	  $result = curl_exec($curl); 
	   
	  print_r($result);die;
	  // Free up the resources $curl is using 
	  curl_close($curl); 
	 
	  return $result;
	  //echo json_encode(array($result)); exit;
	}
    
}
?>
