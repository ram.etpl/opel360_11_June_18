<?php
class Accessories_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
     
    function getaccessoriesCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(accessories_id) AS cnt", FALSE);
		$this->db->from(TB_ACCESSORIES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getaccessoriesPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join =array())
    {
		$this->db->select("accessories_id AS id,accessories_name,accessories_pic,accessosries_thumb,accessories_desc,opl_car_catalog.model_name", FALSE);
		$this->db->from(TB_ACCESSORIES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getAllAccessories($cond = array(), $join =array())
    {
		$this->db->select("accessories_id AS id,accessories_name,accessories_pic,accessosries_thumb,accessories_desc,opl_car_catalog.model_name", FALSE);
		$this->db->from(TB_ACCESSORIES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAccessoriesById($cond = array())
    {
		$this->db->select("accessories_id AS id,accessories_name,accessories_pic,accessosries_thumb,accessories_desc,model_id", FALSE);
		$this->db->from(TB_ACCESSORIES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getaccessoriesTypeCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(acc_type_id) AS cnt", FALSE);
		$this->db->from(TB_ACCESSORIES_TYPE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getaccessoriesTypePerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("acc_type_id AS id,acc_type", FALSE);
		$this->db->from(TB_ACCESSORIES_TYPE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getAccessoriesTypeById($cond = array())
    {
		$this->db->select("acc_type_id AS id,acc_type", FALSE);
		$this->db->from(TB_ACCESSORIES_TYPE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAccesseriesGallery($cond = array())
    {
		$this->db->select("accessories_id AS id,accessories_name,accessories_pic,accessosries_thumb,accessories_desc,model_id", FALSE);
		$this->db->from(TB_ACCESSORIES);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
	//	echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    
    
}
?>
