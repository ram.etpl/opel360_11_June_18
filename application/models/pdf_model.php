<?php
class PDF_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getPDFManualCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(p_id) AS cnt", FALSE);
		$this->db->from(TB_PDF_MANUAL);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		return $query->result_array();
    }
    
    function getPDFManualPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("p_id AS id,p_name,p_file,p_desc,opl_pdf_manual.date_modified,opl_car_catalog.model_name,opl_car_catalog.model_id", FALSE);
		$this->db->from(TB_PDF_MANUAL);
		$this->db->join('opl_car_catalog', 'opl_pdf_manual.model_id = opl_car_catalog.model_id');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getPDFById($cond = array())
    {
		$this->db->select("p_id AS id,p_name,p_file,p_desc,date_modified,model_id", FALSE);
		$this->db->from(TB_PDF_MANUAL);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
}
?>
