<?php
class Login_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function validUser($table,$fields,$where=array(),$join=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$this->db->where("(super_admin=1 OR user_type='ADMIN')");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    function checkUserStatus($table,$fields,$where=array(),$join=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    }
    
    public function getModulePermission($userId,$module){
		$this->db->select("access_id AS id,user_id,user_management,news_and_events_management,service_cost_management,pdf_manuals_management,car_catalogue_management,accessories_catalogue_management,community_gallery_management,reward_management,survey_management,test_drive_request_management,upgrade_or_trade_in_management,feedback_management,banner_management,forum_management", FALSE);
		$this->db->from(TB_USER_ACCESS); 
		$this->db->where("user_id",$userId); 
		$this->db->where($module,'1'); 
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	 function validOpelUser($table,$fields,$where=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
}
?>
