<?php
class Catalog_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getAllCatalogModel($cond = array())
    {
		$this->db->select("model_id AS id,model_name,m_name,chasis_no,make,propellant,no_valves_per_cylinder,engine_capacity,number_of_cylinder,valve_train,cylinder_config,max_output,cooling,acceleration,top_speed,model_description,weight,fuel_consumption,co2_combined,date_modified", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getAllDistinctCatalogModel($cond = array())
    {
		$this->db->select("m_name,propellant,model_id as id", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCatalogModelCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(model_id) AS cnt", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    
    function getCatalogModelPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("model_id AS id,model_name,m_name,chasis_no,make,propellant,no_valves_per_cylinder,engine_capacity,number_of_cylinder,valve_train,cylinder_config,max_output,cooling,acceleration,top_speed,model_description,weight,fuel_consumption,co2_combined,date_modified", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCatalogModelById($cond = array())
    {
		$this->db->select("model_id AS id,model_name,m_name,chasis_no,make,propellant,no_valves_per_cylinder,engine_capacity,number_of_cylinder,valve_train,cylinder_config,max_output,cooling,acceleration,top_speed,model_description,weight,fuel_consumption,co2_combined,car_thumb,date_modified", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    function getCatalogModelGalleryCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(img_id) AS cnt", FALSE);
		$this->db->from(TB_CAR_GALLERY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    
    function getCatalogModelGalleryPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join =array())
    {
		$this->db->select("img_id AS id,model_name,chasis_no,".TB_CAR_GALLERY.".propellant,m_name,img_name,img_file,img_thumb,".TB_CAR_GALLERY.".date_modified", FALSE);
		$this->db->from(TB_CAR_GALLERY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    

    
    function getCarCatalogModelGalleryById($cond = array())
    {
		$this->db->select("img_id AS id,model_id,img_name,img_file,img_thumb,".TB_CAR_GALLERY.".date_modified", FALSE);
		$this->db->from(TB_CAR_GALLERY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("img_id", "desc"); 
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    function getCatalogModelGalleryById($cond = array())
    {
		$this->db->select("img_id AS id,model_id,img_name,img_file,img_thumb,".TB_CAR_GALLERY.".date_modified", FALSE);
		$this->db->from(TB_CAR_GALLERY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
	//	echo $this->db->last_query();die;
		return $query->result_array();
    }
		
		
	function getCatalogModelFirstById($cond = array())
    {
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$this->db->order_by('model_id','desc');
		$this->db->limit(1,0);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }	
    
    
    function getCatalogModelGalleryImageById($cond = array())
    {
		$this->db->select("img_id AS id,model_id,img_name,img_file,img_thumb,".TB_CAR_GALLERY.".date_modified", FALSE);
		$this->db->from(TB_CAR_GALLERY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by('img_id','desc');
		$this->db->limit(1,0);
		$query = $this->db->get();
	//	echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllDistinctCatalogModelHavingAccessories($cond = array())
    {
		$this->db->select("m_name,propellant,model_id as id", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		//$this->db->where("model_id in(Select model_id from ".TB_ACCESSORIES.")");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllDistinctCatalogModelHavingPDF($cond = array())
    {
		$this->db->select("m_name,propellant,model_id as id", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		//$this->db->where("model_id in(Select model_id from ".TB_PDF_MANUAL.")");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
}
?>
