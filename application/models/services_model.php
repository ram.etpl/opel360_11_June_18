<?php
class Services_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getAllServicesCost($cond = array())
    {
		$this->db->select("s_id AS id,service_name,service_cost,opl_service_cost.date_modified,opl_car_catalog.model_name", FALSE);
		$this->db->from(TB_SERVICE_COST);
		$this->db->join('opl_car_catalog', 'opl_service_cost.model_id = opl_car_catalog.model_id');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllServicesCostByModel($cond = array())
    {
		$this->db->select("s_id AS id,service_name,service_cost,opl_service_cost.date_modified,opl_car_catalog.model_name,opl_car_catalog.model_id", FALSE);
		$this->db->from(TB_SERVICE_COST);
		$this->db->join('opl_car_catalog', 'opl_service_cost.model_id = opl_car_catalog.model_id');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("opl_car_catalog.model_id","ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getServicesCostCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(s_id) AS cnt", FALSE);
		$this->db->from(TB_SERVICE_COST);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    
    function getServicesCostPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("s_id AS id,service_name,service_cost,opl_service_cost.date_modified,opl_car_catalog.model_name", FALSE);
		$this->db->from(TB_SERVICE_COST);
		$this->db->join('opl_car_catalog', ' opl_service_cost.model_id = opl_car_catalog.model_id');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getServicesCostById($cond = array())
    {
		$this->db->select("s_id AS id,service_name,service_cost,model_id,date_modified", FALSE);
		$this->db->from(TB_SERVICE_COST);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    /*service exclusive*/
    
    function getAllServicesExclusiveCost($cond = array())
    {
		$this->db->select("se_id AS id,milage,year,cost,opl_service_exclusive.date_modified,opl_car_catalog.model_name", FALSE);
		$this->db->from(TB_SERVICE_EXCLUSIVE);
		$this->db->join('opl_car_catalog', 'opl_service_exclusive.model_id = opl_car_catalog.model_id');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllServicesExclusiveCostByModel($cond = array())
    {
		$this->db->select("se_id AS id,milage,year,cost,opl_service_exclusive.date_modified,opl_car_catalog.model_name,opl_car_catalog.model_id", FALSE);
		$this->db->from(TB_SERVICE_EXCLUSIVE);
		$this->db->join('opl_car_catalog', 'opl_service_exclusive.model_id = opl_car_catalog.model_id');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("opl_car_catalog.model_id","ASC");
		$this->db->order_by("year","ASC");
		$this->db->order_by("milage","ASC");
		$this->db->order_by("cost","ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllServicesExclusiveCostByModelId($cond = array())
    {
		$this->db->select("se_id AS id,milage,year,cost", FALSE);
		$this->db->from(TB_SERVICE_EXCLUSIVE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("model_id","ASC");
		$this->db->order_by("year","ASC");
		$this->db->order_by("milage","ASC");
		$this->db->order_by("cost","ASC");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllModelsOfServicesExclusive($cond = array())
    {
		$this->db->select("model_id,m_name,model_name", FALSE);
		$this->db->from(TB_CAR_CATALOG);
		$this->db->where("model_id IN (select model_id FROM ".TB_SERVICE_EXCLUSIVE.")");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getServicesExclusiveCostCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(se_id) AS cnt", FALSE);
		$this->db->from(TB_SERVICE_EXCLUSIVE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    
    function getServicesExclusiveCostPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("se_id AS id,milage,year,cost,opl_service_exclusive.date_modified,opl_car_catalog.model_name", FALSE);
		$this->db->from(TB_SERVICE_EXCLUSIVE);
		$this->db->join('opl_car_catalog', ' opl_service_exclusive.model_id = opl_car_catalog.model_id');
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getServicesExclusiveCostById($cond = array())
    {
		$this->db->select("se_id AS id,milage,year,cost,model_id,date_modified", FALSE);
		$this->db->from(TB_SERVICE_EXCLUSIVE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllCatalogModelInServiceExclusive()
    {
		$this->db->select("model_id,model_name,m_name");
		$this->db->from(TB_CAR_CATALOG);
		$this->db->where("model_id in(select model_id from ".TB_SERVICE_EXCLUSIVE.")");
		
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
}
?>
