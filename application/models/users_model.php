<?php
class Users_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getAllUsers($cond = array())
    {
		$this->db->select("user_id AS id,c_name,email_address,user_type,password,actual_pwd,status,contact_number,device_type,device_id,customer_code,user_nickname", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    
    function getAllVehicleOwnerUsers($cond = array())
    {
		$this->db->select("registration_no,vehicle_group", FALSE);
		$this->db->from(TB_VEHICLE_OWNER_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getvehicleandvehicleownerdetails($cond = array()) 
    {
		
		$this->db->select("vehicle_id AS id,customer_id,user_id", FALSE);
		$this->db->from(TB_VEHICLE_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
	}
	
	
	function getserviceorderprocessexist($cond = array()) 
    {
		
		$this->db->select("s_order_process_id AS id,customer_id,user_id,s_order_id,line_number,process_code,process_description", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER_PROCESS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
	}
	
	
	
	function getserviceorderexist($cond = array()) 
    {
		
		$this->db->select("s_order_id AS id,customer_id,user_id,vehicle_id,service_order,transaction_date,vehicle_mileage", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
	}
    
    function getLastServiceOrder($cond = array()) 
    {
		
		$this->db->select("s_order_id AS id,customer_id,user_id,vehicle_id,service_order,transaction_date,vehicle_mileage", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by('s_order_id','desc');
		$this->db->limit(1,0);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
	}
	
	function getLastTenServiceOrder($cond = array()) 
    {
		
		$this->db->select("s_order_id AS id,customer_id,user_id,vehicle_id,service_order,transaction_date,vehicle_mileage", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by('s_order_id','desc');
		$this->db->limit(10,0);
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
	}
    
    
    function getCustomerVehicleById($cond = array())
    {
		$this->db->select("registration_no,vehicle_group", FALSE);
		$this->db->from(TB_VEHICLE_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getVehicleById($cond = array())
    {
		$this->db->select("vehicle_id As id,customer_id,user_id,vehicle_no,registration_date,engine_number,chassis_number,registration_no,vehicle_group,make_mode,customer_code", FALSE);
		$this->db->from(TB_VEHICLE_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    function getAllUsersExist($cond = array())
    {
		$this->db->select("customer_id AS id,user_id", FALSE);
		$this->db->from(TB_VEHICLE_OWNER_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getUserRole($cond = array(),$like = array(),$orderBy = array())
    {
		$this->db->select("role_id AS id,role,admin_access", FALSE);
		$this->db->from(TB_USER_ACCESS);
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$query = $this->db->get(); 
		
		return $query->result_array();
	}
    
    function getAdminUserAccess($cond = array())
    {
		$this->db->select("access_id AS id,user_id,user_management,news_and_events_management,service_cost_management,pdf_manuals_management,car_catalogue_management,accessories_catalogue_management,community_gallery_management,reward_management,survey_management,test_drive_request_management,upgrade_or_trade_in_management,feedback_management,banner_management", FALSE);
		$this->db->from(TB_USER_ACCESS);
		foreach($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		 
		
		$query = $this->db->get(); 
		//echo $this->db->last_query();die;
		return $query->result_array();
	}
    
    function getUsersCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(user_id) AS cnt", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    
    function getUsersPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("user_id AS id,user_type,c_name,email_address,status", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getUserById($cond = array())
    {
		$this->db->select("user_id AS id,user_type,c_name,email_address,contact_number,user_nickname,device_id", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    function getUserByIdAll()
    {
		$this->db->select("user_id AS id,user_type,c_name,email_address,contact_number,user_nickname,device_id", FALSE);
		$this->db->from(TB_USERS);
		// foreach ($cond AS $k => $v)
		// {
			$this->db->where('user_type','OPEL');
			$this->db->or_where('user_type','NONOPEL');
		//}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getUserDetailsById($cond = array(),$join = array())
    {
		$this->db->select(TB_USERS.".user_id AS id,customer_id,user_type,c_name,user_type,".TB_USERS.".email_address,vehicle_no,cust_name,phone_no,contact_number,address,rewards,car_brand,car_model,date_of_birth,cust_id,nationality,id_card_no,vehicle_history_visit,vin_no,cust_code,pdpa_consent,maritial_status", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,"LEFT");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCustomerVehicleDetailsById($cond = array(),$join = array())
    {
		$this->db->select(TB_VEHICLE_DETAILS.".vehicle_id AS id,".TB_VEHICLE_DETAILS.".customer_id,".TB_VEHICLE_DETAILS.".user_id,".TB_VEHICLE_DETAILS.".vehicle_no,registration_date,engine_number,chassis_number,registration_no,vehicle_group,make_mode", FALSE);
		$this->db->from(TB_VEHICLE_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,"LEFT");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCustomerVehicleNoById($cond = array())
    {
		$this->db->select("vehicle_id AS id,user_id,registration_no,registration_date,engine_number,chassis_number,vehicle_group,make_mode,customer_code", FALSE);
		$this->db->from(TB_VEHICLE_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCustomerVehicleNoByIdService($cond = array())
    {
		$this->db->select("vehicle_id AS id,user_id,registration_no,registration_date,engine_number,chassis_number,vehicle_group,make_mode,customer_code", FALSE);
		$this->db->from(TB_VEHICLE_DETAILS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->group_by("registration_no");
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCustomerVehicleServiceById($cond = array(),$join = array())
    {
		$this->db->select("s_order_id AS id,".TB_VEHICLE_SERVICE_ORDER.".customer_id,".TB_VEHICLE_SERVICE_ORDER.".vehicle_id,s_order_id,vehicle_no,service_order,transaction_date,vehicle_mileage", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,"LEFT");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
    function getCustomerVehicleServiceOrderProcessById($cond = array())
    {
		$this->db->select("s_order_process_id AS id,customer_id,s_order_id,line_number,process_code,process_description", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER_PROCESS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,"LEFT");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCustomerInfo($cond =array(),$join=array()){
		$this->db->select(TB_VEHICLE_OWNER_DETAILS.".user_id,cust_name,email_address,phone_no,cust_code,registration_no", FALSE);
		$this->db->from(TB_VEHICLE_OWNER_DETAILS);
		
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,"inner");
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
	}
     
}
?>
