<?php
class Survey_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    
    function getSurveyUserCount($cond = array())
    {
		$this->db->select("COUNT(survey_id) AS cnt", FALSE);
		$this->db->from(TB_SURVEY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    function getSurveyCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(survey_id) AS cnt", FALSE);
		$this->db->from(TB_SURVEY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getSurveyCountInBetween($last_date)
    {
		$this->db->select("COUNT(survey_id) AS cnt FROM ".TB_SURVEY." 
		WHERE survey_type = 'workshop survey' AND 
		('".$last_date."' BETWEEN DATE( survey_from_date ) AND DATE( survey_to_date )) ");
		
		
		//$this->db->from(TB_SURVEY);
		
		//$this->db->where($last_date." BETWEEN DATE( survey_from_date ) AND DATE( survey_to_date )");
		//$this->db->protect_identifiers(TB_SURVEY, TRUE);
		$this->db->order_by('survey_id', 'desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getGeneralSurveyInBetween()
    {
		$today = date('Y-m-d');
		//$this->db->select("COUNT(survey_id) AS cnt FROM ".TB_SURVEY." WHERE '".$last_date."' BETWEEN DATE( survey_from_date ) AND DATE( survey_to_date )");
		$this->db->select("* FROM ".TB_SURVEY." 
		WHERE 
		survey_type = 'general survey'
		AND (
		'".$today."'
			BETWEEN DATE( survey_from_date ) AND DATE( survey_to_date )
		)
		OR (
		survey_to_date = ''
		)");
		
		//$this->db->from(TB_SURVEY);
		
		//$this->db->where($last_date." BETWEEN DATE( survey_from_date ) AND DATE( survey_to_date )");
		//$this->db->protect_identifiers(TB_SURVEY, TRUE);
		$this->db->order_by('survey_id', 'desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
     function getSurveyInBetween($last_date)
    {
		$this->db->select("* FROM ".TB_SURVEY." WHERE survey_type='workshop survey' and  '".$last_date."' BETWEEN DATE( survey_from_date ) AND DATE( survey_to_date )");
		//$this->db->from(TB_SURVEY);
		
		//$this->db->where($last_date." BETWEEN DATE( survey_from_date ) AND DATE( survey_to_date )");
		//$this->db->protect_identifiers(TB_SURVEY, TRUE);
		$this->db->order_by('survey_id', 'desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    function getSurveyPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("survey_id AS id,survey_name,survey_type,award_points,survey_from_date,survey_to_date,date_modified", FALSE);
		$this->db->from(TB_SURVEY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getSurveyById($cond = array())
    {
		$this->db->select("survey_id AS id,survey_name,survey_type,award_points,survey_from_date,survey_to_date,date_modified", FALSE);
		$this->db->from(TB_SURVEY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
     
    
    function getAllCurrentVehicleServicedUserslastdate($cond = array())
    {
		$this->db->select("s_order_id AS id,customer_id,user_id,vehicle_id,service_order,transaction_date,vehicle_mileage", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("transaction_date", "desc"); 
		$this->db->limit(1);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getAllCurrentVehicleServicedUsers($cond = array())
    {
		$this->db->select("s_order_id AS id,customer_id,user_id,vehicle_id,service_order,transaction_date,vehicle_mileage", FALSE);
		$this->db->from(TB_VEHICLE_SERVICE_ORDER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
     function getAllUsers($cond = array())
    {
		$this->db->select("user_id AS id,c_name,email_address,user_type,password,status,contact_number,customer_code,device_token,device_id_android", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getSurveyRatingById($cond = array())
    {
		$this->db->select("rating_id AS id,question_id,user_id,rating", FALSE);
		$this->db->from(TB_SURVEY_USER_RATING);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }


    function getSurveyAnswerById($cond = array())
    {
		$this->db->select("answer_id AS id,question_id,user_id,answer", FALSE);
		$this->db->from(TB_SURVEY_USER_ANSWER);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getallSurveyQuestion($cond = array())
    {
		
		$this->db->select("question_id AS id,question_type,question,option_A,option_B,option_C,option_D,answer,fk_survey_id", FALSE);
		$this->db->from(TB_SURVEY_QUESTIONS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
		
		
    }
    
    function getallSurveyQuestionBySID($cond,$user_id)
    {
		//echo "<pre>";print_r($cond);
		$this->db->select("question_id AS id,question_type,question,option_A,option_B,option_C,option_D,answer,fk_survey_id", FALSE);
		$this->db->from(TB_SURVEY_QUESTIONS);
		//echo $cond;
		if($cond != "")
		{
			$i=0;
			$ex_sids = explode(",",$cond);
			//print_r($ex_sids);
			$cnt = count($ex_sids);
			
			foreach($ex_sids as $sid):			
				if($i<$cnt-1){ 
					$q = "fk_survey_id =$sid OR ";
				}
			$i++;
			endforeach;
			//echo $q;
			if($q != ""){ $q = substr($q,0,-3);}
			
			$this->db->where("(".trim($q).")");
		}
		
		$this->db->where("question !='' and question_id NOT IN(select question_id FROM ".TB_SURVEY_USER_RATING." where user_id=$user_id)");
		$this->db->order_by("fk_survey_id","desc");
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getallSurveyQuestionBySurveyId($cond,$user_id)
    {
		//echo "<pre>";print_r($cond);
		$this->db->select("question_id AS id,question_type,question,option_A,option_B,option_C,option_D,answer,fk_survey_id", FALSE);
		$this->db->from(TB_SURVEY_QUESTIONS);
		//echo $cond;
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$this->db->where("question !='' and question_id NOT IN(select question_id FROM ".TB_SURVEY_USER_RATING." where user_id=$user_id)");
		//$this->db->order_by("fk_survey_id","desc");
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getSurveyRewardsOfUser($cond = array(),$join=array())
    {
		$this->db->select("survey_id,survey_type,survey_name,award_points as total_rewards", FALSE);
		$this->db->from(TB_SURVEY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,'left');
		}
		$this->db->group_by('survey_id');
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getRewardSurveyDetails($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join =array())
    {
		$this->db->select(TB_USER_GAINED_REWARDS.".user_id as id,c_name,email_address,sum(gained_rewards) as award_points", FALSE);
		$this->db->from(TB_USER_GAINED_REWARDS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$this->db->group_by(TB_USER_GAINED_REWARDS.".user_id");
		
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"inner join");
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getUsedReward($cond = array())
    {
		$this->db->select("sum(rewards_used) as reward", FALSE);
		$this->db->from(TB_SURVEY_USED_REWARD);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->group_by(TB_SURVEY_USED_REWARD.".user_id");
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    function getUsersUsedReward($cond = array())
    {
		$this->db->select("*", FALSE);
		$this->db->from(TB_SURVEY_USED_REWARD);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllUsersForGeneralSurvey($cond = array())
    {
		$this->db->select("id,c_name,email_address,user_type,password,status,contact_number,customer_code,device_token,device_id_android", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllUsersForSurvey($cond = array())
    {
		$this->db->select("user_id,c_name,email_address,user_type,device_token,device_id_android", FALSE);
		$this->db->from(TB_USERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->or_where($k,$v);
		}
		$this->db->where("device_token !='' OR device_id_android !=''");
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllUsersForSurveyNotification()
    {
		
		
		$this->db->select("user_id,c_name,email_address,user_type,device_token,device_id_android", FALSE);
		$this->db->from(TB_USERS);
		/*foreach ($cond AS $k => $v)
		{
			$this->db->or_where($k,$v);
		}*/
		$this->db->where("(user_type='OPEL' OR user_type='NONOPEL' OR user_type='ADMIN')");
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
     function getAllUsersForSurveyWorkshopNotification()
    {
		
		
		$this->db->select("user_id,c_name,email_address,user_type,device_token,device_id_android", FALSE);
		$this->db->from(TB_USERS);
		/*foreach ($cond AS $k => $v)
		{
			$this->db->or_where($k,$v);
		}*/
		$this->db->where("(user_type='OPEL')");
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    } 
    
    
    function getAllUsersForSurveyMobileNotification()
    {
		
		
		$this->db->select("user_id,c_name,email_address,user_type,device_token,device_id_android", FALSE);
		$this->db->from(TB_USERS);
		/*foreach ($cond AS $k => $v)
		{
			$this->db->or_where($k,$v);
		}*/
		$this->db->where("(user_type='OPEL' OR user_type='NONOPEL' OR user_type='ADMIN') AND (device_token !='' OR device_id_android!='')");
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getAllSurveyNotification($cond = array())
    {
		$this->db->select("notification_id,s_id,user_id,mail_flag,push_android,push_ios,date_modified", FALSE);
		$this->db->from(TB_SURVEY_NOTIFICATION);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		} 
		
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    
    
    
    function getSurveyRewardsCount($cond = array(),$like = array(),$join=array())
    {
		$this->db->select("COUNT(sid) AS cnt", FALSE);
		$this->db->from(TB_USER_GAINED_REWARDS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,'left');
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getSurveyRewardsDetails($cond = array())
    {
		$this->db->select("sum(gained_rewards) as tot_rewards,user_id", FALSE);
		$this->db->from(TB_USER_GAINED_REWARDS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->group_by('user_id');
		  
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getUserRewardSurveyDetails($cond = array(), $join =array())
    {
		$this->db->select(TB_USER_GAINED_REWARDS.".user_id as id,sum(gained_rewards) as total_rewards", FALSE);
		$this->db->from(TB_USER_GAINED_REWARDS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"inner join");
		}
		$this->db->having('count(user_id)>0');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
}
?>
