<?php
class Gallery_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function getComGalleryVoteCount($cond = array())
    {
		$this->db->select("COUNT(like_id) AS cnt", FALSE);
		$this->db->from(TB_COMMUNITY_LIKE);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		} 
		
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
    }
    function getComModelGalleryById($cond = array(),$start)
    {
		$this->db->select("com_gal_id AS id,user_id,img_name,img_thumb,date_modified", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->limit(PER_PAGE_IMAGE,$start);
		$query = $this->db->get();
	
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    function getComModelGalleryByUserId($cond = array(),$start,$join =array())
    {
		$this->db->select("com_gal_id AS id,".TB_COMMUNITY.".user_id,img_name,img_thumb,date_modified", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,'inner');
		}
		$this->db->limit(PER_PAGE_IMAGE,$start);
		$query = $this->db->get();
	
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getComModelGalleryCountById($cond = array())
    {
		$this->db->select("count(com_gal_id) as cnt", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
	//	echo $this->db->last_query();die;
		return $query->result_array();
    }
     
     function getComModelGalleryCountByUserId($cond = array(),$join=array())
    {
		$this->db->select("count(com_gal_id) as cnt", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,'innner');
		}
		$query = $this->db->get();
	//	echo $this->db->last_query();die;
		return $query->result_array();
    }
     
     function getCatalogModelGalleryCount($cond = array(),$like = array(),$join = array())
    {
		$this->db->select("COUNT(com_gal_id) AS cnt", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    
    function getCatalogModelGalleryPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array(),$join =array())
    {
		$this->db->select("com_gal_id as id,".TB_COMMUNITY.".user_id,img_name,".TB_COMMUNITY.".date_modified,vote_by,count( ".TB_COMMUNITY_LIKE.".vehicle_id ) AS cnt", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$this->db->group_by(TB_COMMUNITY.".com_gal_id");
		
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		foreach($join AS $k => $v)
		{
			$this->db->join($k, $v,"left");
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    

    
    function getCarCatalogModelGalleryById($cond = array())
    {
		$this->db->select("com_gal_id AS id,user_id,img_name,date_modified", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->order_by("com_gal_id", "desc"); 
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCarCatalogModelGalleryByUserId($cond = array(),$join=array())
    {
		$this->db->select("com_gal_id AS id,".TB_COMMUNITY.".user_id,img_name,date_modified", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,'inner');
		}
		$this->db->order_by("com_gal_id", "desc"); 
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getNewCarModelGalleryById($cond = array(),$join = array())
    {
		$this->db->select("com_gal_id AS id,".TB_COMMUNITY.".user_id,img_name,img_thumb,date_modified", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,'inner');
		}
		$this->db->order_by("com_gal_id", "desc");
		$this->db->limit(3);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
      
}
?>
