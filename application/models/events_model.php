<?php
class Events_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }

   function getAllEvents($cond = array(),$join=array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
  function getAllRunningEvents($cond = array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_show_to,event_pic,type,is_rsvp", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->where("FIND_IN_SET('2',event_show_to) !=",FALSE);
		$this->db->order_by('event_id', 'desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }  
    
    
  function getAllUpcomingEvents($cond = array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_show_to,event_pic,type,is_rsvp", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->where("FIND_IN_SET('2',event_show_to) !=",FALSE);
		$this->db->order_by('event_id', 'desc');
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }


  //   function getAllPromotion($cond = array())
  //   {
		// $this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_show_to,event_pic,type", FALSE);
		// $this->db->from(TB_EVENTS);
		// foreach ($cond AS $k => $v)
		// {
		// 	$this->db->where($k,$v);
		// }
		// $this->db->where("FIND_IN_SET('3',event_show_to) !=",FALSE);
		
		// $query = $this->db->get();
		// return $query->result_array();
  //   }
    
    
    
   function getAllNonOpelUsersUpcomingEvents($cond = array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_show_to,event_pic,type,is_rsvp", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		//$this->db->where("FIND_IN_SET('1',event_show_to) !=",FALSE);
		$this->db->where("(FIND_IN_SET('1',event_show_to) !=0 OR FIND_IN_SET('2',event_show_to) !=0)");
		$this->db->order_by('event_id', 'desc');
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		return $query->result_array();
    }  
    
    
    
  function getAllOpelUsersUpcomingEvents($cond = array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_show_to,event_pic,type,is_rsvp", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->where("(FIND_IN_SET('0',event_show_to) !=0 OR FIND_IN_SET('2',event_show_to) !=0)");
		$this->db->order_by('event_id', 'desc');
		$query = $this->db->get();
		
		// echo $this->db->last_query();die;
		return $query->result_array();
    }  
    
    
    
  function getEventsCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(event_id) AS cnt", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getEventDetailsById($cond = array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_pic,event_show_to,date_modified", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getContestById($cond = array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_pic,event_show_to,type,is_rsvp,date_modified", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		return $query->result_array();
    }
    
    
    
   function getEventsListPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("event_id as id,event_name,event_description,start_date,end_date,status,event_pic,event_show_to,type,date_modified", FALSE);
		$this->db->from(TB_EVENTS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		//$this->db->limit(PER_PAGE, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    } 
    
    function getEventRsvpUser()
    {
        $sql = "SELECT e.event_name,e.event_description,e.start_date,e.end_date,opl_rsvp_user.* FROM (`opl_rsvp_user`) LEFT JOIN `opl_events` as e ON e.`event_id`=`opl_rsvp_user`.`event_id` ORDER BY `event_id` asc";
    	$query = $this->db->query($sql);
        return $query->result_array();
    }

    function getAllOpelUsersNotification($cond = array())
    {
		$this->db->select("count(user_id) as notification_count", FALSE);
		$this->db->from(TB_NOTIFICATION);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		//$this->db->where("FIND_IN_SET('1',event_show_to) !=",FALSE);
		$this->db->where("(FIND_IN_SET('0',user_type) !=0 OR FIND_IN_SET('2',user_type) !=0)");
		$this->db->order_by('notification_id', 'desc');
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    function getAllNonOpelUsersNotification($cond = array())
    {
		$this->db->select("count(user_id) as notification_count", FALSE);
		$this->db->from(TB_NOTIFICATION);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		//$this->db->where("FIND_IN_SET('1',event_show_to) !=",FALSE);
		$this->db->where("(FIND_IN_SET('1',user_type) !=0 OR FIND_IN_SET('2',user_type) !=0)");
		$this->db->order_by('notification_id', 'desc');
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		return $query->result_array();
    }


    function getAllOpelUsersNotificationList($cond = array())
    {
		$this->db->select("notification_id,nId,json_response,notification_type,created_date", FALSE);
		$this->db->from(TB_NOTIFICATION);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		//$this->db->where("FIND_IN_SET('1',event_show_to) !=",FALSE);
		$this->db->where("(FIND_IN_SET('0',user_type) !=0 OR FIND_IN_SET('2',user_type) !=0)");
		$this->db->order_by('notification_id', 'desc');
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		return $query->result_array();
    }

    function getAllNonOpelUsersNotificationList($cond = array())
    {
		$this->db->select("notification_id,nId,json_response,notification_type,created_date", FALSE);
		$this->db->from(TB_NOTIFICATION);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		//$this->db->where("FIND_IN_SET('1',event_show_to) !=",FALSE);
		$this->db->where("(FIND_IN_SET('1',user_type) !=0 OR FIND_IN_SET('2',user_type) !=0)");
		$this->db->order_by('notification_id', 'desc');
		$query = $this->db->get();
		
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
}
?>
