<?php
class Banner_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    
    
    function getBannersCount($cond = array(),$like = array())
    {
		$this->db->select("COUNT(banner_id) AS cnt", FALSE);
		$this->db->from(TB_BANNERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    function getBannersPerPage($cond = array(), $start = 0, $orderBy = array(),$like = array())
    {
		$this->db->select("banner_id AS id,banner_file_name,banner_file_size,banner_file_type,banner_file_last_modifieddate", FALSE);
		$this->db->from(TB_BANNERS);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($like AS $k => $v)
		{
			$this->db->or_like($k, $v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		
		$this->db->limit(PER_PAGE_OPTION, $start);
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    function getCommunityGallery($cond = array())
    {
		$this->db->select("com_gal_id AS id,user_id,img_name,img_thumb,date_modified", FALSE);
		$this->db->from(TB_COMMUNITY);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    
    
    
    
    
}
?>
