<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Survey extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("users_model");
		$this->load->model("survey_model");
		$this->load->model("login_model");
		$this->load->model("master_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->helper("email_template");
		$this->load->library("ValidateFile");
		$this->pdf_file_path =  realpath('images/PDF');
		//$this->load->library("richtexteditor"); 
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'survey_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	
	public function getSurvey()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				$this->load->view("administrator/survey",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getSurveyById() // Get Single PDF
	{
		if(is_ajax_request())
		{ 
			
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("survey_id" => $this->encrypt->decode($postData["id"]));
				$pdf = $this->survey_model->getSurveyById($cond);
				
				//Get all survey question
				$cond = array("fk_survey_id" => $this->encrypt->decode($postData["id"]));
				$questions = $this->survey_model->getallSurveyQuestion($cond);
				//print_r($questions);
				$html = "";
				$i=0;
				foreach($questions as $question) {
					$label="";
					if($i==0){
						$label = '<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Question to ask<span class="validationerror">*</span></label>';
						$add_or_remove = 'add_more';
						$circle = '<i aria-hidden="true" class="fa fa-plus-circle"></i>';
					}
					else{
						$label = '<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first"></label>';
						$add_or_remove = 'remove_div';
						$circle = '<i aria-hidden="true" class="fa fa-minus-circle"></i>';
					}
					
					$survey_question = $question['question'];
					$survey_question_id .= $question['id'].",";
							$html .='<div class="section row mbn">
					  <div class="col-sm-6">
					  
						<div class="form-group plus_resp">
						  '.$label.'
						  <div class="col-lg-8 text_inpu_new first_que">
							<p class="add_new_row">
								<textarea type="text" id="survey_question'.$i.'" name="survey_question[]"   class="form-control input-sm" placeholder="Question to ask">'.$survey_question.'</textarea>
								<span id="QuestionInfo"  class="text-danger marg"></span>
								<span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle"><a data-qno='.$i.' data-id'.$i.'="'.$question['id'].'" href="javascript:void(0);" id="'.$add_or_remove.'" class="">'.$circle.'
								<input type="hidden" name="question_id'.$i.'" value="'.$question['id'].'" />
							</a></span>
							</p>
						  </div>
						</div>
					  </div>
				</div>';
		$i++;
				}
				
				echo json_encode(array("survey" =>$pdf[0],"questions" => $html,"qid"=>$survey_question_id,"total_questions"=>$i));exit;
			
		}
	}
	
	
	
	
	function send_survey_notification($device_token,$msg_survey) {
		
		$deviceToken= $device_token;
	//	print_r($deviceToken);
		//$passphrase = '123456';
		$passphrase = '123456';
		$message = $msg_survey;
															 
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'O_360_DEV.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
											 
		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		//print_r($fp);
		if (!$fp)
		exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
		$body['aps'] = array('title'=>'Opel Survey','alert' => $message,'badge'=> 'Increment','sound' => 'chime');

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		//print_r($deviceToken);
		
		for($i=0;$i<count($deviceToken);$i++){
			 //echo $deviceToken[$i];
			$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $deviceToken[$i])) . pack('n', strlen($payload)) . $payload;
		}

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		return $result; die;
		
}
	function send_survey_notification_android($device_token = array(),$msg) {
		//print_r($device_token);
		//AIzaSyB7RyOObaLVFygZHGWh4iSIE5cvALzNrnc
		define('API_ACCESS_KEY', 'AIzaSyD51ZoV_nwPsMzb7yX0AxVePy5TiQ2g9RA');
						 
						$message = $msg;
						$registrationIds = $device_token;
						//print_r($registrationIds);//die;
						/*$registrationIds = array(
							"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth",
							"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth",
							"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth"
						);*/
						// prep the bundle
						
						//print_r($registrationIds);die;
						$msg = array
						(
							'title'=>'Opel Survey',
							'message' 	=> $message,
						);
						$fields = array
						(
							'registration_ids' 	=> $registrationIds,
							'data'			=> $msg
						);
						 
						$headers = array
						(
							'Authorization: key=' . API_ACCESS_KEY,
							'Content-Type: application/json'
						);
						 
						//print_r(json_encode( $fields )) ;die;
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$result = curl_exec($ch );
						curl_close( $ch );
						return  $result;die;
		
	}
	
	public function save_survey() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();
					//print_r($postData); die;
					if($postData["survey_id"] == "")
					{
								//date_default_timezone_set('Asia/Singapore');
								$created_date = date('Y-m-d H:i:s');
								$insertId = $this->common_model->insert(TB_SURVEY,array("survey_from_date" => $postData["start_date"],"survey_to_date" => $postData["date_to"],"survey_type" => $postData["survey"],"survey_name" => $postData["survey_name"],"award_points" => $postData["award_points"],"date_modified"=>$created_date));
								$reward_points  = $postData["award_points"];
								if($insertId)
								{
									
									$date = date('Y-m-d');
									//get users
									
									if($postData["survey"] =="workshop survey"){
										
										$cond1 = array("transaction_date" => $date);
										$users = $this->survey_model->getAllCurrentVehicleServicedUsers($cond1);
									}
									else{
										$cond1 = array("user_type"=>"NONOPEL");
										$users = $this->survey_model->getAllUsers($cond1);
										//print_r($users);die;
									}
									
									$emailaddress = array();
									
									
									for($i=0;$i<count($users);$i++) {
										
										if($postData["survey"] =="workshop survey"){
											 $user_id = $users[$i]['user_id'];
										} 	
										else{
											 $user_id = $users[$i]['id'];
										}
										//Get Email id's
										$cond = array("user_id"=>$user_id);
										$allusers = $this->survey_model->getAllUsers($cond);
										//echo "<pre>";print_r($allusers[0]['device_id_android']);
										$emailaddress = $allusers[0]['email_address'];
										if($allusers[0]['device_token']!=""){
											$device_token[] = $allusers[0]['device_token'];
										}
										$allusers[0]['device_id_android'];
										if($allusers[0]['device_id_android']!=""){
											$device_id[] = $allusers[0]['device_id_android'];
										}
										//print_r($device_id);//die;
										$emailids[] = $emailaddress;
										
											 $hostname = $this->config->item('hostname');
											 $config['mailtype'] ='html';
											 $config['charset'] ='iso-8859-1';
											 $this->email->initialize($config);
											 $from  = EMAIL_FROM;
											 $this->messageBody  .= email_header();
											 $uname = ucwords($allusers[0]['c_name']);
											 $this->messageBody  .= "Hello $uname,<br/><br/> Thank you for servicing your opel with us. We hope you have time to fill up this short survey. You will be rewarded $reward_points opel points. <br/><br/>Thank You";
											 $this->messageBody  .= email_footer();
											 
											 //echo $this->messageBody;die;
											 
											 $this->email->from($from, $from);
											 $this->email->to("$emailaddress");
											 //$this->email->to("swapnil.t@exceptionaire.co");	
											 //$this->email->to("swapnil.t@exceptionaire.co");	
											 $this->email->subject('Survey notification');
											 $this->email->message($this->messageBody);	
											 $this->email->send();
											 
											 //Send survey notification
											 $this->send_survey_notification($device_token,$postData["survey_name"]);
											 $this->send_survey_notification_android($device_id,$postData["survey_name"]);
											
									}
									//die;
									//print_r($device_id);die;
									//Add Question
									$count = count($postData['survey_question']); 
									foreach($postData['survey_question'] as $question) {
										$insertsurvey = $this->common_model->insert(TB_SURVEY_QUESTIONS,array("question" => $question,"fk_survey_id" => $insertId));
									}
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Survey has been added successfully.</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							
					}
					else
					{
						
						$created_date = date('Y-m-d H:i:s');
						$cond1 = array("survey_id" => $this->encrypt->decode($postData["survey_id"]));
						
							$updateArr = $this->common_model->update(TB_SURVEY,$cond1,array("survey_from_date" => $postData["start_date"],"survey_to_date" => $postData["date_to"],"survey_type" => $postData["survey"],"survey_name" => $postData["survey_name"],"award_points" => $postData["award_points"],"date_modified"=>$created_date));
							//print_r($postData['survey_question'] );die;
							
							$i = 0;
							foreach($postData['survey_question'] as $question) {
								/*check if $question is exists*/
								
								if($postData['question_id'.$i]!=""){
								 	
									$cond_survey = array("question_id" => $postData['question_id'.$i] , "fk_survey_id" => $this->encrypt->decode($postData["survey_id"]));
									$survey_count = $this->survey_model->getallSurveyQuestion($cond_survey);
									//echo count($survey_count);
									//die;
									if(count($survey_count)==1)	{
										$update_survey_questions = $this->common_model->update(TB_SURVEY_QUESTIONS,array("fk_survey_id" => $this->encrypt->decode($postData["survey_id"]),"question_id" => $postData['question_id'.$i]),array("question" => $question));
										//echo $this->db->last_query();//die;
									}
									else{
										$insertsurvey = $this->common_model->insert(TB_SURVEY_QUESTIONS,array("question" => $question,"fk_survey_id" =>$this->encrypt->decode($postData["survey_id"])));
									}
								}
								else{
										$insertsurvey = $this->common_model->insert(TB_SURVEY_QUESTIONS,array("question" => $question,"fk_survey_id" =>$this->encrypt->decode($postData["survey_id"])));
								}
								//echo $this->db->last_query();die;
								$i++;
							}
							
							//die;
							$row = '
								<td class="text-center"  width="5%"><input type="checkbox" value="'.$postData["survey_id"].'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="25%">'.$postData["survey_name"].'</td>
								<td class="text-center"  width="25%">'.$postData["survey"].'</td>
								<td class="text-center"  width="25%">'.$postData["award_points"].'</td>
								<td class="text-center"  width="25%">'.$postData["start_date"].'</td>
								<td class="text-center"  width="25%">'.$postData["date_to"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["survey_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>';
							//echo $this->encrypt->decode($postData["userId"]);die;
							echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["survey_id"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Survey has been updated successfully.</div>')); exit;
							
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
		exit;
	}
    
	
	
	public function list_survey() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->survey_model->getSurveyCount($cond,$like);
					//echo $this->db->last_query(); die;
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("survey_name","survey_type","award_points","survey_from_date","survey_to_date");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "survey_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$survey = $this->survey_model->getSurveyPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'survey_name\',\''.$corder[0].'\')">Survey name</th>
									<th class="text-center '.$css["1"].'" width="25%" onclick="changePaginate(0,\'survey_type\',\''.$corder[1].'\')">Survey type</th>
									<th class="text-center '.$css["2"].'" width="15%" onclick="changePaginate(0,\'award_points\',\''.$corder[2].'\')">Award points</th>
									<th class="text-center '.$css["3"].'" width="15%" onclick="changePaginate(0,\'survey_from_date\',\''.$corder[2].'\')">From date</th>
									<th class="text-center '.$css["4"].'" width="15%" onclick="changePaginate(0,\'survey_to_date\',\''.$corder[2].'\')">To date</th>
									<th class="text-center" width="15%">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($survey)>0)
					{
							if(count($survey) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($survey as $row) {  
								 
						$table .= '<tr id="row_'.$row["id"].'">
								<td class="text-center"  width="5%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="25%">'.$row["survey_name"].'</td>
								<td class="text-center"  width="25%">'.$row["survey_type"].'</td>
								<td class="text-center"  width="25%">'.$row["award_points"].'</td>
								<td class="text-center"  width="25%">'.$row["survey_from_date"].'</td>
								<td class="text-center"  width="25%">'.$row["survey_to_date"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($row["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr> 
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($survey)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="7">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($survey),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	 
	public function validateCatalogModel($postData)
	{   
		
		return array("status" => 1);
	}
	
	
	public function delete_survey() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("survey_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_SURVEY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	public function delete_survey_question() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 
				$isdelete = 0;
				$cond = array("question_id" => $postData["id"]);
				$isdelete = $this->common_model->delete(TB_SURVEY_QUESTIONS,$cond);
				 
				echo "success";exit;
			}
		}
	}
	
	
	
	public function reward_survey()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				/*$where="1 group by survey_id";
				$join = array(
				'opl_users' => "opl_users.user_id = opl_survey_userrating.user_id | inner",
				'opl_survey_questions' => "opl_survey_questions.question_id = opl_survey_userrating.question_id | inner",
				'opl_survey' => "opl_survey.survey_id = opl_survey_questions.fk_survey_id | inner"
				);
				$data['reward_data'] = $this->master_model->getMaster('opl_survey_userrating',$where,$join);	
					*/		
				$this->load->view("administrator/reward_details",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
		
	}
	
	public function rewardsurvey()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$join = array(
						TB_USERS => TB_USERS.".user_id = ".TB_USER_GAINED_REWARDS.".user_id"
						
					);
					$count = $this->survey_model->getSurveyRewardsCount($cond,$like,$join);
					//echo $this->db->last_query(); die;
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("c_name","email_address","award_points","used_points");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "gid";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$join = array(
						TB_USERS => TB_USERS.".user_id = ".TB_USER_GAINED_REWARDS.".user_id"
						
					);
					
					$survey = $this->survey_model->getRewardSurveyDetails($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%">Sr.No</th>
									<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'c_name\',\''.$corder[0].'\')">User Name</th>
									<th class="text-center '.$css["1"].'" width="25%" onclick="changePaginate(0,\'email_address\',\''.$corder[1].'\')">Email Addrsess</th>
									<th class="text-center '.$css["2"].'" width="15%" onclick="changePaginate(0,\'award_points\',\''.$corder[2].'\')">Total Reward Points</th>
									<th class="text-center '.$css["3"].'" width="15%" onclick="changePaginate(0,\'award_points\',\''.$corder[3].'\')">Available Rewards</th>
									
									<th class="text-center" width="15%">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($survey)>0)
					{
							if(count($survey) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							$cnt=1;
							$used_rewards=0;
							foreach($survey as $row) {  
								$cond1 = array("user_id" => $row['id']);
								$rewards = $this->survey_model->getUsedReward($cond1);
								
								if(sizeof($rewards)==0)
								{
									$used_rewards=$row['award_points'];
								}
								else
								{
									$used_rewards=$row['award_points']-$rewards[0]["reward"]; 
								}
						$table .= '<tr id="row_'.$row["id"].'">
								<td class="text-center"  width="5%">'.$cnt.'</td>
								<td class="text-center"  width="25%">'.($row["c_name"]?$row["c_name"]:"-").'</td>
								<td class="text-center"  width="25%">'.$row["email_address"].'</td>
								<td class="text-center"  width="25%">'.$row["award_points"].'</td>
								<td class="text-center"  width="25%">'.$used_rewards.'</td>
								<td class="text-center">
									<a title="Deduct" alt="Deduct Rewards" id="edit" class="editPayment" data-adward="'.$row["award_points"].'" data-sname="'.$row["survey_name"].'" data-name="'.$row["c_name"].'" data-id="'.$this->encrypt->encode($row["id"]).'" data-option="'.$this->encrypt->encode($row["survey_id"]).'" href="javascript:void(0)"><i class="fa fa-minus"></i></a> | 
									<a title="View Rewards" alt="View" id="view" class="editPayment" data-sname="'.$row["survey_name"].'" data-id="'.$this->encrypt->encode($row["id"]).'" data-option="'.$this->encrypt->encode($row["survey_id"]).'" href="javascript:void(0)"><i class="fa fa-eye"></i></a>
								</td>
							  </tr> 
							  </tr>
							  ';
							  $cnt++;
							}
					}
					if($postData["start"] == 0)
					{
							if(count($survey)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($survey),'paginate' => $paginate)); exit;
				}
		}
	}
	
	public function getRewardDetails()
	{
		$postData = $this->input->post(); 
				
				$tbale="";
				$cond = array("user_id" => $this->encrypt->decode($postData['uid']));
				$reward = $this->survey_model->getUsersUsedReward($cond);

				$cond_user = array("user_id" => $this->encrypt->decode($postData['uid']));
				$user = $this->users_model->getUserById($cond_user);

				$table = '<table class="table table-striped">
				<thead>
					<tr>
						<th>Sr.No</th> 
						<th>Reward Points</th>
						<th>Reason</th>
						<th>Date and Time</th>
					</tr>
				</thead> <tbody>';
			
			$i=1;
			if(count($reward)>0){
				foreach($reward as $value)
				{
					$table .= '<tr>
						<td>'.$i.'</td> 
						<td>'.$value["rewards_used"].'</td>
						<td>'.$value["used_for"].'</td>
						<td>'.date('d-M-Y / H:i',strtotime($value["date_modified"])).'</td>
					</tr>';
					$i++;
				}
			}
			else{
				$table .= '<tr><td colspan="5" align="center"> No Records Available</td></tr>';
			}
			$table .= '</tbody></table>';
		echo json_encode(array('table' => $table,"username"=>$user[0]['c_name']));exit;
	}
	
	
	public function save_used_reward()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				//date_default_timezone_set('Asia/Singapore');
				$created_date = date('Y-m-d H:i:s');
				$save_data=array(
					"user_id" => $this->encrypt->decode($postData["user_id"]) ,
					"rewards_used" => $postData["reward_point"],
					"used_for" => $postData["reson"],
					"date_modified"=>$created_date
							);
				//print_r($save_data); die;
				$total_adward=$postData["total_adward"];
				$cond1 = array("user_id" => $this->encrypt->decode($postData["user_id"]));
				$rewards = $this->survey_model->getUsedReward($cond1);
				if(sizeof($rewards)==0)
				{
					$used_rewards=$total_adward;
				}
				else
				{
					$used_rewards=$total_adward-$rewards[0]["reward"]; 
				}
				
				if($used_rewards>=$postData["reward_point"])
				{
					$insertId = $this->common_model->insert(TB_SURVEY_USED_REWARD,$save_data);
				}	
				else
				{
					echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, You do not have sufficiant available rewards!!.</div>')); exit;
				}							
				echo json_encode(array("status" => 1,"available_rewards"=>$used_rewards,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Rewards point has been deducted successfully.</div>')); exit;
			}
			else
			{
				echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
			}
		}
		else
		{
			echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
		}
	}
	 
}
?>
