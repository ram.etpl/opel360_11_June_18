<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("users_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->load->helper("email_template"); 
		//$this->load->library("richtexteditor");
		$this->profilepicpath =  realpath('profile_pic');
		$this->load->helper('xml');
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'user_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	
	/********* User Access ***********/
	private function genarateRandomPassword($length=10){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$randomstring ='';
		for($i=0;$i<=$length;$i++){
			$randomstring .= $characters[rand(0,strlen($characters) -1)];
		}
		return $randomstring;
	}
	
	public function getUsers()
	{ 
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				 
				$data['userAccess'] = "";
				$data['userAccess'] .='<div class="user_lt">
										<div class="useritle">Modules</div>
										<div class="admin_title">Admin Access</div>
									</div>
										<div class="user_rt">
													<div class="useritle">User Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="User_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">News and Event Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="News_and_Event_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Service Cost Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0"  id="" name="Service_Cost_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Pdf Manuals Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0"  id="" name="Pdf_Manuals_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Car Catalogue Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Car_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Accessories Catalogue Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Accessories_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Community Gallery Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Community_Gallery_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Reward Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Reward_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Survey Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Survey_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access">
											
											<div class="user_rt">
												<div class="useritle">Banner Management</div>
												<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Banner_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access">
											';
									
									
									//print_r($data); die;
				$this->load->view("administrator/users",$data);
			}else{
				
				$this->load->view("administrator/access_denied",$data);
			}
		}
	}
	
	
	
	public function ImportOpelUsers()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			
			if($this->checkAdminPermission()){
				$this->load->view("administrator/import_csv",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
		}
	}
	
	
	
	public function ImportOpelUsersfromXML()
	{
			if(is_user_logged_in())
			{
				ini_set( 'memory_limit', '-1' );
				ini_set('upload_max_filesize', '500M');  
				ini_set('post_max_size', '500M');  
				ini_set('max_input_time', 36000);  
				ini_set('max_execution_time', 36000);
				
				// read XML data string
				//echo $file_path = 'AG_Test.xml';
				$XML_Data_File = $_FILES['XML']['tmp_name'];
				//print_r($_FILES['xml']['size']); die;
				if($_FILES['xml']['size'] <= 30000){
					$xmlData = file_get_contents($XML_Data_File);
					$xml = simplexml_load_string($xmlData) or die("ERROR: Cannot create SimpleXML object");
					$exist_user = 0;
					$new_user_cnt = 0;
					$no_email = 0;
					foreach ($xml->ServiceOrder as $ServiceOrder) {
						foreach($ServiceOrder->Customer as $Customer) {
							$cust_Code = $Customer->Code;
							$cust_Name = $Customer->Name;
							$Cust_Email = $Customer->Email;
							$Cust_Handphone = $Customer->Handphone;
							$Cust_Address = $Customer->Address;
							$Cust_DateofBirth = $Customer->DateofBirth;
							$Cust_ID = $Customer->ID;
							$Cust_PDPAConsent = $Customer->PDPAConsent;
							$Cust_MaritialStatus = $Customer->MaritialStatus;
							$Cust_Nationality = $Customer->Nationality;
							
							$Cust_Vehicle_ServiceOrder_Servicenumber = $Customer->Vehicle->ServiceOrder->Servicenumber;
							$Cust_Vehicle_ServiceOrder_TransactionDate = $Customer->Vehicle->ServiceOrder->TransactionDate;
							$Cust_Vehicle_ServiceOrder_VehicleMileage = $Customer->Vehicle->ServiceOrder->VehicleMileage;
							
							//$Cust_Vehicle_ServiceOrder_ServiceOrderProcess_LineNumber = $Customer->Vehicle->ServiceOrder->ServiceOrderProcess->LineNumber;
							//$Cust_Vehicle_ServiceOrder_ServiceOrderProcess_ProcessCode = $Customer->Vehicle->ServiceOrder->ServiceOrderProcess->ProcessCode;
							//$Cust_Vehicle_ServiceOrder_ServiceOrderProcess_Processdescription = $Customer->Vehicle->ServiceOrder->ServiceOrderProcess->Processdescription;
							
							//Check Email-id is already exist
							//echo $Cust_Email;
							
							$em = trim((string)$Cust_Email); 
							
							if($em != '') {
								$cond1 = array("email_address" => trim((string)$Cust_Email));
								$users = $this->users_model->getAllUsersExist($cond1);
								$exist_cnt = count($users);
								if($exist_cnt>0) {
									$exist_user += $exist_cnt;
								}
								else {
									
									//INSER IN TO USER
									$insertId = $this->common_model->insert(TB_USERS,array("c_name" => trim($cust_Name),"email_address" => trim($Cust_Email),"password" => trim(md5('123')),"user_type" => 'OPEL',"status" => 'active',"contact_number" => trim($Cust_Handphone),"registered_date" => date("Y-m-d H:i:s"),"modified_date" => date("Y-m-d H:i:s")));
									$insert_user_id = $this->db->insert_id();
									//insert to vehicle owners
									$insertId = $this->common_model->insert(TB_VEHICLE_OWNER_DETAILS,array("user_id" => trim($insert_user_id),"maritial_status" => trim($Cust_MaritialStatus),"pdpa_consent" => trim($Cust_PDPAConsent),"cust_id" => trim($Cust_ID),"cust_code" => trim($cust_Code),"vehicle_no" => trim($Cust_Vehicle_RegisteredNumber),"cust_name" => trim($cust_Name),"email_address" => trim($Cust_Email),"phone_no" => trim($Cust_Handphone),"address" => trim($Cust_address),"date_of_birth" => trim($Cust_DateofBirth),"nationality" => trim($Cust_Nationality)));
									
									//insert to vehicle details
									foreach($Customer->Vehicle as $Vehicle) {
										$eng_numner = $Vehicle->EngineNumber;
										$cond_veh = array("engine_number"=>"'.$eng_numner.'","customer_id"=>$insertId);
										
										$check_vehicle_no = $this->users_model->getCustomerVehicleNoById($cond_veh);
										if(count($check_vehicle_no)<1){
											$Cust_Vehicle_RegisteredNumber = $Vehicle->RegisteredNumber;
											$Cust_Vehicle_VehicleNo = $Vehicle->VehicleNo;
											$Cust_Vehicle_VehicleGroup = $Vehicle->VehicleGroup;
											$Cust_Vehicle_MakeMode = $Vehicle->MakeMode;
											$Cust_Vehicle_RegistrationDate = $Vehicle->RegistrationDate;
											$Cust_Vehicle_EngineNumber = $Vehicle->EngineNumber;
											$Cust_Vehicle_ChassisNumber = $Vehicle->ChassisNumber;
											
											$arrayVehicleDetails = array(
												"customer_id" => $insertId,
												"user_id" => trim($insert_user_id),
												"chassis_number" => trim($Cust_Vehicle_ChassisNumber),
												"engine_number" => trim($Cust_Vehicle_EngineNumber),
												"registration_date" => trim($Cust_Vehicle_RegistrationDate),
												"make_mode" => trim($Cust_Vehicle_MakeMode),
												"vehicle_group" => trim($Cust_Vehicle_VehicleGroup),
												"registration_no" => trim($Cust_Vehicle_RegisteredNumber)
											);
											
											 
											$insertVechicleId = $this->common_model->insert(TB_VEHICLE_DETAILS,$arrayVehicleDetails);
											//insert to vehicle services details
											if($insertVechicleId){
													foreach($Vehicle->ServiceOrder as $ServiceOrder) {
														
														$Cust_Vehicle_ServiceOrder_Servicenumber = $ServiceOrder->Servicenumber;
														$Cust_Vehicle_ServiceOrder_TransactionDate = $ServiceOrder->TransactionDate;
														$Cust_Vehicle_ServiceOrder_VehicleMileage = $ServiceOrder->VehicleMileage; 
														
														$arrayVehicleServiceDetails = array(
															"customer_id" => $insertId,
															"user_id" => trim($insert_user_id),
															"vehicle_id" => $insertVechicleId,
															"service_order" => trim($Cust_Vehicle_ServiceOrder_Servicenumber),
															"transaction_date" => date('Y-m-d',strtotime(trim($Cust_Vehicle_ServiceOrder_TransactionDate))),
															"vehicle_mileage" => trim($Cust_Vehicle_ServiceOrder_VehicleMileage),
														);
														$insertVechicleServiceId = $this->common_model->insert(TB_VEHICLE_SERVICE_ORDER,$arrayVehicleServiceDetails);
														if($insertVechicleServiceId){
																
																foreach($ServiceOrder->ServiceOrderProcess as $ServiceOrderProcess) {
																	$Cust_Vehicle_ServiceOrder_LineNumber = $ServiceOrderProcess->LineNumber;
																	$Cust_Vehicle_ServiceOrder_ProcessCode = $ServiceOrderProcess->ProcessCode;
																	$Cust_Vehicle_ServiceOrder_Processdescription = $ServiceOrderProcess->Processdescription; 
																	
																	$arrayVehicleServiceProcessDetails = array(
																		"customer_id" => $insertId,
																		"user_id" => trim($insert_user_id),
																		"s_order_id" => $insertVechicleServiceId,
																		"line_number" => trim($Cust_Vehicle_ServiceOrder_LineNumber),
																		"process_code" => trim($Cust_Vehicle_ServiceOrder_ProcessCode),
																		"process_description" => trim($Cust_Vehicle_ServiceOrder_Processdescription),
																	);
																	
																	$insertVechicleServiceId = $this->common_model->insert(TB_VEHICLE_SERVICE_ORDER_PROCESS,$arrayVehicleServiceProcessDetails);
																}
														}
													}
												}
										} 
										
									}
									$new_user_cnt++;
								}
							}
							else {
								$no_email ++; 
							}
						}
					}
					echo json_encode(array("Already_exist" => $exist_user, "New_Opel_Users" => $new_user_cnt,"blank_email" => $no_email));exit;
				}
				else{
					echo json_encode(array("error" => 'File is too large to upload'));exit;
				}
				//die;
				
			}
	}
	
	
	 
   
	
	/********* User Access ***********/
	public function UserAccess()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{ 
			$userdata = $this->session->userdata("auth_user");
			if($userdata['super_admin'] == '1'){
				$userAccess = $this->users_model->getUserRole(); 
				$data['userAccess'] = "";
				$data['userAccess'] .='<div class="user_lt">
										<div class="useritle">Modules</div>
										<div class="admin_title">Admin Access</div>
									</div>
										';
				if($userAccess!=""){
					$n=1;	
					foreach($userAccess as $access):
					
					if($access['admin_access'] == '1'){
						$checked = 'checked="checked"';
						$value = '1';
						$hidden = '<input type="hidden" name="admin_access" value="1"/>';
					}
					else{
						$checked = ''; 
						$value = '0';
						$hidden = '<input type="hidden" name="admin_access" value="0"/>';
					}
					
					$data['userAccess'] .='<div class="user_rt">
													<div class="useritle">'.$access['role'].'</div>
													<div class="admin_checkbox"><input class="chk_role" type="checkbox" name="'.str_replace(" ","_",$access['role']).'" id="" '.$checked.' value="'. $value.'"/></div>
											</div>';	
					$data['userAccess'] .= $hidden;											
										
					$n++;
					endforeach;
				}
				else{
					$data['userAccess'] .='No User Types found in system !! Please check database';
				}
				//print_r($data);die;
				$this->load->view("administrator/user_access",$data);
			}
		}
	}
	
	public function SaveUserAccessSettings()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{   
				$postData = $this->input->post();  
				$data=""; 
				//print_r($postData);die;
				if($postData){ 
					//foreach($postData as $key=>$val):  

					$user_mgt = ($postData['User_Management']?'1':'0');
					
					$data_update_user = array("admin_access" => $user_mgt);  
					$cond_user = array("role" => 'User Management');
					$this->common_model->update(TB_USER_ACCESS,$cond_user,$data_update_user);	
					
					$news_mgt = ($postData['News_and_Event_Management']?'1':'0');
					
					$data_update_news=array("admin_access" => $news_mgt); 
					$cond_news = array("role" => 'News and Event Management');
					$this->common_model->update(TB_USER_ACCESS,$cond_news,$data_update_news);	
					
					$service_mgt = ($postData['Service_Cost_Management']?'1':'0');
					 
						$data_update_service=array("admin_access" => $service_mgt); 
						$cond_service = array("role" => 'Service Cost Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_service,$data_update_service);	
					 
					$pdf_mgt = ($postData['Pdf_Manuals_Management']?'1':'0');
					 
						$data_update_pdf = array("admin_access" => $pdf_mgt); 
						$cond_pdf = array("role" => 'Pdf Manuals Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_pdf,$data_update_pdf);	
					 
					
					$catlog_mgt = ($postData['Car_Catalogue_Management']?'1':'0');
					 
						$data_update_catlog = array("admin_access" => $catlog_mgt); 
						$cond_catlog = array("role" => 'Car Catalogue Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_catlog,$data_update_catlog);	
					 
					
					$acc_mgt = ($postData['Accessories_Catalogue_Management']?'1':'0');
					 
						$data_update_acc = array("admin_access" => $acc_mgt); 
						$cond_acc = array("role" => 'Accessories Catalogue Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_acc,$data_update_acc);	
					 
					
					$community_mgt = ($postData['Community_Gallery_Management']?'1':'0');
					 
						$data_update_community=array("admin_access" => $community_mgt); 
						$cond_community = array("role" => 'Community Gallery Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_community,$data_update_community);	
					 
					
					$reward_mgt = ($postData['Reward_Management']?'1':'0');
					 
						$data_update_reward=array("admin_access" => $reward_mgt); 
						$cond_reward = array("role" => 'Reward Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_reward,$data_update_reward);	
					 
					$survey_mgt = ($postData['Survey_Management']?'1':'0');
					 
						$data_update_survey =array("admin_access" => $survey_mgt); 
						$cond_survey = array("role" => 'Survey Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_survey,$data_update_survey);	
					  
					$banners_mgt = ($postData['Banner_Management']?'1':'0');
					 
						$data_update_banner=array("admin_access" => $banners_mgt); 
						$cond_banner = array("role" => 'Banner Management');
						$this->common_model->update(TB_USER_ACCESS,$cond_banner,$data_update_banner);
					
					//endforeach;
				}
				
				$userAccess = $this->users_model->getUserRole(); 
				$data['userAccess'] = "";
				$data['userAccess'] .='<div class="user_lt">
										<div class="useritle">Modules</div>
										<div class="admin_title">Admin Access</div>
									</div>';
					if($userAccess!=""){
						$n=1;	
						foreach($userAccess as $access):
						
						$check_user = ($access['user_management']=='1'?'checked="checked"':"");
						
						$data['userAccess'] .='<div class="user_rt">
													<div class="useritle">User Management</div>
													<div class="admin_checkbox"><input '.$check_user.' type="checkbox" value="'.$access['user_management'].'" id="" name="User_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['user_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">News and Event Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['news_and_events_management'].'" id="" name="News_and_Event_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['news_and_events_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Service Cost Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['service_cost_management'].'"  id="" name="Service_Cost_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['service_cost_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Pdf Manuals Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['pdf_manuals_management'].'"  id="" name="Pdf_Manuals_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['pdf_manuals_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Car Catalogue Management</div>
													<div class="admin_checkbox"><input type="checkbox" value='.$access['car_catalogue_management'].'" id="" name="Car_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['car_catalogue_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Accessories Catalogue Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['accessories_catalogue_management'].'" id="" name="Accessories_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['accessories_catalogue_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Community Gallery Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['community_gallery_management'].'" id="" name="Community_Gallery_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['community_gallery_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Reward Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['reward_management'].'" id="" name="Reward_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['reward_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Survey Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['survey_management'].'" id="" name="Survey_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['survey_management'].'" name="admin_access"><div class="user_rt">
													
											<div class="useritle">Banner Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="'.$access['banner_management'].'" id="" name="Banner_Management" class="chk_role"></div>
											</div>
											<input type="hidden" value="'.$access['banner_management'].'" name="admin_access">';
												
						$data['userAccess'] .= $hidden;																			
											
						$n++;
						endforeach;
					}
					else{
						$data['userAccess'] .='No Modules found in system';
					}
			
				echo json_encode(array("userAccess" => $data['userAccess'], "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Settings updated successfully.</div>'));
			}
		}
	}
	
	
	public function getUserById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("user_id" => $this->encrypt->decode($postData["id"]));
				$users = $this->users_model->getUserById($cond);
				//print_r($users);die;
				
				
				$adminuserAccess = $this->users_model->getAdminUserAccess($cond); 
				$userAccess   = "";
				$userAccess  .='<div class="user_lt">
										<div class="useritle">Modules</div>
										<div class="admin_title">Admin Access</div>
									</div>';
				if(count($adminuserAccess)>0){
					$n=1;	
					foreach($adminuserAccess as $access):
					
					$check_user = ($access['user_management']=='1'?'checked="checked"':"");
					$check_news = ($access['news_and_events_management']=='1'?'checked="checked"':"");
					$check_service = ($access['service_cost_management']=='1'?'checked="checked"':"");
					$check_pdf = ($access['pdf_manuals_management']=='1'?'checked="checked"':"");
					$check_car = ($access['car_catalogue_management']=='1'?'checked="checked"':"");
					$check_accessories = ($access['accessories_catalogue_management']=='1'?'checked="checked"':"");
					$check_community = ($access['community_gallery_management']=='1'?'checked="checked"':"");
					$check_reward = ($access['reward_management']=='1'?'checked="checked"':"");
					$check_survey = ($access['survey_management']=='1'?'checked="checked"':"");
					//$check_test = ($access['test_drive_request_management']=='1'?'checked="checked"':"");
					//$check_upgrade = ($access['upgrade_or_trade_in_management']=='1'?'checked="checked"':"");
					//$check_feedback = ($access['feedback_management']=='1'?'checked="checked"':"");
					$check_banner = ($access['banner_management']=='1'?'checked="checked"':"");
					
					$userAccess  .='<div class="user_rt">
													<div class="useritle">User Management</div>
													<div class="admin_checkbox"><input '.$check_user.' type="checkbox" value="'.$access['user_management'].'" id="" name="User_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['user_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">News and Event Management</div>
													<div class="admin_checkbox"><input '.$check_news.' type="checkbox" value="'.$access['news_and_events_management'].'" id="" name="News_and_Event_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['news_and_events_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Service Cost Management</div>
													<div class="admin_checkbox"><input '.$check_service.' type="checkbox" value="'.$access['service_cost_management'].'"  id="" name="Service_Cost_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['service_cost_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Pdf Manuals Management</div>
													<div class="admin_checkbox"><input '.$check_pdf.' type="checkbox" value="'.$access['pdf_manuals_management'].'"  id="" name="Pdf_Manuals_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['pdf_manuals_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Car Catalogue Management</div>
													<div class="admin_checkbox"><input '.$check_car.' type="checkbox" value='.$access['car_catalogue_management'].'" id="" name="Car_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['car_catalogue_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Accessories Catalogue Management</div>
													<div class="admin_checkbox"><input '.$check_accessories.' type="checkbox" value="'.$access['accessories_catalogue_management'].'" id="" name="Accessories_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['accessories_catalogue_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Community Gallery Management</div>
													<div class="admin_checkbox"><input '.$check_community.' type="checkbox" value="'.$access['community_gallery_management'].'" id="" name="Community_Gallery_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['community_gallery_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Reward Management</div>
													<div class="admin_checkbox"><input '.$check_reward.' type="checkbox" value="'.$access['reward_management'].'" id="" name="Reward_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['reward_management'].'" name="admin_access"><div class="user_rt">
													<div class="useritle">Survey Management</div>
													<div class="admin_checkbox"><input '.$check_survey.' type="checkbox" value="'.$access['survey_management'].'" id="" name="Survey_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['survey_management'].'" name="admin_access"><div class="user_rt">
													
											<div class="useritle">Banner Management</div>
													<div class="admin_checkbox"><input '.$check_banner.' type="checkbox" value="'.$access['banner_management'].'" id="" name="Banner_Management" class="chk_role"></div>
											</div><input type="hidden" value="'.$access['banner_management'].'" name="admin_access">';	
					
										
					$n++;
					endforeach;
				}
				else{ 
					$userAccess .='
									<div class="user_rt">
													<div class="useritle">User Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="User_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">News and Event Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="News_and_Event_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Service Cost Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0"  id="" name="Service_Cost_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Pdf Manuals Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0"  id="" name="Pdf_Manuals_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Car Catalogue Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Car_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Accessories Catalogue Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Accessories_Catalogue_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Community Gallery Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Community_Gallery_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Reward Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Reward_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Survey Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Survey_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Test Drive Request Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Test_Drive_Request_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Upgrade/Trade-In Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Upgrade/Trade-In_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access"><div class="user_rt">
													<div class="useritle">Feedback Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Feedback_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access">
											<div class="useritle">Banner Management</div>
													<div class="admin_checkbox"><input type="checkbox" value="0" id="" name="Banner_Management" class="chk_role"></div>
											</div><input type="hidden" value="0" name="admin_access">
									';
				}
				
				echo json_encode(array("users"=>$users[0],"user_access"=>$userAccess));exit;
			
		}
	}
	
	public function save_users() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();  
					//print_r($postData);die;
					if($postData["userId"] == "")
					{   
						$isValid = $this->validateUser($postData);
						//print_r($isValid);die;
						if($isValid["status"] == 1)
						{ 
							$cond = array("email_address"=>$postData["txt_email_address"]);
							$users = $this->users_model->getAllUsers($cond);
							$data["users"] = $users;
							//$randpwd = trim($this->genarateRandomPassword(10)); 
							$randpwd = $postData["txt_password"];  
							$hostname = $this->config->item('hostname');
							if(count($users) == 0)
							{
								
								$insertId = $this->common_model->insert(TB_USERS,array("c_name" => $postData["txt_first_name"],"email_address" => $postData["txt_email_address"],"user_type" => $postData["sel_user_type"],"contact_number" => $postData["txt_contact_number"],"password" => md5($postData["txt_password"]),"status"=> 'active',"registered_date" => date('Y-m-d H:i:s')));
								if($insertId)
								{  
									
									//$insertId = $this->common_model->insert(TB_USERS_PROFILE,array("user_id" => $insertId, "date_modified" => date('Y-m-d H:i:s')));
									
									if($insertId && $postData["sel_user_type"] == "ADMIN"){
										$user_mgt = ($postData['User_Management']?'1':'0');
										$news_mgt = ($postData['News_and_Event_Management']?'1':'0');
										$service_mgt = ($postData['Service_Cost_Management']?'1':'0');
										$pdf_mgt = ($postData['Pdf_Manuals_Management']?'1':'0');
										$catlog_mgt = ($postData['Car_Catalogue_Management']?'1':'0');
										$acc_mgt = ($postData['Accessories_Catalogue_Management']?'1':'0');
										$community_mgt = ($postData['Community_Gallery_Management']?'1':'0');
										$reward_mgt = ($postData['Reward_Management']?'1':'0');
										$survey_mgt = ($postData['Survey_Management']?'1':'0');
										$drive_mgt = ($postData['Test_Drive_Request_Management']?'1':'0');
										$upgrad_mgt = ($postData['Upgrade/Trade-In_Management']?'1':'0');
										$feedback_mgt = ($postData['Feedback_Management']?'1':'0');
										
										
										$this->common_model->insert(TB_USER_ACCESS,array("user_id" => $insertId,"user_management" => $user_mgt,"news_and_events_management" => $news_mgt,"service_cost_management" => $service_mgt,"pdf_manuals_management" => $pdf_mgt,"car_catalogue_management" => $catlog_mgt ,"accessories_catalogue_management" => $acc_mgt ,"community_gallery_management" => $community_mgt,"reward_management" => $reward_mgt,"survey_management" => $survey_mgt,"test_drive_request_management" => $drive_mgt ,"upgrade_or_trade_in_management" => $upgrad_mgt,"feedback_management" => $feedback_mgt));
										//echo $this->db->last_query();
										//die;
									}
									
									/*
									 * Send email of registration
									 * 
									 * */
									
									
									$username = $postData["txt_first_name"];
									$emailaddress = $postData["txt_email_address"];
									$usertype = $postData["sel_user_type"];
									$config['mailtype'] ='html';
									$config['charset'] ='iso-8859-1';
									$this->email->initialize($config);
									if($usertype == "ADMIN"){
										$hostname  = $hostname."/adminlogin";
									}
									$this->messageBody  = email_header();
									$this->messageBody  .= "Hello $username,<br/><br/>
										 
										 Your login details are below:<br/> 
										 Email Address : $emailaddress<br/>
										 Password : $randpwd<br/>
										 User Type : $usertype<br/>
										 Click the link below to login to your user account.<br/><br/>
										 <a href='".$hostname."'>".$hostname."</a>
										 <br/><br/>
										 <br/>If the link is not working properly then copy and paste the link in your browser	 
										 <br/><br/>									 
										 ";
									 //echo $this->messageBody;
									 //die;
									$this->messageBody  .= email_footer();
									$this->email->from(EMAIL_FROM, 'OPEL360');
									$this->email->to("$emailaddress");

									$this->email->subject('OPEL360 - Login details');
									$this->email->message($this->messageBody);	
									$this->email->send();	 
									  
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Users has been added successfully and mail sent with the login credentials</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This user has been already available.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);
						} 
						 
					}
					else
					{ 
						$isValid = $this->validateUser($postData);
						if($isValid["status"] == 1)
						{
							$cond = array("user_id !=" => $this->encrypt->decode($postData["userId"]), "email_address"=>$postData["txt_email_address"]);
							$cond1 = array("user_id" => $this->encrypt->decode($postData["userId"]));
							$users = $this->users_model->getAllUsers($cond);
							$userbyid = $this->users_model->getUserById($cond1); 
							$data["users"] = $users; 
							$randpwd = $postData["txt_password"]; 
							$txt_heading="";
							//echo count($users);die;
							if(count($users) == 0)
							{  
								 
								$updateArr = $this->common_model->update(TB_USERS,$cond1,array("c_name" => $postData["txt_first_name"],"email_address" => $postData["txt_email_address"],"user_type" => $postData["sel_user_type"],"contact_number" => $postData["txt_contact_number"]));
								
								$updatepassword=0; 
								if(isset($postData["txt_password"]) && $postData["txt_password"] != "") {
									
									$updatepassword = $this->common_model->update(TB_USERS,$cond1,array("password" => md5($randpwd)));
									//die;
								}
								if($postData['sel_user_type'] == 'ADMIN'){
									/*check if user id exists*/
									$cond_access = array("user_id" => $this->encrypt->decode($postData["userId"]));
									$check_admin_access = $this->users_model->getAdminUserAccess($cond_access);
									$user_mgt = ($postData['User_Management']?'1':'0');
									$news_mgt = ($postData['News_and_Event_Management']?'1':'0');
									$service_mgt = ($postData['Service_Cost_Management']?'1':'0');
									$pdf_mgt = ($postData['Pdf_Manuals_Management']?'1':'0');
									$catlog_mgt = ($postData['Car_Catalogue_Management']?'1':'0');
									$acc_mgt = ($postData['Accessories_Catalogue_Management']?'1':'0');
									$community_mgt = ($postData['Community_Gallery_Management']?'1':'0');
									$reward_mgt = ($postData['Reward_Management']?'1':'0');
									$survey_mgt = ($postData['Survey_Management']?'1':'0');
									$drive_mgt = ($postData['Test_Drive_Request_Management']?'1':'0');
									$upgrad_mgt = ($postData['Upgrade/Trade-In_Management']?'1':'0');
									$feedback_mgt = ($postData['Feedback_Management']?'1':'0');
									$banner_mgt = ($postData['Banner_Management']?'1':'0');
									//echo count($check_admin_access);die;
									if(count($check_admin_access)>0){
										$this->common_model->update(TB_USER_ACCESS,$cond1,array( "user_management" => $user_mgt,"news_and_events_management" => $news_mgt,"service_cost_management" => $service_mgt,"pdf_manuals_management" => $pdf_mgt,"car_catalogue_management" => $catlog_mgt ,"accessories_catalogue_management" => $acc_mgt ,"community_gallery_management" => $community_mgt,"reward_management" => $reward_mgt,"survey_management" => $survey_mgt,"test_drive_request_management" => $drive_mgt ,"upgrade_or_trade_in_management" => $upgrad_mgt,"feedback_management" => $feedback_mgt ,"banner_management" => $banner_mgt ));
									}
									else{
										
										$this->common_model->insert(TB_USER_ACCESS,array("user_id" => $this->encrypt->decode($postData["userId"]),"user_management" => $user_mgt,"news_and_events_management" => $news_mgt,"service_cost_management" => $service_mgt,"pdf_manuals_management" => $pdf_mgt,"car_catalogue_management" => $catlog_mgt ,"accessories_catalogue_management" => $acc_mgt ,"community_gallery_management" => $community_mgt,"reward_management" => $reward_mgt,"survey_management" => $survey_mgt,"test_drive_request_management" => $drive_mgt ,"upgrade_or_trade_in_management" => $upgrad_mgt,"feedback_management" => $feedback_mgt,"banner_management" => $banner_mgt));
									}
								}
								/*
								 * Send email of registration
								 * 
								 * */
									if($updatepassword){	
										
										$hostname = $this->config->item('hostname');
													$username = $postData["txt_first_name"];
													$emailaddress = $postData["txt_email_address"];
													$config['mailtype'] ='html';
													$config['charset'] ='iso-8859-1';
													$this->email->initialize($config);
													$this->messageBody  = email_header();
													$this->messageBody  .= "Hello, $username<br/><br/>
														
														 Your login details are below:<br/> 
														 Email Address : $emailaddress<br/>
														 Password : $randpwd<br/>
														 Click the link below to login to your account.<br/><br/>
														 <a href='".$hostname."'>".$hostname."</a>
														 <br/><br/><br/>If the link is not working properly, then copy and paste the link in your browser.
														  <br/><br/>If you did not send this request, please ignore this email.
														 <br/><br/>".REGARDS."";
													 //echo $this->messageBody;
													 //die;
													 $this->messageBody  .= email_footer();
													$this->email->from(EMAIL_FROM, 'OPEL360');
													$this->email->to("$emailaddress");

													$this->email->subject('OPEL360 - Login credentials');
													$this->email->message($this->messageBody);	
													$this->email->send();
													
									}
									
									$cond2 = array("user_id" => $this->encrypt->decode($postData["userId"]));
									$users = $this->users_model->getAllUsers($cond2); 
									  
									if($users["0"]["status"] == "active"){
										$status_text = "Make Inactive";
										$status_active = "btn btn-xs btn-success";
										$status_inactive = "btn btn-xs  active btn-default";
										$status = "inactive";
									}
									else{
										$status_text = "Make Active";
										$status_active = "btn btn-xs  active btn-default";
										$status_inactive = "btn btn-xs btn-danger";
										$status = "active";
									}
									
									$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["userId"].'"></td>
											<td class="text-center">'.$postData["txt_first_name"].'</td>
											<td>'.$postData["txt_email_address"].'</td>
											<td>'.ucwords($postData["sel_user_type"]).'</td>
											<td class="text-center">';
												if($postData["sel_user_type"] !="OPEL"){
													$row .='<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["userId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|';
												}
												$row .='<a title="View details" alt="viewdetails" id="view" class="view" data-option="'.$postData["userId"].'" href="javascript:void(0)">View Details</a>| ';
												//$row .='<a title="Edit" alt="Edit" id="status" class="changeStatus" onclick="changeStatus(\''.$this->encrypt->encode($postData["userId"]).'\',\''.$status.'\')" data-option="'.$this->encrypt->encode($postData["userId"]).'" href="javascript:void(0)">'.$status_text.'</a>
												$row .='<div class="btn-group btn-toggle"> <button onclick="changeStatus(\''.$postData["userId"].'\',\'active\')" class="'.$status_active.'">Active</button>
															<button onclick="changeStatus(\''.$postData["userId"].'\',\'inactive\')" class="'.$status_inactive.'">Inactive</button>
														</div>';
											$row .='</td>';
											//echo $this->encrypt->decode($postData["userId"]);die;
									echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Users has been updated successfully.</div>')); exit;
								
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This email address has been already taken.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);exit;
						}
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_users() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					
					$cond = array('super_admin'=>0);
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->users_model->getUsersCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("c_name","email_address","user_type");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "user_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$users = $this->users_model->getUsersPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer acti_set" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'c_name\',\''.$corder[0].'\')">Name</th>
									<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'email_address\',\''.$corder[1].'\')">Email Address</th>
									<th class="'.$css[2].'" width="20%" onclick="changePaginate(0,\'user_type\',\''.$corder[2].'\')">User Type</th>
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($users)>0)
					{
							if(count($users) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($users as $user) {  
								
								if($user["status"] == "active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "active";
								}
								
						$table .= '<tr id="row_'.$user["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($user["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center">'.($user["c_name"]?$user["c_name"]:"-").'</td> 
								<td>'.$user["email_address"].'</td>
								<td>'.($user["user_type"]?$user["user_type"]:"N/A").'</td>
								<td class="text-center">';
								if($user["user_type"] != 'OPEL'){
									$table .='<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($user["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|';
								}
								else{
									$table .='<span class="adjust">&nbsp;</span>';
								}
								$table .='<a title="View details" alt="viewdetails" id="view" class="view" data-option="'.$this->encrypt->encode($user["id"]).'" href="javascript:void(0)">View Details</a>';
								$table .='| <div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["id"]).'\',\'active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["id"]).'\',\'inactive\')" class="'.$status_inactive.'">Inactive</button>
								  </div>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($users)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($users),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	
	public function change_user_status(){
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["userId"] != "" && $postData["status"] != "")
				{  
					
					if($postData["status"] == "active"){
						$status_update = "active";
					}
					else if($postData["status"] == "inactive"){
						$status_update = "inactive";
					}
					
					$cond = array("user_id" => $this->encrypt->decode($postData["userId"]));
					$users = $this->users_model->getAllUsers($cond);
					
					if(count($users) != 0)
					{ 
						$updateArr = $this->common_model->update(TB_USERS,$cond,array("status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "active";
								}
							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["userId"].'"></td>
									<td class="text-center">'.$users["0"]["c_name"].'</td> 
									<td>'.$users["0"]["email_address"].'</td>
									<td>'.ucwords($users["0"]["user_type"]).'</td>
									<td class="text-center">
										<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["userId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>|';
										if($user["user_type"] != 'OPEL'){
											$row .='<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["userId"].'" href="javascript:void(0)">View Details</a>';
										}
										$row .='| <div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$postData["userId"].'\',\'active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$postData["userId"].'\',\'inactive\')" class="'.$status_inactive.'">Inactive</button>
								    </div>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["userId"]), "msg" => "User Status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "User Status cannot be change")); exit;
						}
					}
				}
			}
		}
	}
	
	
	
	
	public function validateUser($postData)
	{   
		/*if(!ctype_alpha($postData["txt_first_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>First name require character only.</div>');
		}
		else if(!ctype_alpha($postData["txt_last_name"]))
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Last name require character only.</div>');
		}
		else if(!ctype_alnum($postData["txt_company"]) && isset($postData["txt_company"]) && $postData["txt_company"]!="")
		{
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Company require character only.</div>');
		}
		
		if (!preg_match('/^\+(?:[0-9] ?){6,14}[0-9]$/', $postData['txt_contact_number']) && $postData['txt_contact_number']!="") {
			return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Invalid phone number, the number should start with a plus sign, followed by the country code and national number</div>');
		}*/
		return array("status" => 1);
	}
	
	
	public function delete_users() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("user_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_USERS,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	 
	public function getUserDetailsById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array(TB_USERS.".user_id" => $this->encrypt->decode($postData["id"]));
				$join = array(TB_VEHICLE_OWNER_DETAILS => TB_USERS.".user_id = ".TB_VEHICLE_OWNER_DETAILS.".user_id");
				$users = $this->users_model->getUserDetailsById($cond,$join);
				//print_r($users);die;
				$html  ="";
				
				if($users[0]['user_type'] == 'NONOPEL' || $users[0]['user_type'] == 'ADMIN'){
					$html .= '<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Name</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$users[0]['c_name'].'</strong>
									  </div>
									</div>
								  </div>
								 
							</div>
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Email Address</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$users[0]['email_address'].'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Contact Number</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($users[0]['contact_number']?$users[0]['contact_number']:"-").'</strong>
									  </div>
									</div>
								  </div>
							</div>';
				}
				elseif($users[0]['user_type'] == 'OPEL'){
					$html .= '
					<div class="col-sm-12 row"><h4>Customer Details</h4></div>
					<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Name</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.($users[0]['c_name']?$users[0]['c_name']:"-").'</strong>
									  </div>
									</div>
								  </div>
								   <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Marital Status</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.($users[0]['maritial_status']?ucwords($users[0]['maritial_status']):"-").'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Email Address</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$users[0]['email_address'].'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Vehicle Number</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.$users[0]['vehicle_no'].'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Customer Name</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$users[0]['cust_name'].'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Phone Number</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($users[0]['phone_no']?$users[0]['phone_no']:"-").'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Address</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.($users[0]['address']?$users[0]['address']:"-").'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Rewards</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($users[0]['rewards']?$users[0]['rewards']:"-").'</strong>
									  </div>
									</div>
								  </div>
							</div> 
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Date Of Birth</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.($users[0]['date_of_birth']?$users[0]['date_of_birth']:"-").'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Cust Id</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.$users[0]['cust_id'].'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Nationality</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.($users[0]['nationality']?$users[0]['nationality']:"-").'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Id Card No</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($users[0]['id_card_no']?$users[0]['id_card_no']:"-").'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Vehicle History Visit</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.($users[0]['vehicle_history_visit']?$users[0]['vehicle_history_visit']:"-").'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Vin No</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($users[0]['vin_no']?$users[0]['vin_no']:"-").'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Cust Code</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.($users[0]['cust_code']?$users[0]['cust_code']:"-").'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn control-label new_first">PDPA Consent</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($users[0]['pdpa_consent']?$users[0]['pdpa_consent']:"-").'</strong>
									  </div>
									</div>
								  </div>
							</div> 
							';
							
							$cond_v = array(TB_VEHICLE_DETAILS.".customer_id" => $users[0]['customer_id']);
							$join_v = array(TB_VEHICLE_OWNER_DETAILS => TB_VEHICLE_DETAILS.".customer_id = ".TB_VEHICLE_OWNER_DETAILS.".customer_id");
							$vehicles = $this->users_model->getCustomerVehicleDetailsById($cond_v,$join_v);
							if(!empty($vehicles)){
							$html .= '<div class="col-sm-12 row"><h4>Vehicle Details</h4></div>';
							$i=0;
							foreach($vehicles as $vehicle):
								  
								$html .= '<div class="section row mbn">
									  <div class="col-sm-6">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Registration Date</label>
										  <div class="col-lg-8 text_inpu_new"> 
												<strong>'.($vehicle['registration_date']?$vehicle['registration_date']:"-").'</strong>
										  </div>
										</div>
									  </div>
									  <div class="col-sm-6">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Engine Number</label>
										  <div class="col-lg-8 text_inpu_new">
												<strong>'.($vehicle['engine_number']?$vehicle['engine_number']:"-").'</strong>
										  </div>
										</div>
									  </div>
								</div>
								<div class="section row mbn">
									  <div class="col-sm-6">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Chassis Number</label>
										  <div class="col-lg-8 text_inpu_new"> 
												<strong>'.($vehicle['chassis_number']?$vehicle['chassis_number']:"-").'</strong>
										  </div>
										</div>
									  </div>
									  <div class="col-sm-6">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Registration No</label>
										  <div class="col-lg-8 text_inpu_new">
												<strong>'.($vehicle['registration_no']?$vehicle['registration_no']:"-").'</strong>
										  </div>
										</div>
									  </div>
								</div>
								<div class="section row mbn">
									  <div class="col-sm-6">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Vehicle Group</label>
										  <div class="col-lg-8 text_inpu_new"> 
												<strong>'.($vehicle['vehicle_group']?$vehicle['vehicle_group']:"-").'</strong>
										  </div>
										</div>
									  </div>
									  <div class="col-sm-6">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Make Mode</label>
										  <div class="col-lg-8 text_inpu_new">
												<strong>'.($vehicle['make_mode']?$vehicle['make_mode']:"-").'</strong>
										  </div>
										</div>
									  </div>
								</div>';
								
							$cond_service = array(TB_VEHICLE_SERVICE_ORDER.".customer_id" => $vehicle['customer_id'],TB_VEHICLE_SERVICE_ORDER.".vehicle_id" => $vehicle['id']);
							$join_service = array(TB_VEHICLE_DETAILS => TB_VEHICLE_SERVICE_ORDER.".customer_id = ".TB_VEHICLE_DETAILS.".customer_id");
							$vehicles_services = $this->users_model->getCustomerVehicleServiceById($cond_service,$join_service);
							if(!empty($vehicles_services)){
								
								
							
							$html .= '<div class="col-sm-12 row"><h4>Customer Vehicle Orders Details</h4></div>';
							$j=1; 
							foreach($vehicles_services as $service):
								if($j%2==0){
									$class_alt = 'cls_yellow';
								}
								else{
									$class_alt = 'cls_grey';
								}
								$cond_service_p = array("customer_id" => $service['customer_id'], "s_order_id" => $service['s_order_id']);
								
								$vehicles_services_process = $this->users_model->getCustomerVehicleServiceOrderProcessById($cond_service_p);
								
								$html .= '
								
	                                <div class="'.$class_alt.'">
									<div class="section row mbn">
										  <div class="col-sm-4">
											<div class="form-group mbn">
											  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Service Order</label>
											  <div class="col-lg-8 text_inpu_new"> 
													<strong>'.($service['service_order']?$service['service_order']:"-").'</strong>
											  </div>
											</div>
										  </div>
										  <div class="col-sm-4 pn">
											<div class="form-group mbn">
											  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Trasaction Date</label>
											  <div class="col-lg-8 text_inpu_new">
													<strong>'.($service['transaction_date']?$service['transaction_date']:"-").'</strong>
											  </div>
											</div>
										  </div>
										  <div class="col-sm-4">
											<div class="form-group mbn">
											  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Vehicle Milage</label>
											  <div class="col-lg-8 text_inpu_new">
													<strong>'.($service['vehicle_mileage']?$service['vehicle_mileage']:"-").'</strong>
											  </div>
											</div>
										  </div>
									</div>
									</div> 							
								';
								
								if(!empty($vehicles_services_process)){
									foreach($vehicles_services_process as $serv_process):
										$html .= ' 
								<div class="section row mbn">
									  <div class="col-sm-4">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Line Number</label>
										  <div class="col-lg-8 text_inpu_new"> 
												<strong>'.($serv_process['line_number']?$serv_process['line_number']:"-").'</strong>
										  </div>
										</div>
									  </div>
									  <div class="col-sm-4">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Process Code</label>
										  <div class="col-lg-8 text_inpu_new">
												<strong>'.($serv_process['process_code']?$serv_process['process_code']:"-").'</strong>
										  </div>
										</div>
									  </div>
									  <div class="col-sm-4">
										<div class="form-group">
										  <label for="inputStandard" class="col-lg-4 pn control-label new_first">Process Description</label>
										  <div class="col-lg-8 text_inpu_new">
												<strong>'.($serv_process['process_description']?$serv_process['process_description']:"-").'</strong>
										  </div>
										</div>
									  </div>
								</div> ';
									endforeach;
								}
								$j++;
								endforeach;
								
							}
							$i++;
								endforeach;
							}
							
				}
				
				echo json_encode(array("user_details" => $html));exit;
			
		}
	}
	
}
?>
