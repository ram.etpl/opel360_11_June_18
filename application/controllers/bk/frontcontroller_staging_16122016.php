<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FrontController extends CI_Controller{
       
    function __construct() {
		parent::__construct(); 
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("users_model");
		$this->load->model("catalog_model");
		$this->load->model("accessories_model");
		$this->load->model("services_model");
		$this->load->model("pdf_model");
		$this->load->model("events_model");		
		$this->load->model("alpine_model");
		$this->load->model("survey_model");
		$this->carimagepath = realpath('car_gallery');
		$this->load->library('form_validation');
		$this->load->library("email");
		$this->load->helper("email_template"); 
		//$this->load->helper("xml_to_object"); 
		$this->load->library('XmlElement');
		$this->load->helper('download'); 
		$this->no_cache();
		
        
	}
	

	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	 
	private function checkUserStatusOpel($uid){ 
			$cond = array('status' => 'Active','user_id' => $uid);									
			$res = $this->login_model->checkUserStatus(TB_USERS,"email_address,password",$cond);  
			
			if ( $res == 0 ) { 
				$this->session->unset_userdata("auth_densouser");
				$this->session->unset_userdata("stat_id");
				redirect("index"); exit;
			} 
	}
	  
	function index(){   
		
		 $data['title']  = 'Welcome To OPEL360';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/index';
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	
	function models($model_id=""){
		 $data['title']  = 'Models';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/models';
		 $cond_model = array();
		
		 if($model_id != ""){
			 $cond = array("model_id"=>$model_id);
			 $car_catalog = $this->catalog_model->getCatalogModelById($cond);
		 }
		 else{
			 $cond = array();
			 $car_catalog = $this->catalog_model->getCatalogModelFirstById($cond);
		 }
		 $data['car_catalog'] = $car_catalog[0];
		 if(!empty($car_catalog)){
			
			$cond_image  = array("model_id" => $car_catalog[0]['id']);
			$car_catalog_gallry = $this->catalog_model->getCatalogModelGalleryImageById($cond_image);
			$data['car_gallery'] = $car_catalog_gallry[0];
			
		 }
		 
		 //print_r($data);die;
		 
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function contact_us(){
		 $data['title']  = 'Contact Us';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/contact_us';
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	
	function add_survey() {
		$postData = $this->input->post();
		//print_r($postData); die;
		$qid  =$postData['qid'];
		$qids = explode(",",$qid);
		$userdata = $this->session->userdata("auth_opeluser");
		
		for($i=0;$i<count($qids);$i++) {
			$ques_id = $qids[$i];
			$question_id = $postData['qid'.$ques_id];
			$rating = $postData['rating'.$ques_id];
			
			$cond = array("question_id"=>$question_id,"user_id"=>$userdata['id']);
			
			$res = $this->survey_model->getSurveyRatingById($cond);
			
			if(count($res)>0){
				echo json_encode(array("status" => 1, "msg" => "<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>You have already give rating for this survey.</div>")); exit;
			}
			else{
			$insertId = $this->common_model->insert(TB_SURVEY_USER_RATING,array("question_id" => $question_id,"user_id" => $userdata['id'],"rating" => $rating));
			
			echo json_encode(array("status" => 1, "msg" => "<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Thank you for rating.</div>")); exit;
		}
		}
	}

	
	
	function upload_files() {
		
		// A list of permitted file extensions
			$allowed = array('png', 'jpg','jpeg');

			if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

				$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

				if(!in_array(strtolower($extension), $allowed)){
					echo '{"status":"error"}';
					return false;
				}

				if(move_uploaded_file($_FILES['upl']['tmp_name'], $this->carimagepath.'/'.$_FILES['upl']['name'])){
					
					$img_name = $_FILES['upl']['name'];
					$model_id = $_POST['car_model_id'];
					
					//Get modelinfo
					$cond = array("model_id" => $this->encrypt->decode($model_id));
					$car_catalog_model = $this->catalog_model->getCatalogModelById($cond);
					$propellant = $car_catalog_model[0]['propellant'];
					
					$insertId = $this->common_model->insert(TB_CAR_GALLERY,array("model_id" => $this->encrypt->decode($model_id),"propellant" => $propellant,"img_name" =>$img_name,"img_file" => $img_name,"date_modified"=>date('Y-m-d H:i:s')));
						
					echo '{"status":"success"}';
					exit;
				}
			}
		
	}
	
	
	
	
	function survey(){
		 
		 $data['title']  = 'Survey';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/survey';
		 $date = date('Y-m-d');
		 $userdata = $this->session->userdata("auth_opeluser");
		 $data['access_denied']  ="";
		 if($userdata['user_type'] == 'OPEL'){
		 
		 $user_id = $userdata['id'];
		 //Get current survey.
		 $cond = array("survey_from_date >" => $date);
		 $like = array();
		 $survey_count = $this->survey_model->getSurveyCount($cond,$like);
		 //print_r($survey_count); die;
		 if($survey_count>0) {
			//Get survey details. 
			$survey_details = $this->survey_model->getSurveyById($cond);
			
			if(count($survey_details)>0) {
				for($i=0;$i<=count($survey_details); $i++) {
					$survey_id  = $survey_details[$i]['id'];
					
					//Get all survey question
					$cond = array("fk_survey_id" => $survey_id);
					$All_survey_questions = $this->survey_model->getallSurveyQuestion($cond);
					if(count($All_survey_questions)>0) {
						$k=0;
					for($j=0;$j<count($All_survey_questions); $j++) {

						$questions = $All_survey_questions[$j]['question'];
						$questions_id = $All_survey_questions[$j]['id'];


					$data['survey_questions_data'] .= <<<HTML
					<div class="sur_inner">
						<div class="lt_survey">
							<div class="how_head">$questions</div>
						</div>
						<div class="rt_survey">
							<div class="rate-review" id="rate-review">
								<div class="rate">
									<input type="hidden" name="user_id" id="user_id" value="$user_id">
									<input type="radio" id="rating0$j" name="rating$j" value="10" /><label class="ratingstar" data-qid="$questions_id" data-val="5"  for="rating0$j" title="5"></label>
									<input type="hidden" id="qid$questions_id" name="qid$questions_id" value="" />
									<input type="hidden" id="rating$questions_id" name="rating$questions_id" value="" />
									<input type="radio" id="rating1$j" name="rating$j" value="9" /><label class="ratingstar half" for="rating1$j" data-qid="$questions_id" data-val="4.5" title="4.5"></label>
									<input type="radio" id="rating2$j" name="rating$j" value="8" /><label class="ratingstar" for="rating2$j" data-qid="$questions_id" data-val="4" title="4"></label>
									<input type="radio" id="rating3$j" name="rating$j" value="7" /><label class="ratingstar half" for="rating3$j" data-qid="$questions_id" data-val="3.5" title="3.5"></label>
									<input type="radio" id="rating4$j" name="rating$j" value="6" /><label class="ratingstar" for="rating4$j" data-qid="$questions_id" data-val="3" title="3"></label>
									<input type="radio" id="rating5$j" name="rating$j" value="5" /><label class="ratingstar half" for="rating5$j" data-qid="$questions_id" data-val="2.5" title="2.5"></label>
									<input type="radio" id="rating6$j" name="rating$j" value="4" /><label class="ratingstar" for="rating6$j" data-qid="$questions_id" data-val="2" title="2"></label>
									<input type="radio" id="rating7$j" name="rating$j" value="3" /><label class="ratingstar half" for="rating7$j" data-qid="$questions_id" data-val="1.5" title="1.5"></label>
									<input type="radio" id="rating8$j" name="rating$j" value="2" /><label class="ratingstar" for="rating8$j" data-qid="$questions_id" data-val="1" title="1"></label>
									<input type="radio" id="rating9$j" name="rating$j" value="1" /><label class="ratingstar half " for="rating9$j" data-qid="$questions_id" data-val="0.5" title="0.5"></label>
									<input type="radio" id="rating10$j" name="rating$j" value="0" /><label style="display:none;" for="rating10$j" title="No star" ></label>
								</div>
							</div>
						</div>
					</div>
HTML;
	$k++;
				}
			}
				}
			}
			else {
			}
		 }
		 else {
			 
		 }
		 
	 }
	 else{
		 $data['access_denied']  ="Access Denied";
	 }
		//print_r($data);die;
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function about_us(){
		 $data['title']  = 'About Us';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/about_us';
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function accessories(){
		 $data['title']  = 'Accessories';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/accessories';
		 $cond = array();
		 $data['models']  = $this->catalog_model->getAllDistinctCatalogModel($cond);
		 //echo $this->db->last_query();die;
		 $data['acctype']  = $this->accessories_model->getAccessoriesTypeById($cond);
		// print_r($data);die;
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	
	function getAccesseriesByModel(){
		 $postData = $_POST;
		   
		 if(!empty($car_catalog)){
			
			$cond_image  = array("model_id" => $postData['id']);
			$car_catalog_gallry = $this->catalog_model->getCatalogModelGalleryImageById($cond_image);
			$data['car_gallery'] = $car_catalog_gallry[0];
			
		 }
	}
	
	function news_and_events(){
		 $data['title']  = 'News And Events';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/news_and_events';
		 
		 $cond_upcoming = array("event_show_to" => "2", "status" => "Upcoming");
		 $join = array();
		 $data['upcoming_events'] = $this->events_model->getAllEvents($cond_upcoming,$join);
		 
		 $cond_running = array("event_show_to" => "2", "status" => "Running");
		 $data['running_events'] = $this->events_model->getAllEvents($cond_running,$join);
		 
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function gallery(){
		 $data['title']  = 'Gallery';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/gallery';
		 $data['models']  = $this->catalog_model->getAllDistinctCatalogModel($cond);
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	
	function getPdf(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			$cond = array("model_id" => $postData["m_id"]);
			$pdf = $this->pdf_model->getPDFById($cond);
			$html ='';
			if(count($pdf)>0){
				$i=0;
				foreach($pdf as $cat):
				
				$html .='<ul class="pdf_wra">
						<li><a target="_blank" href="'.base_url().'images/PDF/'.$cat['p_file'].'"><img src="'.base_url().'images/pdf_icon.png"/>'.$cat['p_name'].'</a></li>
						</ul>';
				$i++;
				endforeach;
			}
			else{
				$html .='<div class="no_records">No Records Available</div>';
			}
			
			echo json_encode(array("html"=>$html));exit;
		}
	}
	
	
	
	function getGallery(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			 
			//print_r($postData);die;
			$cond = array("model_id" => $postData["m_id"]);
			$catlog = $this->catalog_model->getCatalogModelGalleryById($cond);
			//print_r($users);die;
			$html ='';
			if(count($catlog)>0){
				$no_records = 1;
				$i=0;
				foreach($catlog as $cat):
				$last ='';
				if($i == 2){
					$last = 'last_gal';
				}
				$html .='			
						<div class="first_gal '.$last .'">
							<a href="'.base_url().'car_gallery/'.$cat['img_file'].'" class="lightbox">
								<figure class="effect-phoebe">
									<img src="'.base_url().'car_gallery/'.$cat['img_file'].'" alt="'.$cat['img_name'].'"/>
								</figure>
							</a>		
						</div>';
				$i++;
				if($i==3){
					$i=0;
				}
				endforeach;
			}
			else{
				$no_records = 0;
				$html .='<div id="no_records">No Records Available</div>';
			}
			
			echo json_encode(array("html"=>$html,"no_records"=>$no_records));exit;
		}
	}
	


		function getAccesseriesGallery(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			$cond = array("model_id" => $postData["m_id"],"accessories_name" => $postData["acc_id"]);
			$accessories = $this->accessories_model->getAccesseriesGallery($cond);
			//print_r($accessories);die;
			$html ='';
			if(count($accessories)>0){
				
				$no_records = 1;
				$i=0;
				foreach($accessories as $cat):
				$last ='';
				if($i == 2){
					$last = 'last_gal';
				}
				$html .='			
						<div class="first_gal '.$last .'">
							<a href="'.base_url().'images/Accessories/'.$cat['accessories_pic'].'" class="lightbox">
								<figure class="effect-phoebe">
									<img src="'.base_url().'images/Accessories/'.$cat['accessories_pic'].'" alt="'.$cat['accessories_pic'].'"/>
								</figure>
							</a>		
						</div>';
				$i++;
				if($i==3){
					$i=0;
				}
				endforeach;
			}
			else{
				$no_records = 0;
				$html .='<div id="no_records">No Records Available</div>';
			}
			
			echo json_encode(array("html"=>$html,"no_records"=>$no_records));exit;
		}
	}



	
	function profile(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'Profile';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/profile';
			 $this->load->view('frontend/incls/layout', $data);  
		}
		 
	}
	
	/********* User Verification ***********/
	
	function verifyAccount(){
	
       $data['title']  = 'Activation Account';
	   $data['meta_description'] = '';
	   $data['meta_keyword'] = '';
       $data['main_content'] = 'frontend/activate_account';
       $key   = $this->input->get('key');
       $email = $this->input->get('email');
       $cond_key = array("resetkey"=>$key,"email_address"=>$email,"status"=>"Inactive");
       $activate_acc = $this->users_model->getAllUsers($cond_key);
     
       
		if(count($activate_acc)>0){
				$cond_update = array("resetkey"=>$key,"email_address"=>$email);
				$data_update = array("status"=>"active");
				$this->common_model->update(TB_USERS,$cond_update,$data_update);
            $data['message'] = "Your account is verified successfully.Please <a href='javascript:void(0);' onclick='make_login();'>click here</a> to login to your account";
             $this->load->view('frontend/incls/layout', $data);

        }
        else{
			
            $data['validation_message'] = "The key is invalid or You have already verified the account";
             $this->load->view('frontend/incls/layout', $data);
        } 
    }
	
	function service_costs(){
		 $data['title']  = 'Service Costs';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = ''; 
		 $data['main_content'] = 'frontend/service_costs';
		 $cond = array();
		 $data['services_cost'] = $this->services_model->getAllServicesCostByModel($cond);
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	function rent_an_opel(){
		 $data['title']  = 'Rent An Opel';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/rent_an_opel';
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function dashboard(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'Dashboard';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/dashboard';
			 $this->load->view('frontend/incls/layout', $data);
		 }
	}
	
	function pdf_manuals(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'PDF Manuals';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/pdf_manuals';
			 $cond = array();
			 $data['pdf_manuals'] = $this->pdf_model->getPDFById($cond);
			 $data['models']  = $this->catalog_model->getAllDistinctCatalogModel($cond);
			 $this->load->view('frontend/incls/layout', $data);
		 }
	}
	
	function forgotUserPassword(){ 
		   $data['title']  = 'Forgot Password';
		   $data['meta_description'] = '';
		   $data['meta_keyword'] = '';
		   $data['sucess_message'] = $this->session->flashdata('sucess_message');
		   //echo $this->session->userdata('tokenfrm');   
           $this->form_validation->set_rules('uemail', 'Email address', 'valid_email|xss_clean|required'); 
           if ($this->form_validation->run() !== false) { 
					$useremail = trim($this->input->post('uemail'));
                    $cond = array('email_address' => $useremail);
					$userData = $this->login_model->validUser(TB_USERS,"user_id,country,first_name,last_name,email_address,password,v_cat_id,v_sub_cat_id",$cond);
						//echo "test";die;
					if(count($userData) > 0)
					{	 
                        // $username = $this->loginmodel->getUserNameByUserEmail($uname);
                         $reset_key  = md5(uniqid(mt_rand(), true));
                         $datareset = array(
                              'dn_resetkey' =>  $reset_key,
                         ); 
                         $cond1 = array('email_address' =>  $useremail); 
                         $hostname = $this->config->item('hostname');
                         $this->common_model->update(TB_USERS,$cond1,$datareset);
                         $config['mailtype'] ='html';
                         $config['charset'] ='iso-8859-1';
                         $this->email->initialize($config);
                         $from  = EMAIL_FROM; 
						 $uname = $userData[0]['first_name']." ".$userData[0]['last_name'];
						 $this->messageBody .= email_header();
                         $this->messageBody  = "Hello $uname,<br/><br/>
                            We have received your request to reset your password. <br/> <br/>
                             Please click the link below to reset.<br/><br/>
                             <a href=".$hostname."resetuserpwd/?key=".$reset_key.">".$hostname."resetuserpwd/?key=".$reset_key."</a>
                             <br/><br/>If the link is not working properly, then copy and paste the link in your browser.<br/>
                             If you did not send this request, please ignore this email.
                             <br/><br/>Regards,<br/>DENSO AFTERMARKET WEBSITE";
                        $this->messageBody .= email_footer();     
						//echo $this->messageBody;
						//die;
                        
						 $this->email->from($from, 'DENSO ASIA AFTERMARKET');
						 $this->email->to("$useremail");

                         $this->email->subject('DENSO AFTERMARKET WEBSITE - Reset Password');
                         $this->email->message($this->messageBody);	
                             
                        if ( ! $this->email->send()){
                                $this->session->set_flashdata('sucess_message', "The email can not be send ! Server Error.");
                                 //$data['sucess_message'] ="The email can not be send ! Server Error";
                                redirect('forgotpassword');
                                 //$this->load->view('frontend/incls/layout', $data);
                        }else{
                                $this->session->set_flashdata('sucess_message', "The reset password link has been sent to your email address.");
                                //$data['sucess_message'] = "The reset password link has been sent to your email address.";
                                redirect('forgotpassword');
                                //$this->load->view('frontend/incls/layout', $data);
                        }
                    } 
                    else{
						$data['validation_message'] = "The email address is not found in our database";
                         //var_dump($data);
						 $this->load->view('frontend/forgot_password', $data);
					} 
           }
           else{
                $data['message'] = $this->session->flashdata('message');
                $this->load->view('frontend/forgot_password', $data);
           }
           
	}
	
	  
    public function logout()
	{
		
		$this->session->unset_userdata("auth_opeluser");
		echo json_encode(array("status"=>"success","msg"=> "Bad Request"));exit;
	}
	
	
	public function resend_activation_link() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();  
			$uemail = $postData['email'];
			
			//Send activation link
			$activation_key  = md5(uniqid(mt_rand(), true));
							
							$cond = array("email_address" => $uemail);
							$this->common_model->update(TB_USERS,$cond,array("resetkey" => $activation_key ));
							$link ="<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">resend</a>";
							
							$cond = array('email_address' => $uemail, 'resetkey' => $activation_key);
							$userData = $this->users_model->getAllUsers($cond);
							
							$hostname = $this->config->item('hostname');
							 $config['mailtype'] ='html';
							 $config['charset'] ='iso-8859-1';
							 $this->email->initialize($config);
							 $from  = EMAIL_FROM; 
							 $this->messageBodyActivation  .= email_header();
							 $uname = ucwords($userData[0]['name']);
							 $this->messageBodyActivation  .= "Hello $uname,<br/><br/>
								 
								 Click below link for login to your account.<br/>
								<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail."</a>
								 <br/><br/>If link is not working properly then copy and paste the link in browser<br/>
								";
							
							 $this->messageBodyActivation  .= email_footer();
							// echo $this->messageBodyActivation;
							// die;
							 $this->email->from($from, $from);
							 $this->email->to("$uemail");

							 $this->email->subject('Account activation');
							 $this->email->message($this->messageBodyActivation);	
								 
							 $this->email->send();
			
							 echo json_encode(array("status" => 1,"action" => "login", "msg" => "<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Activation link is sent to your account.</div>")); exit;
			
		}
	}
	
	public function resend_activation_link_on_deactivation() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();  
			
			$uemail = $postData['email'];
			
			
			//Send activation link
			$activation_key  = md5(uniqid(mt_rand(), true));
							
							$cond = array("email_address" => $uemail);
							$this->common_model->update(TB_USERS,$cond,array("resetkey" => $activation_key ));
							$link ="<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">resend</a>";
							 
							 
							$cond_resend = array('email_address' => $uemail, 'resetkey' => $activation_key);
							$userData = $this->users_model->getAllUsers($cond_resend);
							//print_r($userData); 
							 
							 $hostname = $this->config->item('hostname');
							 $config['mailtype'] ='html';
							 $config['charset'] ='iso-8859-1';
							 $this->email->initialize($config);
							 $from  = EMAIL_FROM; 
							 $this->messageBodyActivation  .= email_header();
							 $uname = ucwords($userData[0]['c_name']);
							 $this->messageBodyActivation  .= "Hello $uname,<br/><br/>
								 
								 Click below link for login to your account.<br/>
								<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail."</a>
								 <br/><br/>If link is not working properly then copy and paste the link in browser<br/>
								";
							
							 $this->messageBodyActivation  .= email_footer();
							 //echo $this->messageBodyActivation;
							 //die;
							 $this->email->from($from, $from);
							 $this->email->to("$uemail");

							 $this->email->subject('Account activation');
							 $this->email->message($this->messageBodyActivation);	
								 
							 $this->email->send();
			
							 echo json_encode(array("status" => 1,"action" => "login", "msg" => "<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Activation link is sent to your account.</div>")); exit;
			
		}
	}
	
	function service_booking(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{
			
			 $data['title']  = 'Service Booking';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/service_booking';
			 $userdata = $this->session->userdata("auth_opeluser");
			
			 
			 if($userdata['user_type'] =='OPEL'){
				 $cond = array("user_id" => $userdata['id']);
				 $data['vehicle_list'] = $this->users_model->getCustomerVehicleNoById($cond);
				 
				 
				//Get All Registretion nos 
				//print_r($cond['vehicle_list']);die;
			 }
			 $this->load->view('frontend/incls/layout', $data);  
		}
	}
	
	function alpineGetBookingInfo(){
		$postData = $_POST;  
		$post_function  = "GetBookinginfo";
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success", "result_val"=> $result_array));exit;
	}
	
	function alpineGetCalendar(){
		$postData = $_POST;  
		$post_function  = "GetCalendar";
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success", "result_val"=> $result_array));exit;
	}
	
    function alpineCheckValidation(){
		$postData = $_POST;  
		//$curl_post = "VehicleNo=".$postData['VehicleNo']."&doctype=".$postData['doctype']."&deptcode=".$postData['deptcode']."&refno=".$postData['refno']."&cmd=".$postData['cmd']."&cust_code=".$postData['cust_code']."";
		$post_function  = "Checkvalidation";
		//$cond = array("VehicleNo" => $postData['vehicle_no'],"doctype"=>$postData['doctype'],"deptcode" => $postData['deptcode'], "refno" => $postData['refno'], "cmd" => $postData['cmd'] , "cust_code" => $postData['cust_code']);
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success","result_val"=> $result_array));exit;
	}
	
	function alpineGetVehicle(){
		//print_r();
		$postData = $_POST;  
		//$curl_post = "VehicleNo=".$postData['VehicleNo']."&doctype=".$postData['doctype']."&deptcode=".$postData['deptcode']."&refno=".$postData['refno']."&cmd=".$postData['cmd']."&cust_code=".$postData['cust_code']."";
		$post_function  = "GetVeh";
		//$cond = array("VehicleNo" => $postData['vehicle_no'],"doctype"=>$postData['doctype'],"deptcode" => $postData['deptcode'], "refno" => $postData['refno'], "cmd" => $postData['cmd'] , "cust_code" => $postData['cust_code']);
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success","result_val"=> $result_array));exit;
	}
	
	function convert_to_json_array($xml_response){
		$xml_snippet = simplexml_load_string($xml_response);
		
		$json_convert = json_encode($xml_snippet);
		
		$json = json_decode( $json_convert );
		
		$array_response = array();
		
		foreach($json as $oEntry => $val){ 
			
				if(is_array($val)){
				
					$decode = simplexml_load_string($val);
					foreach ($decode as $d):
						foreach ($d as $k => $v):
							$array_response[$k] =  trim((string)$v);
						endforeach;
					endforeach;
					
					return $array_response;
				}
				else{
					return $val;
				}
		}
		
	}
	
	function postAndGetDataWithAlpineService($POST,$post_function){
		
		$xml_response = $this->alpine_model->CheckAlphineService($POST, $post_function);
		//print_r($xml_response);exit;
		$result_array = $this->convert_to_json_array($xml_response);
		//print_r($result_array);
		return array("status"=>"success", "result_val" => $result_array, "result_msg" => $result_array);
	} 
	
	function postAndGetDataWithAlpineServiceForXMLAarray($POST,$post_function){
		
		$xml_response = $this->alpine_model->CheckAlphineService($POST, $post_function);
		
		return array("status"=>"success", "result_val" => $xml_response);
	} 
	
	function getCurrentBookingVehicle(){
		
		$POST['coyId'] = 'OPEL';
		$POST['docId'] = 'WSB';
		$POST['deptId'] = 'ASC';
		
		$first = date('Y-m-d',strtotime('last Sunday'));
		$last = date('Y-m-d',strtotime('next Saturday'));
		
		$POST['startWeek'] = $first.'T00:00:00.000';
		$POST['endWeek'] = $last.'T00:00:00.000';
		$POST['appVehNo'] = 'SKG2091G';
		
		$currentBooking = $this->soapRequest('GetCurrentVehicleBooking',$POST);
		
		print_r($currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram']['NewDataSet']['Table']);die;
		
	} 
	
	function getUnAvailableTimeSlot(){
		if(is_ajax_request())
		{
			
			$first = date('Y-m-d',strtotime('last Sunday'));
			$last = date('Y-m-d',strtotime('next Saturday'));
			 
			$POST['startDate'] = $first.'T08:00:00';
			$POST['endDate'] = $last.'T17:00:00';
			
			$response = $this->soapRequest('GetUnExistTimeSlots',$POST);
			
			//print_r($response);die;
			
			$arr_block = array();
			$arr_push = array();
			$k=1;
			
			$farr = $response['result_soap']['GetUnExistTimeSlotsResult']['diffgram']['NewDataSet']['Table'];
			//print_r($farr);die;
			foreach($farr as $child): 
					
					$value_from =  $child['FR_TIME'];
					$value_to =  $child['TO_TIME'];
					
					$arr_block['selectable'] = 'false';
					$arr_block['title'] = 'blocked';
					$arr_block['start'] =  $value_from;
					$arr_block['end'] =  $value_to;
					$arr_block['rendering'] =  "background";
					$arr_block['color'] =  "#FF5722";
					$k++; 
					
					array_push($arr_push,$arr_block);
			endforeach;
		//	die;
			//print_r($arr_push);die;
			
		}
		
		if(!empty($arr_block)){
			echo json_encode(array("array_block"=>$arr_push));exit;
		}
		else{
			echo json_encode(array( "status" => "success", "from_time" => "", "to_time" => ""));exit;
		}
	}

	
	function bookingSubmitNext(){
		if(is_ajax_request())
		{
			$postData = $_POST;
			//print_r($postData);die;
			
			$arrBookingData = array();
			$arrBookingData['booking_date'] = $postData['book_slot'];
			$arrBookingData['booking_milage'] = $postData['booking_milage']."K";
			$arrBookingData['remarks'] = $postData['remarks'];
			$arrBookingData['ref_no'] = $postData['ref_no'];
			
			echo json_encode(array( "status" => "success", "booking_info" => $arrBookingData));exit;
		}
	}
	
	function bookingConfirm(){
			if(is_ajax_request())
			{
				$postData = $_POST;
				
				//print_r($postData);die;
				$join = array(TB_VEHICLE_DETAILS => TB_VEHICLE_OWNER_DETAILS.".customer_id = ".TB_VEHICLE_DETAILS.".customer_id");
				$cond_veh = array("registration_no" => $postData['veh_no']);
				$custInfo = $this->users_model->getCustomerInfo($cond_veh,$join); 
				$POST['cust_code']  = $custInfo[0]['cust_code'];
				$POST['veh_no'] = $postData['veh_no'];
				
				
				if($postData['book_slot']!=""){
				
				
				/*check whether 5 max booking */
				
				$POST_MAX['vehDate'] = $postData['book_slot'];
				$POST_MAX['frTime'] =  $postData['from_time'];
				$POST_MAX['toTime'] = $postData['from_time'];
				$POST_MAX['coyID'] = 'OPEL';
				$POST_MAX['deptID'] = 'ASC';
				$POST_MAX['runNo'] = '';
				
				
				$maxBookingTimeslot = $this->soapRequest('GetMaxSlotBooking',$POST_MAX);
				
				$totalbookings = $maxBookingTimeslot['result_soap']['GetMaxSlotBookingResult'];
				
				//echo $POST['vehicle_recv_date'] =  date('Y-m-dTh:i:s',strtotime($postData['book_slot']));//2016-05-20T08:15:00
				
				if($totalbookings<=5){
					$custData = $this->soapRequest('GetCust',$POST);
					$vehData = $this->soapRequest('GetVeh',$POST); 
					
					$arrBookingData = array();
					$xmlCust = new SimpleXMLElement($custData['result_soap']['GetCustResult']); 
					$xmlCust->asXML();
					$xmlVeh = new SimpleXMLElement($vehData['result_soap']['GetVehResult']); 
					$xmlVeh->asXML();
					
					if($postData['book_action'] == "I"){
						$ref_no  =  '';
						$doctype  = "WSB";
						$doccode  = "ASC";
						$is_cur_booking = "N";
						$action = "I";
					}
					elseif($postData['book_action'] == "U"){
						$ref_no  =  $postData['ref_no'];
						
						$doctype  = "WSB";
						$doccode  = "ASC";
						$is_cur_booking = "Y";
						$action = "U";
					}
					else{
						$ref_no='';
						$doctype  = "WSB";
						$doccode  = "ASC";
						$is_cur_booking = "N";
						$action = "I";
					}
					
					$arrBookingData['COMP_CODE'] = trim((string)$xmlCust->GetCust->COMP_CODE);
					$arrBookingData['DOC_TYPE'] = $doctype;
					$arrBookingData['DEPT_CODE'] = $doccode;
					$arrBookingData['REF_NO'] = $ref_no;
					$arrBookingData['VEH_NO'] = $postData['veh_no'];
					$arrBookingData['CUST_CODE'] = trim((string)$xmlCust->GetCust->CUST_CODE);
					$arrBookingData['CUST_NAME'] = trim((string)$xmlCust->GetCust->CUST_NAME);
					$arrBookingData['BOOK_BY_NAME'] = trim((string)$xmlCust->GetCust->CUST_NAME);
					$arrBookingData['TXT_MLEG'] = "";
					$arrBookingData['MLEG_IN'] = $postData['booking_milage'];
					$arrBookingData['PC_IND'] = "N";
					$arrBookingData['PYMT_MODE'] = trim((string)$xmlCust->GetCust->PYMT_MODE);
					$arrBookingData['VEH_RECV_DATE'] = $postData['book_slot'];
					$arrBookingData['VEH_GRP'] = trim((string)$xmlVeh->Getveh->VEH_GRP);
					$arrBookingData['MAKE_MDL'] = trim((string)$xmlVeh->Getveh->MAKE_MDL);
					$arrBookingData['COLR_CODE'] = trim((string)$xmlVeh->Getveh->COLR_CODE);
					$arrBookingData['YR_MNFC'] = trim((string)$xmlVeh->Getveh->YR_MNFC);
					$arrBookingData['CHAS_NO'] = trim((string)$xmlVeh->Getveh->CHAS_NO);
					$arrBookingData['ENG_NO'] = trim((string)$xmlVeh->Getveh->ENG_NO);
					$arrBookingData['REG_DATE'] = trim((string)$xmlVeh->Getveh->REG_DATE);
					$arrBookingData['CUST_BLK'] = trim((string)$xmlCust->GetCust->BLDG_NO);
					$arrBookingData['CUST_FLR'] = trim((string)$xmlCust->GetCust->FLR_NO);
					$arrBookingData['CUST_UNIT'] = trim((string)$xmlCust->GetCust->UNIT_NO);
					$arrBookingData['CUST_ADDR'] = trim((string)$xmlCust->GetCust->ST_NAME);
					$arrBookingData['CUST_CTY'] = trim((string)$xmlCust->GetCust->CUST_CTY);
					$arrBookingData['CUST_POSTAL_CODE'] = trim((string)$xmlCust->GetCust->CUST_CTY);
					$arrBookingData['CUST_DISTRICT'] = "";
					$arrBookingData['CUST_HOME'] = "";
					$arrBookingData['CUST_HDPH'] = trim((string)$xmlCust->GetCust->TEL_NO);
					$arrBookingData['CUST_OFCE'] = trim((string)$xmlCust->GetCust->COLR_3);
					$arrBookingData['CUST_PGR'] = trim((string)$xmlCust->GetCust->COLR_5);
					$arrBookingData['CUST_EMAIL'] = trim((string)$xmlCust->GetCust->CUST_EMAIL);
					$arrBookingData['REMK'] = $postData['remarks'];			
					$arrBookingData['STS'] = "I";
					$arrBookingData['RSRV_CHAR_FIELD_1'] = trim((string)$xmlVeh->Getveh->RSRV_CHAR_FIELD_1);
					$arrBookingData['RSRV_CHAR_FIELD_2'] = "Y";
					$arrBookingData['RSRV_CHAR_FIELD_6'] = trim((string)$xmlCust->GetCust->COLR_4);
					$arrBookingData['RSRV_CHAR_FIELD_7'] = "Not Exist";
					$arrBookingData['RSRV_CHAR_FIELD_8'] = "S";
					$arrBookingData['CLOSED_FLAG'] ="N";
					
					//print_r($arrBookingData);//die; 
					$xmlb = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><Data></Data>");
					$node = $xmlb->addChild('Booking'); 
					// function call to convert array to xml
					$this->array_to_xml($arrBookingData, $node);
					// display XML to screen
					$infoBooking  =  $xmlb->asXML();
					//print_r($infoBooking);die;
					$PostBooking["is_current_booking"] = $is_cur_booking;
					$PostBooking["action"] = $action;
					$PostBooking["info"] = $infoBooking;
					
					//print_r($PostBooking);
					//die;
					$responseBooking = $this->soapRequest('SetAlpineBooking',$PostBooking);
					
					//print_r($responseBooking);die;
					
					//die();
					if($responseBooking['result_soap']['SetAlpineBookingResult'] !=""){
						echo json_encode(array( "status" => 'success', "message_booking" => "<div class='alert alert-success'>".$responseBooking['result_soap']['SetAlpineBookingResult'].""));exit;
					}
					else{
						echo json_encode(array( "status" => 'failed', "message_booking" => "<div class='alert alert-danger'>Oops !!! Someting went wrong</div>"));exit;
					}
				}
				else{
						echo json_encode(array( "status" => 'failed', "message_booking" => "<div class='alert alert-danger'>Maximum 5 bookings are allowed for this date and time.<br/>Please select another time slot to booked.</div>"));exit;
				}
			}
			else{
				echo json_encode(array( "status" => 'failed', "message_booking" => "<div class='alert alert-danger'>Please select a valid booking date</div>"));exit;
			}
		}
	}
	
	
	function array_to_xml($array, &$xml) {
		foreach($array as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml->addChild("$key");
					array_to_xml($value, $subnode);
				} else {
					array_to_xml($value, $xml);
				}
			} else {
				$xml->addChild("$key","$value");
			}
		}
	}
	
	function bookingAction(){
		if(is_ajax_request())
		{
			$postData = $_POST;
			
			$join = array(TB_VEHICLE_DETAILS => TB_VEHICLE_OWNER_DETAILS.".customer_id = ".TB_VEHICLE_DETAILS.".customer_id");
			$cond_veh = array("registration_no" => $postData['veh_no']);
			$custInfo = $this->users_model->getCustomerInfo($cond_veh,$join);
			
			switch($postData['book_action']){
				case 'I':
					//$this->session->set_userdata(); 
				
						//print_r($custInfo);die;
						
						$POST['cust_code']  = $custInfo[0]['cust_code'];
						$POST['veh_no']  = $postData['veh_no'];
						 
						/*check whether the booking is available for the current vehicle or not*/
						
						$POST['doc_type'] = 'WSB';
						$POST['dept_code'] = 'ASC';
						
						//$custData = $this->postAndGetDataWithAlpineService($POST,'GetCust');
						//$checkBooking = $this->postAndGetDataWithAlpineService($POST,'GetCalendar');
						
						$custData = $this->soapRequest('GetCust',$POST);
						$checkBooking = $this->soapRequest('GetValidity',$POST);
						
						//print_r($custData);
						//print_r($checkBooking);
						//die;
						
						if($checkBooking['result_soap']['GetValidityResult'] == ""){
						
							$xml = new SimpleXMLElement($custData['result_soap']['GetCustResult']); 
							$xml->asXML();
			 
							$CUST_NAME =   ($xml->GetCust->CUST_NAME?$xml->GetCust->CUST_NAME:"-");
							$TEL_NO    =   ($xml->GetCust->TEL_NO?$xml->GetCust->TEL_NO:"-"); 
							
							$cust_html = '<div class="form-group select_drop">
													<label class="col-md-5">Customer Name - </label>
													<div class="col-md-7 rt_cust">
														<div class="view_text">'.$CUST_NAME.'</div>
													</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Vehicle No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$postData['veh_no'].'</div>
												</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Customer HP No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$TEL_NO.'</div>
												</div>
										</div>';
							echo json_encode(array( "status" => "success", "cust_info" => $custInfo[0], "customer_html" => $cust_html ,"booking_action" => "I" ,"message" => ""));exit;			
						}
						else{
							
							$first = date('Y-m-d',strtotime('-3 days'));
							$last = date('Y-m-d',strtotime('+30 days'));
							$POST_EXIST['coyId'] = 'OPEL';
							$POST_EXIST['docId'] = 'WSB';
							$POST_EXIST['deptId'] = 'ASC';
							$POST_EXIST['startWeek'] = $first.'T00:00:00.000';
							$POST_EXIST['endWeek'] = $last.'T00:00:00.000';
							$POST_EXIST['appVehNo'] = $postData['veh_no'];
							//print_r($POST_EXIST);die;
							$currentBooking = $this->soapRequest('GetCurrentVehicleBooking',$POST_EXIST);
							$revArr = $currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram']['NewDataSet']['Table'];
							//print_r($respBook);//die;
							//$revArr = array_reverse($respBook);
							
							//print_r($revArr);
							
							//echo $revArr[0]['START_DATE'];
							//echo sizeof($revArr);die;
							if($revArr[0]){
								//echo "test1";
								if(isset($revArr[0]['START_DATE'])){
									$vehicle_received_date = substr($revArr[0]['START_DATE'],0,10);
									$vehicle_received_time = substr($revArr[0]['START_DATE'],11,16);
									$vehicle_received_time = explode("+",$vehicle_received_time);
									$vehicle_rec_time = $vehicle_received_time[0];
								}
								else{
									$vehicle_received_date = substr($revArr[0]['VEH_RECV_DATE'],0,10);
									$vehicle_received_time = substr($revArr[0]['VEH_RECV_DATE'],11,16);
									$vehicle_received_time = explode("+",$vehicle_received_time);
									$vehicle_rec_time = $vehicle_received_time[0];
								}
								$ref_no = $revArr[0]['RUN_NO'];
							}
							else{
								//echo "test2";
								if(isset($revArr['START_DATE'])){
									$vehicle_received_date = substr($revArr['START_DATE'],0,10);
									$vehicle_received_time = substr($revArr['START_DATE'],11,16);
									$vehicle_received_time = explode("+",$vehicle_received_time);
									$vehicle_rec_time = $vehicle_received_time[0];
								}
								else{
									$vehicle_received_date = substr($revArr['VEH_RECV_DATE'],0,10);
									$vehicle_received_time = substr($revArr['VEH_RECV_DATE'],11,16);
									$vehicle_received_time = explode("+",$vehicle_received_time);
									$vehicle_rec_time = $vehicle_received_time[0];
								}
								$ref_no = $revArr['RUN_NO'];
							}
							
							//die;
							$html_booking_info = "<div class='alert alert-info'><b>Latest Booking Info</b>:<br/><div>Booking Date and Time : $vehicle_received_date at $vehicle_rec_time</div><div>Reference Number : $ref_no</div></div>";
							echo json_encode(array( "status" => "failed","booking_info"=>$html_booking_info, "message" => '<div class="alert alert-danger">Booking is already in maintenance for this vehicle'));exit;
						}	
					break;
					
					case 'U':
						$POST['doctype'] = "WSB";
						$POST['deptcode']  = "ASC";
						$POST['cust_code']  = $custInfo[0]['cust_code'];
						$POST['VehicleNo']  = $postData['veh_no'];
						$POST['refno'] = $postData['ref_no'];
						$POST['cmd'] = $postData['book_action'];
						$resp = $this->soapRequest('Checkvalidation',$POST);
						$custData = $this->soapRequest('GetCust',$POST);
						//print_r($resp);die;
						$msg = "";
						
						if($resp['result_soap']['CheckvalidationResult'] != ""){
							$resp_msg = $resp['result_soap']['CheckvalidationResult'];					
							$status = "failed";
							echo json_encode(array( "status" => $status,"veh_no"=> $postData['veh_no'],"message"=>'<div class="alert alert-danger">'.$resp_msg.'</div>'));exit;
						}
						else{
							$POST['coyId'] = 'OPEL';
							$POST['docId'] = 'WSB';
							$POST['deptId'] = 'ASC';
							$xmlc = new SimpleXMLElement($custData['result_soap']['GetCustResult']); 
							$xmlc->asXML();
							$CUST_NAME =   ($xmlc->GetCust->CUST_NAME?$xmlc->GetCust->CUST_NAME:"-");
							$TEL_NO    =   ($xmlc->GetCust->TEL_NO?$xmlc->GetCust->TEL_NO:"-"); 
							
							$cust_html = '<div class="form-group select_drop">
													<label class="col-md-5">Customer Name - </label>
													<div class="col-md-7 rt_cust">
														<div class="view_text">'.$CUST_NAME.'</div>
													</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Vehicle No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$postData['veh_no'].'</div>
												</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Customer HP No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$TEL_NO.'</div>
												</div>
										</div>';
							
							
							$first = date('Y-m-d',strtotime('-3 days'));
							$last = date('Y-m-d',strtotime('+30 days'));
							
							/*post parameter for remarks ad milage*/
							$POST_REM['run_no'] = $postData['ref_no'];
							
							$respRem = $this->soapRequest('GetRemarkinfo',$POST_REM);
							
							$POST_REM['doc_type'] = 'WSB';
							$POST_REM['dept_code'] = 'ASC';
							
							//print_r($POST_REM);
							$respBookingInfo = $this->soapRequest('GetBookinginfo',$POST_REM); 
							
							//print_r($respBookingInfo);die;
							
							$xml = new SimpleXMLElement($respRem['result_soap']['GetRemarkinfoResult']); 
							$xml->asXML();
							$xmlBooking = new SimpleXMLElement($respBookingInfo['result_soap']['GetBookinginfoResult']); 
							$xmlBooking->asXML();
							//print_r($xml);die;
							
							$remark = (trim((string)$xml->Remark->REMK)?trim((string)$xml->Remark->REMK):"");
							$bookingDateTime = (trim((string)$xmlBooking->BookInfo->BookingDateTim)?trim((string)$xmlBooking->BookInfo->BookingDateTim):"");
							$milage = (trim((string)$xmlBooking->BookInfo->MLEG_IN)?trim((string)$xmlBooking->BookInfo->MLEG_IN):"");
							//die;
							$status = "success";
							if($resp_msg){
								$resp_msg = '<div class="alert alert-success">'.$resp_msg.'</div>';
							}
							else{
								$resp_msg ='';
							}
							echo json_encode(array( "status" => $status,"ref_no" => $postData['ref_no'], "veh_no"=> $postData['veh_no'],"customer_html"=>$cust_html, "booking_action" => "U","booked_date" => $bookingDateTime, "milage" => $milage, "remark" => $remark,"message"=>''));exit;
						}
						
					
					break;
					case 'C':
						$POST['doctype'] = "WSB";
						$POST['deptcode']  = "ASC";
						$POST['cust_code']  = $custInfo[0]['cust_code'];
						$POST['VehicleNo']  = $postData['veh_no'];
						$POST['refno'] = $postData['ref_no'];
						$POST['cmd'] = $postData['book_action'];
						$resp = $this->soapRequest('Checkvalidation',$POST);
						
						//print_r($resp);die;
						$msg = "";
						
						if($resp['result_soap']['CheckvalidationResult'] != ""){
							$resp_msg = $resp['result_soap']['CheckvalidationResult'];					
							$status = "failed";
						}
						else{
												
							$POST['coyId'] = "OPEL";
							$POST['docType'] = "WSB";
							$POST['deptCode'] = "ASC";
							$POST['runNo'] = $postData['ref_no'];
							
							
							$arrBookingData = array();
							
							$arrBookingData['DOC_TYPE'] = $POST['docType'];
							$arrBookingData['DEPT_CODE'] = $POST['deptCode'];
							$arrBookingData['REF_NO'] = $POST['runNo'];
							$arrBookingData['VEH_NO'] = $POST['VehicleNo'];
							$arrBookingData['CUST_CODE'] = $POST['cust_code'];
							
							$xmlb = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><Data></Data>");
							$node = $xmlb->addChild('Booking'); 
							// function call to convert array to xml
							$this->array_to_xml($arrBookingData, $node);
							// display XML to screen
							$infoBooking  =  $xmlb->asXML();
							//print_r($infoBooking);die; 
							$PostDelBooking["info"] = $infoBooking;
							
							//print_r($PostBooking);
							//die;
							$respDelBooking = $this->soapRequest('DeleteAlpineBooking',$PostDelBooking);
							
							//print_r($respDelBooking);die;
							
							if($respDelBooking['result_soap']['DeleteAlpineBookingResult'] != ""){
								$resp_msg = $respDelBooking['result_soap']['DeleteAlpineBookingResult'];
							}
							
							$status = "success";
							echo json_encode(array( "status" => $status,"veh_no"=> $postData['veh_no'], "customer_html" => $cust_html ,"booking_action" => "C", "message" => '<div class="alert alert-success">'.$resp_msg.'</div>'));exit;
						}
						
						echo json_encode(array( "status" => $status,"veh_no"=> $postData['veh_no'], "customer_html" => $cust_html ,"booking_action" => "C", "message" => '<div class="alert alert-danger">'.$resp_msg.'</div>'));exit;
						
					//}
					
					break; 	
			}
		}
	}
	 
	
	
	function Tickets(){
		 $data['title'] = 'Ticket';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/ticket';
		 $userdata = $this->session->userdata("auth_opeluser"); 
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	function postTickets(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			
			//print_r($postData);die;
			
			$pdata = array("user"=>1,  "user_firstname" => $postData['user_firstname'] , "user_email" => $postData['user_email'], "department" => 1, "status" => 1, "priority" => $postData['priority'],"subject" => $postData['subject'],"text" => $postData['text_msg'],"send_user_email
"=>1);
			
			
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'POST');
			
			//print_r($result);die;
			echo $result;exit;
		}
	}
	
	
	function doAPICall($apiCall, $data = array(), $method = 'GET')
	{
		// Variables
		
		$baseUrl = 'https://opel360.sg/ticketsystem';
		$apiToken = '2SjYW+fnaBQmPP4QYMfyh=YFUOv23vPF';
	 
		// Start cURL
		$c = curl_init();
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER,0); 
		curl_setopt($c, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($c, CURLOPT_USERPWD, $apiToken . ":ChangeMe!74");
		
		// Start building the URL
		$apiCall = $baseUrl . $apiCall;
		
		
		// Check what type of API call we are making
		if ($method == 'GET') {
	 
			// Add the array of data to the URL
			if (is_array($data)) {
				$apiCall .= "?";
				foreach ($data AS $key => $value) {
					if (isset($value)) {
						$apiCall .= $key . "=" . $value . "&";
					}
				}
	 
				// Remove the final &
				$apiCall = rtrim($apiCall, '&');
			}
	 
		} else if ($method == 'POST' || $method == 'DELETE') {
	 
			// PUT and DELETE require an $id variable to be appended to the URL
			if (isset($data['id'])) {
				$apiCall .= "/" . $data['id'];
			}
			
			
			
			// Setup the remainder of the cURL request
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($c, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: ' . $method));
		} else {
	 
			// Setup the remainder of the cURL request
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
	 
		}
		// Set the URL
		curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($c, CURLOPT_URL, $apiCall);
	 
		// Execute the API call and return the response
		$result = curl_exec($c);
		//print_r($result); exit;
		curl_close($c);
		// Return the results of the API call
		
		return $result;
	 
	}
	
	
	//Change event status
	
	public function changeEventStatus()
	{
		$current_date = date('Y-m-d'); //2016-04-20
		//Get all events
		$cond = array();
		$like = array();
		$events = $this->events_model->getEventDetailsById($cond);
		
		foreach($events as $event) {
			$event_id = $event['id'];
			//Get event details
			$cond = array("event_id" => $event_id);
			$event_details = $this->events_model->getEventDetailsById($cond);
			//print_r($event_details); die;
			$event_start_date = $event_details[0]['start_date'];
			$event_end_date = $event_details[0]['end_date']; 
			//Check closed events
				if(strtotime($current_date) > strtotime($event_end_date)) {
					//change event status to closed
					$cond1 = array("event_id" => $event_details[0]['id']);
					$updateevent = $this->common_model->update(TB_EVENTS,$cond1,array("status" => "Closed"));
				}
				
			//Check for Runing events
				if(strtotime($current_date) > strtotime($event_start_date) && strtotime($current_date) < strtotime($event_end_date)) {
					//change event status to Runing
					$cond1 = array("event_id" => $event_details[0]['id']);
					$updateevent = $this->common_model->update(TB_EVENTS,$cond1,array("status" => "Running"));
				}
		}
	}
	
	
	function postToAlpineWithSoapCurl(){
		//$dataFromTheForm = $postdata; // request data from the form
        $soapUrl = "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx?op=SetAlpineBooking"; // asmx URL of WSDL
        
        // xml post structure
		
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                              <soap:Body>
                                <SetAlpineBooking xmlns="http://www.starvisionit.com/webservices/">
                                 <is_current_booking>Y</is_current_booking>
								  <action>I</action>
								 <info><Data><booking><COMP_CODE>OPEL</COMP_CODE><DOC_TYPE>WSB</DOC_TYPE><DEPT_CODE>ASC</DEPT_CODE><REF_NO>
</REF_NO><VEH_NO>SKG2091G</VEH_NO><CUST_CODE>1200003458</CUST_CODE><CUST_NAME>MOHAMMAD ADIB BIN MOHAMMAD
 HAMBER</CUST_NAME><TXT_MLEG></TXT_MLEG><MLEG_IN>1000</MLEG_IN><PC_IND>N</PC_IND><PYMT_MODE></PYMT_MODE
><VEH_RECV_DATE>2016-05-18T08:15:00</VEH_RECV_DATE><VEH_GRP>ZAFIRA B</VEH_GRP><MAKE_MDL>ZAFIRA 1.4 AUTO
 TURBO PANO</MAKE_MDL><COLR_CODE>GAZ</COLR_CODE><YR_MNFC>2012</YR_MNFC><CHAS_NO>W0LPD9DC4C2103974</CHAS_NO
><ENG_NO>A14NET19AK9206</ENG_NO><REG_DATE>8/8/2012 12:00:00 AM</REG_DATE><CUST_BLK>55</CUST_BLK><CUST_FLR>02</CUST_FLR><CUST_UNIT>51</CUST_UNIT><CUST_ADDR>SIMEI RISE</CUST_ADDR>
<CUST_CTY>SG</CUST_CTY><CUST_POSTAL_CODE>528791</CUST_POSTAL_CODE><CUST_DISTRICT><CUST_DISTRICT/><CUST_HOME><CUST_HOME/><CUST_HDPH>96246005</CUST_HDPH>
<CUST_OFCE><CUST_OFCE/><CUST_PGR><CUST_PGR/><CUST_EMAIL>0.00</CUST_EMAIL><REMK>test</REMK><STS>I</STS><RSRV_CHAR_FIELD_1>NEW/NEW/12VM00143E</RSRV_CHAR_FIELD_1>
<RSRV_CHAR_FIELD_2>Y</RSRV_CHAR_FIELD_2><RSRV_CHAR_FIELD_6>96246005</RSRV_CHAR_FIELD_6><RSRV_CHAR_FIELD_7>Not Exist</RSRV_CHAR_FIELD_7><RSRV_CHAR_FIELD_8>S</RSRV_CHAR_FIELD_8><CLOSED_FLAG>N</CLOSED_FLAG></booking></Data></info>
                                 </SetAlpineBooking>
                              </soap:Body>
                            </soap:Envelope>';   // data from the form, e.g. some ID number

           $headers = array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "Accept: application/soap+xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "SOAPAction: http://www.starvisionit.com/webservices/SetAlpineBooking", 
                        "Content-length: ".strlen($xml_post_string),
                    );

            $url = $soapUrl;

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            

            // converting
            $response = curl_exec($ch); 
            print_r($response);die;
            curl_close($ch);
			
            // converting
            $response1 = str_replace("<soap:Body>","",$response);
            $response2 = str_replace("</soap:Body>","",$response1);

			print_r($response1);
			print_r($response2);
			die;
            // convertingc to XML
            $parser = simplexml_load_string($response2);
            // user $parser to get your data out of XML response and to display it.
    
	}
	
	
	function soapRequest($service, $params = array()){
		 require_once('library/lib/nusoap.php'); //includes nusoap
		
		$client = new nusoap_client("http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx?WSDL",true);

		$error = $client->getError();
		if ($error) {
			echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
		}

		$result = $client->call($service, array($params),array("SOAPAction"=>"http://www.starvisionit.com/webservices/"));

		if ($client->fault) {
			echo "<h2>Fault</h2><pre>";
			print_r($result);
			echo "</pre>";
		}
		else {
			$error = $client->getError();
			if ($error) {
				echo "<h2>Error</h2><pre>" . $error . "</pre>";
			}
			else {
					return array("result_soap"=>$result);
			}
		}
         
	}
    
    function testDrive(){
		//Get All Modelss
		 $cond = array();
		 $all_models = $this->catalog_model->getCatalogModelById($cond);
		 
		 for($i=0; $i<count($all_models); $i++) {
				$model_name = $all_models[$i]['m_name'];
				$model_id = $all_models[$i]['id'];
				
			 $opt .= '<option value='.$model_id.'>'.$model_name.'</option>';
		 }
		 
		 $data['title'] = 'Test Drive';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['models'] = $opt;
		 $data['main_content'] = 'frontend/test_drive';
		 $userdata = $this->session->userdata("auth_opeluser"); 
		 $this->load->view('frontend/incls/layout', $data);  
	}
   
	function request_test_drive() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			//print_r($postData); die;
			
			$customfield = array("1" => $postData['model'],"2" => $postData['title'],"3" => $postData['name'],"4" => $postData['mobno'],"5" => $postData['address'],"6" => $postData['email'],"7" => $postData['existing_model'],"8" => $postData['year_purchased'],"9" => $postData['general_com']);
			$pdata = array("user"=>1,  "user_firstname" => $postData['name'] , "user_email" => $postData['email'], "department" => 1, "status" => 1, "priority" => 3,"subject" => 'Request a Test Drive',"customfield" => $customfield, "text" => "Hello, <br/>Test drive is requested from a ".$postData['name'].". <br/>So please confirmed it. <br/>Thanks</br>","send_user_email"=>1);
			//print_r($pdata);die;
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'POST');
			
			echo $result;exit;
		}
	}
	
}

?>
