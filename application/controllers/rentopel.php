<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class RentOpel extends CI_Controller{
       
    function __construct() {
		parent::__construct(); 
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("users_model");
		$this->load->model("catalog_model");
		$this->load->model("accessories_model");
		$this->load->model("services_model");
		$this->load->model("pdf_model");
		$this->load->model("events_model");		
		$this->load->model("alpine_model");
		$this->load->model("survey_model");
		$this->load->model("banner_model");
		$this->carimagepath = realpath('car_gallery');
		$this->load->library('form_validation');
		$this->load->library("email");
		$this->load->helper("email_template"); 
		//$this->load->helper("xml_to_object"); 
		$this->load->library('XmlElement');
		$this->load->helper('download'); 
		//$this->no_cache();
	}
	

	function testvehicle(){
 		$postData['key'] = 'eExCN+NqbQTK8NrusdGabFnTDCWxxMUNAz9sKfZTWAUNucoV';
 		//$postData['Optional']['ID'] = 'Opel';
		//$postData['VehAvailableGet']['Model'] = 'Astra Gtc 1.4 A';
		$POST['key'] = 'eExCN+NqbQTK8NrusdGabFnTDCWxxMUNAz9sKfZTWAUNucoV';
		$POST['VehAvailableGet']['Make'] = 'Opel';
        $POST['VehAvailableGet']['IDCar'] = '59';
        
        
        $respAvailbleVehicleInfo = $this->soapRequest('VehAvailableGet',$POST);
        $vehicles = $respAvailbleVehicleInfo['result_soap']['VehAvailableGetResult']['VehUnAvaiable']['VehAvailable'];
        
        //echo "<pre>";print_r($vehicles);die;
        
        if(isset($vehicles[0])) {
			foreach($vehicles as $key => $val) :
				$postData['Vehicle_Opel360']['IDCar'] = $vehicles[$key]['IDCar'];
				$postData['Vehicle_Opel360']['Make'] = 'Opel'; 
				$respVehicleInfo = $this->soapRequest('Vehicle_Opel360Get',$postData);
				$respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['StartPrice'];
				 $array_cars[] = array(
								"CompCode" => $vehicles[$key]['CompCode'],
								"IDCar" => $vehicles[$key]['IDCar'],
								"Model" => $vehicles[$key]['Model'],
								"VehGroup"  => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['VehGroup'],
								"Transmission" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Transmission'],
								"EngineType" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['EngineType'],
								"StartPrice" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['StartPrice'],
								"OutDate" => $vehicles[$key]['OutDate'],
								"ReturnDate" => $vehicles[$key]['ReturnDate'],
								"VehNo" => $vehicles[$key]['VehNo'],
								"Units" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Units'],
								"Info" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Info'],
								"Image" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Image'] 
							 );
			endforeach;
		}
		
		
		echo "<pre>";print_r($array_cars);die;
	}
	
	
	
	function soapRequest($service, $params = array()){ 
		require_once('library/lib/nusoap.php'); //includes nusoap
		
		$client = new nusoap_client("http://webmail.alpinemotors.sg/CR_WS_TEST/alpinecr.asmx?WSDL",true);
		
		$error = $client->getError();
		
		if ($error) {
			echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
		}
		 
		$result = $client->call($service, array($params),array("SOAPAction"=>"http://www.starvisionit.com/webservices/"));
		if ($client->fault) {
			echo "<h2>Fault</h2><pre>";
			echo "</pre>";
		}
		else {
			$error = $client->getError();
			if ($error) {
				echo "<h2>Error</h2><pre>" . $error . "</pre>";
			}
			else {
					return array("result_soap"=>$result);
			}
		}
         
	}
    
    
}

?>
