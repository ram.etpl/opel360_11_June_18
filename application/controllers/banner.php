<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model("banner_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->pdf_file_path =  realpath('images/PDF');
		$this->banner_file_path =  realpath('images/Banners');
		//$this->load->library("richtexteditor"); 
		$_REQUEST = json_decode(file_get_contents('php://input'), true);
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'banner_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;http://192.168.100.14/opel/banner/getBanners
		}
	}
	
	
	public function upload_banners() {
		
		if ( !empty( $_FILES ) ) {

				$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
				
				$img_info = getimagesize($_FILES['file']['tmp_name']);
			
				$image_width = $img_info[0];
				$image_height = $img_info[1];
				$flag_err = 0;
				/*if($image_width <=800  || $image_height <=250) { 
					$flag_err = 1;
					$error = "The image width and height should be greater than 100 pixel";
					return array("status" => 0,"error" => $error); exit;
				}
				
				/if($flag_err != 1){*/
				
					$uploadPath = $this->banner_file_path ."/". $_FILES[ 'file' ][ 'name' ];
					move_uploaded_file( $tempPath, $uploadPath );
					
					$answer = array( 'answer' => 'File transfer completed' );
					$json = json_encode( $answer );
					
					echo $json; die();

			} else {

				echo 'No files'; die();

			}
	}


	public function add_banners() {
		
		$data = $_REQUEST;
		
		//print_r($data);die;
		
		$insertId = $this->common_model->insert(TB_BANNERS,array("banner_file_name" => $data["file_name"],"banner_file_size" => $data["file_size"],"banner_file_type" => $data["file_type"],"banner_file_last_modifieddate" => $data["file_last_modifieddate"]));
		if($insertId!="") {
			echo "success"; die();
		}
		else {
			echo "fail"; die();
		}
	}
	
	
	
	public function getBanners()
	{
		
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				$this->load->view("administrator/banner",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getSurveyById() // Get Single PDF
	{
		if(is_ajax_request())
		{ 
			
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("survey_id" => $this->encrypt->decode($postData["id"]));
				$pdf = $this->survey_model->getSurveyById($cond);
				
				//Get all survey question
				$cond = array("fk_survey_id" => $this->encrypt->decode($postData["id"]));
				$questions = $this->survey_model->getallSurveyQuestion($cond);
				$html = "";
				foreach($questions as $question) {
					$survey_question = $question['question'];
					$html .='<div class="section row mbn">
			  <div class="col-sm-6">
				<div class="form-group plus_resp">
				  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Question to ask<span class="validationerror">*</span></label>
				  <div class="col-lg-8 text_inpu_new">
					<textarea type="text" id="survey_question" name="survey_question[]" class="form-control input-sm" placeholder="Question to ask">'.$survey_question.'</textarea>
					<span id="QuestionInfo"  class="text-danger marg"></span>
				  </div>
				</div>
			  </div>
			  
			   <div class="col-sm-6">
				<div class="form-group">				  
					<span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle"><a href="javascript:void(0);" id="add_more" ><i class="fa fa-plus-circle" aria-hidden="true"></i>
					</a></span>
				</div>
			  </div>
			  
			  
		</div>';
				}
				
				echo json_encode(array("survey" =>$pdf[0],"questions" => $html));exit;
			
		}
	}
	
	public function save_survey() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();
					//print_r($postData); die;
					if($postData["survey_id"] == "")
					{
								//date_default_timezone_set('Asia/Singapore');
								$created_date = date('Y-m-d H:i:s');
								$insertId = $this->common_model->insert(TB_SURVEY,array("survey_from_date" => $postData["start_date"],"survey_to_date" => $postData["date_to"],"survey_type" => $postData["survey"],"survey_name" => $postData["survey_name"],"award_points" => $postData["award_points"],"date_modified"=>$created_date));
								
								if($insertId)
								{    
									
									//Add Question
									$count = count($postData['survey_question']); 
									foreach($postData['survey_question'] as $question) {
										$insertsurvey = $this->common_model->insert(TB_SURVEY_QUESTIONS,array("question" => $question,"fk_survey_id" => $insertId));
									}
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Survey has been added successfully.</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							
					}
					else
					{ 
						
						
						$created_date = date('Y-m-d H:i:s');
						$cond1 = array("survey_id" => $this->encrypt->decode($postData["survey_id"]));
						
							$updateArr = $this->common_model->update(TB_SURVEY,$cond1,array("survey_from_date" => $postData["start_date"],"survey_to_date" => $postData["date_to"],"survey_type" => $postData["survey"],"survey_name" => $postData["survey_name"],"award_points" => $postData["award_points"],"date_modified"=>$created_date));
							
							
							$row = '
								<td class="text-center"  width="5%"><input type="checkbox" value="'.$postData["survey_id"].'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="25%">'.$postData["survey_name"].'</td>
								<td class="text-center"  width="25%">'.$postData["survey"].'</td>
								<td class="text-center"  width="25%">'.$postData["award_points"].'</td>
								<td class="text-center"  width="25%">'.$postData["start_date"].'</td>
								<td class="text-center"  width="25%">'.$postData["date_to"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["survey_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>';
							//echo $this->encrypt->decode($postData["userId"]);die;
							echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["survey_id"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Survey has been updated successfully.</div>')); exit;
							
						
						
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_banners() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					//print_r($postData); die;
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->banner_model->getBannersCount($cond,$like);
					
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("banner_file_name","banner_file_type","banner_file_last_modifieddate");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "banner_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$banners = $this->banner_model->getBannersPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'banner_file_name\',\''.$corder[0].'\')">Banner Image</th>
									<th class="text-center '.$css["1"].'" width="25%" onclick="changePaginate(0,\'banner_file_type\',\''.$corder[1].'\')">Banner Image Type</th>
									<th class="text-center '.$css["2"].'" width="15%" onclick="changePaginate(0,\'banner_file_last_modifieddate\',\''.$corder[2].'\')">Last Modified Date</th>
								  </tr>
								</thead>
								<tbody>';
					}
					
					
					
					if(count($banners)>0)
					{
							if(count($banners) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($banners as $row) {  
								 
								 $file_last_modifieddate = substr($row["banner_file_last_modifieddate"],0,10);
								 
						$table .= '<tr id="row_'.$row["id"].'">
								<td class="text-center"  width="5%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="25%"><img style="width:100%;height:auto;" src="'.base_url().'images/Banners/'.$row["banner_file_name"].'"></td>
								<td class="text-center"  width="25%">'.$row["banner_file_type"].'</td>
								<td class="text-center"  width="25%">'.$file_last_modifieddate.'</td>
							  </tr> 
							  </tr>
							  ';
							}
		
					}
					if($postData["start"] == 0)
					{
							if(count($banners)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="5">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					//print_r($table); die;
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($banners),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	
	
	
	public function validateCatalogModel($postData)
	{   
		
		return array("status" => 1);
	}
	
	
	public function delete_banner() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("banner_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_BANNERS,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
}
?>
