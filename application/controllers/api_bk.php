<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("cars_model");
    }
    public function loadGlobalVariables()
    {
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
        $_GET = json_decode(file_get_contents('php://input'), true);
        $_POST = json_decode(file_get_contents('php://input'), true);
    }
    
    function registerUser_post()
    {
        $segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();
        }
        $postData = $_POST;
        if(trim($postData["password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter password."), 200);
        }else if(!min_length($postData["password"],4)){
            $this->response(array("status"=>"error", "msg"=> "Please enter more than 4 character password."), 200);
        }else if(trim($postData["first_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter first name."), 200);
        }else if(!alpha($postData["first_name"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter only alphabets in first name."), 200);
        }else if(trim($postData["last_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter last name."), 200);
        }else if(!alpha($postData["last_name"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter only alphabets in last name."), 200);
        }else if(trim($postData["email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }else if(valid_email($postData["email"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid email."), 200);
        }else if(trim($postData["mobile_phone"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter mobile number."), 200);
        }else if(!phone_number($postData["mobile_phone"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid  mobile number."), 200);
        }/*else if(trim($postData["address1"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter address."), 200);
        }*/else if(trim($postData["user_type"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please choose user type."), 200);
        }

        $emailArr = $this->common_model->select("email",TB_USERS,array("email"=>trim($postData["email"])));
        if(count($emailArr) > 0)
        {
            $this->response(array("status"=>"error", "msg"=> "User already exist with this email."), 200);
        }else{
            $date = date("Y-m-d H:i:s");
            $user_token = md5(uniqid(rand(), true));
            $insertArr = array(
                                "password"      =>md5(trim($postData["password"])),
                                "user_token"    =>$user_token,
                                "first_name"    =>trim($postData["first_name"]),
                                "last_name"     =>trim($postData["last_name"]),
                                "email"         =>trim($postData["email"]),
                                "mobile_phone"  =>trim($postData["mobile_phone"]),
                                "address1"      =>trim($postData["address1"]),
                                //"license_pic"   =>$imageData["upload_data"]["file_name"],
                                "user_type"     =>trim($postData["user_type"]),
                                "created_date"  => $date,
                                "modified_date"  => $date,
                                "device_token" => trim($postData["device_token"]),
                                "device_type" => trim($postData["device_type"]),
                                "imie" => trim($postData["device_imei"]),
                                "ip_address" => $_SERVER['REMOTE_ADDR'],
                            );
            if($insertArr["user_type"] == 'driver'){
                $insertArr["user_status"] = 'unpublished';    
            }
            
            $user = $this->common_model->insert(TB_USERS,$insertArr);
        }
        if($user)
        {
            $this->response(array("status"=>"success","userid"=>$user, "user_token"=>$user_token, "msg"=> "Thank you for registration."), 200);    
        }else{
            $this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
        
    }

    public function login_post()
    {   
        $segment = $this->uri->segment(2);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST; 
        if(trim($postData["email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }else if(trim($postData["password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter password."), 200);
        }
    	$userArr = $this->common_model->select("user_token,first_name,last_name,email,user_type,userid",TB_USERS,array("email"=>trim($postData["email"]),"password"=>md5(trim($postData["password"]))));
    	if(count($userArr)>0){
            if($segment == IS_WEB){
                set_user_logged_in($userArr[0]);
            }
    		$this->response(array("status"=>"success", "user"=> $userArr[0]), 200); exit;
    	}else{
    		$this->response(array("status"=>"error", "msg"=> "Email or password is in-correct."), 200);
    	}
    }

    public function carCategoryListing_post()
    {      
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
        if($postData["start"]==""){
            $postData["start"] = 0;
        }
        if($postData["start_date"] == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter start date."), 200);
        }
        else if($postData["end_date"] == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter end date."), 200);
        }else{

            $start_date = strtotime($postData["start_date"]); // or your date as well
            $end_date = strtotime($postData["end_date"]);
            $datediff = $end_date - $start_date;
            $dateNum = floor($datediff/(60*60*24))+1;
            
            $base_url = base_url();
            $date = date("Y-m-d H:i:s");

            $cars = $this->cars_model->joinQuery(TB_CAR_CATEGORY.".catid,features,".TB_CAR_CATEGORY.".name AS category,daily_price,hourly_price, CONCAT('".$base_url."','upload/categoryimages/',cat_image) AS model_image,CONCAT('".CURRENCY."',' ',IF(daily_price > 0,".$dateNum."*daily_price,0)) as price,IF(daily_price > 0,".$dateNum."*daily_price,0) as rental_price,CONCAT('for ','".$dateNum."',' days') AS label",TB_CAR_CATEGORY,array("season_from < "=>$date,"season_to > "=>$date,TB_CAR_CATEGORY_PRICE.".del_status"=>"no",TB_CAR_CATEGORY.".del_status"=>"no"),array(),array(),array(TB_CAR_CATEGORY_PRICE=>TB_CAR_CATEGORY.".catid = ".TB_CAR_CATEGORY_PRICE.".catid"));

            for($i=0;$i<count($cars);$i++){
				$brandArr = $this->common_model->select("GROUP_CONCAT(name) as brand",TB_CAR_BRAND,array("del_status"=>"no"),array('brandid' => UniqueRandomNumbersWithinRange(1,16,3)));
				$cars[$i]["brand_names"] = $brandArr[0]["brand"];
            }
            
            if(count($cars)>0){
                $start = $postData["start"] + count($cars);

                $this->response(array("status"=>"success", "start"=>$start, "cars"=> $cars), 200);
            }else{
                $this->response(array("status"=>"success", "start"=>$start, "msg"=> "No record found."), 200);
            }
        }
    }


    public function carListing_post()
    {      
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
        if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }
        $user = $this->common_model->select("userid",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        if(count($user)>0){
        	
            $cars = $this->cars_model->joinQuery("carid,car_address,year,mileage,Registration_no,CONCAT(".p.".name,' ',".TB_CAR_BRAND.".name) As name,".TB_CAR_CATEGORY.".name as cat_name",TB_CAR_LIST,array("userid"=>$user[0]["userid"]),array(),array(),array(TB_CAR_BRAND=>TB_CAR_BRAND.".brandid = ".TB_CAR_LIST.".brandid",TB_CAR_BRAND." AS p"=>p.".brandid = ".TB_CAR_BRAND.".parentid",TB_CAR_CATEGORY=>TB_CAR_CATEGORY.".catid = ".TB_CAR_BRAND.".catid"));
			$start = 0;
            $base_url = base_url();
            for($i=0;$i<count($cars);$i++){
                $carImages = $this->common_model->selectQuery("CONCAT('".$base_url."','upload/carimages/',image_name) AS image_name",TB_CAR_IMAGES,array("carid"=>$cars[$i]["carid"]),array(),array('0'=>'1'));
                if(count($carImages)>0){
                    $cars[$i]["car_image"] = $carImages[0]['image_name'];
                }else{
                    $cars[$i]["car_image"] = $base_url.'upload/carimages/default.png';
                }
            }
	        if(count($cars)>0){
	            $this->response(array("status"=>"success","cars"=> $cars), 200);
	        }else{
	            $this->response(array("status"=>"success", "msg"=> "No record found."), 200);
	        }
        }else{
        	$this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
        
    }

    function parentBrands_post()
    {
        $brands = $this->common_model->select("brandid,name",TB_CAR_BRAND,array("parentid"=>null,"del_status"=>"no"));
        if(count($brands)>0){
            $this->response(array("status"=>"success","parent_brands"=> $brands), 200);
        }else{
            $this->response(array("status"=>"success","msg"=> "No record found."), 200);
        }
    }

    function getBrands_post()
    {
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
        if($postData["parent_brand"]==""){
            $this->response(array("status"=>"error","msg"=> "Please select parent brand."), 200);
        }else{
            $brands = $this->common_model->select("brandid,name",TB_CAR_BRAND,array("parentid"=>$postData["parent_brand"],"del_status"=>"no"));
            if(count($brands)>0){
                $this->response(array("status"=>"success","brands"=> $brands), 200);
            }else{
                $this->response(array("status"=>"success","msg"=> "No record found."), 200);
            }
        }
    }


    function getCategory_post()
    {
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
        $category = $this->common_model->select("catid,name",TB_CAR_CATEGORY,array("del_status"=>"no"));
        if(count($category)>0){
            $this->response(array("status"=>"success","categories"=> $category), 200);
        }else{
            $this->response(array("status"=>"success","msg"=> "No record found."), 200);
        }
    }


    function addCar_post()
    {
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
        $user = $this->common_model->select("userid",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        if(count($user)>0){
            if(trim($postData["parent_brand"])==""){
                $this->response(array("status"=>"error","msg"=> "Please select parent brand."), 200);
            }else if(trim($postData["brand"])==""){
                $this->response(array("status"=>"error","msg"=> "Please select brand."), 200);
            }else if(trim($postData["transmission"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter transmission."), 200);
            }else if(trim($postData["year"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter year."), 200);
            }else if($postData["year"]>date('Y')){
                $this->response(array("status"=>"error","msg"=> "Please enter valid year."), 200);
            }else if(trim($postData["fuel_type"])==""){
                $this->response(array("status"=>"error","msg"=> "Please choose fuel type."), 200);
            }else if(trim($postData["mileage"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter mileage."), 200);
            }else if(!numeric($postData["mileage"])){
                $this->response(array("status"=>"error","msg"=> "Please enter valid mileage."), 200);
            }else if(trim($postData["registration_no"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter registration no."), 200);
            }else if(trim($postData["car_address"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter address."), 200);
            }else if(trim($postData["car_latitude"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter latitude."), 200);
            }else if(trim($postData["car_longitude"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter longitude."), 200);
            }/*else if(trim($postData["not_available_from"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter not available from date."), 200);
            }else if(trim($postData["not_available_to"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter not available to date."), 200);
            }*/
            $number_of_files = 0;
            if($_FILES['uploadedimages']['tmp_name'][0]!=""){
                $number_of_files = sizeof($_FILES['uploadedimages']['tmp_name']);
            }
		    $files = $_FILES['uploadedimages'];
		    $errors = array();
		    for($i=0;$i<$number_of_files;$i++)
		    {
		     	if($_FILES['uploadedimages']['error'][$i] != 0) $errors[$i][] = 'Couldn\'t upload file '.$_FILES['uploadedimages']['name'][$i];
		    }
		    if(sizeof($errors)==0)
		    {
		    	$this->load->library('upload');
		    	$config['upload_path'] = './upload/carimages/';
		    	$config['allowed_types'] = 'gif|jpg|jpeg|png';
		    	$config['encrypt_name'] = TRUE;
		    	for ($i = 0; $i < $number_of_files; $i++) {
		      	$_FILES['uploadedimage']['name'] = $files['name'][$i];
		        $_FILES['uploadedimage']['type'] = $files['type'][$i];
		        $_FILES['uploadedimage']['tmp_name'] = $files['tmp_name'][$i];
		        $_FILES['uploadedimage']['error'] = $files['error'][$i];
		        $_FILES['uploadedimage']['size'] = $files['size'][$i];
		        $this->upload->initialize($config);
		        if ($this->upload->do_upload('uploadedimage'))
		        {
		        	$data['uploads'][$i] = $this->upload->data();
		        }
		        else
		        {
		          	$data['upload_errors'][$i] = $this->upload->display_errors();
		        }
		      }
		    }
		    else
		    {
		      $this->response(array("status"=>"error","msg"=> "Please try again.","error"=>$errors), 200);
		    }
		    if(count($data["upload_errors"]) > 0){
		    	foreach ($data["uploads"] AS $uploadedFile) {
		    		unlink('./upload/carimages/'.$uploadedFile["file_name"]);
		    	}
		    	$this->response(array("status"=>"error","msg"=> "Please try again.","error"=>$data["upload_errors"]), 200);
		    }
		    else
		    {
	            $date = date("Y-m-d H:i:s");
	            $insertArr = array(
	                                "brand_parentid" =>trim($postData["parent_brand"]),
	                                "brandid"       =>trim($postData["brand"]),
	                                "userid"        =>$user[0]["userid"],
	                                "transmission"     =>trim($postData["transmission"]),
	                                "year"         =>trim($postData["year"]),
	                                "fuel_type"  =>trim($postData["fuel_type"]),
	                                "mileage"      =>trim($postData["mileage"]),
	                                "Registration_no"   =>trim($postData["registration_no"]),
	                                "car_address"     =>trim($postData["car_address"]),
	                                "car_longitude"  => trim($postData["car_longitude"]),
	                                "car_latitude"  => trim($postData["car_latitude"]),
	                                //"not_available_from"  => date("Y-m-d H:i:s",strtotime($postData["not_available_from"])),
	                                //"not_available_to"  => date("Y-m-d H:i:s",strtotime($postData["not_available_to"])),
	                                "status"  => "unpublished",
	                                "created_date"  => $date,
	                                "modified_date"  => $date,
	                            );
                if(trim($postData["carid"])!=""){
                    $this->common_model->update(TB_CAR_LIST,array("carid"=>trim($postData["carid"])),$insertArr);
                    $insert = trim($postData["carid"]);
                }else{
                    $insert = $this->common_model->insert(TB_CAR_LIST,$insertArr);    
                }
	            
	            if($insert){
	            	$imagArr = array();
	            	foreach ($data["uploads"] AS $uploadedFile) {
			    		$imagArr[] = array("carid"=>$insert,"image_name"=>$uploadedFile["file_name"],"created_date"=>$date,"modified_date"=>$date);
			    	}
			    	if(count($imagArr)>0){
                        /*if(trim($postData["carid"])!=""){
                            $imgArr = $this->common_model->select("image_name",TB_CAR_IMAGES,array("carid"=>trim($postData["carid"])));
                            $isDelete = $this->common_model->delete(TB_CAR_IMAGES,array("carid"=>trim($postData["carid"])));
                            if($isDelete){
                                foreach ($imgArr AS $img) {
                                   unlink('./upload/carimages/'.$img["image_name"]);
                                }
                            }
                        }*/
                        $this->common_model->insert_batch(TB_CAR_IMAGES,$imagArr);
			    	}
                    if(trim($postData["carid"])==""){
                        $this->response(array("status"=>"success","action"=>"add","msg"=> "Car has been added successfully."), 200);
                    }else{
                        $this->response(array("status"=>"success","action"=>"update","msg"=> "Car has been updated successfully."), 200);
                    }
	            }else{
	                $this->response(array("status"=>"error","msg"=> "Please try again."), 200);
	            }
	        }
        }else{
            $this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
    }

    function getCar_post(){
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
        $user = $this->common_model->select("userid",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        if(count($user)>0){
            $base_url = base_url();
            $cars = $this->common_model->select("carid,brandid,brand_parentid,transmission,year,fuel_type,mileage,Registration_no,car_address,car_latitude,car_longitude",TB_CAR_LIST,array("userid"=>$user[0]["userid"],"carid"=>$postData["carid"]));
            $carImages = $this->common_model->select("CONCAT('".$base_url."','upload/carimages/',image_name) AS image_name,cimageid",TB_CAR_IMAGES,array("carid"=>$postData["carid"]));
            $cars[0]["images"] = $carImages;
            $this->response(array("status"=>"success","car"=> $cars[0]), 200);
        }else{
            $this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
    }

    function deleteCar_post(){
       if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }else if(trim($postData["carid"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send car id."), 200);
        }
        $user = getValidUser(trim($postData["user_token"]));
        if(count($user)>0){
            $isDelete = $this->common_model->delete(TB_CAR_LIST,array("userid"=>$user[0]["userid"],"carid"=>trim($postData["carid"])));
            if($isDelete){
                $imgArr = $this->common_model->select("image_name",TB_CAR_IMAGES,array("carid"=>trim($postData["carid"])));
                $this->common_model->delete(TB_CAR_IMAGES,array("carid"=>trim($postData["carid"])));
                foreach ($imgArr AS $img) {
                   unlink('./upload/carimages/'.$img["image_name"]);
                }
                $this->response(array("status"=>"success","msg"=> "Car has been deleted successfully."), 200);
            }else{
                $this->response(array("status"=>"success","msg"=> "Please try again."), 200);
            }
        }else{
            $this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        } 
    }

    function deleteCarImage_post(){
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }else if(trim($postData["image_id"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send image id."), 200);
        }
        $user = getValidUser(trim($postData["user_token"]));
        if(count($user)>0){
            $imgArr = $this->common_model->select("image_name,carid",TB_CAR_IMAGES,array("cimageid"=>trim($postData["image_id"])));
            if(count($imgArr)>0){
                $userArr = $this->common_model->select("userid",TB_CAR_LIST,array("carid"=>$imgArr[0]["carid"]));
                if($userArr[0]["userid"] == $user[0]["userid"]){
                    $isDelete = $this->common_model->delete(TB_CAR_IMAGES,array("cimageid"=>trim($postData["image_id"])));     
                }else{
                    $this->response(array("status"=>"error","msg"=> "This car picture is not rlated to you."), 200);
                }
                if($isDelete){
                    unlink('./upload/carimages/'.$imgArr[0]["image_name"]);
                    $this->response(array("status"=>"success","msg"=> "Car picture has been deleted successfully."), 200);
                }else{
                    $this->response(array("status"=>"success","msg"=> "Please try again."), 200);
                }
            }else{
                $this->response(array("status"=>"success","msg"=> "Car picture not exist."), 200);
            }
        }else{
            $this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
    }

    function getExtraFacilities_post(){
    	$base_url = base_url();
        $facilities = $this->common_model->select("facilityid,title,CONCAT('".$base_url."','upload/modelimages/',image_name) AS image_name,price",TB_EXTRA_FACILITIES,array("status"=>"active"));
        if(count($facilities)>0){
            $this->response(array("status"=>"success","facilities"=> $facilities), 200);
        }else{
            $this->response(array("status"=>"error","msg"=> "Record not found."), 200);
        }
    }

    function carBooking_post(){
    	if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }
        $user = getValidUser(trim($postData["user_token"]));
         
        if(count($user)>0){
        	if(trim($postData["address"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter pickup address."), 200);
            }else if(trim($postData["return_location"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter return address."), 200);
            }else if(trim($postData["pickup_datetime"])==""){
                $this->response(array("status"=>"error","msg"=> "Please select pickup date."), 200);
            }else if(trim($postData["return_datetime"])==""){
                $this->response(array("status"=>"error","msg"=> "Please select return date."), 200);
            }/*else if(trim($postData["catid"])==""){
                $this->response(array("status"=>"error","msg"=> "Please select car category."), 200);
            }*/else if(trim($user[0]["userid"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter user information."), 200);
            }else if(trim($postData["latitude"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter pickup latitude."), 200);
            }else if(trim($postData["longitude"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter pickup longitude."), 200);
            }else if(trim($postData["return_lat"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter return latitude."), 200);
            }else if(trim($postData["return_long"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter return longitude."), 200);
            }else if(trim($postData["rental_amount"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter rental amount."), 200);
            }else if(trim($postData["payment_mode"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter payment mode."), 200);
            }/*else if(trim($postData["flight_number"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter flight number."), 200);
            }*/else if(trim($postData["ip_address"])==""){
                $this->response(array("status"=>"error","msg"=> "Please enter ip address."), 200);
            }else{
            	$date = date("Y-m-d H:i:s");
	            $insertArr = array(
	                                "catid" =>trim($postData["catid"]),//
	                                "userid"       =>$user[0]["userid"],//
	                                "required_location"        =>trim($postData["address"]),//
	                                "latitude"     =>trim($postData["latitude"]),//
	                                "longitude"         =>trim($postData["longitude"]),//
	                                "return_location"  =>trim($postData["return_location"]),//
	                                "return_lat"      =>trim($postData["return_lat"]),//
	                                "return_long"   =>trim($postData["return_long"]),//
	                                "pickup_datetime"     =>date("Y-m-d H:i:s",strtotime($postData["pickup_datetime"])),//
	                                "return_datetime"  => date("Y-m-d H:i:s",strtotime($postData["return_datetime"])),//
	                                "rental_amount"  => trim($postData["rental_amount"]),
	                                "payment_mode"  => trim($postData["payment_mode"]),
                                    "flight_number"  => trim($postData["flight_number"]),
                                    "ip_address"  => trim($postData["ip_address"]),
	                                "created_date"  => $date,
	                                "modified_date"  => $date,
	                            );
	             //$this->response(array("status"=>"error","userr&tbl"=>$insertArr), 200);
                 $insert = $this->common_model->insert(TB_CAR_BOOKING,$insertArr);
                 //$this->response(array("status"=>"error","insert"=> $insert), 200);
                if($insert){
                    $facilities = json_decode($postData["facilities"]);
                    $facilities = $facilities->facilities;                    
                    for($i=0;$i<count($facilities);$i++){
                        $insertFacilities[$i]["bookingid"] = $insert;
                        $insertFacilities[$i]["facilityid"] = $facilities[$i]->facility;
                        $insertFacilities[$i]["amount"] = $facilities[$i]->amount;
                    }
                    if(count($insertFacilities)>0){
                        $this->common_model->insert_batch(TB_CAR_BOOKING_EXTRA,$insertFacilities);
                    }
                    $this->response(array("status"=>"success","bookingid"=>$insert,"msg"=> "Your booking has been done."), 200);
                }else{
                    $this->response(array("status"=>"error","msg"=> "Please try again."), 200);
                }
            }
        }else{
        	$this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
    }

    function sendNotificationToOwner_post(){
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
        $bookingArr = $this->common_model->select("request_sent_to_carowners,bookingid,latitude,longitude,catid,pickup_datetime,return_datetime",TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"],"payment_status"=>"success"));
		if(count($bookingArr)>0){
           if($bookingArr[0]["request_sent_to_carowners"] == "no"){
			$this->common_model->update(TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"]),array("request_sent_to_carowners"=>"yes"));
				$categoryArr = $this->common_model->select("brandid",TB_CAR_BRAND,array("catid"=>$bookingArr[0]["catid"]));
				$ids = implode(",",array_map(function($cat) { return $cat["brandid"]; },$categoryArr)); 
                /*$arrCars = $this->cars_model->joinQuery("device_token,device_type,Registration_no,carid,".TB_USERS.".userid AS userid,(3959 * acos(cos(radians(".$bookingArr[0]["latitude"]."))*cos(radians(car_latitude))*cos(radians(car_longitude) - radians(".$bookingArr[0]["longitude"].")) + sin 
(radians(".$bookingArr[0]["latitude"]."))*sin(radians(car_latitude))))*1.609344 AS distance",TB_CAR_LIST,array(TB_USERS.".user_status"=>"published", "user_type"=>"car_owner","latitude != "=> '',"longitude != "=> ''),array(),array(),array(TB_USERS=>TB_USERS.".userid = ".TB_CAR_LIST.".userid"),"",'distance < 15.534275',array("brandid"=>$ids));*/
				$arrCars = $this->cars_model->joinQuery("device_token,device_type,Registration_no,carid,".TB_USERS.".userid AS userid,(3959 * acos(cos(radians(".$bookingArr[0]["latitude"]."))*cos(radians(car_latitude))*cos(radians(car_longitude) - radians(".$bookingArr[0]["longitude"].")) + sin 
(radians(".$bookingArr[0]["latitude"]."))*sin(radians(car_latitude))))*1.609344 AS distance",TB_CAR_LIST,array(TB_USERS.".user_status"=>"published", "user_type"=>"car_owner"),array(),array(),array(TB_USERS=>TB_USERS.".userid = ".TB_CAR_LIST.".userid"),"",'distance < 15.534275',array("brandid"=>$ids)); //,"car_latitude != "=> '',"car_longitude != "=> ''
	            foreach($arrCars AS $arr){

					$blocked_dates=$this->common_model->select("from_date,to_date",TB_CAR_CALENDAR,array("carid"=>trim($arr["carid"]),'type'=>'blocked'),array(),NULL,"(from_date >= '".$bookingArr[0]["pickup_datetime"]."' AND from_date <= '".$bookingArr[0]["return_datetime"]."') OR (to_date >= '".$bookingArr[0]["pickup_datetime"]."' AND to_date <= '".$bookingArr[0]["return_datetime"]."')");
					//echo $this->db->last_query();
					//echo "<pre>";print_r($blocked_dates);exit;
					if(!count($blocked_dates))
					{
						$arrUsers[$arr["userid"]]["insertArr"][] = array("carid"=>$arr["carid"],"userid"=>$arr["userid"],"bookingid"=>$postData["bookingid"]);
						$arrUsers[$arr["userid"]]["registration_no"][] = $arr["Registration_no"];
						$arrUsers[$arr["userid"]]["device_token"] = $arr["device_token"];
						$arrUsers[$arr["userid"]]["device_type"] = $arr["device_type"];
						$arrUsers[$arr["userid"]]["bookingid"] = $postData["bookingid"];
					}
                }
				if(count($arrUsers)){
                    $nunmber = sendNotification($arrUsers);
                    if($nunmber){
                       //$this->common_model->update(TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"]),array("request_sent_to_carowners"=>"yes"));
                       $this->response(array("status"=>"success","msg"=> "Booking notifications are sent successfully to car owners."), 200);
                    }else{
                        $this->response(array("status"=>"error","msg"=> "Problem with sending notifications to car owners."), 200);
                    }
                }else{
                    $this->response(array("status"=>"error","msg"=> "No car listed in your selected criteria."), 200);
                }
           }else{
            $this->response(array("status"=>"error","msg"=> "Booking notifications are already sent to car owners."), 200);
           }
        }else{
            $this->response(array("status"=>"error","msg"=> "Booking Id not valid"), 200);
        }
    }
	function sendNotificationToOwnerCron_post(){
        
        $postData = $_POST;
        $bookingArr = $this->common_model->select("request_sent_to_carowners,bookingid,latitude,longitude,catid,pickup_datetime,return_datetime",TB_CAR_BOOKING,array("payment_status"=>"success"));
		
		if(count($bookingArr)>0){
			foreach($bookingArr as $k => $v)
			{	
           if($v["request_sent_to_carowners"] == "no"){
			$this->common_model->update(TB_CAR_BOOKING,array("bookingid"=>$v["bookingid"]),array("request_sent_to_carowners"=>"yes"));
				$categoryArr = $this->common_model->select("brandid",TB_CAR_BRAND,array("catid"=>$v["catid"]));
				$ids = implode(",",array_map(function($cat) { return $cat["brandid"]; },$categoryArr)); 
                /*$arrCars = $this->cars_model->joinQuery("device_token,device_type,Registration_no,carid,".TB_USERS.".userid AS userid,(3959 * acos(cos(radians(".$bookingArr[0]["latitude"]."))*cos(radians(car_latitude))*cos(radians(car_longitude) - radians(".$bookingArr[0]["longitude"].")) + sin 
(radians(".$bookingArr[0]["latitude"]."))*sin(radians(car_latitude))))*1.609344 AS distance",TB_CAR_LIST,array(TB_USERS.".user_status"=>"published", "user_type"=>"car_owner","latitude != "=> '',"longitude != "=> ''),array(),array(),array(TB_USERS=>TB_USERS.".userid = ".TB_CAR_LIST.".userid"),"",'distance < 15.534275',array("brandid"=>$ids));*/
				$arrCars = $this->cars_model->joinQuery("device_token,device_type,Registration_no,carid,".TB_USERS.".userid AS userid,(3959 * acos(cos(radians(".$v["latitude"]."))*cos(radians(car_latitude))*cos(radians(car_longitude) - radians(".$v["longitude"].")) + sin 
(radians(".$v["latitude"]."))*sin(radians(car_latitude))))*1.609344 AS distance",TB_CAR_LIST,array(TB_USERS.".user_status"=>"published", "user_type"=>"car_owner"),array(),array(),array(TB_USERS=>TB_USERS.".userid = ".TB_CAR_LIST.".userid"),"",'distance < 15.534275',array("brandid"=>$ids)); //,"car_latitude != "=> '',"car_longitude != "=> ''
	            foreach($arrCars AS $arr){

					$blocked_dates=$this->common_model->select("from_date,to_date",TB_CAR_CALENDAR,array("carid"=>trim($arr["carid"]),'type'=>'blocked'),array(),NULL,"(from_date >= '".$v["pickup_datetime"]."' AND from_date <= '".$v["return_datetime"]."') OR (to_date >= '".$v["pickup_datetime"]."' AND to_date <= '".$v["return_datetime"]."')");
					//echo $this->db->last_query();
					//echo "<pre>";print_r($blocked_dates);exit;
					if(!count($blocked_dates))
					{
						$arrUsers[$arr["userid"]]["insertArr"][] = array("carid"=>$arr["carid"],"userid"=>$arr["userid"],"bookingid"=>$v["bookingid"]);
						$arrUsers[$arr["userid"]]["registration_no"][] = $arr["Registration_no"];
						$arrUsers[$arr["userid"]]["device_token"] = $arr["device_token"];
						$arrUsers[$arr["userid"]]["device_type"] = $arr["device_type"];
						$arrUsers[$arr["userid"]]["bookingid"] = $v["bookingid"];
					}
                }
				if(count($arrUsers)){
                    $nunmber = sendNotification($arrUsers);
                    if($nunmber){
                       //$this->common_model->update(TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"]),array("request_sent_to_carowners"=>"yes"));
                      // $this->response(array("status"=>"success","msg"=> "Booking notifications are sent successfully to car owners."), 200);
                    }
                }
           }
		}
        }
    }
    function sendNotificationToDriver_post(){
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }else if(trim($postData["bookingid"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter booking id."), 200);
        }else if(trim($postData["carid"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter car id."), 200);
        }
        $bookingArr = $this->common_model->select("request_sent_to_dirvers,bookingid,latitude,longitude",TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"]));
        if(count($bookingArr)>0){
            if($bookingArr[0]["request_sent_to_dirvers"] == "no"){
                $user = getValidUser(trim($postData["user_token"]));
				$carArr = $this->common_model->select("carid,car_latitude,car_longitude",TB_CAR_LIST,array("carid"=>$postData["carid"],"userid"=>$user[0]['userid']));
                if(count($carArr)>0){
                    //$this->common_model->delete(TB_BOOKING_REQUEST_TO_OWNER,array("bookingid"=>$postData["bookingid"]));
					
					$this->common_model->update(TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"]),array("confirmed_carid"=>$carArr[0]["carid"],"request_sent_to_dirvers"=>"yes"));
                    $arrUsers = $this->common_model->select("device_token,device_type,userid,(3959 * acos(cos(radians(".$carArr[0]["car_latitude"]."))*cos(radians(latitude))*cos(radians(longitude) - radians(".$carArr[0]["car_longitude"].")) + sin 
(radians(".$carArr[0]["car_latitude"]."))*sin(radians(latitude))))*1.609344 AS distance",TB_USERS,array("user_status"=>"published", "user_type"=>"driver","latitude != "=> '',"longitude != "=> ''),'distance < 15.534275');
                    if(count($arrUsers)){
                        $nunmber = sendNotificationToDriver($arrUsers,$postData["bookingid"],$carArr[0]["carid"]);
                        if($nunmber){
                           //$this->common_model->update(TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"]),array("confirmed_carid"=>$carArr[0]["carid"],"request_sent_to_dirvers"=>"yes"));
                           $this->response(array("status"=>"success","msg"=> "Booking notifications are sent successfully to car drivers."), 200);
                        }else{
                            $this->response(array("status"=>"error","msg"=> "Problem with sending notifications to car drivers."), 200);
                        }
                    }else{
                        $this->response(array("status"=>"error","msg"=> "No car listed in your selected criteria."), 200);
                    }  
                }else{
                    $this->response(array("status"=>"error","msg"=> "Car Id not valid."), 200);
                }
           }else{
            $this->response(array("status"=>"error","msg"=> "Booking notifications are already sent to car drivers."), 200);
           }
        }else{
            $this->response(array("status"=>"error","msg"=> "Booking Id not valid"), 200);
        }
    }

    function getOwnerNotifications_post()
	{
		if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
		$postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }
			$user = $this->common_model->select("*",TB_USERS,array("user_type"=>"car_owner","user_token"=>trim($postData["user_token"])));
		if(count($user)>0){
			$join_arr=array(
							TB_USERS=>TB_USERS.".userid = ".TB_BOOKING_REQUEST_TO_OWNER.".userid",
							TB_CAR_LIST=>TB_CAR_LIST.".carid = ".TB_BOOKING_REQUEST_TO_OWNER.".carid",
							TB_CAR_BOOKING=>TB_CAR_BOOKING.".bookingid = ".TB_BOOKING_REQUEST_TO_OWNER.".bookingid",
							);
		
			$arrCars = $this->cars_model->joinQuery("*,".TB_CAR_BOOKING.".latitude as pickup_latitude,".TB_CAR_BOOKING.".longitude as pickup_longitude",TB_BOOKING_REQUEST_TO_OWNER,array(TB_BOOKING_REQUEST_TO_OWNER.".userid"=>$user[0]['userid']),array(),array(),$join_arr);
		
			$data=array();
			$i=0;
			foreach($arrCars AS $arr)
			{
				$data[$i]["bookingid"] = $arr["bookingid"];
				$data[$i]["userid"] = $arr["bookingid"];
				$data[$i]["carid"] = $arr["carid"];
				
				$data[$i]["required_location"] = $arr["required_location"];
				$data[$i]["pickup_latitude"] = $arr["pickup_latitude"];
				$data[$i]["pickup_longitude"] = $arr["pickup_longitude"];
				$data[$i]["return_location"] = $arr["return_location"];
				$data[$i]["return_lat"] = $arr["return_lat"];
				$data[$i]["return_long"] = $arr["return_long"];
				$data[$i]["pickup_datetime"] = $arr["pickup_datetime"];
				$data[$i]["return_datetime"] = $arr["return_datetime"];
				
				$data[$i]["car_address"] = $arr["car_address"];
				$data[$i]["car_latitude"] = $arr["car_latitude"];
				$data[$i]["car_longitude"] = $arr["car_longitude"];
				$data[$i]["rental_amount"] = $arr["rental_amount"];
				
				$parent_brands = $this->common_model->select("brandid,name,catid",TB_CAR_BRAND,array("brandid"=>$arr["brand_parentid"]));
				$data[$i]["parent_brand"] = $parent_brands[0]['name'];
				$brands = $this->common_model->select("brandid,name,catid",TB_CAR_BRAND,array("brandid"=>$arr["brandid"]));
				$data[$i]["brand"] = $brands[0]['name'];
				
				$car_categories = $this->common_model->select("catid,name",TB_CAR_CATEGORY,array("catid"=>$brands[0]['catid']));
				$data[$i]['car_category']=$car_categories[0]['name'];
				$data[$i]["Registration_no"] = $arr['Registration_no'];
				$i++;
			}
			$this->response(array("status"=>"success","user_token"=>$postData["user_token"],"booking"=> $data), 200);
		}
		else
		{
			$this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
		}
	}
    function getUser_post(){
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }else if(trim($postData["userid"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send user id."), 200);
        }
        $user = $this->common_model->select("*",TB_USERS,array("userid"=>trim($postData["userid"]),"user_token"=>trim($postData["user_token"])));
        if(count($user)>0){
            //~ $base_url = base_url();
            //~ $cars = $this->common_model->select("carid,brandid,brand_parentid,transmission,year,fuel_type,mileage,Registration_no,car_address,car_latitude,car_longitude",TB_CAR_LIST,array("userid"=>$user[0]["userid"],"carid"=>$postData["carid"]));
            //~ $carImages = $this->common_model->select("CONCAT('".$base_url."','upload/carimages/',image_name) AS image_name,cimageid",TB_CAR_IMAGES,array("carid"=>$postData["carid"]));
            //~ $cars[0]["images"] = $carImages;
            $this->response(array("status"=>"success","user_token"=>$postData["user_token"],"user"=> $user[0]), 200);
        }else{
            $this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
    }
	function getUserBank_post(){
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }
		$user = $this->common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        $user = $this->common_model->select("*",TB_USER_PAYMENT_PREFERENCE,array("userid"=>trim($user[0]["userid"])));
        if(count($user)>0){
           $this->response(array("status"=>"success","userBank"=> $user[0]), 200);
        }else{
            $this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
    }
	function updatePaymentStatus_post(){
        if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter access token."), 200);
        }else if(trim($postData["bookingid"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter booking id."), 200);
        }else if(trim($postData["payment_status"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please enter payment status."), 200);
        }
		
        $user = $this->common_model->select("*",TB_USERS,array("user_token"=>trim($postData["user_token"])));
        if(count($user)>0){
            $book=$this->common_model->update(TB_CAR_BOOKING,array("bookingid"=>$postData["bookingid"],"userid"=>$user[0]['userid']),array("payment_status"=>$postData["payment_status"]));
           // if($book)
				$this->response(array("status"=>"success","bookingid"=> $postData["bookingid"],"msg"=> "Payment status updated successfully."), 200);
			//else
			//	$this->response(array("status"=>"error","bookingid"=> $postData["bookingid"],"msg"=> "No record found."), 200);
        }else{
            $this->response(array("status"=>"error","msg"=> "This user is not valid."), 200);
        }
    }
    function updateUser_post()
    {
        $segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }
		$user = $this->common_model->select("userid",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		
        if(trim($postData["first_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter first name."), 200);
        }else if(!alpha($postData["first_name"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter only alphabets in first name."), 200);
        }else if(trim($postData["last_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter last name."), 200);
        }else if(!alpha($postData["last_name"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter only alphabets in last name."), 200);
        }else if(trim($postData["birth_date"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter birth date."), 200);
        }else if(trim($postData["mobile_phone"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter mobile number."), 200);
        }else if(!phone_number($postData["mobile_phone"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid  mobile number."), 200);
        }else if(numeric($postData["postal_code"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid postal code."), 200);
        }/*else if(trim($postData["user_type"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please choose user type."), 200);
        }*/

        //$emailArr = $this->common_model->select("email",TB_USERS,array("email"=>trim($postData["email"])));
        //if(count($emailArr) > 0)
        //{
        //    $this->response(array("status"=>"error", "msg"=> "User already exist with this email."), 200);
        //}else
	{
            $date = date("Y-m-d H:i:s");
            //$user_token = md5(uniqid(rand(), true));
			if(isset($_FILES["license_pic"]))
			{
				$src_file=$_FILES["license_pic"]["tmp_name"];
				$target_dir = "images/license/";
				$ext=explode('.',basename($_FILES["license_pic"]["name"]));
				$ext=$ext[1];
				$file_name=$user[0]["userid"].".".$ext;
				$target_file = $target_dir.$file_name;
				if(move_uploaded_file($src_file,$target_file))
				{
					$imageData["upload_data"]["file_name"]=$file_name;
				}
			}
			//echo "<pre>";print_r($imageData);exit;
            $insertArr = array(
                                //"password"      =>md5(trim($postData["password"])),
                                //"user_token"    =>$user_token,
                                "first_name"    =>trim($postData["first_name"]),
                                "last_name"     =>trim($postData["last_name"]),
                                //"email"         =>trim($postData["email"]),
                                "mobile_phone"  =>trim($postData["mobile_phone"]),
								"birth_date"	=>trim(date("Y-m-d",strtotime($postData["birth_date"]))),
								"place_of_birth"=>trim($postData["place_of_birth"]),
                                "address1"     =>trim($postData["address1"]),
                                "address2"     =>trim($postData["address2"]),
                                "postal_code"  => trim($postData["postal_code"]),
                                "city" => trim($postData["city"]),
                                "country" => trim($postData["country"]),
                                "same_car_parking_address" => trim($postData["same_car_parking_address"]),
								"license_no"=> trim($postData["license_no"]),
								"first_issue_date"=>trim(date("Y-m-d",strtotime($postData["first_issue_date"]))),
                                "country_of_issue"=> trim($postData["country_of_issue"]),
								//"license_pic"   =>$imageData["upload_data"]["file_name"],
								"ip_address"   =>$_SERVER['REMOTE_ADDR'],
								"modified_date"  => $date,
                           );
			if($imageData["upload_data"]["file_name"]!="")
			{
				$insertArr['license_pic']=$imageData["upload_data"]["file_name"];
			}
            $user = $this->common_model->update(TB_USERS,array("userid"=>$user[0]["userid"]),$insertArr);
        }
        if($user)
        {
            $this->response(array("status"=>"success","userid"=>$user[0]["userid"], "user_token"=>$postData['user_token'], "msg"=> "User updated successfully."), 200);    
        }else{
            $this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
        
    }

	function userAccount_post()
	{
		$segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }
		$user = $this->common_model->select("userid",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		
        if(trim($postData["acc_email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }else if(valid_email($postData["acc_email"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid email."), 200);
        }else if(trim($postData["acc_password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter password."), 200);
		}
        $emailArr = $this->common_model->select("email",TB_USERS,array("userid != "=>trim($user[0]["userid"]),"email"=>trim($postData["acc_email"])));
        if(count($emailArr) > 0)
        {
            $this->response(array("status"=>"error", "msg"=> "User already exist with this email."), 200);
        }else
		{
            $date = date("Y-m-d H:i:s");
            $insertArr = array(
                                "password"      =>md5(trim($postData["acc_password"])),
                                "email"         =>trim($postData["acc_email"]),
								"modified_date"  => $date,
                           );
            $user = $this->common_model->update(TB_USERS,array("userid"=>$user[0]["userid"]),$insertArr);
        }
        if($user)
        {
            $this->response(array("status"=>"success","userid"=>$user[0]["userid"], "user_token"=>$postData["user_token"], "msg"=> "User updated successfully."), 200);    
        }else{
            $this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
	}
	
	function userNotify_post()
	{
		$segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }
		$user = $this->common_model->select("userid",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		
//        if(trim($postData["acc_email"]) == ""){
//            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
//        }else if(trim($postData["acc_password"]) == ""){
//            $this->response(array("status"=>"error", "msg"=> "Please enter password."), 200);
//		}
//        $emailArr = $this->common_model->select("email",TB_USERS,array("userid != "=>trim($postData["acc_userid"]),"email"=>trim($postData["acc_email"])));
//        if(count($emailArr) > 0)
//        {
//            $this->response(array("status"=>"error", "msg"=> "User already exist with this email."), 200);
//
		{
            $date = date("Y-m-d H:i:s");
            $insertArr = array(
                                "email_notification"=>trim(($postData["email_notification"]=='yes')?$postData["email_notification"]:'no'),
                                "newsletter_notification"=>trim(($postData["newsletter_notification"]=='yes')?$postData["newsletter_notification"]:'no'),
								"app_push_notification"=>trim(($postData["app_push_notification"]=='yes')?$postData["app_push_notification"]:'no'),
								"modified_date"  => $date,
                           );
            $user = $this->common_model->update(TB_USERS,array("userid"=>$user[0]["userid"]),$insertArr);
        }
        if($user)
        {
            $this->response(array("status"=>"success","userid"=>$user[0]["userid"], "user_token"=>$postData["user_token"], "msg"=> "User updated successfully."), 200);    
        }else{
            $this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
	}
	
	function updateUserBank_post()
	{
		$segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["user_token"]) == ""){
        	$this->response(array("status"=>"error","msg"=> "Please send access token."), 200);
        }
		$user = $this->common_model->select("userid",TB_USERS,array("user_token"=>trim($postData["user_token"])));
		
        if(trim($postData["cc_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter credit card no."), 200);
        }else if(numeric($postData["cc_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid credit card."), 200);
        }else if(trim($postData["bank_acct_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter account no."), 200);
		}else if(numeric($postData["bank_acct_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid account no."), 200);
        }
		else if(trim($postData["bank_iban_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter iban no."), 200);
		}else if(numeric($postData["bank_iban_no"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid iban no."), 200);
        }
		else if(trim($postData["bank_name"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter bank name."), 200);
		}
		else if(trim($postData["bank_postal_code"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter postal code."), 200);
		}else if(numeric($postData["bank_postal_code"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid postal code."), 200);
        }
		else if(trim($postData["bank_city"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter bank city."), 200);
		}
		else if(trim($postData["bank_country"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter bank country."), 200);
		}
//		else if(trim($postData["other_notes"]) == ""){
//            $this->response(array("status"=>"error", "msg"=> "Please enter other notes."), 200);
//		}
			$date = date("Y-m-d H:i:s");
            $insertArr = array(
                                "cc_no"      =>trim($postData["cc_no"]),
                                "bank_acct_no"         =>trim($postData["bank_acct_no"]),
								"bank_iban_no"      =>trim($postData["bank_iban_no"]),
                                "bank_name"         =>trim($postData["bank_name"]),
								"bank_postal_code"      =>trim($postData["bank_postal_code"]),
                                "bank_city"         =>trim($postData["bank_city"]),
								"bank_country"      =>trim($postData["bank_country"]),
                                "other_notes"         =>trim($postData["other_notes"]),
								"modified_date"  => $date,
                           );
        $ids = $this->common_model->select("payid",TB_USER_PAYMENT_PREFERENCE,array("userid"=>trim($user[0]["userid"])));
        //echo "<pre>";print_r($ids);
		//echo count($ids);
		//exit;
		if(!count($ids))
        {
            //insert
			$insertArr['created_date']=$date;
			$insertArr['userid']=$user[0]["userid"];
			$user = $this->common_model->insert(TB_USER_PAYMENT_PREFERENCE,$insertArr);
			
        }else
		{
            
            $user = $this->common_model->update(TB_USER_PAYMENT_PREFERENCE,array("userid"=>$user[0]["userid"]),$insertArr);
        }
        if($user)
        {
            $this->response(array("status"=>"success","userid"=>$user[0]["userid"], "user_token"=>$postData["user_token"], "msg"=> "User updated successfully."), 200);    
        }else{
            $this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
	}
	function get_password_post()
	{
		 if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }
        $postData = $_POST;
		if(trim($postData["email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }else if(valid_email($postData["email"])){
            $this->response(array("status"=>"error", "msg"=> "Please enter valid email."), 200);
        }
		else
		{
			$email=$postData['email'];
			$userdata=$this->common_model->select("userid,first_name",TB_USERS,array("email"=>trim($email)));
			if(!count($userdata))
			{
				//echo "Please enter correct email, this email does not exist.";
				//exit;
				$this->response(array("status"=>"error", "msg"=> "Please enter correct email, this email does not exist."), 200);
			}
			else
			{
				$userId=$userdata[0]['userid'];
				$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$randstring = '';
				for ($i = 0; $i < 8; $i++) {
					$randstring.= $characters[rand(0, strlen($characters))];
				}
				
				$this->common_model->update(TB_USERS,array("userid"=>$userId),array('password'=>md5($randstring)));
				
				//echo $randstring;
				/**********/
				$from = "Carky";
				$to = $email; 
				$subject = "Your new Password";
				$headers  = 'MIME-Version: 1.0' . PHP_EOL;
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$headers .= "From:".ADMIN_EMAIL." \r\n";
			  
			  
						// More headers
			  
				$message = "Dear ".$userdata[0]['first_name']." 
				<br/><br/>Please refer this password.  
				<br/>Password: " . $randstring ."
				<br/><br/>==================================
				<br/>Carky";
				mail($to,$subject,$message,$headers);
				/**********/
				//echo "Your password has been sent to your Email. please check your email.";
				//exit;
				$this->response(array("status"=>"success", "msg"=> "Your password has been sent to your Email. please check your email."), 200);
			}
		}
	}
    
    
    function user_delete()
    {
        //$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
}
?>
