<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Xml extends CI_Controller{
       
    function __construct() {
		parent::__construct();
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("users_model");
		//$this->xml_file_path = realpath('cust_xml');
		$this->xml_file_path = realpath('starvisionXML');
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	function get_customers() {
		
		libxml_use_internal_errors(true);
		//$Url = base_url().'cust_xml/KMC_logCustomer.xml';
		$Url = base_url().'starvisionXML/AG_logCustomer.xml';
		//echo $Url;die;
		
		$xml = simplexml_load_file($Url);
		//print_r($xml); die;
		if ($xml === false) {
		echo "Failed loading XML: ";
			foreach(libxml_get_errors() as $error) {
				echo "<br>", $error->message;
			}
		}
		else {
			$customer_count = count($xml->Customer);
			$cond = '';
			
			foreach($xml->Customer as $customer) {
				
				 $cust_code = $customer->CustomerCode;
				 $cust_email = $customer->Email;
				 $time_log = $customer->TimeLog;
				 
				 $timestamp = strtotime($time_log);
				 $reg_date = date( 'Y-m-d h:i:s', $timestamp );
				 
				 				
				 //Check customer code and customer email is already exist
				 if(trim((string)$cust_code) !="" && trim((string)$cust_email)!="") {
					
					 $cond_user = array("email_address" => trim((string)$cust_email));
					 $alreadyexistusers = $this->users_model->getAllUsers($cond_user);
					 
					 $is_user_exist = count($alreadyexistusers);
					 if($is_user_exist>0) {
						 //Update user
								$cond_update = array("email_address"=>trim((string)$cust_email));
								$data_update = array("c_name"=>trim((string)$customer->CustomerName),"contact_number"=>trim((string)$customer->MobileNumber));
								$this->common_model->update(TB_USERS,$cond_update,$data_update);
					 }
					 
					
					 
					 else {
								if (!filter_var(trim((string)$customer->Email), FILTER_VALIDATE_EMAIL) === false) {
									  //Add Customer
									$insertId = $this->common_model->insert(TB_USERS,array("c_name"=>trim((string)$customer->CustomerName),"email_address"=>trim((string)$customer->Email),"password"=>trim(md5('123')),"user_type"=>'OPEL',"status" => 'active',"contact_number"=>trim((string)$customer->MobileNumber),"registered_date"=>$reg_date,"customer_code" => trim((string)$customer->CustomerCode)));
									//echo $this->db->last_query();die;
								} else {
									
								}
					 }
					
				
					 
					 
					 $cond = array("email_address" => trim((string)$cust_email),"cust_code"=>trim((string)$cust_code));
					 $alreadyexistuserscount = $this->users_model->getAllUsersExist($cond);
						$is_exist = count($alreadyexistuserscount);
						if($is_exist>0) {
								//Update Customer
								$cond_update = array("email_address"=>trim((string)$cust_email),"cust_code"=>trim((string)$cust_code));
								$data_update = array("cust_name"=>trim((string)$customer->CustomerName),"phone_no"=>trim((string)$customer->MobileNumber),"address"=>trim((string)$customer->Address),"date_of_birth"=>trim((string)$customer->DateofBirth),"nationality"=>trim((string)$customer->Nationality),"cust_code"=>trim((string)$customer->CustomerCode),"pdpa_consent"=>trim((string)$customer->PDPAConsent),"id_card_no"=>trim((string)$customer->IDNumber),"maritial_status"=>trim((string)$customer->MartialStatus),"date_modified"=>trim((string)$customer->TimeLog));
								$this->common_model->update(TB_VEHICLE_OWNER_DETAILS,$cond_update,$data_update);
							}
						else {
							
								if (!filter_var(trim((string)$customer->Email), FILTER_VALIDATE_EMAIL) === false) {
									  //Add Customer
									//Get User id
									 $cond = array("email_address"=>trim((string)$cust_email));
									 $get_users = $this->users_model->getAllUsers($cond);
									 
									 
									$insertId = $this->common_model->insert(TB_VEHICLE_OWNER_DETAILS,array("user_id"=>$get_users['0']['id'],"cust_name"=>trim((string)$customer->CustomerName),"email_address"=>trim((string)$customer->Email),"phone_no"=>trim((string)$customer->MobileNumber),"address"=>trim((string)$customer->Address),"date_of_birth"=>trim((string)$customer->DateofBirth),"nationality"=>trim((string)$customer->Nationality),"cust_code"=>trim((string)$customer->CustomerCode),"pdpa_consent"=>trim((string)$customer->PDPAConsent),"id_card_no"=>trim((string)$customer->IDNumber),"maritial_status"=>trim((string)$customer->MartialStatus),"date_modified"=>trim((string)$customer->TimeLog)));
									//echo $this->db->last_query();die;
									
									
									
								} else {
									
								}
							
								
							}
				 }
				 else {
					 
				 }
			}
		}
}


	function get_vehicle() {
		
		libxml_use_internal_errors(true);
		//$Url = base_url().'cust_xml/KMC_logVehicle.xml';
		$Url = base_url().'starvisionXML/AG_LogVehicle.xml';
		$xml = simplexml_load_file($Url);
		if ($xml === false) {
		echo "Failed loading XML: ";
			foreach(libxml_get_errors() as $error) {
				echo "<br>", $error->message;
			}
		}
		else {
			$vehicle_count = count($xml->VehicleDetails);
			$cond = '';
			$k = 0;
			
				$arrAttribute = array();
				$reg_number 	= "";
				 $vehiclegroup  = "";
				 $makemodel 	= "";
				 $reg_date 		= "";
				 $enginenumber  = "";
				 $chassisnumber = "";
				 $customercode  = "";
				 
				 
			foreach($xml->VehicleDetails as $vehicle) {
				
				 $reg_number 	= trim((string)$vehicle->RegisteredNumber);
				 $vehiclegroup  = trim((string)$vehicle->VehicleGroup);
				 $makemodel 	= trim((string)$vehicle->MakeModel);
				 $reg_date 		= trim((string)$vehicle->RegistrationDate);
				 $enginenumber  = trim((string)$vehicle->EngineNumber);
				 $chassisnumber = trim((string)$vehicle->ChassisNumber);
				 $customercode  = trim((string)$vehicle->CustomerCode);
				 
					//Check vehicle already exist 
					if($reg_number!="") {
						$cond_vehicle = array("registration_no" => $reg_number);
						$vehicleexist = $this->users_model->getCustomerVehicleById($cond_vehicle);
						$vehicle_exist_count = count($vehicleexist);
						
						if($vehicle_exist_count>0) {
							//Update vehicle details
									$cond_vehicle_onwer = array("cust_code" => $customercode);
									$vehicle_cust_code = $this->users_model->getAllUsersExist($cond_vehicle_onwer);
									$vehicle_cust_code_exist_count = count($vehicle_cust_code);
									if($vehicle_cust_code_exist_count>0) {
										//Get Customer id
										$customer_id = $vehicle_cust_code['0']['id'];
										
										$arrAttribute[$k]["engine_number"]   = $enginenumber;
										$arrAttribute[$k]["chassis_number"]  = $chassisnumber;
										$arrAttribute[$k]["registration_no"] = $reg_number;
										$arrAttribute[$k]["vehicle_group"]   = $vehiclegroup;
										$arrAttribute[$k]["make_mode"]       = $makemodel;
										$arrAttribute[$k]["customer_code"]   = $customercode;
										$arrAttribute[$k]["registration_date"] = $reg_date;
										   
										//echo $this->db->update_batch(); die;
										
											 
												 
										//echo $this->db->last_query();
									}
						}
						else {
							//Add vehicle details
								//check customer code is exist or not
								
									
								
									$cond_vehicle_onwer = array("cust_code" => $customercode);
									$vehicle_cust_code = $this->users_model->getAllUsersExist($cond_vehicle_onwer);
									$vehicle_cust_code_exist_count = count($vehicle_cust_code);
									if($vehicle_cust_code_exist_count>0) {
										//Get Customer id and User id
										
										$cond_vehicle_user = array("cust_code" => $customercode);
										$vehicle_users = $this->users_model->getAllUsersExist($cond_vehicle_user);
										$user_id = $vehicle_users['0']['user_id'];
										
										$customer_id = $vehicle_cust_code['0']['id'];
										
										
										
										$insertId = $this->common_model->insert(TB_VEHICLE_DETAILS,array("customer_id" => $customer_id,"user_id" => $user_id,"engine_number"=>$enginenumber,"chassis_number"=>$chassisnumber,"registration_no"=>$reg_number,"vehicle_group"=>$vehiclegroup,"make_mode" => $makemodel,"customer_code"=>$customercode,"registration_date"=>$reg_date));
									}
							}
				    }
				 
				 
				 
				foreach($vehicle->ServiceOrder as $ServiceOrder) {
				 		 		
				 		 $servicenumber =  trim((string)$ServiceOrder->ServiceNumber);
				 		 $transactiondate = trim((string)$ServiceOrder->TransactionDate);
				 		 $vehiclemileage = trim((string)$ServiceOrder->VehicleMileage);
				 		 $serviceadvisor = trim((string)$ServiceOrder->ServiceAdvisor);
				 		 $time_log = trim((string)$ServiceOrder->Time_Log);
				 		 $timestamp = strtotime($time_log);
						 $reg_date = date('Y-m-d h:i:s', $timestamp);
						 
						 
						 
						 $cond_vehicle_onwer = array("customer_code" => $customercode);
						 $vehicle_cust_code = $this->users_model->getvehicleandvehicleownerdetails($cond_vehicle_onwer);
						 $vehicle_cust_code_exist_count = count($vehicle_cust_code);
						 
						 $cond_service_order = array("service_order" => $servicenumber);
						 $vehicle_service_orders = $this->users_model->getserviceorderexist($cond_service_order);
						 $vehicle_service_orders_exist_count = count($vehicle_service_orders);
						 
						  if($vehicle_cust_code_exist_count>0 && $vehicle_service_orders_exist_count==0) {
							  
							  // Get Vehicle and vehicle owner details
							 $cond_vehicle_onwer = array("customer_code" => $customercode);
							 $vehicle_details = $this->users_model->getvehicleandvehicleownerdetails($cond_vehicle_onwer);
							 if($servicenumber!='') {
								 //$s_order_id = $vehicle_details['0']['id'];
								 $vehicle_id = $vehicle_details['0']['id'];
								 $customer_id = $vehicle_details['0']['customer_id'];
								 $user_id = $vehicle_details['0']['user_id'];
								 $transactiondate = date('Y-m-d',strtotime($transactiondate));
								 $soinsertId = $this->common_model->insert(TB_VEHICLE_SERVICE_ORDER,array("customer_id" => $customer_id,"user_id"=>$user_id,"vehicle_id"=>$vehicle_id,"service_order"=>$servicenumber,"transaction_date"=>$transactiondate,"vehicle_mileage" => $vehiclemileage));
							 }
						  }
						
					foreach($ServiceOrder->ServiceOrderProcess as $ServiceOrderProcess) {
						 
						 $LineNumber = trim((string)$ServiceOrderProcess->LineNumber);
						 $ProcessCode = trim((string)$ServiceOrderProcess->ProcessCode);
						 $ProcessDescription = trim((string)$ServiceOrderProcess->ProcessDescription);
						 if($LineNumber!='') {
							 $cond_vehicle_onwer = array("customer_code" => $customercode);
							 $vehicle_cust_code = $this->users_model->getvehicleandvehicleownerdetails($cond_vehicle_onwer);
							 $vehicle_cust_code_exist_count = count($vehicle_cust_code);
							 if($vehicle_cust_code_exist_count>0) {
								  
								 // Get Vehicle and vehicle owner details
								 $cond_vehicle_onwer = array("customer_code" => $customercode);
								 $vehicle_details = $this->users_model->getvehicleandvehicleownerdetails($cond_vehicle_onwer);
								 $customer_id = $vehicle_details['0']['customer_id'];	 
								 $user_id = $vehicle_details['0']['user_id'];	 
								 if($soinsertId!='') {		
									$insertId = $this->common_model->insert(TB_VEHICLE_SERVICE_ORDER_PROCESS,array("customer_id" => $customer_id,"user_id"=>$user_id,"s_order_id"=>$soinsertId,"line_number"=>$LineNumber,"process_code"=>$ProcessCode,"process_description"=>$ProcessDescription)); 
								 }
							}
						 }  
					}
				}
			$k++;
			}
			$this->common_model->update_batch(TB_VEHICLE_DETAILS,'customer_code',$arrAttribute);	
			//echo $this->db->last_query();die;
			//die;
			//die;
		}
}


}
?>
