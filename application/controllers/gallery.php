<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("users_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->load->helper("email_template"); 
		$this->load->model("gallery_model"); 
		//$this->load->library("richtexteditor");
		$this->profilepicpath =  realpath('profile_pic');
		$this->load->helper('xml');
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'user_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	 
	public function list_community_gallery() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
				$postData = $this->input->post();
				//print_r($postData);die;
				$cond = array();
				$like = array();
				$join = array(TB_COMMUNITY_LIKE => TB_COMMUNITY.".com_gal_id = ".TB_COMMUNITY_LIKE.".vehicle_id");
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				
				$count = $this->gallery_model->getCatalogModelGalleryCount($cond,$like,$join);
				//echo $this->db->last_query();die;
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("img_name");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "cnt";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "DESC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				
				$catalog = $this->gallery_model->getCatalogModelGalleryPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
				
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
								<th class="text-center '.$css["0"].'" width="30%" onclick="changePaginate(0,\'img_name\',\''.$corder[0].'\')">Car Images</th>
								<th class="text-center '.$css["1"].'" width="30%")">User Name</th>
								<th class="text-center '.$css["2"].'" width="30%")">Total Likes</th>
								 
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($catalog)>0)
				{
						if(count($catalog) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
						foreach($catalog as $row) {  
							 $cond_user = array("user_id" =>$row['user_id']);
							 $user_detail = $this->users_model->getAllUsers($cond_user);
							 
							 $cond_total = array("vehicle_id" => $row['id']);
							 $count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);
							 
					$table .= '<tr id="row_'.$row["id"].'">
							<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-center"  width="30%"><a target="_blank" href="'.base_url().'community_gallery/'.$row["img_name"].' "><img width="200" height="100" src="'.base_url().'community_gallery/'.$row["img_name"].'"/></a></td> 
							<td class="text-center"  width="30%">'.(trim($user_detail[0]['c_name'])?$user_detail[0]['c_name']:"-").'</td> 
							<td class="text-center"  width="30%">'.$row['cnt'].'</td> 
							 
						  </tr> 
						  </tr>
						  ';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($catalog)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="4">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
				// $this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE_OPTION;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($catalog),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	public function delete_catalog_model_gallery() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
				
					$isdelete = 0;
					
					$cond = array("com_gal_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_COMMUNITY,$cond);
					
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
}
?>
