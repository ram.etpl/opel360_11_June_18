<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pdf extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model("pdf_model");
		$this->load->model("login_model");
		$this->load->model("catalog_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->pdf_file_path =  realpath('images/PDF');
		//$this->load->library("richtexteditor"); 
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'pdf_manuals_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	
	public function getPdf()
	{ 
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{
			if($this->checkAdminPermission()){
				//GET ALL MODELS
				$cond = array();
				$catalog = $this->catalog_model->getAllCatalogModel($cond);
				foreach($catalog as $x => $catalog_value) {
					
					$model_list .= '<option value="'.$catalog_value['id'].'">'.$catalog_value['model_name'].'</option>';
					
				}
				$data['models'] = $model_list;
				
				$this->load->view("administrator/pdf",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getPDFById() // Get Single PDF
	{
		if(is_ajax_request())
		{
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("p_id" => $this->encrypt->decode($postData["id"]));
				$pdf = $this->pdf_model->getPDFById($cond);
				echo json_encode($pdf[0]);exit;
			
		}
	}
	
	
	//Validate File
	
	public function validateManualfile($postData,$files)
	{
	
		$count = count($files['name']);
		if (isset($files['name'][0]) && $files['name'][0] != "") {
            $config = array(
                'upload_path' => $this->pdf_file_path,
                'allowed_types' => 'pdf|PDF',
                'max_width' => '6000',
                'max_height' => '5000'
            );
            
        // load Upload library
           $this->load->library('upload', $config);
           $check_upload = $this->upload->do_upload('pdf_manual_file');
 
			if($check_upload){
 
				$uploaded_manual = $this->upload->data('pdf_manual_file'); 
				return array("status" => 1,"count" => $count,"data_flat" => $uploaded_manual); die;
			 }
			 else { 
				$error = $this->upload->display_errors();
				return array("status" => 0,"error" => $error); 
				 
			 }
		 }
			 
		else {
				return array("status" => 1,"is_file"=>0);
			}
}
	
	
	public function save_pdf() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();
					$FILEDATA = $_FILES['pdf_manual_file'];
					
					if($postData["manual_id"] == "")
					{   
						$isValid = $this->validateManualfile($postData,$FILEDATA);
						if($isValid["status"] == 1)
						{ 
								//date_default_timezone_set('Asia/Singapore');
								$created_date = date('Y-m-d H:i:s');
								$insertId = $this->common_model->insert(TB_PDF_MANUAL,array("p_name" => $postData["txt_pdf_manual_name"],"p_file" => $isValid['data_flat']['file_name'],"p_desc" => $postData["pdf_manual_desc"],"model_id" => $postData["model"],"date_modified"=>$created_date));
								
								if($insertId)
								{    
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>PDF manual has been added successfully.</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValid['error'].'</div>')); exit;
						} 
						 
					}
					else
					{  
						if($FILEDATA['name']!='') {
						$isValid = $this->validateManualfile($postData,$FILEDATA);
							$file_name = $isValid['data_flat']['file_name'];
						}
					
						else {
							$isValid["status"] = 1;
							$file_name = $postData['pdf_manual_file_exist'];
						}
						
						
						
						if($isValid["status"] == 1)
						{ 
							$cond1 = array("p_id" => $this->encrypt->decode($postData["manual_id"]));
								//date_default_timezone_set('Asia/Singapore');
							$updated_date = date('Y-m-d H:i:s');
							$updateArr = $this->common_model->update(TB_PDF_MANUAL,$cond1,array("p_name" => $postData["txt_pdf_manual_name"], "p_file" => $file_name,"p_desc" => $postData["pdf_manual_desc"],"model_id" => $postData["model"],"date_modified" => $updated_date));
							
							//$cond = array("p_id" => $postData["manual_id"]);
							//$pdf_data = $this->pdf_model->getPDFById($cond);
							
							$row = '<td class="text-center" width="5%"><input type="checkbox" value="'.$postData["manual_id"].'" name="check" id="check" class="chk"></td>
							<td class="text-center"  width="25%">'.$postData["txt_pdf_manual_name"].'</td>
							<td class="text-center"  width="25%"><a target="_blank" href="'.base_url().'images/PDF/'.$file_name.'"><img width="70" height="70" src="'.base_url().'img/pdf.ico"/></a></td>
							<td class="text-center"  width="25%">'.$updated_date.'</td>
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["manual_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								
							</td>';
							//echo $this->encrypt->decode($postData["userId"]);die;
							echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["manual_id"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>PDF manual has been updated successfully.</div>')); exit;
							
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValid['error'].'</div>')); exit;
						}
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_pdf_manual() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					//print_r($postData);die;
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->pdf_model->getPDFManualCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("p_name","p_file","model_name","date_modified");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "p_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$pdf = $this->pdf_model->getPDFManualPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'p_name\',\''.$corder[0].'\')">PDF name</th>
									<th class="text-center '.$css["1"].'" width="25%" onclick="changePaginate(0,\'p_file\',\''.$corder[1].'\')">PDF file</th>
									<th class="text-center '.$css["2"].'" width="25%" onclick="changePaginate(0,\'model_name\',\''.$corder[2].'\')">Model name</th>
									<th class="text-center '.$css["3"].'" width="15%" onclick="changePaginate(0,\'date_modified\',\''.$corder[3].'\')">PDF Created date </th>
									<th class="text-center" width="15%">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($pdf)>0)
					{
							if(count($pdf) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($pdf as $row) {  
								 
						$table .= '<tr id="row_'.$row["id"].'">
								<td class="text-center"  width="5%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="25%">'.$row["p_name"].'</td>
								<td class="text-center"  width="25%"><a target="_blank" href="'.base_url().'images/PDF/'.$row["p_file"].'"><img width="70" height="70" src="'.base_url().'img/pdf.ico"/></a></td>
								<td class="text-center"  width="25%">'.$row["model_name"].'</td>
								<td class="text-center"  width="25%">'.$row["date_modified"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($row["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr> 
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($pdf)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="5">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($pdf),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	
	
	
	public function validateCatalogModel($postData)
	{   
		
		return array("status" => 1);
	}
	
	
	public function delete_pdf() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("p_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_PDF_MANUAL,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
}
?>
