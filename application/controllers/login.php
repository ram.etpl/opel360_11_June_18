<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("users_model");
		$this->load->library("form_validation");
	}
	
	
	public function index()
	{ 
		if(is_user_logged_in())
		{
			redirect("users/getUsers");
			exit;
		}
		else if($this->input->post())
		{
			$postData = $this->input->post();
			
			$this->session->set_flashdata("postData", $postData);
			if($postData["txt_username"] == "" && $postData["txt_password"] == "")
			{
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Please enter Username/Email Address & Password.</div>');
				redirect("login"); exit;
			} 
			else if($postData["txt_username"] == "")
			{
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Please enter Username/Email Address.</div>');
				redirect("login"); exit;
			}
			else if($postData["txt_password"] == "")
			{   
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Please enter Password.</div>');
				redirect("login"); exit;
			}
			
			$cond = array("email_address" => $postData["txt_username"],"password" => md5($postData["txt_password"]));
			
			$userData = $this->login_model->validUser(TB_USERS,"user_id,super_admin,email_address,password,user_type",$cond);
			//echo $this->db->last_query();die;   
			//die;
			//echo count($userData);
			if(count($userData) > 0)
			{
				
				$cond_active = array("email_address" => $postData["txt_username"],"password" => md5($postData["txt_password"]),"status"=>"active");
			
				$userDataActive = $this->login_model->validUser(TB_USERS,"user_id,super_admin,email_address,password,user_type",$cond_active);
				if(count($userDataActive)>0){
					$this->session->set_userdata("auth_user", $userData[0]);
				}
				else{
					$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">Your account is inactive. Please contact to administrator.</div>');
					redirect("adminlogin"); exit;
				}
				//echo "test";
				//print_r($this->session->userdata("auth_user"));die;
				redirect("users/getUsers");
				exit;
			}
			else
			{
				$this->session->set_flashdata("msg", '<div class="p5 mbn alert-dismissable text-danger pln">The Username/Email Address or Password do not match, Please try again.</div>');
				redirect("adminlogin"); exit;
			}
		}
		else
		{
			$this->load->view("administrator/login");
		}
	}
	
	public function logout()
	{
		//$this->session->sess_destroy();
		$this->session->unset_userdata("auth_user");
		redirect("adminlogin"); exit;
	}
	
	public function logoutuser()
	{ 
		$this->session->unset_userdata("auth_opeluser");
		redirect("index"); exit;
	}
		
	public function save_password()
	{   
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{
			$postData = $this->input->post();
			$userid = get_user_data("username");
			
			$cond = array('password' => md5($postData['currentPassword']),'user_id' => $userid);
			$currentUser = $this->login_model->validUser(TB_USERS,'password',$cond);
			
			if($postData['currentPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter current password.</div>')); exit;
			}
			else if($postData['newPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter new password.</div>')); exit;
			}
			else if($postData['confirmPassword'] == "")
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter confirm password.</div>')); exit;
			}
			else if($postData['confirmPassword'] != $postData['newPassword'])
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>New password and Confirm password must be same.</div>')); exit;
			}
			else if(strlen($postData['newPassword']) < 5)
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Password must be more than 5 characters.</div>')); exit;
			}
			else if($currentUser[0]['password'] != md5($postData['currentPassword']))
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Please enter correct current password.</div>')); exit;
			}
			else if($currentUser[0]['password'] == md5($postData['newPassword']))
			{
				echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>New password and old password can\'t be same.</div>')); exit;
			}
			else
			{
				$cond = array("email_address" => $userid);
				$updateData = array("password" => md5($postData['newPassword']));
				$result = $this->common_model->update(TB_USERS,$cond,$updateData);
				if($result)
				{
					echo json_encode(array('status' => 'success','msg1' => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Password has been updated successfully.</div>')); exit;
				}
				else
				{
					echo json_encode(array('status' => 'error','msg' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Error occured during updating Password.</div>')); exit;
				}
			}
		}
	}
	
}
?>
