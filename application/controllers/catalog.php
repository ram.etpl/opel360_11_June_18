<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("users_model");
		$this->load->model("catalog_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->load->library("image_lib");
		$this->car_images = realpath('car_gallery');
		$this->car_images_thumb = realpath('car_gallery/thumb');
		$this->car_images_actthumb = realpath('car_gallery/thumb/actual_thumb');
		
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'car_catalogue_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	
	public function getCatalogModel()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				$this->load->view("administrator/catalog",$data);
			}else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	public function getCatalogModelById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("model_id" => $this->encrypt->decode($postData["id"]));
				$catlog = $this->catalog_model->getCatalogModelById($cond);
				//print_r($users);die;
				echo json_encode($catlog[0]);exit;
			
		}
	}
	
	
	//UPLOAD CAR THUMBNAIL.
	
	public function validatecarthumb($files)
	{
	
		$count = count($files['name']);
		
		if (isset($files['car_thumb']['name']) && $files['car_thumb']['name'] != "") {
            $config = array(
                'upload_path' => $this->car_images_thumb,
                'allowed_types' => 'jpg|JPG|png|PNG|jpeg|JPEG',
                'max_width' => '6000',
                'max_height' => '5000'
            );
            
        // load Upload library
           $this->load->library('upload', $config);
           $check_upload = $this->upload->do_upload('car_thumb');
		   
			if($check_upload){
 
				$uploaded_manual = $this->upload->data('car_thumb'); 
				
				$path_to_image = $uploaded_manual['full_path'];
				$new_img_path  = $this->car_images_actthumb.'/'.$uploaded_manual['file_name'];
			
			    $config1['image_library'] = 'gd2';
				$config1['source_image'] = $path_to_image;
				$config1['new_image'] = $new_img_path;
				$config1['create_thumb'] = TRUE;
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 75;
				$config1['height'] = 50;
			    $this->image_lib->initialize($config1);
			    $this->image_lib->resize();
			   
				return array("status" => 1,"count" => $count,"data_flat" => $uploaded_manual); exit;
			 }
			 else {
				$error = $this->upload->display_errors();
				return array("status" => 0,"error" => $error); exit;
				 
			 }
		}	 
		else {
				return array("status" => 1,"is_file"=>0);exit;
		}
	}
	
	
	public function save_catalog_model() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();  
					//print_r($postData);die;
					//print_r($_FILES); die;
					if($postData["modelId"] == "")
					{   
						$isValid = $this->validateCatalogModel($postData);
						//print_r($isValid);die;
						if($isValid["status"] == 1)
						{ 
							$cond = array("model_name"=>$postData["txt_model_name"],"chasis_no"=>$postData["txt_chasis_no"]);
							$catalog = $this->catalog_model->getAllCatalogModel($cond);
							$data["catalog"] = $catalog;
							//$randpwd = trim($this->genarateRandomPassword(10)); 
							 
							if(count($catalog) == 0)
							{
								$isValidPic = $this->validatecarthumb($_FILES);
								
								if($isValidPic["status"] == 1)
								{
									//print_r($isValidPic);die;
									if($isValidPic['data_flat']['raw_name'] != ""){
										$file_name = $isValidPic['data_flat']['raw_name']."_thumb".$isValidPic['data_flat']['file_ext'];
									}
									$insertId = $this->common_model->insert(TB_CAR_CATALOG,array("model_name" => $postData["txt_m_name"], "m_name" => $postData["txt_m_name"],"weight" => $postData["txt_weight"],"propellant" => $postData["txt_propellant"],"no_valves_per_cylinder" => $postData["txt_no_of_valve_cylinder"],"engine_capacity" => $postData["txt_engine_capacity"],"number_of_cylinder"=> $postData["txt_no_of_cylinder"],"valve_train"=> $postData["txt_valve_train"],"cylinder_config"=> $postData["txt_cylinder_config"],"max_output"=> $postData["txt_max_output"],"cooling" => $postData["txt_cooling"],"acceleration" => $postData["txt_acceleration"],"top_speed"=>$postData["txt_top_speed"],"fuel_consumption"=>$postData["txt_fuel"],"co2_combined"=>$postData["txt_co2"],"car_thumb" => $file_name, "date_modified" => date('Y-m-d H:i:s')));
									if($insertId)
									{    
										echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Catelog model has been added successfully.</div>')); exit;
									}
									else
									{
										echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
									}
								}
								else {
										echo json_encode($isValidPic); die;
								}
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This model has been already available.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);
						} 
						 
					}
					else
					{ 
						$isValid = $this->validateCatalogModel($postData);
						if($isValid["status"] == 1)
						{
							$cond = array("model_id !=" => $this->encrypt->decode($postData["modelId"]), "model_name" => $postData["txt_model_name"],"chasis_no"=>$postData["txt_chasis_no"]);
							$cond1 = array("model_id" => $this->encrypt->decode($postData["modelId"]));
							$catalog = $this->catalog_model->getAllCatalogModel($cond);
							$userbyid = $this->catalog_model->getCatalogModelById($cond1); 
							$data["catalog"] = $catalog; 
						
							if(count($catalog) == 0)
							{  
								$isValidPic = $this->validatecarthumb($_FILES);
								
								if($isValidPic["status"] == 1)
								{
									if($isValidPic['data_flat']['raw_name'] != ""){
										$file_name = $isValidPic['data_flat']['raw_name']."_thumb".$isValidPic['data_flat']['file_ext'];
										$updateArr = $this->common_model->update(TB_CAR_CATALOG,$cond1,array("car_thumb" => $file_name));
									}
									$updateArr = $this->common_model->update(TB_CAR_CATALOG,$cond1,array("model_name" => $postData["txt_m_name"], "m_name" => $postData["txt_m_name"],"weight" => $postData["txt_weight"],"propellant" => $postData["txt_propellant"],"no_valves_per_cylinder" => $postData["txt_no_of_valve_cylinder"],"engine_capacity" => $postData["txt_engine_capacity"],"number_of_cylinder"=> $postData["txt_no_of_cylinder"],"valve_train"=> $postData["txt_valve_train"],"cylinder_config"=> $postData["txt_cylinder_config"],"max_output"=> $postData["txt_max_output"],"cooling" => $postData["txt_cooling"],"acceleration" => $postData["txt_acceleration"],"top_speed"=>$postData["txt_top_speed"],"fuel_consumption"=>$postData["txt_fuel"],"co2_combined"=>$postData["txt_co2"], "date_modified" => date('Y-m-d H:i:s')));
								
									$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["userId"].'"></td>
												<td class="text-center">'.$postData["txt_m_name"].'</td>
												<td class="text-center">'.$postData["txt_propellant"].'</td>
												<td class="text-center">
													<a title="Edit" alt="Edit" id="edit" class="" data-option="'.$postData["modelId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
													<a title="Add New Record" data-target="#myModal" data-toggle="modal"  data-option="'.$this->encrypt->encode($postData["id"]).'" class="btn btn-lg" id="add_car"><i data-toggle="modal" class="fa fa-plus"></i></a>
												</td>';
												//echo $this->encrypt->decode($postData["userId"]);die;
									echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["modelId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Model has been updated successfully.</div>')); exit;
								}
								else {
										echo json_encode($isValidPic);exit;
								}
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This model has been already taken.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);exit;
						}
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_catalog_model() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					//print_r($postData);die;
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->catalog_model->getCatalogModelCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("model_name","propellant");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "model_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$catalog = $this->catalog_model->getCatalogModelPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//echo $this->db->last_query();die;
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="30%" onclick="changePaginate(0,\'model_name\',\''.$corder[0].'\')">Model Name</th>
									<th class="text-center '.$css["1"].'" width="30%" onclick="changePaginate(0,\'propellant\',\''.$corder[1].'\')">Propellant</th>
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($catalog)>0)
					{
							if(count($catalog) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($catalog as $row) {  
								 
						$table .= '<tr id="row_'.$row["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="30%">'.$row["model_name"].'</td>
								<td class="text-center"  width="30%">'.$row["propellant"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($row["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
									<a title="Add New Image" data-target="#myModal" data-toggle="modal"  data-option="'.$this->encrypt->encode($row["id"]).'" class="btn btn-lg" id="add_car"><i data-toggle="modal" class="fa fa-plus"></i></a>
								</td>
							  </tr> 
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($catalog)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="4">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($catalog),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	
	
	
	public function validateCatalogModel($postData)
	{   
		
		return array("status" => 1);
	}
	
	
	public function delete_catalog_model() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("model_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_CAR_CATALOG,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	public function delete_catalog_car_thumb() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$cond = array("model_id" => $postData["id"]);
				$contest = $this->catalog_model->getCatalogModelGalleryById($cond); 
				//print_r($contest);die;
				unlink($this->car_images_thumb."/".$contest[0]['car_thumb']);
				$data_update = array("car_thumb"=>"");
				$isupdate = $this->common_model->update(TB_CAR_CATALOG,$cond,$data_update);
				
				if($isupdate){
					echo json_encode(array("status" => "success")); die;
				}
			}
		}
	}
	
	public function getCatalogModelGallery()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				$cond = array();
				$catalog = $this->catalog_model->getAllCatalogModel($cond);
				$data['car_models'] = $catalog;
				$this->load->view("administrator/catalog_gallery",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
		}
	}
	
	public function getCatalogModelGalleryById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			
			$postData = $this->input->post(); 
			//print_r($postData);die;
			$cond = array("img_id" => $this->encrypt->decode($postData["id"]));
			$catlog = $this->catalog_model->getCatalogModelGalleryById($cond);
			//print_r($users);die;
			echo json_encode($catlog[0]);exit;
			
		}
	}
	
	
	public function save_catalog_model_gallery()  
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 	
				//print_r($postData);die;
				if($postData["imgId"] == "")
				{ 
					
					$isValid = $this->validateModelGallery($postData,$_FILES);
					if($isValid["status"] == 1)
					{ 
						
						//print_r($isValid);die;
						$cond = array();
						$gallery = $this->catalog_model->getCatalogModelGalleryById($cond);
						$data["gallery"] = $gallery;
						
						if(isset($_FILES['car_image']['name']) && $_FILES['car_image']['name'] != "" && $_FILES['car_image']['size'] != 0){ 
						 
							$img_file = $isValid["0"]["img_file"]; 
							$img_thumb = $isValid["0"]["img_thumb"]; 
							$act_img_name = $isValid["0"]["act_img_file"]; 
							
							$insertId = $this->common_model->insert(TB_CAR_GALLERY,array("model_id" => $postData['model_id'],"propellant" => $postData['propellant'],"img_name" =>$act_img_name, "img_file" => $img_file, "img_thumb" => $img_thumb,"date_modified"=>date('Y-m-d H:i:s')));
						}
						if($insertId != ""){
							echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>The car model image has been added successfully.</div>')); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
						} 
					} 
					else
					{
						echo json_encode($isValid);exit;
					} 
					 
				}
				else
				{
					$isValid = $this->validateModelGallery($postData,$_FILES);
					if($isValid["status"] == 1){
						$cond = array("img_id" => $this->encrypt->decode($postData["imgId"]));
						$gallery = $this->catalog_model->getCatalogModelGalleryById($cond);
						//echo $this->db->last_query();die;
						$data["gallery"] = $gallery;
						
							if(count($gallery) == 1)
							{
								$cond_gallery = array("img_id" => $this->encrypt->decode($postData["imgId"]));
								$img_id = $this->catalog_model->getCatalogModelGalleryById($cond_gallery);		
								
								$updateArr = $this->common_model->update(TB_CAR_GALLERY,array("img_id" => $this->encrypt->decode($postData["imgId"])),array("model_id" => $postData["model_id"],"propellant" => $postData["propellant"]));
							}
						
							if(isset($_FILES['car_image']['name']) && $_FILES['car_image']['name'] != "" && $_FILES['car_image']['size'] != 0){ 
						 
									$img_file = $isValid["0"]["img_file"]; 
									$img_thumb = $isValid["0"]["img_thumb"]; 
									$act_img_name = $isValid["0"]["act_img_file"]; 
									$img_update = array("img_name" => $act_img_name,"img_file" => $img_file,"img_thumb" => $img_thumb);
									$updateArr = $this->common_model->update(TB_CAR_GALLERY,array("img_id" => $this->encrypt->decode($postData["imgId"])),$img_update);
							}
							
							//print_r($postData);
							if($updateArr)
							{ 
								$cond = array("model_id" => $postData["model_id"]);
								$model = $this->catalog_model->getCatalogModelById($cond);
								//echo $this->db->last_query();die;
								$cond_gal = array("img_id" => $this->encrypt->decode($postData["imgId"]));
								$gallery_data = $this->catalog_model->getCatalogModelGalleryById($cond_gal);
								
								$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["imgId"].'"></td>
										<td class="text-center">'.$model["0"]["model_name"]."-".$model["0"]["propellant"].'</td>
										<td class="text-center"><a target="_blank" href="'.base_url().'car_gallery/'.$gallery_data[0]["img_file"].'"><img src="'.base_url().'car_gallery/thumb/'.$gallery_data[0]["img_thumb"].'"/></a></td>
										<td class="text-center">
											<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["imgId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
										
										</td>';
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["imgId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>The car model image has been updated successfully.</div>')); exit;
							}
							else
							{
								//echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You didn\'t make any change.</div>')); exit;
								echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["imgId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>The car model image has been updated successfully.</div>')); exit;
							}
					}
					else
					{
						echo json_encode($isValid);exit;
					}
					
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_catalog_model_gallery() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					//print_r($postData);die;
					$cond = array();
					$like = array();
					$join = array(TB_CAR_CATALOG => TB_CAR_GALLERY.".model_id = ".TB_CAR_CATALOG.".model_id");
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					
					$count = $this->catalog_model->getCatalogModelGalleryCount($cond,$like,$join);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("model_name");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "img_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "DESC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$catalog = $this->catalog_model->getCatalogModelGalleryPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="30%" onclick="changePaginate(0,\'model_name\',\''.$corder[0].'\')">Model Name</th>
									<th class="text-center '.$css["1"].'" width="30%" onclick="changePaginate(0,\'img_name\',\''.$corder[1].'\')">Car Images</th>
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($catalog)>0)
					{
							if(count($catalog) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($catalog as $row) {  
								 
						$table .= '<tr id="row_'.$row["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="30%">'.$row["model_name"]."-".$row["propellant"].'</td> 
								<td class="text-center"  width="30%"><a target="_blank" href="'.base_url().'car_gallery/'.$row["img_file"].'"><img src="'.base_url().'car_gallery/thumb/'.$row["img_thumb"].'"/></a></td> 
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($row["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr> 
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($catalog)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="4">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($catalog),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	
	public function validateModelGallery($postData,$FILES)
	{
		if($_FILES!=""){ 
			$bool = false;
			$actual_img_file  = array(); 
			$merge_update = array();
			$cond1 = array("img_id" => $this->encrypt->decode($postData["imgId"])); 
			$imgbyid = $this->catalog_model->getCatalogModelGalleryById($cond1);
			if(isset($_FILES['car_image']['name']) && $_FILES['car_image']['name'] != ""){
				$ext = pathinfo($_FILES['car_image']['name'], PATHINFO_EXTENSION); 
				$img = 'img_'.date('Y-m-d-H-i-s') . '_' . uniqid().'.'.$ext;
				$img_name = $_FILES['car_image']['name']; 
				
				//echo $fname;die;
				$actual_img_file = $_FILES['car_image']['name'];
				
				$config = array( 
					'allowed_types' => 'png|PNG|jpg|JPG',
					'upload_path'   => $this->car_images,
					'file_name'		=> $img,
					'max_size'      => 50000,
					'min_width'		=> 100
				); 
				
				$this->validatefile->initialize($config);
				$chk_valid_file = $this->validatefile->validate_file('car_image');
				$actual_pdf_file = array("img_name" => $actual_img_file,"img_file" => $img);
				//print_r($actual_pdf_file);
				if(!$chk_valid_file){
					return array("status" => 2, "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$img_name.':'.$this->validatefile->display_errors().'</div>');
				}
				else{
					//return array("status" => 1, "user_file" => $fname);
					
					$bool = true;
					@unlink($this->car_images.'/'.$imgbyid["0"]["img_file"]);
					$this->validatefile->do_upload('car_image');
					$upload_data  = $this->validatefile->data();
					
					$uniqid = date('Y-m-d-H-i-s') . '_' . uniqid();
					
					$img_thumb = $uniqid.'.'.$ext;
					//echo $this->car_images_thumb."/".$img_thumb;die;
					if($upload_data){
						$config_thumb = array(
							'image_library' => 'gd2',
							'source_image' => $this->car_images."/".$img,
							'new_image' => $this->car_images_thumb."/".$img_thumb, 
							'create_thumb' => TRUE,
							'maintain_ratio' => TRUE,
							'width' => 80,
							'height' => 40
						);
						$this->image_lib->initialize($config_thumb);
						$this->load->library('image_lib', $config_thumb); 
						$this->image_lib->resize();
						$upload_data_thumb  = $this->validatefile->data();
						
					}
					//print_r($upload_data_thumb);die;
					$img_thumb_name = $uniqid.'_thumb.'.$ext;
					
					$actual_img_file = array("act_img_file" => $img_name,"img_file" => $img,"img_thumb" => $img_thumb_name);
				}
			}
			
			if($bool){ 
				return array("status" => 1,$actual_img_file);
			}
		}
		 
		return array("status" => 1);
	}
	
	public function delete_catalog_model_gallery() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond1 = array("img_id" => $this->encrypt->decode($postData["ids"][$i])); 
					$imgbyid = $this->catalog_model->getCatalogModelGalleryById($cond1);
					
					@unlink($this->car_images.'/'.$imgbyid["0"]["img_file"]);
					@unlink($this->car_images_thumb.'/'.$imgbyid["0"]["img_thumb"]);
					$cond = array("img_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_CAR_GALLERY,$cond);
					if($isdelete)
					{
						
						
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	function deletecarcatelogimages() {
			
			$postData = $this->input->post();
			$model_image_id = $postData['model_image_id'];
			$model_id = $this->encrypt->decode($postData['model_id']);
			$isdelete = 0;
			$cond = array("img_id" => $model_image_id); 
			$isdelete = $this->common_model->delete(TB_CAR_GALLERY,$cond);
			if($isdelete)
					{
						$cond1 = array("model_id" => $model_id); 
						$carcatlogimages = $this->catalog_model->getCarCatalogModelGalleryById($cond1);
						$images_count = count($carcatlogimages);
						for($i=0;$i<$images_count;$i++) {
			
							$car_img = base_url().'car_gallery/'.$carcatlogimages[$i]['img_name'];
							$img_id = $carcatlogimages[$i]['id'];
			
				$images .= <<<HTML
								<li>
								<a href="#image-$i">
									
									<img alt="image01" src="$car_img">
									<span data-option="$img_id" class="close_gallery" id="close_gallery"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span>		
								</a>
								<div id="image-1" class="lb-overlay">
									
									<img alt="image0$i" src="$car_img">
									<div>
										<h3> <span></span></h3>
										<p></p>
									</div>
									<a class="lb-close" href="#page">x Close</a>
								</div>
							</li>
HTML;
		}
					}
			
			
			echo json_encode(array("status" => 200,"data" => $images)); die;
	}
	
	
	
	function getcarcatelogimages() {
		$postData = $this->input->post();
		
		$model_id = $this->encrypt->decode($postData['model_id']);
		
		$cond1 = array("model_id" => $model_id); 
		$carcatlogimages = $this->catalog_model->getCarCatalogModelGalleryById($cond1);
		
		$images_count = count($carcatlogimages);
		for($i=0;$i<$images_count;$i++) {
			
			$car_img = base_url().'car_gallery/'.$carcatlogimages[$i]['img_name'];
			$img_id = $carcatlogimages[$i]['id'];
			
				$images .= <<<HTML
								<li>
								<a href="#image-$i">
									
									<img alt="image01" src="$car_img">
									<span data-option="$img_id" class="close_gallery" id="close_gallery"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span>		
								</a>
								<div id="image-1" class="lb-overlay">
									
									<img alt="image0$i" src="$car_img">
									<div>
										<h3> <span></span></h3>
										<p></p>
									</div>
									<a class="lb-close" href="#page">x Close</a>
								</div>
							</li>
HTML;
		}
		
		echo json_encode(array("status" => 200,"data" => $images)); die;
		
		
	}
	
}
?>
