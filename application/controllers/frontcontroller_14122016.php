<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class FrontController extends CI_Controller{
       
    function __construct() {
		parent::__construct(); 
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("users_model");
		$this->load->model("catalog_model");
		$this->load->model("accessories_model");
		$this->load->model("services_model");
		$this->load->model("pdf_model");
		$this->load->model("events_model");		
		$this->load->model("alpine_model");
		$this->load->model("survey_model");
		$this->load->model("banner_model");
		$this->load->model("gallery_model");	
			
		$this->carimagepath = realpath('car_gallery');
		$this->carthumbimagepath = realpath('car_gallery/car_thumb');
		$this->communityimagepath = realpath('community_gallery');
		$this->communityimagepath_thumb = realpath('community_gallery/thumb');
		$this->load->library('form_validation');
		$this->load->library("email");
		$this->load->library("image_lib");
		$this->load->helper("email_template"); 
		//$this->load->helper("xml_to_object"); 
		$this->load->library('XmlElement');
		$this->load->helper('download'); 
		date_default_timezone_set("Asia/Singapore"); 
		$this->no_cache();
		
		
		
		$_REQUEST = json_decode(file_get_contents('php://input'), true);
		$userdata_cons = $this->session->userdata("auth_opeluser");
		//print_r($userdata_cons);die;
		date_default_timezone_set("Asia/Singapore"); 
		
		if($userdata_cons != ""){
			$this->checkUserStatusOpel($userdata_cons['id']);
		}
	}
	

	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	 
	private function checkUserStatusOpel($uid){ 
			//echo "die";
			
		    $userdata = $this->session->userdata("auth_opeluser");
			$cond = array('status' => 'Active','user_id' => $uid,"email_address"=>$userdata['email_address']);									
			$res = $this->login_model->checkUserStatus(TB_USERS,"email_address,password",$cond);  
			//echo $this->db->last_query();die;
			if ( $res == 0 ) { 
				$this->session->unset_userdata("auth_opeluser");
				redirect("index"); exit;
			} 
	}
	  
	function index(){
		//Get Banners
		$cond = array();
		$banners = $this->banner_model->getBannersPerPage($cond);
		$base_url = base_url();
		if(count($banners)>0) {
			foreach($banners as $key => $banner) {
				$active = '';
				if($key=='0') {
					$active = 'active';
				}
				$banner_img = $banner['banner_file_name'];
				$img_path = $base_url."images/Banners/$banner_img";
				$data['banners'] .= <<<HTML
				<div class="item $active">
					<img src="$img_path" alt="$banner_img">
				</div>
HTML;
		
			}
		}
		
		
		 $data['title']  = 'Welcome To OPEL360';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/index';
		 
		 
		 $feed_url = 'http://media.opel.com/content/media/intl/en/opel/news/jcr:content/righttabsocialmedia/rss.newsrss.html';
		 $content = file_get_contents($feed_url);
		 //print_r($content);die;
		 $rss_feeds = new SimpleXmlElement($content);
		 
		 foreach($rss_feeds->channel->item as $feed):
		 
			$data['news'] .= '<li>
								<a target="_blank" href="'.$feed->link.'">'.$feed->title.'</a>
								<div>'.substr($feed->description,0,300).'...'.'
								<span class="read_news"><a target="_blank" href="'.$feed->link.'">Read More</a></span></div>
							</li>';
		 endforeach;
		 
		// print_r($x);die;
		 $cond_upcoming = array("status" => "Upcoming");
		 $upcoming_events = $this->events_model->getAllUpcomingEvents($cond_upcoming);
		 $data['upcoming_events'] = '';
		 if(count($upcoming_events)>0){
			 foreach($upcoming_events as $upcoming_event) {
				 $event_name = $upcoming_event['event_name'];
				 $event_img = base_url().'images/Events/'.$upcoming_event['event_pic'];
				
				 $data['upcoming_events'] .= '<div class="upcoming_inner_wrapper">
										<div class="col-md-3 lt_event">
											<img src="'.$event_img.'" />
										</div>
										<div class="col-md-9 rt_event">
											<div class="ru_title">'.$event_name.'</div>
											<div class="start_date">Start Date :&nbsp;'.$upcoming_event['start_date'].'</div>
											<div class="start_date end_da">End Date :&nbsp;'.$upcoming_event['end_date'].'</div>
											<div class="ru_text">'.$upcoming_event['event_description'].'</div>
										</div>
									</div>';
			}
		}
		else{
			$data['upcoming_events'] ='No Records Available';
		}
		$cond_featured = array();
		$join = array(TB_USERS => TB_COMMUNITY.".user_id=".TB_USERS.".user_id");
		$featured_cars = $this->gallery_model->getNewCarModelGalleryById($cond_featured,$join);
		//print_r($featured_cars);
		//die;
		$data['featured_cars'] = $featured_cars;
		$this->load->view('frontend/incls/layout', $data);
	}
	
	public function change_password()
	{   
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else
		{ 
			$postData = $this->input->post(); 
			$userdata = $this->session->userdata("auth_opeluser");
			$this->checkUserStatusOpel($userdata['id']);
			$cond_check = array('password' => md5($postData['old_pwd']),'user_id' => $userdata['id']);
			$currentUser = $this->login_model->validOpelUser(TB_USERS,'password',$cond_check);
			if(md5($postData['old_pwd']) == md5($postData['new_pwd'])){
				echo json_encode(array('status' => 'error','message' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Old password and new password should not be same.</div>')); exit;
			}
			if(count($currentUser)==0){
				echo json_encode(array('status' => 'error','message' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>You have entered incorrect old passowrd.</div>')); exit;
			}
			
			$cond = array("user_id" => $userdata['id']);
			$updateData = array("password" => md5($postData['new_pwd']));
			$result = $this->common_model->update(TB_USERS,$cond,$updateData);
			if($result)
			{
				echo json_encode(array('status' => 'success','message' => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Password has been updated successfully.</div>')); exit;
			}
			else
			{
				echo json_encode(array('status' => 'error','message' => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Error occured during updating Password.</div>')); exit;
			} 
		}
	}
	
	/*
	 * My Rewards
	 * 
	 * */
	 
	public function my_rewards()
	{   
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else
		{ 
			$data['title']  = 'Welcome To OPEL360';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/my_rewards';
			$userdata = $this->session->userdata("auth_opeluser");
			$this->checkUserStatusOpel($userdata['id']);
			$cond = array("user_id"=>$userdata['id']);
			$join = array(
			
			);
			$data['rewards'] = $this->survey_model->getUserRewardSurveyDetails($cond,$join);
			$this->load->view('frontend/incls/layout', $data);
		}
	}
	
	
	function my_rewards_details(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else
		{ 
			if(is_ajax_request())
			{
				
				$postData = $this->input->post();
				$userdata = $this->session->userdata("auth_opeluser");
				$this->checkUserStatusOpel($userdata['id']);
				$cond = array("user_id"=>$userdata["id"]);
				$rewards = $this->survey_model->getUsersUsedReward($cond);
				$html ='';
				if(count($rewards)>0){
					$i=0;
					foreach($rewards as $rwd):
					$html .='<tr> 
								<td width="30%" class="text-center">S$'.$rwd['rewards_used'].'</td> 
								<td width="70%" class="text-center">'.$rwd['used_for'].'</td> 
								
							</tr>';
					$i++;
					endforeach;
				}
				else{
					$html .='<tr ><td colspan="2" class="text-center">No Records Available</td></tr>';
				}
				
				echo json_encode(array("html"=>$html));exit;
			}
		}
	}
	
	
	function models($model_id=""){
		 $data['title']  = 'Models';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/models';
		 $cond_model = array();
		 $data['selected_car'] = "";
		 
		 
		 
		 if($model_id != ""){
			 
			 $cond = array("model_id"=>$model_id);
			 $car_catalog = $this->catalog_model->getCatalogModelById($cond);
			 
			 if(count($car_catalog)>0){
				 
				 $data['selected_car'] = $car_catalog[0]['model_name'];
			 }
			 else{
				 	$cond_model = array();
					$car_catalog = $this->catalog_model->getCatalogModelFirstById($cond_model);

				 $data['no_exists'] = "The car model does not exists in our database";
			 }
		 }
		 else{
			 $cond = array();
			 $car_catalog = $this->catalog_model->getCatalogModelFirstById($cond);
		 }
		 //print_r($car_catalog);die;
		$data['car_catalog'] = $car_catalog[0];
		 
		 if(!empty($car_catalog)){
			
			$cond_image  = array("model_id" => $car_catalog[0]['id']);
			$car_catalog_gallry = $this->catalog_model->getCatalogModelGalleryImageById($cond_image);
			//$data['car_gallery'] = json_encode(array("text"=>$car_catalog_gallry));
			$cond_gal = array();
			$data['car_gallery'] = $this->catalog_model->getCatalogModelById($cond_gal);
			
			//print_r($data_gallery);die;
			
		 }
		 $cond_img = array("model_id"=>$model_id);
		 $data['cgallery'] = $this->catalog_model->getCatalogModelGalleryById($cond_img);
		 
		// print_r($data['cgallery']);die;
		 
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function contact_us(){
		 $data['title']  = 'Contact Us';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/contact_us';
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	
	function terms_api(){
		 $data['title']  = 'Contact Us';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/terms_api';
		 $this->load->view('frontend/terms_api', $data);
	}
	
	function terms_gallery_api(){
		 $data['title']  = 'Contact Us';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/terms_gallery_api';
		 $this->load->view('frontend/terms_gallery_api', $data);
	}
	
	function add_survey() {
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else
		{ 
			$postData = $this->input->post();
			//print_r($postData); die;
			$qid  = $postData['qid'];
			
			$qids = explode(",",$qid);
			if(!empty($qids)){
				$userdata = $this->session->userdata("auth_opeluser");
				$exists = 0;
				$ratings ="";
				for($i=0;$i<count($qids);$i++) {
						
					$ques_id = $qids[$i];
					$question_id = $postData['qid'.$ques_id];
					$rating = $postData['ratingQ'.$ques_id];
					//echo $rating;
					$cond = array("question_id"=>$question_id,"user_id"=>$userdata['id']);
					
					$res = $this->survey_model->getSurveyRatingById($cond);
					
					if(count($res)>0){
						/*$ratings[] = array(
							'qid' => $question_id,
							'user_id' => $user_id,
							'rating' => 'already done rating'
						);*/
					}
					else{
						
						/*get survey points from question id*/
						
						$cond_q = array("question_id"=>$question_id);
						$res_quest = $this->survey_model->getallSurveyQuestion($cond_q);
						
						$insertId = $this->common_model->insert(TB_SURVEY_USER_RATING,array("question_id" => $question_id,"user_id" => $userdata['id'],"rating" => $rating,"comments"=>$postData['comments']));
						
						if($insertId){
							if($res_quest){
								foreach($res_quest as $srv):
									$cond_s = array("sid" => $srv['fk_survey_id'],"user_id"=>$userdata['id']);
									$res_ganed = $this->survey_model->getSurveyRewardsDetails($cond_s); 
									
									$cond_s = array("survey_id" => $srv['fk_survey_id']);
									$res_srv = $this->survey_model->getSurveyById($cond_s); 
									
									//print_r(count($res_ganed));
									if(count($res_ganed)==0){
										$this->common_model->insert(TB_USER_GAINED_REWARDS,array("sid" => $res_srv[0]['id'],"user_id" => $userdata['id'],"gained_rewards"=>$res_srv[0]['award_points'],"date_modified"=>date('Y-m-d H:i:s')));	
									}
								endforeach;
							}
						}
						
						 $ratings[] = array(
							'qid' => $question_id,
							'user_id' => $userdata['id'],
							'rating' => $rating
						);
					}
				}
				/*if($exists==1){
					echo json_encode(array("status" => 1, "msg" => "<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>You have already give rating for this survey.</div>")); exit;
				}
				else{*/
					echo json_encode(array("status" => 1,"qids"=>count($qids),"status"=>$ratings, "msg" => "<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Input has been submitted successfully.</div>")); exit;
				//}
			}
			else{
				echo json_encode(array("status" => 1, "msg" => "<div class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'>×</button>Please give a rating to survey</div>")); exit;
			}
		} 
	}
 
	function upload_files() {
		
		// A list of permitted file extensions
			$allowed = array('png', 'jpg','jpeg');

			if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

				$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

				if(!in_array(strtolower($extension), $allowed)){
					//echo '{"status":"error","msg":"Invalid File"}';
					echo json_encode(array("status"=>"error","msg"=>"Invalid File"));
					exit;
				}
				
				if(move_uploaded_file($_FILES['upl']['tmp_name'], $this->carimagepath.'/'.$_FILES['upl']['name'])){
					
					$img_name = str_replace(' ','-',$_FILES['upl']['name']);
					$ext = pathinfo($img_name, PATHINFO_EXTENSION);
					$uniqid = uniqid();
					$model_id = $_POST['car_model_id'];
					$img_thumb = $uniqid.".".$ext;
					//Get modelinfo
					$cond = array("model_id" => $this->encrypt->decode($model_id));
					$car_catalog_model = $this->catalog_model->getCatalogModelById($cond);
					$propellant = $car_catalog_model[0]['propellant'];
					$path_to_image = $this->carimagepath."/".$img_name;
					$new_img_path  = $this->carthumbimagepath.'/'.$img_thumb;			
					
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $path_to_image;
					$config1['new_image'] = $new_img_path;
					$config1['create_thumb'] = TRUE;
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 150;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
					
					//$upload_data  = $this->validatefile->data();
					
					$img_thumb_name = $uniqid."_thumb.".$ext;
					
					$insertId = $this->common_model->insert(TB_CAR_GALLERY,array("model_id" => $this->encrypt->decode($model_id),"propellant" => $propellant,"img_name" =>$img_name,"img_file" => $img_name,"img_thumb" => $img_thumb_name,"date_modified"=>date('Y-m-d H:i:s')));
						
					echo '{"status":"success"}';
					exit;
				}
			}
		
	}
	
		
	function survey(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else
		{ 
		 $data['title']  = 'Survey';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/survey';
		 $date = date('Y-m-d');
		 $date1 = date('Ymd');
		 $userdata = $this->session->userdata("auth_opeluser");
		 
		 //print_r($userdata);die;
		 
		 $user_id = $userdata['id'];
		 //get last serviced date
		 $cond1 = array("user_id" => $user_id);
		 $serviced_users = $this->survey_model->getAllCurrentVehicleServicedUserslastdate($cond1);
		
		 $last_serviced_date = $serviced_users[0]['transaction_date'];
		 $last_serviced_date_user = date('Y-m-d', strtotime($last_serviced_date)); 
		
		 $data['survey_questions_data'] ="";
		 $data['access_denied']  = "";
		 if($userdata['user_type'] == 'OPEL' || $userdata['user_type'] == 'NONOPEL' || $userdata['user_type'] == 'ADMIN'){
		 
		 //Get current survey.
		 $cond = array("survey_from_date >=" => $last_serviced_date_user,"survey_to_date >=" => $last_serviced_date_user);
		 $like = array();
		 
		 //$survey_count = $this->survey_model->getSurveyCount($cond,$like);
		 //echo $this->db->last_query(); die;
		 /*
		  * check for survey type
		  * */
		  
		if($userdata['user_type'] == 'OPEL' || $userdata['user_type'] == 'NONOPEL' || $userdata['user_type'] == 'ADMIN'){  
			
			$cond_sur = array("survey_type" => 'general survey');  
			$all_survey = $this->survey_model->getGeneralSurveyInBetween();
			//echo $this->db->last_query();die;
			
			$data['survey_questions_data']="";
			$k=0;
			if($all_survey){
						$p=0;
						foreach($all_survey as $ser):
							$ser_id = $ser['survey_id'];  
						
							$cond_servey = array("fk_survey_id" => $ser['survey_id']);
						
						//echo $this->db->last_query();//die;
						//echo "<pre>";print_r($All_survey_questions);
						$cond_ssrv = array("survey_id" => $ser['survey_id']);
							$sdetails = $this->survey_model->getSurveyById($cond_ssrv);   
							
								$sname = $sdetails[0]['survey_name']; 
								$data['survey_questions_data'] .= "<input type='hidden' name='sid' value='$ser_id'>
										<div class='how_head_survey'>".$sname."</div>"; 
						
						if(count($all_survey)>0) {
						
						$All_survey_questions = $this->survey_model->getallSurveyQuestionBySurveyId($cond_servey,$user_id);
							//echo "<pre>";print_r($All_survey_questions);
							$j=$p;	
							foreach($All_survey_questions as $question) {
							
							$questions = $question['question'];
								$questions_id = $question['id'];
								$sid = $question['fk_survey_id'];   

								$data['survey_questions_data'] .= <<<HTML
									<div class="sur_inner">
										
										<div class="lt_survey">
											<div class="how_head">$questions</div>
										</div>
										<div class="rt_survey">
											<div class="rate-review" id="rate-review">
												<div class="rate" id="rate$questions_id">
													<input type="hidden" name="user_id" id="user_id" value="$user_id">
													<input type="radio" id="rating0$j" name="rating$j" value="10" /><label class="ratingstar" data-qid="$questions_id" data-val="5"  for="rating0$j" title="5"></label>
													<input type="hidden" id="qid$questions_id" name="qid$questions_id" value="" />
													<input type="hidden" id="ratingQ$questions_id" name="ratingQ$questions_id" value="" />
													<input type="radio" id="rating1$j" name="rating$j" value="9" /><label class="ratingstar half" for="rating1$j" data-qid="$questions_id" data-val="4.5" title="4.5"></label>
													<input type="radio" id="rating2$j" name="rating$j" value="8" /><label class="ratingstar" for="rating2$j" data-qid="$questions_id" data-val="4" title="4"></label>
													<input type="radio" id="rating3$j" name="rating$j" value="7" /><label class="ratingstar half" for="rating3$j" data-qid="$questions_id" data-val="3.5" title="3.5"></label>
													<input type="radio" id="rating4$j" name="rating$j" value="6" /><label class="ratingstar" for="rating4$j" data-qid="$questions_id" data-val="3" title="3"></label>
													<input type="radio" id="rating5$j" name="rating$j" value="5" /><label class="ratingstar half" for="rating5$j" data-qid="$questions_id" data-val="2.5" title="2.5"></label>
													<input type="radio" id="rating6$j" name="rating$j" value="4" /><label class="ratingstar" for="rating6$j" data-qid="$questions_id" data-val="2" title="2"></label>
													<input type="radio" id="rating7$j" name="rating$j" value="3" /><label class="ratingstar half" for="rating7$j" data-qid="$questions_id" data-val="1.5" title="1.5"></label>
													<input type="radio" id="rating8$j" name="rating$j" value="2" /><label class="ratingstar" for="rating8$j" data-qid="$questions_id" data-val="1" title="1"></label>
													<input type="radio" id="rating9$j" name="rating$j" value="1" /><label class="ratingstar half " for="rating9$j" data-qid="$questions_id" data-val="0.5" title="0.5"></label>
													<input type="radio" id="rating10$j" name="rating$j" value="0" /><label style="display:none;" for="rating10$j" title="No star" ></label>
												</div>
											</div>
										</div>
									</div>
HTML;
								$k++;
								$j++;
								$p = $j;
			
							} 
						}
						endforeach; 
			}
			
			//print_r($data);die;
			//	die;
		} 
		
		if($userdata['user_type'] == 'OPEL'){  	
			$survey_count = $this->survey_model->getSurveyCountInBetween($last_serviced_date_user);
		
		// echo $this->db->last_query(); //die;
		
		 //echo $this->db->last_query();die;
		//print_r($survey_count); die;
			 if($survey_count[0]['cnt']>0) {
				  
				//Get survey details. 
				$survey_details1 = $this->survey_model->getSurveyInBetween($last_serviced_date_user);
				//echo $this->db->last_query();
				//echo "<pre>";print_r($survey_details1); //die; 
					$s=0;
					foreach($survey_details1 as $ser):
							$ser_id = $ser['survey_id'];  
						
							$cond_servey = array("fk_survey_id" => $ser['survey_id']);
						
						//echo $this->db->last_query();//die;
						//echo "<pre>";print_r($All_survey_questions);
						$cond_ssrv = array("survey_id" => $ser['survey_id']);
							$sdetails = $this->survey_model->getSurveyById($cond_ssrv);   
							
								$sname = $sdetails[0]['survey_name']; 
								$data['survey_questions_data'] .= "<input type='hidden' name='sid' value='$ser_id'>
										<div class='how_head_survey'>".$sname."</div>
							"; 
						
						if(count($all_survey)>0) {
							$k=0;
							$j = $p;
							$All_survey_questions = $this->survey_model->getallSurveyQuestionBySurveyId($cond_servey,$user_id);
							foreach($All_survey_questions as $question) {

								$questions = $question['question'];
								$questions_id = $question['id'];


								$data['survey_questions_data'] .= <<<HTML
									<div class="sur_inner">
										<div class="lt_survey">
											<div class="how_head">$questions</div>
										</div>
										<div class="rt_survey">
											<div class="rate-review" id="rate-review">
												<div class="rate">
													<input type="hidden" name="user_id" id="user_id" value="$user_id">
													<input type="radio" id="rating0$j" name="rating$j" value="10" /><label class="ratingstar" data-qid="$questions_id" data-val="5"  for="rating0$j" title="5"></label>
													<input type="hidden" id="qid$questions_id" name="qid$questions_id" value="" />
													<input type="hidden" id="ratingQ$questions_id" name="ratingQ$questions_id" value="" />
													<input type="radio" id="rating1$j" name="rating$j" value="9" /><label class="ratingstar half" for="rating1$j" data-qid="$questions_id" data-val="4.5" title="4.5"></label>
													<input type="radio" id="rating2$j" name="rating$j" value="8" /><label class="ratingstar" for="rating2$j" data-qid="$questions_id" data-val="4" title="4"></label>
													<input type="radio" id="rating3$j" name="rating$j" value="7" /><label class="ratingstar half" for="rating3$j" data-qid="$questions_id" data-val="3.5" title="3.5"></label>
													<input type="radio" id="rating4$j" name="rating$j" value="6" /><label class="ratingstar" for="rating4$j" data-qid="$questions_id" data-val="3" title="3"></label>
													<input type="radio" id="rating5$j" name="rating$j" value="5" /><label class="ratingstar half" for="rating5$j" data-qid="$questions_id" data-val="2.5" title="2.5"></label>
													<input type="radio" id="rating6$j" name="rating$j" value="4" /><label class="ratingstar" for="rating6$j" data-qid="$questions_id" data-val="2" title="2"></label>
													<input type="radio" id="rating7$j" name="rating$j" value="3" /><label class="ratingstar half" for="rating7$j" data-qid="$questions_id" data-val="1.5" title="1.5"></label>
													<input type="radio" id="rating8$j" name="rating$j" value="2" /><label class="ratingstar" for="rating8$j" data-qid="$questions_id" data-val="1" title="1"></label>
													<input type="radio" id="rating9$j" name="rating$j" value="1" /><label class="ratingstar half " for="rating9$j" data-qid="$questions_id" data-val="0.5" title="0.5"></label>
													<input type="radio" id="rating10$j" name="rating$j" value="0" /><label style="display:none;" for="rating10$j" title="No star" ></label>
												</div>
											</div>
										</div>
									</div>
HTML;
								$k++;
								$j++;
								$s = $j;
								}
					
						}
					endforeach;
			 } 
	 	 //die;
	  }
	  //die;
	 }
	 else{
		 $data['access_denied']  ="Access Denied";
	 }
	// die;
		//print_r($data);die;
		 $this->load->view('frontend/incls/layout', $data);
	 }
	}
	
	function about_us(){
		 $data['title']  = 'About Us';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/about_us';
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function service_exclusive(){
		 $data['title']  = 'Service Exclusive';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/service_exclusive';
		 $cond = array();
		 $data['models_data'] = $this->services_model->getAllModelsOfServicesExclusive($cond);
		 //$data['service_data']=$this->services_model->getAllServicesExclusiveCostByModel($cond);
		 		 
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	
	function accessories(){
		 $data['title']  = 'Accessories';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/accessories';
		 $cond = array();
		 $data['models']  = $this->catalog_model->getAllDistinctCatalogModel($cond);
		 //echo $this->db->last_query();die;
		 $data['acctype']  = $this->accessories_model->getAccessoriesTypeById($cond);
		// print_r($data);die;
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	
	function getAccesseriesByModel(){
		 $postData = $_POST;
		   
		 if(!empty($car_catalog)){
			
			$cond_image  = array("model_id" => $postData['id']);
			$car_catalog_gallry = $this->catalog_model->getCatalogModelGalleryImageById($cond_image);
			$data['car_gallery'] = $car_catalog_gallry[0];
			
		 }
	}
	
	
	function car_onwer_events(){
		 $data['title']  = 'Car Onwer Events';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/carowner_events';
		 $userdata = $this->session->userdata("auth_opeluser");
		 $usertype = $userdata['user_type'];
		 
		 if($usertype=='OPEL') {
		 $cond_upcoming = array("status" => "Upcoming");
		 $upcoming_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_upcoming);
		 // echo $this->db->last_query();die;
		 $data['upcoming_events'] = '';
		 
		 foreach($upcoming_events as $upcoming_event) {
			 $event_name = $upcoming_event['event_name'];
			 if($upcoming_event['event_pic']!=""){
				$event_img = base_url().'images/Events/'.$upcoming_event['event_pic'];
			 }
			 else{
				$event_img = base_url().'images/event.jpg';	
			 }
			
			 $data['upcoming_events'] .= '<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="'.$event_img.'" alt="No image available" />
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">'.$event_name.'</div>
										<div class="start_date">Start Date :&nbsp;'.$upcoming_event['start_date'].'</div>
										<div class="start_date end_da">End Date :&nbsp;'.$upcoming_event['end_date'].'</div>
										<div class="ru_text">'.$upcoming_event['event_description'].'</div>
									</div>
								</div>';
		}
		
		$cond_running = array("status" => "Running");
		$running_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_running);
		$data['Running_events'] = '';
		foreach($running_events as $running_event) {
			 $event_name = $running_event['event_name'];
			 if($running_event['event_pic'] != ""){
				$event_img = base_url().'images/Events/'.$running_event['event_pic'];
			 }
			 else{
				$event_img = base_url().'images/event.jpg';	
			 }
			 
			
			 $data['Running_events'] .= '<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="'.$event_img.'" alt="No image available"/>
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">'.$event_name.'</div>
										<div class="start_date">Start Date :&nbsp;'.$running_event['start_date'].'</div>
										<div class="start_date end_da">End Date :&nbsp;'.$running_event['end_date'].'</div>
										<div class="ru_text">'.$running_event['event_description'].'</div>
									</div>
								</div>';
		}
		
	 }
	 
	 else {
		 
		 $cond_upcoming = array("status" => "Upcoming");
		 $upcoming_events = $this->events_model->getAllNonOpelUsersUpcomingEvents($cond_upcoming);
		
		 $data['upcoming_events'] = '';
		 foreach($upcoming_events as $upcoming_event) {
			 $event_name = $upcoming_event['event_name'];
			 $event_img = base_url().'images/Events/'.$upcoming_event['event_pic'];
			if($upcoming_event['event_pic'] != ""){
				$event_img = base_url().'images/Events/'.$upcoming_event['event_pic'];
			 }
			 else{
				$event_img = base_url().'images/event.jpg';	
			 }
			 $data['upcoming_events'] .= '<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="'.$event_img.'" alt="No image available"/>
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">'.$event_name.'</div>
										<div class="start_date">Start Date :&nbsp;'.$upcoming_event['start_date'].'</div>
										<div class="start_date end_da">End Date :&nbsp;'.$upcoming_event['end_date'].'</div>
										<div class="ru_text">'.$upcoming_event['event_description'].'</div>
									</div>
								</div>';
		}
		
		$cond_running = array("status" => "Running");
		 $running_events = $this->events_model->getAllNonOpelUsersUpcomingEvents($cond_running);
		 $data['Running_events'] = '';
		 foreach($running_events as $running_event) {
			 $event_name = $running_event['event_name'];
			 $event_img = base_url().'images/Events/'.$running_event['event_pic'];
			if($running_event['event_pic'] != ""){
				$event_img = base_url().'images/Events/'.$running_event['event_pic'];
			 }
			 else{
				$event_img = base_url().'images/event.jpg';	
			 }
			 $data['Running_events'] .= '<div class="upcoming_inner_wrapper">
									<div class="col-md-3 lt_event">
										<img src="'.$event_img.'" alt="No image available"/>
									</div>
									<div class="col-md-9 rt_event">
										<div class="ru_title">'.$event_name.'</div>
										<div class="start_date">Start Date :&nbsp;'.$running_event['start_date'].'</div>
										<div class="start_date end_da">End Date :&nbsp;'.$running_event['end_date'].'</div>
										<div class="ru_text">'.$running_event['event_description'].'</div>
									</div>
								</div>';
		 }
		 
	 }
	 
		 
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	
	
	
	
	function news_and_events(){
		
		 $data['title']  = 'News And Events';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/news_and_events';
		 /*$this->load->library('rssparser');  
		 $this->rssparser->set_feed_url('http://media.gm.com/media/us/en/gm/home.html');
		 $this->rssparser->set_cache_life(30);        
		 $rss = $this->rssparser->getFeed(10);   
		  print_r($rss)*/
		 $feed_url = 'http://media.opel.com/content/media/intl/en/opel/news/jcr:content/righttabsocialmedia/rss.newsrss.html';
		 $content = file_get_contents($feed_url);
		 //print_r($content);die;
		 $rss_feeds = new SimpleXmlElement($content);
		 
		 foreach($rss_feeds->channel->item as $feed):
		 
			$data['news'] .= '<li>
								<a target="_blank" href="'.$feed->link.'">'.$feed->title.'</a>
								<div>'.substr($feed->description,0,300).'...'.'
								<span class="read_news"><a target="_blank" href="'.$feed->link.'">Read More</a></span></div>
							</li>';
		 endforeach;
		 
		// print_r($x);die;
		 $cond_upcoming = array("status" => "Upcoming");
		 $upcoming_events = $this->events_model->getAllUpcomingEvents($cond_upcoming);
		 $data['upcoming_events'] = '';
		 if(count($upcoming_events)>0){
			 foreach($upcoming_events as $upcoming_event) {
				 $event_name = $upcoming_event['event_name'];
				 if($upcoming_event['event_pic'] != ""){
					$event_img = base_url().'images/Events/'.$upcoming_event['event_pic'];
				 }
				 else{
					$event_img = base_url().'images/event.jpg';
				 }
				
				 $data['upcoming_events'] .= '<div class="upcoming_inner_wrapper">
										<div class="col-md-3 lt_event">
											<img src="'.$event_img.'" />
										</div>
										<div class="col-md-9 rt_event">
											<div class="ru_title">'.$event_name.'</div>
											<div class="start_date">Start Date :&nbsp;'.$upcoming_event['start_date'].'</div>
											<div class="start_date end_da">End Date :&nbsp;'.$upcoming_event['end_date'].'</div>
											<div class="ru_text">'.$upcoming_event['event_description'].'</div>
										</div>
									</div>';
			}
		}
		else{
			$data['upcoming_events'] ='No Records Found';
		}
		 
		 
		 
		 
		 $cond_running = array("status" => "Running");
		 $running_events = $this->events_model->getAllRunningEvents($cond_running);
		 $data['Running_events'] = '';
		 if(count($running_events)>0){
			 foreach($running_events as $running_event) {
				 $event_name = $running_event['event_name'];
				 if($running_event['event_pic']!=""){
					$event_img = base_url().'images/Events/'.$running_event['event_pic'];
				 }
				 else{
					$event_img = base_url().'images/event.jpg';
				 }
				 $data['Running_events'] .= '<div class="upcoming_inner_wrapper">
										<div class="col-md-3 lt_event">
											<img src="'.$event_img.'" />
										</div>
										<div class="col-md-9 rt_event">
											<div class="ru_title">'.$event_name.'</div>
											<div class="start_date">Start Date :&nbsp;'.$running_event['start_date'].'</div>
											<div class="start_date end_da">End Date :&nbsp;'.$running_event['end_date'].'</div>
											<div class="ru_text">'.$running_event['event_description'].'</div>
										</div>
									</div>';
			 }
		 }
		 else{
			 $data['Running_events'] ='No Records Found';
		}
		
		 $this->load->view('frontend/incls/layout', $data);
	}
	
	function gallery(){
		 $data['title']  = 'Gallery';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/gallery';
		 $data['models']  = $this->catalog_model->getAllDistinctCatalogModel($cond);
		 
		 $data['logged_in'] = 0;
		 if(is_opeluser_logged_in())
		 { 
			$data['logged_in'] = 1;
		 }
		 $this->load->view('frontend/incls/layout', $data);  
	}
	function term_and_conditions(){
		 $data['title']  = 'Terms and Conditions';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/terms_and_conditions';
		 
		  
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	
	function getPdf(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else
		{ 
			if(is_ajax_request())
			{
				
				$postData = $this->input->post();
				$userdata = $this->session->userdata("auth_opeluser");
				$this->checkUserStatusOpel($userdata['id']);
				$cond = array("model_id" => $postData["m_id"]);
				$pdf = $this->pdf_model->getPDFById($cond);
				$html ='';
				if(count($pdf)>0){
					$i=0;
					foreach($pdf as $cat):
					
					$html .='<ul class="pdf_wra">
							<li><a target="_blank" href="'.base_url().'images/PDF/'.$cat['p_file'].'"><img src="'.base_url().'images/pdf_icon.png"/>'.$cat['p_name'].'</a></li>
							</ul>';
					$i++;
					endforeach;
				}
				else{
					$html .='<div class="no_records">No Records Available</div>';
				}
				
				echo json_encode(array("html"=>$html));exit;
			}
		}
	}
	
	
	
	function getGallery(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			 
			//print_r($postData);die;
			$cond = array();
			$join = array(TB_USERS => TB_COMMUNITY.".user_id=".TB_USERS.".user_id");
			$count = $this->gallery_model->getComModelGalleryCountByUserId($cond,$join);
			//print_r($count);die;
			if(!isset($postData["start"]) || $postData["start"]==""){ $postData["start"] = 0; }
			$catlog = $this->gallery_model->getComModelGalleryByUserId($cond,$postData["start"],$join);
			//echo $this->db->last_query();die;
			//print_r(count($catlog));die;
			$html ='';
			if(count($catlog)>0){
				$no_records = 1;
				$i=0;
				foreach($catlog as $cat):
				$last ='';
				if($i == 2){
					$last = 'last_gal';
				}
				$cond_total = array("vehicle_id" => $cat['id']);
				$count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);

				if(is_opeluser_logged_in())
				{	
					$userdata = $this->session->userdata("auth_opeluser");	 
					$cond_gallery = array("vote_by" => $userdata['id'], "vehicle_id" => $cat['id']);
					$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
					$btn_info = '';
					if($count_vote[0]['cnt'] <=0 ){
						//$btn_info = '<p class="like_wrapper" id="change_'.$cat['id'].'"><a href="javascript:void(0);" title="Like" class="veh_vote" data-attr="like" data-cat="'.$cat['id'].'" data-uid="'.$cat['user_id'].'" id="like_'.$cat['id'].'"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a></p>';
						$btn_info ='<a class="veh_vote" data-attr="like" data-cat="'.$cat['id'].'" data-uid="'.$cat['user_id'].'" id="like_'.$cat['id'].'" href="javascript:void(0);"><i class="fa fa-fw fa-heart"></i><span id="cnt_'.$cat['id'].'">'.$count_total[0]['cnt'].'</span></a>';
						
					}
					else{
						//$btn_info = '<p class="dis_like_wrapper" id="change_'.$cat['id'].'"><a href="javascript:void(0);" title="Dislike" class="veh_vote" data-attr="dislike"  data-cat="'.$cat['id'].'" data-uid="'.$cat['user_id'].'" id="dislike_'.$cat['id'].'"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a></p>';
						$btn_info ='<a class="veh_vote" data-attr="dislike" data-cat="'.$cat['id'].'" data-uid="'.$cat['user_id'].'" id="like_'.$cat['id'].'" href="javascript:void(0);"><i class="fa fa-fw fa-heart red"></i><span id="cnt_'.$cat['id'].'">'.$count_total[0]['cnt'].'</span></a>';
						
					}
					
					
				}
				else{
					$cond_gallery = array("vehicle_id" => $cat['id']);
					$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
					$btn_info ='<a class="veh_vote" href="javascript:void(0);"><i class="fa fa-fw fa-heart"></i><span id="cnt_'.$cat['id'].'">'.$count_total[0]['cnt'].'</span></a>';
				}
				
				$html .='<div class="first_gal '.$last .'">
								<a href="'.base_url().'community_gallery/'.$cat['img_name'].'" class="lightbox">
								<figure class="effect-phoebe">
									<img class="pop_lightbox" src="'.base_url().'community_gallery/thumb/'.$cat['img_thumb'].'" alt="'.$cat['img_name'].'"/>
									
								</figure> 	
								</a>
								<figcaption> 
											<p>
												'.$btn_info.'
											</p> 
									</figcaption>
						</div>';
				$i++;
				if($i==3){
					$i=0;
				}
				endforeach;
			}
			
			if($postData["start"] == 0)
				{
					if(count($catlog)==0)
					{
					
						$html .='<li class="no-data-found">No Records Available</li>';
					}
					$html .='</ul>';
				}
			$config = get_pagination_config($postData["start"], $count[0]["cnt"], '', 'desc', "");
			$this->pagination->initialize($config);
			
			$to = $postData["start"]+PER_PAGE_IMAGE;			
			
			if($to > $count[0]["cnt"])
			{
				$to = $count[0]["cnt"];
				$paginate = $to;
			}
			else
			{
				$paginate = $to;
			}
			
			echo json_encode(array("html"=>$html,'limit' =>PER_PAGE_IMAGE,'totalrec' => count($catlog), 'start' => $postData["start"],'paginate' => $paginate));exit;
		}
	}
	
	
	function voteToCommunityGallery(){
		if(is_ajax_request())
		{
			if(is_opeluser_logged_in())
			{	
				$postData = $this->input->post();
				//print_r($postData);die;
				$userdata = $this->session->userdata("auth_opeluser");	 
				$this->checkUserStatusOpel($userdata['id']); 
				$cond_total = array("vehicle_id" => $postData['vid']);
				$count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);
				$cond_gallery = array("vote_by" => $userdata['id'], "vehicle_id" => $postData['vid']);
				
				$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
				//echo $this->db->last_query();die;
				if($postData['vote'] == 'like'){
					if($count_vote[0]['cnt']>0){
						
						echo json_encode(array("heart_cnt"=>$count_vote[0]['cnt'],"status" => "like","msg"=>"already vote up"));exit;
					}
					else{
						$insertId = $this->common_model->insert(TB_COMMUNITY_LIKE,array("user_id" => $postData['uid'],"vote_by" => $userdata['id'],"vehicle_id" =>$postData['vid']));
						$cond_gallery = array("vehicle_id" => $postData['vid']);
						$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
						$html = '<i class="fa fa-fw fa-heart red"></i><span id="cnt_14">'.$count_vote[0]['cnt'].'</span>';
						echo json_encode(array("id"=>$postData['vid'],"status" => "like","heart_cnt"=>$count_vote[0]['cnt'],"msg"=>"vote up","html"=>$html));exit;
					}
				}
				if($postData['vote'] == 'dislike'){
					if($count_vote[0]['cnt']>0){
						$insertId = $this->common_model->delete(TB_COMMUNITY_LIKE,array("user_id" => $postData['uid'],"vote_by" => $userdata['id'],"vehicle_id" =>$postData['vid']));
						$cond_gallery = array("vehicle_id" => $postData['vid']);
						$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
						$html = '<i class="fa fa-fw fa-heart"></i><span id="cnt_14">'.$count_vote[0]['cnt'].'</span>';
						echo json_encode(array("id"=>$postData['vid'],"status" => "dislike","heart_cnt"=>$count_vote[0]['cnt'],"msg"=>"vote down","html" => $html));exit;
						
					}
					else{
						echo json_encode(array("heart_cnt"=>$count_vote[0]['cnt'],"status" => "dislike","msg"=>"already vote down"));exit;
					}
				}

			}
		}
	}

	function getAccesseriesGallery(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			//echo $postData["m_id"];die;
			
			if($postData["m_id"]!=""){
				$cond = array("model_id" => $postData["m_id"]);
			}
			else{
				$cond = array();
			}
			
			$accessories = $this->accessories_model->getAccesseriesGallery($cond);
			//print_r($accessories);die;
			$html ='';
			if(count($accessories)>0){
				
				$no_records = 1;
				$i=0;
				foreach($accessories as $cat):
				$last ='';
				if($i == 2){
					$last = 'last_gal';
				}
				$html .='<div class="first_gal '.$last .'">
							<a href="'.base_url().'images/Accessories/'.$cat['accessories_pic'].'" class="lightbox">
								<figure class="effect-phoebe">
									<img src="'.base_url().'images/Accessories/thumb/'.$cat['accessosries_thumb'].'" alt="'.$cat['accessories_pic'].'"/>
								</figure>
							</a>
						<div class="acces_imgtext">'.str_replace("\n","<br/>",$cat['accessories_desc']).'</div>		
						</div>';
				$i++;
				if($i==3){
					$i=0;
				}
				endforeach;
			}
			else{
				$no_records = 0;
				$html .='<div id="no_records">No Records Available</div>';
			}
			
			echo json_encode(array("html"=>$html,"no_records"=>$no_records));exit;
		}
	}



	
	function profile(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'Profile';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/profile';
			 $userdata = $this->session->userdata("auth_opeluser");
			$this->checkUserStatusOpel($userdata['id']);
			 $this->load->view('frontend/incls/layout', $data);  
		}
		 
	}
	
	function communitygallery(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'Community Gallery';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/community_gallery';
			 $userdata = $this->session->userdata("auth_opeluser");
			 $this->checkUserStatusOpel($userdata['id']);
			 $this->load->view('frontend/incls/layout', $data);  
		}
		 
	}
	
	/*uploading community gallery images*/
	public function upload_com_gallery() {
		if(is_opeluser_logged_in())
		{ 
			$userdata = $this->session->userdata("auth_opeluser");
			$user_id = $userdata['id'];	
			if ( !empty( $_FILES ) ) {
	
				$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
				$fname = $_FILES[ 'file' ][ 'name' ]; 
				$ext = pathinfo($fname, PATHINFO_EXTENSION);
				$filename  = $user_id."_".uniqid().".".$ext;
				$uploadPath = $this->communityimagepath ."/". $filename;
				move_uploaded_file( $tempPath, $uploadPath );
				
				$path_to_image = $this->communityimagepath ."/". $filename;
				$new_img_path  = $this->communityimagepath_thumb.'/'.$filename;
				
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = $path_to_image;
				$config1['new_image'] = $new_img_path;
				$config1['create_thumb'] = TRUE;
				$config1['maintain_ratio'] = TRUE;
				$config1['height'] = 360;
				$config1['width'] = 360;
				
			    $this->image_lib->initialize($config1);
			    $this->image_lib->resize();
			 
			    $expl_file = explode('.',$filename);
			   
			    $file_thumb =  $expl_file[0]."_thumb.".$ext;
				
				$insertId = $this->common_model->insert(TB_COMMUNITY,array("img_name" => $filename,"user_id" => $user_id,"img_thumb" => $file_thumb,"date_modified" => date('Y-m-d H:i:s')));
				if($insertId != "") {
				 
				    
				 
				  //$update =  $this->common_model->udpate(TB_COMMUNITY,array("com_gal_id" => $insertId),array("img_thumb" => $file_thumb));
				 	//echo ;die;
				 $hostname = $this->config->item('hostname');
				 $usrname = $userdata['c_name'];
				 $config['mailtype'] ='html';
				 $config['charset'] ='iso-8859-1';
				 $this->email->initialize($config);
				 $from  = EMAIL_FROM; 
				 $to = EMAIL_FROM;
				 //$this->messageBody  .= email_header();
				 $this->messageBody  = email_header();
				 $this->messageBody  .= "Hello Admin,<br/><br/>
				 New images for community gallery has been uploaded on OPEL360 website.
				 <br/><br/>
				 Please login to your account to check it.
				 ";
				
				 $this->messageBody  .= email_footer();
				 //echo $this->messageBody;
				 //die;
				 $this->email->from($from, $from);
				 //$this->email->to("swapnil.t@exceptionaire.co");
				 //$this->email->to("sunny@exceptionaire.co");
				//$this->email->to("mat@mdsm-consulting.com");
				 
				 $this->email->to("$to");

				 $this->email->subject('Opel360 Notification : Community Gallery');
				 $this->email->message($this->messageBody);	
					 
				 $res = $this->email->send();
					
				 echo "success"; die();
				}
				else {
				 echo "fail"; exit;
				}
				
				$answer = array( 'answer' => 'File transfer completed' );
				$json = json_encode( $answer );

				echo $json; exit;

			} else {

				echo 'No files'; exit;

			}
		}
	}
	
	/*public function add_com_gallery() {
		if(is_opeluser_logged_in())
		{ 
			$data = $_REQUEST;
			$userdata = $this->session->userdata("auth_opeluser");
			$user_id = $userdata['id'];
			
			//$insertId = $this->common_model->insert(TB_COMMUNITY,array("img_name" => $data["file_name"],"user_id" => $user_id,"date_modified" => date('Y-m-d H:i:s')));
			if($insertId!="") {
				echo "success"; die();
			}
			else {
				echo "fail"; die();
			}
		}
	}*/
	
	function get_community_gallery(){
		
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else
		{  
				$userdata = $this->session->userdata("auth_opeluser");
				$user_id = $userdata['id']; 
				$this->checkUserStatusOpel($userdata['id']);
				$cond = array("user_id"=>$user_id);
				$com_gallery = $this->banner_model->getCommunityGallery($cond);
				
				$html = '<div class="grid" style="display: block;">';
				if($com_gallery){
					$i=1;
					foreach($com_gallery as $gal):
						if($i == 3){
							$last = "last_gal";
						}
						else{
							
							$last = "";
						}
						$html .= '<div class="first_gal '.$last.'">
						<a href="javascript:void(0);" onclick="deleteCommunityGalleryImage('.$gal['id'].')" class="del_cimage"><i class="fa fa-times" aria-hidden="true"></i></a>
						<a class="lightbox" target="_blank" href="'.base_url().'community_gallery/'.$gal['img_name'].'">
							<figure class="effect-phoebe">
								<img alt="'.$gal['img_name'].'" src="'.base_url().'community_gallery/'.$gal['img_name'].'">
							</figure>
						</a>		
						</div>';
						
						if($i==4){
							$i=1;
						}
						$i++;
					endforeach; 
				}
				$html .= '</div>';
				
				echo json_encode(array("html"=>$html));exit;
		}
	}
	
	function delete_community_gallery(){
		
		if(is_opeluser_logged_in())
		{ 
			$userdata = $this->session->userdata("auth_opeluser");
			$user_id = $userdata['id'];
			$img_id = $_POST['img_id'];
			$cond = array("user_id" => $user_id, "com_gal_id" => $img_id);
			$com_gallery = $this->banner_model->getCommunityGallery($cond);
			$cond = array("com_gal_id" => $img_id );
			@unlink($this->communityimagepath.'/'.$com_gallery["0"]["img_name"]);
			@unlink($this->communityimagepath_thumb.'/'.$com_gallery["0"]["img_thumb"]);
			$this->common_model->delete(TB_COMMUNITY,$cond);
			
			echo json_encode(array("html"=>$html));exit;
		}
	}
	
	/********* User Verification ***********/
	
	function verifyAccount(){
	
       $data['title']  = 'Activation Account';
	   $data['meta_description'] = '';
	   $data['meta_keyword'] = '';
       $data['main_content'] = 'frontend/activate_account';
       $key   = $this->input->get('key');
       $email = $this->input->get('email');
       $cond_key = array("resetkey"=>$key,"email_address"=>$email,"status"=>"Inactive");
       $activate_acc = $this->users_model->getAllUsers($cond_key);
     
       
		if(count($activate_acc)>0){
				$cond_update = array("resetkey"=>$key,"email_address"=>$email);
				$data_update = array("status"=>"active");
				$this->common_model->update(TB_USERS,$cond_update,$data_update);
            $data['message'] = "Your account is verified successfully.Please <a href='javascript:void(0);' onclick='make_login();'>click here</a> to login to your account";
             $this->load->view('frontend/incls/layout', $data);

        }
        else{
			
            $data['validation_message'] = "The key is invalid or You have already verified the account";
             $this->load->view('frontend/incls/layout', $data);
        } 
    }
	
	function service_costs(){
		 $data['title']  = 'Service Costs';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = ''; 
		 $data['main_content'] = 'frontend/service_costs';
		 $cond = array();
		 $data['services_cost'] = $this->services_model->getAllServicesCostByModel($cond);
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	function service_records(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{
			 $data['title']  = 'Service Records';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/service_record';
			 $userdata = $this->session->userdata("auth_opeluser"); 
			 $this->checkUserStatusOpel($userdata['id']);
			 $cond_veh = array("user_id" => $userdata['id']); 
			 $vehicle_details = $this->users_model->getCustomerVehicleNoById($cond_veh);
			 //print_r($vehicle_details);die;
			 if(!empty($vehicle_details)){
					$i=1;
					foreach($vehicle_details as $veh):	
						/*$html  = 'registration_no'.$veh['registration_no'],
						'registration_date'.date('Y-m-d',strtotime($veh['registration_date'])),
						'chassis_number'.($veh['chassis_number']?$veh['chassis_number']:""),
						'engine_number'.($veh['engine_number']?$veh['engine_number']:""),
						'customer_code'.($veh['customer_code']?$veh['customer_code']:""),;*/
						 
						 if($i==1){
							 $in = 'in';
							 $graphic = '<span class="glyphicon glyphicon-minus"></span>';
						 }else{
							 $in ='';
							 $graphic = '<span class="glyphicon glyphicon-plus"></span>';
						 }
						$html .= '<div class="panel panel-default accordion">
							<div class="panel-heading" role="tab" id="heading'.$i.'">
							  <h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$i.'" aria-expanded="true" aria-controls="collapse'.$i.'">
								'.$graphic.'
								 Vehicle Number : <strong>'.$veh['registration_no'].'</strong>
								</a>
							  </h4>
							</div>
							<div id="collapse'.$i.'" class="animated zoomIn panel-collapse collapse '.$in.'" role="tabpanel" aria-labelledby="heading'.$i.'">
							<div class="table-responsive">
							<table  class="table table-striped">
							<thead>
								<tr>
									<th width="15%">Service Order</th>
									<th width="15%">Transaction Date</th>
									<th width="15%">Vehicle Mileage</th>
									<th width="55%">Process Description</th>
								</tr>
							</thead>
							<tbody>
							';
						 $veh_id = $veh['id'];
						 $cond_order = array("user_id" => $userdata['id'],"vehicle_id" => $veh_id);
						 $vehicle_service = $this->users_model->getLastTenServiceOrder($cond_order);
						 if(!empty($vehicle_service)){
						 foreach($vehicle_service as $service):
							
								$cond_order = array("user_id" => $userdata['id'], "s_order_id" => $service['id']);
								$description = "";
								$vehicle_service_proccess = $this->users_model->getCustomerVehicleServiceOrderProcessById($cond_order);
								if($vehicle_service_proccess){
									foreach($vehicle_service_proccess as $service_pro):	
										$description .=  $service_pro['process_description'].",";
									endforeach;
								}	
							
							//echo $description;die;
							$html .='<tr><td>'.($service['service_order']?$service['service_order']:"").'</td>
									<td>'.($vehicle_service[0]['transaction_date']?date('Y-m-d',strtotime($vehicle_service[0]['transaction_date'])):"").'</td>
									<td>'.($vehicle_service[0]['vehicle_mileage']?$vehicle_service[0]['vehicle_mileage']:"").'</td>
									<td>'.$description.'</td></tr>';
							  
						 endforeach;
					 }
					 else{
						 $html .='<tr><td colspan="4" align="center">No Records Available</td></tr>';
					 }
					$html .='</tbody></table></div></div>
						  </div>';
						  $i++;
					endforeach;
					$data['html']  = $html;
			 }
			 else{
				 $html .='<div>No Records Available</div>';
			 }
			 $this->load->view('frontend/incls/layout', $data);
		}
	}
	
	function rent_an_opel(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
		 $data['title']  = 'Rent An Opel';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/rent_an_opel';
		 $POST['Vehicle']['Make'] = 'opel';
		 $POST['key'] = 'eExCN+NqbQTK8NrusdGabFnTDCWxxMUNAz9sKfZTWAUNucoV';
		 $respVehicleInfo = $this->soapRequestCarRental('VehicleGet',$POST); 
		 $userdata = $this->session->userdata("auth_opeluser");
		$this->checkUserStatusOpel($userdata['id']);
		// echo "<pre>";print_r($respVehicleInfo);die;
		 $cars = "";
		 $data['error_message'] = $this->session->flashdata('error_message');
		 
		 if(!empty($respVehicleInfo)){
			 //VehicleGetResult
			 if($respVehicleInfo['result_soap']['VehicleGetResult']['ResultMessage']['ResultCode'] ==1){
				 $cars .='<select id="sel_model" name="sel_model" class="form-control">';
				 //$cars .='<option value="'.$vehicles[$key]['IDCar'].'">All</option>';
				 $vehicles = $respVehicleInfo['result_soap']['VehicleGetResult']['Vehicles']['Vehicle'];
				 
				 foreach($vehicles as $key => $val) :
						$cars .='<option value="'.$vehicles[$key]['IDCar'].'">'.$vehicles[$key]['Model'].'</option>';
					 
				 endforeach;
				$cars .='</select>';
			 }
		 }
		 $data['cars']=$cars;
		 $this->load->view('frontend/incls/layout', $data);
		}
	}
	
	
	function rent_car_next(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'Confirm booking';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 
			 $data['main_content'] = 'frontend/rent_booking_next_confirm';
			 $this->load->view('frontend/incls/layout', $data);
		 }
	}
	
	
	
	function rent_car_confirm(){
		if(is_ajax_request())
		{
			if(!is_opeluser_logged_in())
			{ 
				redirect('index'); exit;
			}
			else{  
				//
				 if($_POST){
					 
					 //print_r($_POST);die;
					 $POST['VehAvailableGet']['Make'] = 'Opel';
					 $POST['VehAvailableGet']['IDCar'] = $_POST['sel_model'];
					 
					 $POST['key'] = API_KEY;
					 $respAvailbleVehicleInfo = $this->soapRequestCarRental('VehAvailableGet',$POST); 
					// echo "<pre>";print_r($respAvailbleVehicleInfo);die;
					 if(!empty($respAvailbleVehicleInfo['result_soap'])){
					 if($respAvailbleVehicleInfo['result_soap']['VehAvailableGetResult']['ResultMessage']['ResultCode'] == 1){
						 $vehicles = $respAvailbleVehicleInfo['result_soap']['VehAvailableGetResult']['VehUnAvaiable']['VehAvailable'];
						// echo "<pre>";print_r($vehicles);die;
						 if(isset($vehicles[0])) {
							foreach($vehicles as $key => $val) :  
							 //echo $vehicles[$val];
							 
							 $POST_AVAIL['Vehicle_Opel360']['Make'] = 'Opel'; 
							 $POST_AVAIL['Vehicle_Opel360']['IDCar'] = $vehicles[$key]['IDCar']; 
							 $POST_AVAIL['key'] = API_KEY;
							 
							 $respVehicleInfo = $this->soapRequestCarRental('Vehicle_Opel360Get',$POST_AVAIL); 	
							 //echo "<pre>"; print_r($respVehicleInfo);die; 
							 $Model = $vehicles[$key]['Model'];
							 $IDCar = $vehicles[$key]['IDCar'];
							 $Units = $vehicles[$key]['Units'];
							 $StartPrice  = $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['StartPrice'];
							 //print_r($respVehicleInfo);die;
							 //echo $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['VehGroup'];
							 $array_cars[] = array(
								"CompCode" => $vehicles[$key]['CompCode'],
								"IDCar" => $vehicles[$key]['IDCar'],
								"Model" => $vehicles[$key]['Model'],
								"VehGroup"  => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['VehGroup'],
								"Transmission" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Transmission'],
								"EngineType" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['EngineType'],
								"StartPrice" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['StartPrice'],
								"OutDate" => $vehicles[$key]['OutDate'],
								"ReturnDate" => $vehicles[$key]['ReturnDate'],
								"VehNo" => $vehicles[$key]['VehNo'],
								"Units" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Units'],
								"Info" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Info'],
								"Image" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Image'] 
							 );
							 endforeach;
							 //print_r($array_cars);die;
						 }
						 else{
						   
						  $Model = $vehicles['Model'];
						  $IDCar = $vehicles['IDCar'];
						  $Units = $vehicles['Units'];
						  $StartPrice  = $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['StartPrice'];
							
						  $POST_AVAIL['Vehicle']['Make'] = 'opel'; 
						  $POST_AVAIL['Vehicle']['IDCar'] = $vehicles['IDCar'];
						  $POST_AVAIL['key'] = API_KEY;
						  $respVehicleInfo = $this->soapRequestCarRental('VehicleGet',$POST_AVAIL); 	
						  $array_cars[] = array(
								"CompCode" => $vehicles[$key]['CompCode'],
								"IDCar" => $vehicles[$key]['IDCar'],
								"Model" => $vehicles[$key]['Model'],
								"VehGroup"  => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['VehGroup'],
								"Transmission" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Transmission'],
								"EngineType" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['EngineType'],
								"StartPrice" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['StartPrice'],
								"OutDate" => $vehicles[$key]['OutDate'],
								"ReturnDate" => $vehicles[$key]['ReturnDate'],
								"VehNo" => $vehicles[$key]['VehNo'],
								"Units" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Units'],
								"Info" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Info'],
								"Image" => $respVehicleInfo['result_soap']['Vehicle_Opel360GetResult']['Vehicles']['Vehicle_Opel360']['Image']
						  );
						 }
						 
					 }
					// die;
					
					
					 
					 if(!empty($array_cars)){
						$data['cars_found'] = count($array_cars);
						$cars_array = $array_cars;
						$BookingDateFrom = $_POST['pickup_date']." ".$_POST['pickup_time_hr'].":".$_POST['pickup_time_min'];
						$BookingDateTo = $_POST['return_date']." ".$_POST['return_time_hr'].":".$_POST['return_time_min'];
						
						$html_next_view="";
						if($cars_array != ""){
						foreach($cars_array  as $car):
						$html_next_view .= '
						<div class="rent_next">
		<div class="list_car_name">'.$car['Model'].'</div>
		<div class="li_decar_wra">
			<div class="col-md-7 car_next">
				<div class="lt_img_car">
					<img alt="'.$car['Model'].'" src="'.$car['Image'].'">
				</div>
				
				<div class="rt_car_det">';
					if($car['Info'] != ""){
					$expl_info = explode('<br/>',$car['Info']);
			
					$html_next_view .='
						<div class="rt_car_info"><img alt="Air Conditioned" src="images/air_conditioned.png">'.$expl_info[0].'</div>
						<div class="rt_car_info"><img alt="Automatic" src="images/automatic.png">'.$expl_info[1].'</div>
						<div class="rt_car_info"><img alt="Door" src="images/door.png">'.$expl_info[2].'</div>
						<div class="rt_car_info"><img alt="Passenger" src="images/passenger.png">'.$expl_info[3].'</div>
						<div class="rt_car_info"><img alt="Luggage" src="images/luggage.png">'.$expl_info[4].'</div>';
					}
				
					$html_next_view .='</div> 
			</div>
			<div class="col-md-5 full_pric_wrapp">
			  <div class="submit_select_box"> 	
					<div class="price_wrapper">
						<div class="star_frm">
							Starting From
						</div>
						<div class="euro">
							$'.$car['StartPrice'].'
						</div>
						<div class="next_btn col-md-12">
								<a href="javascript:void(0);" id="" name="cmdSubmit" data-attr="">Continue</a>
						</div>
					</div>
					<div class="price_rt">
						<div class="arow_wra">
							<img alt="Choose Car" src="'.base_url().'images/arrow_full.png" alt="">
						</div>
					</div>
			  </div>
			</div>
		</div>
	</div>';
						endforeach;
						}
						$arr_data = array("pickup_date_time" => $BookingDateFrom, "return_date_time" => $BookingDateTo, "IDCar" => $IDCar,"Make" => $POST['Vehicle']['Make'],"Model" => $Model,"Units"=>$Units,"Startprice" =>$StartPrice,"Tax"=>(($StartPrice*7)/100),"pickup_location" =>$_POST['pickup_location'],"dropoff_location" => $_POST['dropoff_location']);
						$this->session->set_userdata('booking_data',$arr_data);
						//$_SEESION['booking_data'] = $arr_data;
					 }
					 else{
						 echo json_encode(array("status"=>"success","message"=>"<div class='alert alert-danger'>No vehicle available for this model</div>","booking_action" => "new_step2", "html_next" => ""));exit;
					 }
				 } 
				 else{  
					 echo json_encode(array("status"=>"success","message"=>"Something Went Wrong","booking_action" => "new_step2", "html_next" => ""));exit;
				 }
					 //print_r($data['cars_found']);
					 //echo "<pre>";print_r($array_cars);die;
				
			 
				
			 }
				 //die;
				 //data['main_content'] = 'frontend/rent_booking_next_confirm';
				 //$this->load->view('frontend/incls/layout', $data);
				 
				 echo json_encode(array("status"=>"success","message"=>"","booking_action" => "new_step2", "html_next" => $html_next_view));exit;
			}
		}
	}
	
	function rent_pay_now(){
		if(!is_opeluser_logged_in())
		{ 
			$this->session->set_userdata('referred_from', current_url_string());
			redirect('pay-now?msg=need_login'); exit; 
		}
		else{ 
			 $data['title']  = 'Pay For Car Booking';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/rent_booking_pay_now';
			 //print_r($this->session->userdata('booking_data'));die;
			 if($this->session->userdata('booking_data')){
				$userdata = $this->session->userdata("auth_opeluser");
				 $data['booking_data'] = $this->session->userdata('booking_data');
				// print_r($data['booking_data']);die;
				 if(isset($_POST)){
					$arrBookingData["key"] = 'eExCN+NqbQTK8NrusdGabFnTDCWxxMUNAz9sKfZTWAUNucoV';
					
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CMD'] = 'I';
					$arrBookingData["BookingRequest"]['BookingCreateChange']['BookingNo'] = "";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['BookingID'] = "ORDER".$userdata['user_id'].uniqid();
					$arrBookingData["BookingRequest"]['BookingCreateChange']['BookingDateTimeFrom'] = '2016-07-03 01:10:00';
					$arrBookingData["BookingRequest"]['BookingCreateChange']['BookingDateTimeTo'] = '2016-07-04 01:10:00';
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Make'] = 'Opel';
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Model'] = "Astra GTC";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['IDCar'] = "79";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Remark'] = "";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CreateDateTime'] = date('Y-m-d H:i:s');
					$arrBookingData["BookingRequest"]['BookingCreateChange']['LastModifiedDateTime'] = "";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['NumberDays'] = "1";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Amount'] = "86";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CrCardNo'] = "4111111111111111";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CrCardDate'] = "04/21";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CustCode'] = "";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CustomerType'] = "P";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['FirstName'] = 'aaaa';
					$arrBookingData["BookingRequest"]['BookingCreateChange']['LastName'] = 'bbbb';
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Salutation'] = "Mr";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Gender'] = "Male";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CustomerEmail'] = "srt1985@gmail.com";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['DateOfBirth'] = "1985-10-13";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['CustomerID'] = "";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['MobileNo'] = "8979878979";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Street'] = "";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Unit'] = "5";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['Country'] = "SG";
					$arrBookingData["BookingRequest"]['BookingCreateChange']['PostalCode'] = "339410";
					
					//$PostBooking["BookingRequest"] = $arrBookingData;
					
					//echo "<pre>";print_r($arrBookingData);
					//die;
					
					$responseBooking = $this->soapRequestCarRental('BookingCreateChange',$arrBookingData);
					//echo "<pre>";print_r($responseBooking);die;
					//print_r($respAvailbleVehicleInfo['result_soap']['VehAvailableGetResult']['ResultMessage']['ResultCode'];die;
					
					if($responseBooking['result_soap']['BookingCreateChangeResult']['ResultMessage']['ResultCode']=='-1'){
						
						 $this->session->set_userdata('booking_data',$data['booking_data']);
						 $error_msg = $responseBooking['result_soap']['BookingCreateChangeResult']['ResultMessage']['Errors']['Error']['ErrorMessage'];
						 //die;
						 $this->session->set_flashdata('error_message',$error_msg);
						 redirect('rent-an-opel');
						 exit;
					}
					
					//die;
				 }
			 }
			 else{
				 redirect('rent-an-opel');
			 }
			 $this->load->view('frontend/incls/layout', $data);
		 }
	}
	
	
	function dashboard(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'Dashboard';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/dashboard';
			 $this->load->view('frontend/incls/layout', $data);
		 }
	}
	
	function pdf_manuals(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{ 
			 $data['title']  = 'PDF Manuals';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/pdf_manuals';
			 $userdata = $this->session->userdata("auth_opeluser");
				$this->checkUserStatusOpel($userdata['id']);
			 $cond = array();
			 $data['pdf_manuals'] = $this->pdf_model->getPDFById($cond);
			 $data['models']  = $this->catalog_model->getAllDistinctCatalogModel($cond);
			 $this->load->view('frontend/incls/layout', $data);
		 }
	}
	
	function forgotUserPassword(){ 
		   $data['title']  = 'Forgot Password';
		   $data['meta_description'] = '';
		   $data['meta_keyword'] = '';
		   $data['sucess_message'] = $this->session->flashdata('sucess_message');
		   //echo $this->session->userdata('tokenfrm');   
           $this->form_validation->set_rules('uemail', 'Email address', 'valid_email|xss_clean|required'); 
           if ($this->form_validation->run() !== false) { 
					$useremail = trim($this->input->post('uemail'));
                    $cond = array('email_address' => $useremail);
					$userData = $this->login_model->validUser(TB_USERS,"user_id,country,first_name,last_name,email_address,password,v_cat_id,v_sub_cat_id",$cond);
						//echo "test";die;
					if(count($userData) > 0)
					{	 
                        // $username = $this->loginmodel->getUserNameByUserEmail($uname);
                         $reset_key  = md5(uniqid(mt_rand(), true));
                         $datareset = array(
                              'dn_resetkey' =>  $reset_key,
                         ); 
                         $cond1 = array('email_address' =>  $useremail); 
                         $hostname = $this->config->item('hostname');
                         $this->common_model->update(TB_USERS,$cond1,$datareset);
                         $config['mailtype'] ='html';
                         $config['charset'] ='iso-8859-1';
                         $this->email->initialize($config);
                         $from  = EMAIL_FROM; 
						 $uname = $userData[0]['first_name']." ".$userData[0]['last_name'];
						 $this->messageBody .= email_header();
                         $this->messageBody  = "Hello $uname,<br/><br/>
                            We have received your request to reset your password. <br/> <br/>
                             Please click the link below to reset.<br/><br/>
                             <a href=".$hostname."resetuserpwd/?key=".$reset_key.">".$hostname."resetuserpwd/?key=".$reset_key."</a>
                             <br/><br/>If the link is not working properly, then copy and paste the link in your browser.<br/>
                             If you did not send this request, please ignore this email.
                             <br/><br/>Regards,<br/>DENSO AFTERMARKET WEBSITE";
                        $this->messageBody .= email_footer();     
						//echo $this->messageBody;
						//die;
                        
						 $this->email->from($from, 'DENSO ASIA AFTERMARKET');
						 $this->email->to("$useremail");

                         $this->email->subject('DENSO AFTERMARKET WEBSITE - Reset Password');
                         $this->email->message($this->messageBody);	
                             
                        if ( ! $this->email->send()){
                                $this->session->set_flashdata('sucess_message', "The email can not be send ! Server Error.");
                                 //$data['sucess_message'] ="The email can not be send ! Server Error";
                                redirect('forgotpassword');
                                 //$this->load->view('frontend/incls/layout', $data);
                        }else{
                                $this->session->set_flashdata('sucess_message', "The reset password link has been sent to your email address.");
                                //$data['sucess_message'] = "The reset password link has been sent to your email address.";
                                redirect('forgotpassword');
                                //$this->load->view('frontend/incls/layout', $data);
                        }
                    } 
                    else{
						$data['validation_message'] = "The email address is not found in our database";
                         //var_dump($data);
						 $this->load->view('frontend/forgot_password', $data);
					} 
           }
           else{
                $data['message'] = $this->session->flashdata('message');
                $this->load->view('frontend/forgot_password', $data);
           }
	   //}
           
	}
	
	  
    public function logout()
	{
		
		$this->session->unset_userdata("auth_opeluser");
		
		echo json_encode(array("status"=>"success","msg"=> "success logout"));exit;
	}
	
	
	public function resend_activation_link() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();  
			$uemail = $postData['email'];
			
			//Send activation link
			$activation_key  = md5(uniqid(mt_rand(), true));
							
							$cond = array("email_address" => $uemail);
							$this->common_model->update(TB_USERS,$cond,array("resetkey" => $activation_key ));
							$link ="<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">resend</a>";
							
							$cond = array('email_address' => $uemail, 'resetkey' => $activation_key);
							$userData = $this->users_model->getAllUsers($cond);
							
							$hostname = $this->config->item('hostname');
							 $config['mailtype'] ='html';
							 $config['charset'] ='iso-8859-1';
							 $this->email->initialize($config);
							 $from  = EMAIL_FROM; 
							 $this->messageBodyActivation  .= email_header();
							 $uname = ucwords($userData[0]['name']);
							 $this->messageBodyActivation  .= "Hello $uname,<br/><br/>
								 
								 Click the below link for login to your account.<br/>
								<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail."</a>
								 <br/><br/>If the link is not working properly then copy and paste the link in browser<br/>
								";
							
							 $this->messageBodyActivation  .= email_footer();
							// echo $this->messageBodyActivation;
							// die;
							 $this->email->from($from, $from);
							 $this->email->to("$uemail");

							 $this->email->subject('Account activation');
							 $this->email->message($this->messageBodyActivation);	
								 
							 $this->email->send();
			
							 echo json_encode(array("status" => 1,"action" => "login", "msg" => "<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Activation link is sent to your account.</div>")); exit;
			
		}
	}
	
	public function resend_activation_link_on_deactivation() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();  
			
			$uemail = $postData['email'];
			
			
			//Send activation link
			$activation_key  = md5(uniqid(mt_rand(), true));
							
							$cond = array("email_address" => $uemail);
							$this->common_model->update(TB_USERS,$cond,array("resetkey" => $activation_key ));
							$link ="<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">resend</a>";
							 
							 
							$cond_resend = array('email_address' => $uemail, 'resetkey' => $activation_key);
							$userData = $this->users_model->getAllUsers($cond_resend);
							//print_r($userData); 
							 
							 $hostname = $this->config->item('hostname');
							 $config['mailtype'] ='html';
							 $config['charset'] ='iso-8859-1';
							 $this->email->initialize($config);
							 $from  = EMAIL_FROM; 
							 $this->messageBodyActivation  .= email_header();
							 $uname = ucwords($userData[0]['c_name']);
							 $this->messageBodyActivation  .= "Hello $uname,<br/><br/>
								 
								 Click below link for login to your account.<br/>
								<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail.">".$hostname."verifyaccount/?key=".$activation_key."&email=".$uemail."</a>
								 <br/><br/>If link is not working properly then copy and paste the link in browser<br/>
								";
							
							 $this->messageBodyActivation  .= email_footer();
							 //echo $this->messageBodyActivation;
							 //die;
							 $this->email->from($from, $from);
							 $this->email->to("$uemail");

							 $this->email->subject('Account activation');
							 $this->email->message($this->messageBodyActivation);	
								 
							 $this->email->send();
			
							 echo json_encode(array("status" => 1,"action" => "login", "msg" => "<div class='alert alert-success'><button data-dismiss='alert' class='close' type='button'>×</button>Activation link is sent to your account.</div>")); exit;
			
		}
	}
	
	function service_booking(){
		if(!is_opeluser_logged_in())
		{ 
			redirect('index'); exit;
		}
		else{
			
			 $data['title']  = 'Service Booking';
			 $data['meta_description'] = '';
			 $data['meta_keyword'] = '';
			 $data['main_content'] = 'frontend/service_booking';
			 $userdata = $this->session->userdata("auth_opeluser");
			 $POST['key']  = API_KEY;
			 
			 if($userdata['user_type'] =='OPEL'){
				 $cond = array("user_id" => $userdata['id']);
				 $data['vehicle_list'] = $this->users_model->getCustomerVehicleNoByIdService($cond);
				 
				 
				//Get All Registretion nos 
				//print_r($cond['vehicle_list']);die;
			 } 
			//die;
			//echo $html_booking_info;die;
			$data['cur_booking_data']	 = $html_booking_info;
			$this->load->view('frontend/incls/layout', $data);  
		}
	}
	
	function viewCurrentBooking(){
				$postData = $_POST;
				
				$html_booking_info = "";
				$first = date('Y-m-d',strtotime('Today'));
				$last = date('Y-m-d',strtotime('+30 days'));
				$POST_EXIST['coyId'] = 'OPEL';
				$POST_EXIST['docId'] = 'WSB';
				$POST_EXIST['deptId'] = 'ASC';
				$POST_EXIST['startWeek'] = $first.'T00:00:00.000';
				$POST_EXIST['endWeek'] = $last.'T00:00:00.000';
				$POST_EXIST['appVehNo'] = $postData['veh_no'];
				
				$join = array(TB_VEHICLE_DETAILS => TB_VEHICLE_OWNER_DETAILS.".customer_id=".TB_VEHICLE_DETAILS.".customer_id");
				$cond_veh = array("registration_no" => trim($postData['veh_no']));
				$custInfo = $this->users_model->getCustomerInfo($cond_veh,$join); 
				//echo $this->db->last_query(); 
				
				$POST['cust_code']  = $custInfo[0]['cust_code'];
				$POST['veh_no']  = $postData['veh_no']; 
				$POST['doc_type'] = 'WSB';
				$POST['dept_code'] = 'ASC'; 
				//print_r($POST);
				$checkBooking = $this->soapRequest('GetValidity',$POST);
				
				$currentBooking = $this->soapRequest('GetCurrentVehicleBooking',$POST_EXIST);
				//echo "<pre>";print_r($currentBooking);die; 
				//echo $checkBooking['result_soap']['GetValidityResult'];
				//die;
				if($currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram'] != "" && $checkBooking['result_soap']['GetValidityResult'] == ""){
					$revArr = $currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram']['NewDataSet']['Table'];
					//echo "<pre>";print_r($revArr);//die;
					//$revArr = array_reverse($respBook);
					
					//echo "<pre>";print_r($revArr);die;
					
					//echo $revArr[0]['START_DATE'];
					//echo sizeof($revArr);die;
					if($revArr[0] && !empty($revArr)){
						//echo "test1";
						if(isset($revArr[0]['START_DATE'])){ 
							$cur_booking_flag=0;
							$vehicle_received_date = substr($revArr[0]['START_DATE'],0,10);
							$vehicle_received_time = substr($revArr[0]['START_DATE'],11,16); 
							
							if($vehicle_received_date >= $first){ 
								$vehicle_received_time = explode("+",$vehicle_received_time);
								$vehicle_rec_time = $vehicle_received_time[0];
								$cur_booking_flag=1;
							}
						}
						else{ 
							$cur_booking_flag=0;
							$vehicle_received_date = substr($revArr[0]['VEH_RECV_DATE'],0,10);
							$vehicle_received_time = substr($revArr[0]['VEH_RECV_DATE'],11,16);
							if($vehicle_received_date >= $first){
								
								$vehicle_received_time = explode("+",$vehicle_received_time);
								$vehicle_rec_time = $vehicle_received_time[0];
								$cur_booking_flag=1;
							}
						}
						
						if($cur_booking_flag == 1){
							$ref_no = $revArr[0]['RUN_NO'];
							$html_booking_info .= "<div class='alert alert-info'><b class='latest'>Latest Booking Info</b>&nbsp;:<br/><div class='book_dt'>Booking Date and Time : $vehicle_received_date at $vehicle_rec_time</div><div class='book_dt'>Reference Number : $ref_no</div></div>";
							echo json_encode(array("status"=>"success", "result_val"=> $html_booking_info)); exit;
							
						}
						else{
							$html_booking_info = "<div>No records available for current booking.</div>";
							echo json_encode(array("status"=>"success", "result_val"=> $html_booking_info)); exit;
						}
					}
					else{
						//echo "test2";die;
						if(isset($revArr['START_DATE'])){
							$cur_booking_flag=0;
							$vehicle_received_date = substr($revArr['START_DATE'],0,10);
							$vehicle_received_time = substr($revArr['START_DATE'],11,16);
							if($vehicle_received_date >= $first){
								$vehicle_received_time = explode("+",$vehicle_received_time);
								$vehicle_rec_time = $vehicle_received_time[0];
								$cur_booking_flag=1;
							}
						}
						else{
							//echo "test2";die;
							$cur_booking_flag=0;
							
							$vehicle_received_date = substr($revArr['VEH_RECV_DATE'],0,10);
							$vehicle_received_time = substr($revArr['VEH_RECV_DATE'],11,16);
							
							if($vehicle_received_date >= $first){
								
								$vehicle_received_time = explode("+",$vehicle_received_time);
								$vehicle_rec_time = $vehicle_received_time[0];
								$cur_booking_flag=1;
							}
						}
						 
						if($cur_booking_flag == 1){
							$ref_no = $revArr['RUN_NO'];
							$html_booking_info .= "<div class='alert alert-info'><b class='latest'>Latest Booking Info</b>&nbsp;:<br/><div class='book_dt'>Booking Date and Time : $vehicle_received_date at $vehicle_rec_time</div><div class='book_dt'>Reference Number : $ref_no</div></div>";
							echo json_encode(array("status"=>"success", "result_val"=> $html_booking_info)); exit;
							
						}
						else{
							$html_booking_info = "<div>No records available for current booking.</div>";
							echo json_encode(array("status"=>"success", "result_val"=> $html_booking_info)); exit;
						}
					}
					
					//die;
					echo json_encode(array("status"=>"success", "result_val"=> $html_booking_info)); exit;
			}
			else{
				$html_booking_info = "<div>No records available for current booking.</div>";
				echo json_encode(array("status"=>"success", "result_val"=> $html_booking_info)); exit;
			}
	}
	
	function alpineGetBookingInfo(){
		$postData = $_POST;  
		$post_function  = "GetBookinginfo";
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success", "result_val"=> $result_array));exit;
	}
	
	function alpineGetCalendar(){
		$postData = $_POST;  
		$post_function  = "GetCalendar";
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success", "result_val"=> $result_array));exit;
	}
	
    function alpineCheckValidation(){
		$postData = $_POST;  
		//$curl_post = "VehicleNo=".$postData['VehicleNo']."&doctype=".$postData['doctype']."&deptcode=".$postData['deptcode']."&refno=".$postData['refno']."&cmd=".$postData['cmd']."&cust_code=".$postData['cust_code']."";
		$post_function  = "Checkvalidation";
		//$cond = array("VehicleNo" => $postData['vehicle_no'],"doctype"=>$postData['doctype'],"deptcode" => $postData['deptcode'], "refno" => $postData['refno'], "cmd" => $postData['cmd'] , "cust_code" => $postData['cust_code']);
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success","result_val"=> $result_array));exit;
	}
	
	function alpineGetVehicle(){
		//print_r();
		$postData = $_POST;  
		//$curl_post = "VehicleNo=".$postData['VehicleNo']."&doctype=".$postData['doctype']."&deptcode=".$postData['deptcode']."&refno=".$postData['refno']."&cmd=".$postData['cmd']."&cust_code=".$postData['cust_code']."";
		$post_function  = "GetVeh";
		//$cond = array("VehicleNo" => $postData['vehicle_no'],"doctype"=>$postData['doctype'],"deptcode" => $postData['deptcode'], "refno" => $postData['refno'], "cmd" => $postData['cmd'] , "cust_code" => $postData['cust_code']);
		$xml_response = $this->alpine_model->CheckAlphineService($postData,$post_function);
		$result_array  = $this->convert_to_json_array($xml_response);
		$this->response(array("status"=>"success","result_val"=> $result_array));exit;
	}
	
	function convert_to_json_array($xml_response){
		$xml_snippet = simplexml_load_string($xml_response);
		
		$json_convert = json_encode($xml_snippet);
		
		$json = json_decode( $json_convert );
		
		$array_response = array();
		
		foreach($json as $oEntry => $val){ 
			
				if(is_array($val)){
				
					$decode = simplexml_load_string($val);
					foreach ($decode as $d):
						foreach ($d as $k => $v):
							$array_response[$k] =  trim((string)$v);
						endforeach;
					endforeach;
					
					return $array_response;
				}
				else{
					return $val;
				}
		}
		
	}
	
	function postAndGetDataWithAlpineService($POST,$post_function){
		
		$xml_response = $this->alpine_model->CheckAlphineService($POST, $post_function);
		//print_r($xml_response);exit;
		$result_array = $this->convert_to_json_array($xml_response);
		//print_r($result_array);
		return array("status"=>"success", "result_val" => $result_array, "result_msg" => $result_array);
	} 
	
	function postAndGetDataWithAlpineServiceForXMLAarray($POST,$post_function){
		
		$xml_response = $this->alpine_model->CheckAlphineService($POST, $post_function);
		
		return array("status"=>"success", "result_val" => $xml_response);
	} 
	
	function getCurrentBookingVehicle(){
		
		$POST['coyId'] = 'OPEL';
		$POST['docId'] = 'WSB';
		$POST['deptId'] = 'ASC';
		
		$first = date('Y-m-d',strtotime('last Sunday'));
		$last = date('Y-m-d',strtotime('next Saturday'));
		
		$POST['startWeek'] = $first.'T00:00:00.000';
		$POST['endWeek'] = $last.'T00:00:00.000';
		$POST['appVehNo'] = 'SKG2091G';
		
		$currentBooking = $this->soapRequest('GetCurrentVehicleBooking',$POST);
		
		print_r($currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram']['NewDataSet']['Table']);die;
		
	} 
	
	function getUnAvailableTimeSlot(){
		if(is_ajax_request())
		{
			
			$first = date('Y-m-d',strtotime('last Sunday'));
			$last = date('Y-m-d',time());
			$lastTime = date('H:i:s',time()); 
		//	echo $last."T".$lastTime;
			$POST['startDate'] = $first.'T08:00:00';
			$POST['endDate'] = $last.'T'.$lastTime;
			
			$response = $this->soapRequest('GetUnExistTimeSlots',$POST);
			
			//print_r($response);die;
			
			$arr_block = array();
			$arr_push = array();
			$k=1;
			
			$farr = $response['result_soap']['GetUnExistTimeSlotsResult']['diffgram']['NewDataSet']['Table'];
			//print_r($farr);die;
			foreach($farr as $child): 
					 $arr_block[] = array(
                'title' => 'Blocked',
                'start' => $child['FR_TIME'],
                'end' => $child['TO_TIME'],
                //'rendering'=> 'background',
                'color' =>'#CCC !important',
                'editable' => false,
                //'multi' => 0,
                //"body" => "",
                //"allDay" => false,
                //"extension_id" => 2,
               
            );
					/*$value_from =  $child['FR_TIME'];
					$value_to =  $child['TO_TIME'];					
					//$arr_block['selectable'] = false;
					$arr_block['editable'] = false;
					$arr_block['multi'] = 0;
					$arr_block['title'] = 'blocked';
					$arr_block['start'] =  $value_from;
					$arr_block['end'] =  $value_to;
					$arr_block['rendering'] =  "background";
					$arr_block['color'] =  "grey";
					$k++; */
					
					//array_push($arr_push,$arr_block);
			endforeach;
		//	die;
			//print_r($arr_push);die;
			
		}
		
		if(!empty($arr_block)){
			echo json_encode(array("array_block"=>$arr_block));exit;
		}
		else{
			echo json_encode(array( "status" => "success", "from_time" => "", "to_time" => ""));exit;
		}
	}

	
	function bookingSubmitNext(){
		if(is_ajax_request())
		{
			$postData = $_POST;
			//print_r($postData);die;
			
			$arrBookingData = array();
			$arrBookingData['booking_date'] = $postData['book_slot'];
			$arrBookingData['booking_milage'] = $postData['booking_milage']."K";
			$arrBookingData['remarks'] = $postData['remarks'];
			$arrBookingData['ref_no'] = $postData['ref_no'];
			
			echo json_encode(array( "status" => "success", "booking_info" => $arrBookingData));exit;
		}
	}
	
	function bookingConfirm(){
			if(is_ajax_request())
			{
				$postData = $_POST;
				$userdata = $this->session->userdata("auth_opeluser");
				$this->checkUserStatusOpel($userdata['id']);
				$POST['key']  = API_KEY;
				//print_r($postData);die;
				$join = array(TB_VEHICLE_DETAILS => TB_VEHICLE_OWNER_DETAILS.".customer_id = ".TB_VEHICLE_DETAILS.".customer_id");
				$cond_veh = array("registration_no" => $postData['veh_no']);
				$custInfo = $this->users_model->getCustomerInfo($cond_veh,$join); 
				$POST['cust_code']  = $custInfo[0]['cust_code'];
				$POST['veh_no'] = $postData['veh_no'];
				
				
				if($postData['book_slot'] != ""){
				
				
				/*check whether 5 max booking */
				
				$POST_MAX['vehDate'] = $postData['book_slot'];
				$POST_MAX['frTime'] =  $postData['from_time'];
				$POST_MAX['toTime'] = $postData['from_time'];
				$POST_MAX['coyID'] = 'OPEL';
				$POST_MAX['deptID'] = 'ASC';
				$POST_MAX['runNo'] = '';
				
				
				$maxBookingTimeslot = $this->soapRequest('GetMaxSlotBooking',$POST_MAX);
				
				$totalbookings = $maxBookingTimeslot['result_soap']['GetMaxSlotBookingResult'];
				
				//echo $POST['vehicle_recv_date'] =  date('Y-m-dTh:i:s',strtotime($postData['book_slot']));//2016-05-20T08:15:00
				
				if($totalbookings<=5){
					$custData = $this->soapRequest('GetCust',$POST);
					$vehData = $this->soapRequest('GetVeh',$POST); 
					
					$arrBookingData = array();
					$xmlCust = new SimpleXMLElement($custData['result_soap']['GetCustResult']); 
					$xmlCust->asXML();
					$xmlVeh = new SimpleXMLElement($vehData['result_soap']['GetVehResult']); 
					$xmlVeh->asXML();
					
					if($postData['book_action'] == "I"){
						$ref_no  =  '';
						$doctype  = "WSB";
						$doccode  = "ASC";
						$is_cur_booking = "N";
						$action = "I";
					}
					elseif($postData['book_action'] == "U"){
						$ref_no  =  $postData['ref_no'];
						
						$doctype  = "WSB";
						$doccode  = "ASC";
						$is_cur_booking = "Y";
						$action = "U";
					}
					else{
						$ref_no='';
						$doctype  = "WSB";
						$doccode  = "ASC";
						$is_cur_booking = "N";
						$action = "I";
					}
					
					$arrBookingData['COMP_CODE'] = trim((string)$xmlCust->GetCust->COMP_CODE);
					$arrBookingData['DOC_TYPE'] = $doctype;
					$arrBookingData['DEPT_CODE'] = $doccode;
					$arrBookingData['REF_NO'] = $ref_no;
					$arrBookingData['VEH_NO'] = $postData['veh_no'];
					$arrBookingData['CUST_CODE'] = trim((string)$xmlCust->GetCust->CUST_CODE);
					$arrBookingData['CUST_NAME'] = trim((string)$xmlCust->GetCust->CUST_NAME);
					$arrBookingData['BOOK_BY_NAME'] = trim((string)$xmlCust->GetCust->CUST_NAME);
					$arrBookingData['TXT_MLEG'] = "";
					$arrBookingData['MLEG_IN'] = $postData['booking_milage'];
					$arrBookingData['PC_IND'] = "N";
					$arrBookingData['PYMT_MODE'] = trim((string)$xmlCust->GetCust->PYMT_MODE);
					$arrBookingData['VEH_RECV_DATE'] = $postData['book_slot'];
					$arrBookingData['VEH_GRP'] = trim((string)$xmlVeh->Getveh->VEH_GRP);
					$arrBookingData['MAKE_MDL'] = trim((string)$xmlVeh->Getveh->MAKE_MDL);
					$arrBookingData['COLR_CODE'] = trim((string)$xmlVeh->Getveh->COLR_CODE);
					$arrBookingData['YR_MNFC'] = trim((string)$xmlVeh->Getveh->YR_MNFC);
					$arrBookingData['CHAS_NO'] = trim((string)$xmlVeh->Getveh->CHAS_NO);
					$arrBookingData['ENG_NO'] = trim((string)$xmlVeh->Getveh->ENG_NO);
					$arrBookingData['REG_DATE'] = trim((string)$xmlVeh->Getveh->REG_DATE);
					$arrBookingData['CUST_BLK'] = trim((string)$xmlCust->GetCust->BLDG_NO);
					$arrBookingData['CUST_FLR'] = trim((string)$xmlCust->GetCust->FLR_NO);
					$arrBookingData['CUST_UNIT'] = trim((string)$xmlCust->GetCust->UNIT_NO);
					$arrBookingData['CUST_ADDR'] = trim((string)$xmlCust->GetCust->ST_NAME);
					$arrBookingData['CUST_CTY'] = trim((string)$xmlCust->GetCust->CUST_CTY);
					$arrBookingData['CUST_POSTAL_CODE'] = trim((string)$xmlCust->GetCust->CUST_CTY);
					$arrBookingData['CUST_DISTRICT'] = "";
					$arrBookingData['CUST_HOME'] = "";
					$arrBookingData['CUST_HDPH'] = trim((string)$xmlCust->GetCust->TEL_NO);
					$arrBookingData['CUST_OFCE'] = trim((string)$xmlCust->GetCust->COLR_3);
					$arrBookingData['CUST_PGR'] = trim((string)$xmlCust->GetCust->COLR_5);
					$arrBookingData['CUST_EMAIL'] = trim((string)$xmlCust->GetCust->CUST_EMAIL);
					$arrBookingData['REMK'] = $postData['remarks'];			
					$arrBookingData['STS'] = "I";
					$arrBookingData['RSRV_CHAR_FIELD_1'] = trim((string)$xmlVeh->Getveh->RSRV_CHAR_FIELD_1);
					$arrBookingData['RSRV_CHAR_FIELD_2'] = "Y";
					$arrBookingData['RSRV_CHAR_FIELD_6'] = trim((string)$xmlCust->GetCust->COLR_4);
					$arrBookingData['RSRV_CHAR_FIELD_7'] = "Not Exist";
					$arrBookingData['RSRV_CHAR_FIELD_8'] = "S";
					$arrBookingData['CLOSED_FLAG'] ="N";
					
					//print_r($arrBookingData);die; 
					$xmlb = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><Data></Data>");
					$node = $xmlb->addChild('Booking'); 
					// function call to convert array to xml
					$this->array_to_xml($arrBookingData, $node);
					// display XML to screen
					$infoBooking  =  $xmlb->asXML();
					//print_r($infoBooking);die;
					$PostBooking["key"] = $POST['key'];
					$PostBooking["is_current_booking"] = $is_cur_booking;
					$PostBooking["action"] = $action;
					$PostBooking["info"] = $infoBooking;
					
					//print_r($PostBooking);
					//die;
					$responseBooking = $this->soapRequest('SetAlpineBooking',$PostBooking);
					
					//print_r($responseBooking);die;
					
					//die();
					if($responseBooking['result_soap']['SetAlpineBookingResult'] !=""){
						 
						if (strpos($responseBooking['result_soap']['SetAlpineBookingResult'], 'Booking is submitted successfully') !== false) {
								
							$config['mailtype'] ='html';
							$config['charset'] ='iso-8859-1';
							$this->email->initialize($config);
							 $from  = EMAIL_FROM; 
							 $vehicle_received_date = substr($postData['book_slot'],0,10);
							 $vehicle_received_time = substr($postData['book_slot'],11,16);
							 $vechiel_rec_date = $vehicle_received_date." At ".$vehicle_received_time;
							 $ref = explode(",",$responseBooking['result_soap']['SetAlpineBookingResult']);
							 $ref_no = $ref[1];
							 $veh_no = $postData['veh_no'];
							 $remarks = $postData['remarks'];
							 $milage = $postData['booking_milage']."K";
							 $uname = trim((string)$xmlCust->GetCust->CUST_NAME);
							 $useremail = $userdata['email_address'];
							 //$useremail = 'mat@mdsm-consulting.com';
							 //$useremail = 'car1@mdsm-consulting.com';
							 //$useremail = 'swapnil.t@exceptionaire.co';
							 //$useremail_cc = 'sunny@exceptionaire.co';
							 //$this->email->to("sunny@exceptionaire.co");
							if($postData['book_action'] == "I"){
								
								 $this->messageBody .= email_header();
								 $this->messageBody  .= "Hello $uname,<br/><br/>
									Your OPEL service booking has been done.<br/>
									Your booking details are - <br/><br/>
									Vehicle Received Date And Time : $vechiel_rec_date<br/>
									Reference Number :  $ref_no<br/>
									Vehicle No : $veh_no<br/>
									Mileage : $milage<br/>
									";
								$this->messageBody .= email_footer();     
								//echo $this->messageBody;
								//die;
								
								 $this->email->from($from, 'OPEL360');
								 $this->email->to("$useremail");
								// $this->email->cc("$useremail_cc");
								 $this->email->subject('OPEL360 - New Booking');
								 $this->email->message($this->messageBody);	
									 
								 if ( $this->email->send()){
										$mail_status = "Booking details are sent to your registered email address";
								 }else{
										$mail_status = "Booking has been done but email can not be send.";
								 }
								
							}
							elseif($postData['book_action'] == "U"){
								 
								 $this->messageBody .= email_header();
								 $this->messageBody  .= "Hello $uname,<br/><br/>
									Your OPEL service booking has been changed.<br/>
									Your booking details are - <br/>
									Vehicle Received Date And Time : $vechiel_rec_date<br/>
									Reference Number :  $ref_no<br/>
									Vehicle No : $veh_no<br/>
									Mileage : $milage<br/>
									";
								$this->messageBody .= email_footer();     
								//echo $this->messageBody;
								//die;
								
								 $this->email->from($from, 'OPEL360');
								 
								 //$useremail = trim((string)$xmlCust->GetCust->CUST_EMAIL);
								 
								 $this->email->to("$useremail");

								 $this->email->subject('OPEL360 - Booking Changed');
								 $this->email->message($this->messageBody);	
									 
								 if ( $this->email->send()){
										$mail_status = "Booking details are sent to your registered email address";
								 }else{
										$mail_status = "Booking has been changed but email can not be send.";
								 }
							}
							elseif($postData['book_action'] == "C"){
			 
								 $this->messageBody .= email_header();
								 $this->messageBody  .= "Hello $uname,<br/><br/>
									You have cancelled your OPEL service booking<br/>
									Cancellation details are - 
									Reference Number :  $ref_no<br/>
									Vehicle No : $veh_no<br/>
									";
								$this->messageBody .= email_footer();     
								//echo $this->messageBody;
								//die;
								
								 $this->email->from($from, 'OPEL360');
								 
								 //$useremail = trim((string)$xmlCust->GetCust->CUST_EMAIL);
								 
								 $this->email->to("$useremail");

								 $this->email->subject('OPEL360 - Booking Cancelled');
								 $this->email->message($this->messageBody);	
									 
								 if ( $this->email->send()){
										$mail_status = "The booking has been cancelled and Cancellation details are sent to your registered email address";
								 }else{
										$mail_status = "Booking has been cancelled but email can not be send.";
								 }
								 echo json_encode(array("status"=>"success","message"=> $mail_status));exit;
							}
							
							 $res_book = explode(",",$responseBooking['result_soap']['SetAlpineBookingResult']);
							 
							 
							 if($res_book[0]!="" && $res_book[1] !=""){
								$msg_booking = "<div class='alert alert-success'>".$res_book[0]." <br/> Reference Number is : ".$res_book[1].""."<br/>$mail_status"; 
							 }
							 else{
								$msg_booking = "<div class='alert alert-danger'>Something went wrong! Booking failed.</div>"; 
							 }
							echo json_encode(array( "status" => 'success', "message_booking" =>$msg_booking ));exit;
						 }
						 else{							 
							 $msg_failed = "<div class='alert alert-danger'>".$responseBooking['result_soap']['SetAlpineBookingResult']."</div>"; 
							 echo json_encode(array( "status" => 'success', "message_booking" =>$msg_failed ));exit;
						 }
					}
					else{
						echo json_encode(array( "status" => 'failed', "message_booking" => "<div class='alert alert-danger'>Oops !!! Someting went wrong</div>"));exit;
					}
				}
				else{
						echo json_encode(array( "status" => 'failed', "message_booking" => "<div class='alert alert-danger'>Maximum 5 bookings are allowed for this date and time.<br/>Please select another time slot to booked.</div>"));exit;
				}
			}
			else{
				echo json_encode(array( "status" => 'failed', "message_booking" => "<div class='alert alert-danger'>Please select a valid booking date</div>"));exit;
			}
		}
	}
	
	
	function array_to_xml($array, &$xml) {
		foreach($array as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml->addChild("$key");
					array_to_xml($value, $subnode);
				} else {
					array_to_xml($value, $xml);
				}
			} else {
				$xml->addChild("$key","$value");
			}
		}
	}
	
	function bookingAction(){
		if(is_ajax_request())
		{
			$postData = $_POST;
			
			$join = array(TB_VEHICLE_DETAILS => TB_VEHICLE_OWNER_DETAILS.".customer_id = ".TB_VEHICLE_DETAILS.".customer_id");
			$cond_veh = array("registration_no" => $postData['veh_no']);
			$custInfo = $this->users_model->getCustomerInfo($cond_veh,$join);
			
			$POST['key']  = API_KEY;
			
			switch($postData['book_action']){ 
				
				case 'I':
					//$this->session->set_userdata(); 
				
						//print_r($custInfo);die;
						
						$POST['cust_code']  = $custInfo[0]['cust_code'];
						$POST['veh_no']  = $postData['veh_no'];
						 
						/*check whether the booking is available for the current vehicle or not*/
						
						$POST['doc_type'] = 'WSB';
						$POST['dept_code'] = 'ASC';
						
						//$custData = $this->postAndGetDataWithAlpineService($POST,'GetCust');
						//$checkBooking = $this->postAndGetDataWithAlpineService($POST,'GetCalendar');
						
						$custData = $this->soapRequest('GetCust',$POST);
						$checkBooking = $this->soapRequest('GetValidity',$POST);
						
						//print_r($custData);
						//print_r($checkBooking);
						//die;
						
						if($checkBooking['result_soap']['GetValidityResult'] == ""){
						
							$xml = new SimpleXMLElement($custData['result_soap']['GetCustResult']); 
							$xml->asXML();
			 
							$CUST_NAME =   ($xml->GetCust->CUST_NAME?$xml->GetCust->CUST_NAME:"-");
							$TEL_NO    =   ($xml->GetCust->TEL_NO?$xml->GetCust->TEL_NO:"-"); 
							
							$cust_html = '<div class="form-group select_drop">
													<label class="col-md-5">Customer Name - </label>
													<div class="col-md-7 rt_cust">
														<div class="view_text">'.$CUST_NAME.'</div>
													</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Vehicle No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$postData['veh_no'].'</div>
												</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Customer HP No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$TEL_NO.'</div>
												</div>
										</div>';
							echo json_encode(array( "status" => "success", "cust_info" => $custInfo[0], "customer_html" => $cust_html ,"booking_action" => "I" ,"message" => ""));exit;			
						}
						else{
							
							$first = date('Y-m-d',strtotime('-3 days'));
							$last = date('Y-m-d',strtotime('+30 days'));
							$POST_EXIST['coyId'] = 'OPEL';
							$POST_EXIST['docId'] = 'WSB';
							$POST_EXIST['deptId'] = 'ASC';
							$POST_EXIST['startWeek'] = $first.'T00:00:00.000';
							$POST_EXIST['endWeek'] = $last.'T00:00:00.000';
							$POST_EXIST['appVehNo'] = $postData['veh_no'];
							//print_r($POST_EXIST);die;
							$currentBooking = $this->soapRequest('GetCurrentVehicleBooking',$POST_EXIST);
							$revArr = $currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram']['NewDataSet']['Table'];
							//print_r($respBook);//die;
							//$revArr = array_reverse($respBook);
							
							//print_r($revArr);
							
							//echo $revArr[0]['START_DATE'];
							//echo sizeof($revArr);die;
							
							
							if($revArr[0]){
								//echo "test1";
								if(isset($revArr[0]['START_DATE'])){
									$cur_booking_flag=0;
									$vehicle_received_date = substr($revArr[0]['START_DATE'],0,10);
									$vehicle_received_time = substr($revArr[0]['START_DATE'],11,16); 
									
									if($vehicle_received_date >= $first){ 
										$vehicle_received_time = explode("+",$vehicle_received_time);
										$vehicle_rec_time = $vehicle_received_time[0];
										$cur_booking_flag=1;
									}
								}
								else{
									$cur_booking_flag=0;
									$vehicle_received_date = substr($revArr[0]['VEH_RECV_DATE'],0,10);
									$vehicle_received_time = substr($revArr[0]['VEH_RECV_DATE'],11,16);
									if($vehicle_received_date >= $first){
										
										$vehicle_received_time = explode("+",$vehicle_received_time);
										$vehicle_rec_time = $vehicle_received_time[0];
										$cur_booking_flag=1;
									}
								}
								if($cur_booking_flag == 1){
									$ref_no = $revArr[0]['RUN_NO'];
									$html_booking_info .= "<div class='alert alert-info'><b class='latest'>Latest Booking Info</b>&nbsp;:<br/><div class='book_dt'>Booking Date and Time : $vehicle_received_date at $vehicle_rec_time</div><div class='book_dt'>Reference Number : $ref_no</div></div>";
									echo json_encode(array("status"=>"failed", "result_val"=> $html_booking_info)); exit;
									
								}
								else{
									$html_booking_info = "<div>No records available for current booking.</div>";
									echo json_encode(array("status"=>"failed", "result_val"=> $html_booking_info)); exit;
								} 
							}
							else{
								//echo "test2";
								if(isset($revArr['START_DATE'])){
									$cur_booking_flag=0;
									$vehicle_received_date = substr($revArr['START_DATE'],0,10);
									$vehicle_received_time = substr($revArr['START_DATE'],11,16);
									if($vehicle_received_date >= $first){
										$vehicle_received_time = explode("+",$vehicle_received_time);
										$vehicle_rec_time = $vehicle_received_time[0];
										$cur_booking_flag=1;
									}
								}
								else{
									$cur_booking_flag=0;
							
									$vehicle_received_date = substr($revArr['VEH_RECV_DATE'],0,10);
									$vehicle_received_time = substr($revArr['VEH_RECV_DATE'],11,16);
									
									if($vehicle_received_date >= $first){
										
										$vehicle_received_time = explode("+",$vehicle_received_time);
										$vehicle_rec_time = $vehicle_received_time[0];
										$cur_booking_flag=1;
									}
								}
								if($cur_booking_flag == 1){
									$ref_no = $revArr['RUN_NO'];
									$html_booking_info .= "<div class='alert alert-info'><b class='latest'>Latest Booking Info</b>&nbsp;:<br/><div class='book_dt'>Booking Date and Time : $vehicle_received_date at $vehicle_rec_time</div><div class='book_dt'>Reference Number : $ref_no</div></div>";
									echo json_encode(array("status"=>"failed", "result_val"=> $html_booking_info)); exit;
									
								}
								else{
									$html_booking_info = "<div>No records available for current booking.</div>";
									echo json_encode(array("status"=>"failed", "result_val"=> $html_booking_info)); exit;
								}
							}
							
						}	
					break;
					
					case 'U':
						$POST['doctype'] = "WSB";
						$POST['deptcode']  = "ASC";
						$POST['cust_code']  = $custInfo[0]['cust_code'];
						$POST['VehicleNo']  = $postData['veh_no'];
						$POST['refno'] = $postData['ref_no'];
						$POST['cmd'] = $postData['book_action'];
						$resp = $this->soapRequest('Checkvalidation',$POST);
						$custData = $this->soapRequest('GetCust',$POST);
						//print_r($resp);die;
						$msg = "";
						
						if($resp['result_soap']['CheckvalidationResult'] != ""){
							$resp_msg = $resp['result_soap']['CheckvalidationResult'];					
							$status = "failed";
							echo json_encode(array( "status" => $status,"veh_no"=> $postData['veh_no'],"message"=>'<div class="alert alert-danger">'.$resp_msg.'</div>'));exit;
						}
						else{
							$POST['coyId'] = 'OPEL';
							$POST['docId'] = 'WSB';
							$POST['deptId'] = 'ASC';
							$xmlc = new SimpleXMLElement($custData['result_soap']['GetCustResult']); 
							$xmlc->asXML();
							$CUST_NAME =   ($xmlc->GetCust->CUST_NAME?$xmlc->GetCust->CUST_NAME:"-");
							$TEL_NO    =   ($xmlc->GetCust->TEL_NO?$xmlc->GetCust->TEL_NO:"-"); 
							
							$cust_html = '<div class="form-group select_drop">
													<label class="col-md-5">Customer Name - </label>
													<div class="col-md-7 rt_cust">
														<div class="view_text">'.$CUST_NAME.'</div>
													</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Vehicle No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$postData['veh_no'].'</div>
												</div>
										  </div>
										  <div class="form-group select_drop">
												<label class="col-md-5">Customer HP No. - </label>
												<div class="col-md-7 rt_cust">
													<div class="view_text">'.$TEL_NO.'</div>
												</div>
										</div>';
							
							
							$first = date('Y-m-d',strtotime('-3 days'));
							$last = date('Y-m-d',strtotime('+30 days'));
							
							/*post parameter for remarks ad milage*/
							$POST_REM['run_no'] = $postData['ref_no'];
							$POST_REM['key'] = $POST['key'];
							$respRem = $this->soapRequest('GetRemarkinfo',$POST_REM);
							
							$POST_REM['doc_type'] = 'WSB';
							$POST_REM['dept_code'] = 'ASC';
							
							//print_r($POST_REM);
							$respBookingInfo = $this->soapRequest('GetBookinginfo',$POST_REM); 
							
							//print_r($respBookingInfo);die;
							
							$xml = new SimpleXMLElement($respRem['result_soap']['GetRemarkinfoResult']); 
							$xml->asXML();
							$xmlBooking = new SimpleXMLElement($respBookingInfo['result_soap']['GetBookinginfoResult']); 
							$xmlBooking->asXML();
							//print_r($xml);die;
							
							$remark = (trim((string)$xml->Remark->REMK)?trim((string)$xml->Remark->REMK):"");
							$bookingDateTime = (trim((string)$xmlBooking->BookInfo->BookingDateTim)?trim((string)$xmlBooking->BookInfo->BookingDateTim):"");
							$milage = (trim((string)$xmlBooking->BookInfo->MLEG_IN)?trim((string)$xmlBooking->BookInfo->MLEG_IN):"");
							//die;
							$status = "success";
							if($resp_msg){
								$resp_msg = '<div class="alert alert-success">'.$resp_msg.'</div>';
							}
							else{
								$resp_msg ='';
							}
							echo json_encode(array( "status" => $status,"ref_no" => $postData['ref_no'], "veh_no"=> $postData['veh_no'],"customer_html"=>$cust_html, "booking_action" => "U","booked_date" => $bookingDateTime, "milage" => $milage, "remark" => $remark,"message"=>''));exit;
						}
						
					
					break;
					case 'C':
						$POST['doctype'] = "WSB";
						$POST['deptcode']  = "ASC";
						$POST['cust_code']  = $custInfo[0]['cust_code'];
						$POST['VehicleNo']  = $postData['veh_no'];
						$POST['refno'] = $postData['ref_no'];
						$POST['cmd'] = $postData['book_action'];
						$resp = $this->soapRequest('Checkvalidation',$POST);
						
						//print_r($resp);die;
						$msg = "";
						
						if($resp['result_soap']['CheckvalidationResult'] != ""){
							$resp_msg = $resp['result_soap']['CheckvalidationResult'];					
							$status = "failed";
						}
						else{
												
							$POST['coyId'] = "OPEL";
							$POST['docType'] = "WSB";
							$POST['deptCode'] = "ASC";
							$POST['runNo'] = $postData['ref_no'];
							
							
							$arrBookingData = array();
							
							$arrBookingData['DOC_TYPE'] = $POST['docType'];
							$arrBookingData['DEPT_CODE'] = $POST['deptCode'];
							$arrBookingData['REF_NO'] = $POST['runNo'];
							$arrBookingData['VEH_NO'] = $POST['VehicleNo'];
							$arrBookingData['CUST_CODE'] = $POST['cust_code'];
							
							$xmlb = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><Data></Data>");
							$node = $xmlb->addChild('Booking'); 
							// function call to convert array to xml
							$this->array_to_xml($arrBookingData, $node);
							// display XML to screen
							$infoBooking  =  $xmlb->asXML();
							//print_r($infoBooking);die; 
							$PostDelBooking["info"] = $infoBooking;
							$PostDelBooking['key'] = $POST['key'];
							//print_r($PostBooking);
							//die;
							$respDelBooking = $this->soapRequest('DeleteAlpineBooking',$PostDelBooking);
							
							//print_r($respDelBooking);die;
							
							if($respDelBooking['result_soap']['DeleteAlpineBookingResult'] != ""){
								$resp_msg = $respDelBooking['result_soap']['DeleteAlpineBookingResult'];
							}
							
							$status = "success";
							echo json_encode(array( "status" => $status,"veh_no"=> $postData['veh_no'], "customer_html" => $cust_html ,"booking_action" => "C", "message" => '<div class="alert alert-success">'.$resp_msg.'</div>'));exit;
						}
						
						echo json_encode(array( "status" => $status,"veh_no"=> $postData['veh_no'], "customer_html" => $cust_html ,"booking_action" => "C", "message" => '<div class="alert alert-danger">'.$resp_msg.'</div>'));exit;
						
					//}
					
					break; 	
			}
		}
	}
	 
	
	
	function Tickets(){
		 $data['title'] = 'Ticket';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['main_content'] = 'frontend/ticket';
		 $userdata = $this->session->userdata("auth_opeluser"); 
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	function postTickets(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			
			//print_r($postData);die;
			
			$pdata = array("user"=>1,  "user_firstname" => $postData['user_firstname'] , "user_email" => $postData['user_email'], "department" => 1, "status" => 1, "priority" => $postData['priority'],"subject" => $postData['subject'],"text" => $postData['text_msg'],"send_user_email"=>1);
			
			
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'POST');
			
			//print_r($result);die;
			echo $result;exit;
		}
	}
	
	
	function doAPICall($apiCall, $data = array(), $method = 'GET')
	{
		// Variables
		
		$baseUrl = 'http://autogermany.com.sg/support';
		$apiToken = 'ebCVwM9StQM7tGfTOPl&eQ78kEvKeSn3';
	 
		// Start cURL
		$c = curl_init();
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER,0); 
		curl_setopt($c, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($c, CURLOPT_USERPWD, $apiToken . ":changeme");
		
		// Start building the URL
		$apiCall = $baseUrl . $apiCall;
		
		
		// Check what type of API call we are making
		if ($method == 'GET') {
	 
			// Add the array of data to the URL
			if (is_array($data)) {
				$apiCall .= "?";
				foreach ($data AS $key => $value) {
					if (isset($value)) {
						$apiCall .= $key . "=" . $value . "&";
					}
				}
	 
				// Remove the final &
				$apiCall = rtrim($apiCall, '&');
			}
	 
		} else if ($method == 'POST' || $method == 'DELETE') {
	 
			// PUT and DELETE require an $id variable to be appended to the URL
			if (isset($data['id'])) {
				$apiCall .= "/" . $data['id'];
			}
			
			
			
			// Setup the remainder of the cURL request
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($c, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: ' . $method));
		} else {
	 
			// Setup the remainder of the cURL request
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
	 
		}
		// Set the URL
		curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($c, CURLOPT_URL, $apiCall);
	 
		// Execute the API call and return the response
		$result = curl_exec($c);
		//print_r($result); exit;
		curl_close($c);
		// Return the results of the API call
		
		return $result;
	 
	}
	
	
	//Change event status
	
	public function changeEventStatus()
	{
		$current_date = date('Y-m-d'); //2016-04-20
		//Get all events
		$cond = array();
		$like = array();
		$events = $this->events_model->getEventDetailsById($cond);
		
		foreach($events as $event) {
			$event_id = $event['id'];
			//Get event details
			$cond = array("event_id" => $event_id);
			$event_details = $this->events_model->getEventDetailsById($cond);
			//print_r($event_details); die;
			$event_start_date = $event_details[0]['start_date'];
			$event_end_date = $event_details[0]['end_date']; 
			//Check closed events
				if(strtotime($current_date) > strtotime($event_end_date)) {
					//change event status to closed
					$cond1 = array("event_id" => $event_details[0]['id']);
					$updateevent = $this->common_model->update(TB_EVENTS,$cond1,array("status" => "Closed"));
				}
				
			//Check for Runing events
				if(strtotime($current_date) >= strtotime($event_start_date) && strtotime($current_date) <= strtotime($event_end_date)) {
					//change event status to Runing
					$cond1 = array("event_id" => $event_details[0]['id']);
					$updateevent = $this->common_model->update(TB_EVENTS,$cond1,array("status" => "Running"));
				}
		}
	}
	
	
	function postToAlpineWithSoapCurl(){
		//$dataFromTheForm = $postdata; // request data from the form
        $soapUrl = "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx?op=SetAlpineBooking"; // asmx URL of WSDL
        
        // xml post structure
		
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                              <soap:Body>
                                <SetAlpineBooking xmlns="http://www.starvisionit.com/webservices/">
                                 <is_current_booking>Y</is_current_booking>
								  <action>I</action>
								 <info><Data><booking><COMP_CODE>OPEL</COMP_CODE><DOC_TYPE>WSB</DOC_TYPE><DEPT_CODE>ASC</DEPT_CODE><REF_NO>
</REF_NO><VEH_NO>SKG2091G</VEH_NO><CUST_CODE>1200003458</CUST_CODE><CUST_NAME>MOHAMMAD ADIB BIN MOHAMMAD
 HAMBER</CUST_NAME><TXT_MLEG></TXT_MLEG><MLEG_IN>1000</MLEG_IN><PC_IND>N</PC_IND><PYMT_MODE></PYMT_MODE
><VEH_RECV_DATE>2016-05-18T08:15:00</VEH_RECV_DATE><VEH_GRP>ZAFIRA B</VEH_GRP><MAKE_MDL>ZAFIRA 1.4 AUTO
 TURBO PANO</MAKE_MDL><COLR_CODE>GAZ</COLR_CODE><YR_MNFC>2012</YR_MNFC><CHAS_NO>W0LPD9DC4C2103974</CHAS_NO
><ENG_NO>A14NET19AK9206</ENG_NO><REG_DATE>8/8/2012 12:00:00 AM</REG_DATE><CUST_BLK>55</CUST_BLK><CUST_FLR>02</CUST_FLR><CUST_UNIT>51</CUST_UNIT><CUST_ADDR>SIMEI RISE</CUST_ADDR>
<CUST_CTY>SG</CUST_CTY><CUST_POSTAL_CODE>528791</CUST_POSTAL_CODE><CUST_DISTRICT><CUST_DISTRICT/><CUST_HOME><CUST_HOME/><CUST_HDPH>96246005</CUST_HDPH>
<CUST_OFCE><CUST_OFCE/><CUST_PGR><CUST_PGR/><CUST_EMAIL>0.00</CUST_EMAIL><REMK>test</REMK><STS>I</STS><RSRV_CHAR_FIELD_1>NEW/NEW/12VM00143E</RSRV_CHAR_FIELD_1>
<RSRV_CHAR_FIELD_2>Y</RSRV_CHAR_FIELD_2><RSRV_CHAR_FIELD_6>96246005</RSRV_CHAR_FIELD_6><RSRV_CHAR_FIELD_7>Not Exist</RSRV_CHAR_FIELD_7><RSRV_CHAR_FIELD_8>S</RSRV_CHAR_FIELD_8><CLOSED_FLAG>N</CLOSED_FLAG></booking></Data></info>
                                 </SetAlpineBooking>
                              </soap:Body>
                            </soap:Envelope>';   // data from the form, e.g. some ID number

           $headers = array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "Accept: application/soap+xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "SOAPAction: http://www.starvisionit.com/webservices/SetAlpineBooking", 
                        "Content-length: ".strlen($xml_post_string),
                    );

            $url = $soapUrl;

            // PHP cURL  for https connection with auth
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
            

            // converting
            $response = curl_exec($ch); 
            print_r($response);die;
            curl_close($ch);
			
            // converting
            $response1 = str_replace("<soap:Body>","",$response);
            $response2 = str_replace("</soap:Body>","",$response1);

			print_r($response1);
			print_r($response2);
			die;
            // convertingc to XML
            $parser = simplexml_load_string($response2);
            // user $parser to get your data out of XML response and to display it.
    
	}
	
	
	function soapRequest($service, $params = array()){
		 require_once('library/lib/nusoap.php'); //includes nusoap
		
		$client = new nusoap_client(API_SERVICE_URL,true);

		$error = $client->getError();
		if ($error) {
			echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
		}

		$result = $client->call($service, array($params),array("SOAPAction"=>"http://www.starvisionit.com/webservices/"));

		if ($client->fault) {
			echo "<h2>Fault</h2><pre>";
			print_r($result);
			echo "</pre>";
		}
		else {
			$error = $client->getError();
			if ($error) {
				echo "<h2>Error</h2><pre>" . $error . "</pre>";
			}
			else {
					return array("result_soap"=>$result);
			}
		}
         
	}
    
    /*Car rental soap request*/
    
    
    function soapRequestCarRental($service, $params = array()){
		 require_once('library/lib/nusoap.php'); //includes nusoap
		
		$client = new nusoap_client("http://webmail.alpinemotors.sg/CR_WS_TEST/alpinecr.asmx?WSDL",true);

		$error = $client->getError();
		if ($error) {
			echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
		}

		$result = $client->call($service, array($params),array("SOAPAction"=>"http://www.starvisionit.com/webservices/"));

		if ($client->fault) {
			echo "<h2>Fault</h2><pre>";
			print_r($result);
			echo "</pre>";
		}
		else {
			$error = $client->getError();
			if ($error) {
				echo "<h2>Error</h2><pre>" . $error . "</pre>";
			}
			else {
					return array("result_soap"=>$result);
			}
		}
         
	}
    
    function testDrive(){
		//Get All Modelss
		 $cond = array();
		 $all_models = $this->catalog_model->getCatalogModelById($cond);
		 
		 for($i=0; $i<count($all_models); $i++) {
				$model_name = $all_models[$i]['m_name'];
				$model_id = $all_models[$i]['id'];
				
			 $opt .= '<option value='.$model_name.'>'.$model_name.'</option>';
		 }
		 
		 $data['title'] = 'Test Drive';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['models'] = $opt;
		 $data['main_content'] = 'frontend/test_drive';
		 $userdata = $this->session->userdata("auth_opeluser"); 
		 $this->load->view('frontend/incls/layout', $data);  
	}
   
	function request_test_drive() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			//print_r($postData); die;
			
			$customfield = array("1" => $postData['model'],"2" => $postData['title'],"3" => $postData['name'],"4" => $postData['mobno'],"5" => $postData['address'],"6" => $postData['email'],"7" => $postData['existing_model'],"8" => $postData['year_purchased'],"9" => $postData['general_com']);
			
			$table = '<br/><table border="1" cellpadding="2" cellspacing="2" class="table table-bordered" id="services_tb">
					<thead>
						<tr>							
							<th colspan="2">Test Drive Details</th>							
						</tr>
					</thead>
					<tbody>
					<tr>
							<td><strong>Name</td>
							<td class="text-center">'.($postData['name']?$postData['name']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Email Address</td>
							<td class="text-center">'.($postData['email']?$postData['email']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Model</td>
							<td class="text-center">'.($postData['model']?$postData['model']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Title</td>
							<td class="text-center">'.($postData['title']?$postData['title']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Mobile No.</td>
							<td class="text-center">'.($postData['mobno']?$postData['mobno']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Address</td>
							<td class="text-center">'.($postData['address']?$postData['address']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Existing Model</td>
							<td class="text-center">'.($postData['existing_model']?$postData['existing_model']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Year Purchased</td>
							<td class="text-center">'.($postData['year_purchased']?$postData['year_purchased']:":").'</td> 
					</tr>
					<tr>
							<td><strong>General Comments</td>
							<td class="text-center">'.($postData['general_com']?$postData['general_com']:":").'</td> 
					</tr>
					</tbody>
				</table>';
			$pdata = array( "user_firstname" => $postData['name'] , "user_email" => $postData['email'], "department" => 1, "status" => 1, "priority" => 3,"subject" => 'Request a Test Drive',"customfield" => $customfield, "text" => "Hello, <br/>Test drive is requested from a ".$postData['name'].". <br/>Details are below<br/>".$table."<br/> So please confirmed it. <br/>Thanks</br>","send_user_email"=>1);
			//print_r($pdata);die;
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'POST');
			
			echo $result;exit;
		}
	}
	
	function upgradeTrade(){
		//Get All Modelss
		 $cond = array();
		 $all_models = $this->catalog_model->getCatalogModelById($cond);
		 
		 for($i=0; $i<count($all_models); $i++) {
				$model_name = $all_models[$i]['m_name'];
				$model_id = $all_models[$i]['id'];
				
			 $opt .= '<option value='.$model_name.'>'.$model_name.'</option>';
		 }
		 
		 $data['title'] = 'Upgrade/Trade In';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['models'] = $opt;
		 $data['main_content'] = 'frontend/upgrade_trade';
		 $userdata = $this->session->userdata("auth_opeluser"); 
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	function upgrade_post() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			//print_r($postData); die;
			
			$customfield = array("1" => $postData['current_model'],"2" => $postData['year_purchased'],"3" => $postData['model_interested'],"4" => $postData['action'],"5" => $postData['name'],"6" => $postData['existing_model'],"7" => $postData['text_msg'],"8"=>$postData["mobile_number"]);
			$table = '<br/><table border="1" cellpadding="2" cellspacing="2" class="table table-bordered" id="services_tb">
					<thead>
						<tr>							
							<th colspan="2">Upgrade/Trade Details</th>							
						</tr>
					</thead>
					<tbody>
					<tr>
							<td><strong>Name</td>
							<td class="text-center">'.($postData['name']?$postData['name']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Mobile Number</td>
							<td class="text-center">'.($postData['mobile_number']?$postData['mobile_number']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Address</td>
							<td class="text-center">'.($postData['address']?$postData['address']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Current Make/Model</td>
							<td class="text-center">'.($postData['current_model']?$postData['current_model']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Email Address</td>
							<td class="text-center">'.($postData['email']?$postData['email']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Model Interested</td>
							<td class="text-center">'.($postData['model_interested']?$postData['model_interested']:":").'</td> 
					</tr>					
					<tr>
							<td><strong>Action</td>
							<td class="text-center">'.($postData['action']?$postData['action']:":").'</td> 
					</tr> 
					<tr>
							<td><strong>Year Purchased</td>
							<td class="text-center">'.($postData['year_purchased']?$postData['year_purchased']:":").'</td> 
					</tr>
					<tr>
							<td><strong>General Comments</td>
							<td class="text-center">'.($postData['text_msg']?$postData['text_msg']:":").'</td> 
					</tr>
					</tbody>
				</table>';
			$pdata = array("user_firstname" => $postData['name'] , "user_email" => $postData['email'], "department" => 1, "status" => 1, "priority" => 3,"subject" => 'Upgrade/Trade In',"customfield" => $customfield, "text" => "Hello, <br/><br/>Upgrade/trade is requested from a ".$postData['name'].". <br/>Details are below<br/><br/>".$table."<br/>So please confirmed it. <br/><br/>Thanks</br>","send_user_email"=>1);
			//print_r($pdata);die;
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'POST');
			
			echo $result;exit;
		}
	}
	
	
	function feedbackForm(){
		//Get All Modelss
		 $cond = array();
		 $all_models = $this->catalog_model->getCatalogModelById($cond);
		 
		 for($i=0; $i<count($all_models); $i++) {
				$model_name = $all_models[$i]['m_name'];
				$model_id = $all_models[$i]['id'];
				
			 $opt .= '<option value='.$model_name.'>'.$model_name.'</option>';
		 }
		 
		 $data['title'] = 'Feedback';
		 $data['meta_description'] = '';
		 $data['meta_keyword'] = '';
		 $data['models'] = $opt;
		 $data['main_content'] = 'frontend/feedback';
		 $userdata = $this->session->userdata("auth_opeluser"); 
		 $this->load->view('frontend/incls/layout', $data);  
	}
	
	
	function feedback_post() {
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			//print_r($postData); die;
			
			$customfield = array("1" => $postData['department']);
			$table = '<br/><table border="1" cellpadding="2" cellspacing="2" class="table table-bordered" id="services_tb">
					<thead>
						<tr>							
							<th colspan="2">Feedback Details</th>							
						</tr>
					</thead>
					<tbody>
					<tr>
							<td><strong>Name</td>
							<td class="text-center">'.($postData['name']?$postData['name']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Email Address</td>
							<td class="text-center">'.($postData['email']?$postData['email']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Department</td>
							<td class="text-center">'.($postData['sel_department']?$postData['sel_department']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Comments</td>
							<td class="text-center">'.($postData['text_msg']?$postData['text_msg']:":").'</td> 
					</tr>
					</tbody>
				</table>';
			$pdata = array( "user_firstname" => $postData['name'] , "user_email" => $postData['email'], "department" => 1, "status" => 1, "priority" => 3,"subject" => 'Feedback',"customfield" => $customfield, "text" => "Hello, <br/><br/>feedback from a ".$postData['name'].".<br/> Details are below<br/>".$table."<br/><br/>Thanks</br>","send_user_email"=>1);
			//print_r($pdata);die;
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'POST');
			
			echo $result;exit;
		}
	}
	
	
	function servey_user_push(){
		$get_general_srv = $this->survey_model->getGeneralSurveyInBetween();
		//echo "<pre>"; print_r($get_general_srv);
		foreach($get_general_srv as $sev){
			
			$cond_srv = array("s_id" => $sev['survey_id']);
			$srv_exits = $this->survey_model->getAllSurveyNotification($cond_srv);
			//echo "<br/>".count($srv_exits);
			//echo $this->db->last_query();//die;
			if(count($srv_exits)==0){ 
				//echo "test";
				$users_push = $this->survey_model->getAllUsersForSurveyNotification();
				
				foreach($users_push as $usr):
					$this->common_model->insert(TB_SURVEY_NOTIFICATION,array("s_id" =>$sev['survey_id'],"user_id" => $usr['user_id'],"mail_flag"=>'0',"push_android"=>'0',"push_ios"=>'0'));
				endforeach;
				
			}
		} 
		
		$last_date = date("Y-m-d");
		$get_workshop_srv = $this->survey_model->getSurveyInBetween($last_date);
		//echo "<pre>"; print_r($get_workshop_srv);
		foreach($get_workshop_srv as $sev){
			
			$cond_srv = array("s_id" => $sev['survey_id']);
			$srv_exits = $this->survey_model->getAllSurveyNotification($cond_srv);
			//echo "<br/>".count($srv_exits);
			//echo $this->db->last_query();//die;
			if(count($srv_exits)==0){ 
				//echo "test";
				$cond1 = array("transaction_date" => $last_date);
				$users_push = $this->survey_model->getAllCurrentVehicleServicedUsers($cond1);
				
				foreach($users_push as $usr):
					$this->common_model->insert(TB_SURVEY_NOTIFICATION,array("s_id" =>$sev['survey_id'],"user_id" => $usr['user_id'],"mail_flag"=>'0',"push_android"=>'0',"push_ios"=>'0'));
				endforeach;
				
			}
		} 
		
	}
	
	
	
	function survey_notification(){
		
		/*
		 * Check servey exists
		 * */
		  
		/*for general survey */
		
		$gen_survey = $this->survey_model->getGeneralSurveyInBetween();
		//print_r($gen_survey);
		if(count($gen_survey)>0){
			
			
			//$cond2 = array("user_type"=>"NONOPEL","user_type"=>"OPEL");
			//print_r($cond2);
			$users = $this->survey_model->getAllUsersForSurveyNotification();
			//echo "web<pre>";print_r($users);
			$users_mobile = $this->survey_model->getAllUsersForSurveyMobileNotification();
			//echo "mobile<pre>";print_r($users_mobile);
			//die;
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			
			foreach($gen_survey as $gen_s):
				$survey_id = $gen_s["survey_id"];
				$survey_name = $gen_s["survey_name"]; 
				$reward_points = $gen_s["award_points"];
				
				foreach($users as $user):
					$user_id = $user['user_id']; 
					
					if($user['c_name'] != ""){
						$uname = $user['c_name'];
					}
					else{
						$uname = "User";
					}
					
					$emailaddress = $user['email_address'];
					/*check survey id exists*/
					$cond_s_exists = array("s_id"=>$survey_id,"user_id"=>$user_id,"mail_flag"=>'0');
					$survey_exists = $this->survey_model->getAllSurveyNotification($cond_s_exists);
					if($emailaddress!=""){
						 $from  = EMAIL_FROM;
						 $this->messageBody = "";
						 $this->messageBody  .= email_header(); 
						 $this->messageBody  .= "Hello $uname,<br/><br/> Thank you for giving Opel360 the opportunity to service your vehicle.<br/>
						 We have created a really short survey which you can take by following below steps that will help you earn $reward_points Opel Reward Points.
						 <br/><br/>
	1. Please login to your account and click on survey menu.<br/>
	2. Start rating and add comments to answer a survey.<br/><br/>
	Thanks for taking the time to fill it out for us!";
						 $this->messageBody  .= email_footer();
						 $this->email->from($from, $from);
						 $this->email->to("$emailaddress"); 
						 $this->email->subject('Survey notification');
						 $this->email->message($this->messageBody);
						// echo SERVER_ENVIRONMENT;die;	
						if(SERVER_ENVIRONMENT == "LIVE"){
							$res_mail = $this->email->send();
							$res_mail="";
							if($res_mail){
								 /*update the mail for survey send*/
								 
								 $cond_mail  = array("s_id" => $survey_id,"user_id" => $user_id);
								 $update_email = array("mail_flag"=>'1');
								 $this->common_model->update(TB_SURVEY_NOTIFICATION,$cond_mail,$update_email);
								
							 } 
						 }
					}
				endforeach;	
				
				foreach($users_mobile as $user):
					//echo "test";print_r($user['user_id']);
					//if(count($survey_exists)>0){
					$user_ios_id = "";
					if($user['device_token'] != ""){
						$device_token = trim($user['device_token']);
						$user_ios_id = $user['user_id']; 
					} 
					$user_and_id = "";
					if($user['device_id_android'] != ""){
						$device_id = trim($user['device_id_android']);
						$user_and_id = $user['user_id'];
						
					}
					if($device_id != ""){  
						
						//echo "user_and:".$user_and_id;
						//echo "<br/>";
						
						$cond_a_exists = array("s_id" => $survey_id,"user_id"=>$user_and_id,"push_android"=>'0');
						$survey_a_exists = $this->survey_model->getAllSurveyNotification($cond_a_exists);
						$message = $survey_name;
						if(count($survey_a_exists)>0){
						 
							 //$res_android = $this->send_survey_notification_android($user_id,$survey_id);
							 
							 /*sending notification to android*/
							 
							define('API_ACCESS_KEY', 'AIzaSyD51ZoV_nwPsMzb7yX0AxVePy5TiQ2g9RA');
							 
							//$message = $msg;
							 
							$registrationIds = $device_id;
							//print_r($registrationIds);die;
							/*$registrationIds = array(
								"flECike3rY8:APA91bF1btDqwXuyW3yEwUSKjumxaUoySfanhDvWEJ5GqueGwV6Obpg6TaGAIBcZmVXiTQVRnPW8Q7TgTpjrUxTQhPIX4Gzt2TAWdKkQwRu7GSt94Y4_h3xNPlrGVHtMA1iJ-LMtd3T2"
								//"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth",
								//"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth"
							);*/
							// prep the bundle
							
							//print_r($registrationIds);die;
							$msg = array
							(
								'title'=>'Opel Survey',
								'message' 	=> $message,
							);
							$fields = array
							(
								'registration_ids' 	=> array($device_id),
								'data'			=> $msg
							);
							 
							$headers = array
							(
								'Authorization: key=' . API_ACCESS_KEY,
								'Content-Type: application/json'
							);
							 
							//print_r(json_encode( $fields )) ;die;
							$ch = curl_init();
							curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
							$res_android = curl_exec($ch );
							curl_close( $ch );
							 
							if($res_android){
								 /*update the mail for survey send*/
								 
								 $cond_and  = array("s_id" => $survey_id,"user_id" => $user_and_id);
								 $update_and = array("push_android"=>'1');
								 $this->common_model->update(TB_SURVEY_NOTIFICATION,$cond_and,$update_and);
								 
							}	
						} 
					}
				 
					if($device_token!=""){
					 
							$cond_ios_exists = array("s_id"=>$survey_id,"user_id"=>$user_ios_id,"push_ios"=>'0');
							$survey_ios_exists = $this->survey_model->getAllSurveyNotification($cond_ios_exists);
							if(count($survey_ios_exists)>0){
								 
								 //$res_ios = $this->send_survey_notification($device_token,$survey_name);
								 
									 //$deviceToken = $device_id;
									//print_r($deviceToken);die;
									//$passphrase = '123456';
									$deviceToken = $device_token;
									$passphrase = '123456'; 
									$message = $survey_name;													 
									$ctx = stream_context_create();
									stream_context_set_option($ctx, 'ssl', 'local_cert', 'O_360_DEV.pem');
									stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
																		 
									// Open a connection to the APNS server
									$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
									//print_r($fp);
									if (!$fp)
									exit("Failed to connect: $err $errstr" . PHP_EOL);
									//echo 'Connected to APNS' . PHP_EOL;

									// Create the payload body
									$body['aps'] = array('title'=>'Opel Survey','alert' => $message,'badge'=> 'Increment','sound' => 'chime');
									$payload = json_encode($body);
									//for($i=0;$i<count($deviceToken);$i++){
										$devtoken = $device_token;
										$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $devtoken)) . pack('n', strlen($payload)) . $payload;
										$result = fwrite($fp, $msg, strlen($msg));
										//echo "<br/>";echo $result;
										
									//}
									if($result){
										//echo "After send IOS:".$user_ios_id;
										//echo "<br>";
										 /*update the mail for survey send*/										 
										 $cond_ios  = array("s_id" => $survey_id,"user_id" => $user_ios_id);
										 $update_ios = array("push_ios"=>'1');
										 $this->common_model->update(TB_SURVEY_NOTIFICATION,$cond_ios,$update_ios);
									}	
									fclose($fp); 
							}  
					} 
				endforeach;
				
			endforeach;
			//die;
		}
		
	}	
	
	
	
	function survey_workshop_notification(){
		
		/*
		 * Check servey exists
		 * */
		  
		/*for general survey */ 
		$last_date = date('Y-m-d');
		$wordshop_survey = $this->survey_model->getSurveyInBetween($last_date);
		//print_r($gen_survey);
		if(count($wordshop_survey)>0){
			
			
			//$cond2 = array("user_type"=>"NONOPEL","user_type"=>"OPEL");
			//print_r($cond2);
			$users = $this->survey_model->getAllUsersForSurveyNotification();
			//echo "web<pre>";print_r($users);
			$users_mobile = $this->survey_model->getAllUsersForSurveyMobileNotification();
			//echo "mobile<pre>";print_r($users_mobile);
			//die;
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			
			
			
			foreach($wordshop_survey as $gen_s):
				$survey_id = $gen_s["survey_id"];
				$survey_name = $gen_s["survey_name"]; 
				$reward_points = $gen_s["award_points"];
				
				foreach($users as $user):
					$user_id = $user['user_id']; 
					
					if($user['c_name'] != ""){
						$uname = $user['c_name'];
					}
					else{
						$uname = "User";
					}
					
					$emailaddress = $user['email_address'];
					/*check survey id exists*/
					$cond_s_exists = array("s_id"=>$survey_id,"user_id"=>$user_id,"mail_flag"=>'0');
					$survey_exists = $this->survey_model->getAllSurveyNotification($cond_s_exists);
					if($emailaddress!=""){
						 $from  = EMAIL_FROM;
						 $this->messageBody = "";
						 $this->messageBody  .= email_header(); 
						 $this->messageBody  .= "Hello $uname,<br/><br/> Thank you for giving Opel360 the opportunity to service your vehicle.<br/>
						 We have created a really short survey you can take by following below steps that will help you earn $reward_points Opel Reward Points.
						 <br/><br/>
	1. Please login to your account and go to survey menu.<br/>
	2. Give your rating and comments to answer a survey.<br/><br/>
	Thanks for taking the time to fill it out for us!";
						 $this->messageBody  .= email_footer();
						 $this->email->from($from, $from);
						 $this->email->to("$emailaddress"); 
						 $this->email->subject('Survey notification');
						 $this->email->message($this->messageBody);	
						
						if(SERVER_ENVIRONMENT == "LIVE"){
							$res_mail = $this->email->send();
							$res_mail="";
							if($res_mail){
								 /*update the mail for survey send*/
								 
								 $cond_mail  = array("s_id" => $survey_id,"user_id" => $user_id);
								 $update_email = array("mail_flag"=>'1');
								 $this->common_model->update(TB_SURVEY_NOTIFICATION,$cond_mail,$update_email);
								 
							 }
						 } 
					}
				endforeach;	
				
				foreach($users_mobile as $user):
					//echo "test";print_r($user['user_id']);
					//if(count($survey_exists)>0){
					if($user['device_token'] != ""){
						$device_token = trim($user['device_token']);
						$user_ios_id = $user['user_id'];
					} 
					if($user['device_id_android'] != ""){
						$device_id = trim($user['device_id_android']);
						$user_and_id = $user['user_id'];
					}
					if($device_id != ""){  
						$cond_a_exists = array("s_id" => $survey_id,"user_id"=>$user_and_id,"push_android"=>'0');
						$survey_a_exists = $this->survey_model->getAllSurveyNotification($cond_a_exists);
						$message = $survey_name;
						if(count($survey_a_exists)>0){
						 
							 //$res_android = $this->send_survey_notification_android($user_id,$survey_id);
							 
							 /*sending notification to android*/
							 
							define('API_ACCESS_KEY', 'AIzaSyD51ZoV_nwPsMzb7yX0AxVePy5TiQ2g9RA');
							 
							//$message = $msg;
							 
							$registrationIds = $device_id;
							//print_r($registrationIds);die;
							/*$registrationIds = array(
								"flECike3rY8:APA91bF1btDqwXuyW3yEwUSKjumxaUoySfanhDvWEJ5GqueGwV6Obpg6TaGAIBcZmVXiTQVRnPW8Q7TgTpjrUxTQhPIX4Gzt2TAWdKkQwRu7GSt94Y4_h3xNPlrGVHtMA1iJ-LMtd3T2"
								//"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth",
								//"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth"
							);*/
							// prep the bundle
							
							//print_r($registrationIds);die;
							$msg = array
							(
								'title'=>'Opel Survey',
								'message' 	=> $message,
							);
							$fields = array
							(
								'registration_ids' 	=> array($device_id),
								'data'			=> $msg
							);
							 
							$headers = array
							(
								'Authorization: key=' . API_ACCESS_KEY,
								'Content-Type: application/json'
							);
							 
							//print_r(json_encode( $fields )) ;die;
							$ch = curl_init();
							curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
							$res_android = curl_exec($ch );
							curl_close( $ch );
							 
							if($res_android){
								 /*update the mail for survey send*/
								 
								 $cond_and  = array("s_id" => $survey_id,"user_id" => $user_and_id);
								 $update_and = array("push_android"=>'1');
								 $this->common_model->update(TB_SURVEY_NOTIFICATION,$cond_and,$update_and);
								 
							}	
						} 
					}
				 
					if($device_token!=""){
					 
							$cond_ios_exists = array("s_id"=>$survey_id,"user_id"=>$user_ios_id,"push_ios"=>'0');
							$survey_ios_exists = $this->survey_model->getAllSurveyNotification($cond_ios_exists);
							if(count($survey_ios_exists)>0){
								 
								 //$res_ios = $this->send_survey_notification($device_token,$survey_name);
								 
									 //$deviceToken = $device_id;
									//print_r($deviceToken);die;
									//$passphrase = '123456';
									$deviceToken = $device_token;
									$passphrase = '123456'; 
									$message = $survey_name;													 
									$ctx = stream_context_create();
									stream_context_set_option($ctx, 'ssl', 'local_cert', 'O_360_DEV.pem');
									stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
																		 
									// Open a connection to the APNS server
									$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
									//print_r($fp);
									if (!$fp)
									exit("Failed to connect: $err $errstr" . PHP_EOL);
									//echo 'Connected to APNS' . PHP_EOL;

									// Create the payload body
									$body['aps'] = array('title'=>'Opel Survey','alert' => $message,'badge'=> 'Increment','sound' => 'chime');
									$payload = json_encode($body);
									//for($i=0;$i<count($deviceToken);$i++){
										$devtoken =  $device_token;
										$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $devtoken)) . pack('n', strlen($payload)) . $payload;
										$result = fwrite($fp, $msg, strlen($msg));
										//echo "<br/>";echo $result;
										
									//}
									if($result){
										 /*update the mail for survey send*/										 
										 $cond_ios  = array("s_id" => $survey_id,"user_id" => $user_ios_id);
										 $update_ios = array("push_ios"=>'1');
										 $this->common_model->update(TB_SURVEY_NOTIFICATION,$cond_ios,$update_ios);
									}	
									fclose($fp); 
							}  
					} 
				endforeach;
				
			endforeach;
			//die;
		}
		
	}	
	
	
	function subscribeToMailchimp(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
				
				if(!empty($postData)){
					
					$cond = array("email_address" => $postData['email']);
					$subscription =  $this->administration_model->getNewsletterEmailAddress($cond);
					if(count($subscription)==0){
						$insertId = $this->common_model->insert(TB_NEWSLETTER_MAILCHIMP,array("email_address" => $postData['email'],"date_modified" => date('Y-m-d H:i:s')));		
					}
						/*
						 * subscribe to mailchimp newletter
						 * */
						if(count($subscription)==0){
							$this->load->library('mcapi');
							//$listID = "b2f892c659"; // obtained by calling lists();  
							$listID = "570f9d0348";
							$emailAddress = $postData['email'];  
							$rest = $this->mcapi->listSubscribe($listID, $emailAddress);  
							
							$mailchimp="";
										  
								$mailchimp .= "You have successfully subscribe to our newsletter\n";  
								echo json_encode(array('msg' => "<div class='alert alert-success'>$mailchimp</div>")); exit;		  
										  
						}
						else{
							$mailchimp .= "You are already subscribed to newsletter.";   
							echo json_encode(array('msg' => "<div class='alert alert-danger'>$mailchimp</div>")); exit;		  
						}
				}
		}	
	}
	
	function UnSubscribeToMailchimp(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
				
				if(!empty($postData)){
					
					$cond = array("email_address" => $postData['email']);
					$subscription =  $this->administration_model->getNewsletterEmailAddress($cond);
					if(count($subscription)==0){ 
						$insertId = $this->common_model->delete(TB_NEWSLETTER_MAILCHIMP,$cond);		
					}
						/*
						 * subscribe to mailchimp newletter
						 * */
						if(count($subscription)==1){
							$this->load->library('mcapi');
							$listID = "b2f892c659"; // obtained by calling lists();  
							$emailAddress = $postData['email'];  
							$rest = $this->mcapi->listUnsubscribe($listID, $emailAddress);  
							
							$mailchimp="";
										  
								$mailchimp .= "You have successfully unsubscribe from our newsletter\n";  
								echo json_encode(array('msg' => "<div class='alert alert-success'>$mailchimp</div>")); exit;		  
										  
						}
						/*else{
							$mailchimp .= "";   
							echo json_encode(array('msg' => "<div class='alert alert-danger'>$mailchimp</div>")); exit;		  
						}*/
				}
		}	
	}
}

?>
