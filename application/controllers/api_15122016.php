<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';
class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
       // echo "tssdf";die;
        $this->load->database();
        $this->load->model("users_model");
        $this->load->model("catalog_model");
        $this->load->model("accessories_model");
        $this->load->model("services_model");
        $this->load->model("survey_model");
        $this->load->model("common_model");
        $this->load->model("alpine_model");
        $this->load->model("events_model");
        $this->load->model("gallery_model");
        $this->load->model("pdf_model");
        $this->load->model("banner_model"); 
        $this->load->library("email"); 
        $this->load->helper("email_template"); 
        $this->load->library("ValidateFile");
		$this->load->library("image_lib");
		$this->load->model("login_model"); 
		//$this->load->helper("userauth"); 
		date_default_timezone_set("Asia/Singapore"); 
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
        $this->communityimagepath = realpath('community_gallery');
		$this->communityimagepath_thumb = realpath('community_gallery/thumb');
		define("USER_EXIST_MSG","The user with this account does not exists.");
    }
   
    /*public function loadGlobalVariables()
    {
        $_REQUEST = json_decode(file_get_contents('php://input'), true);
        $_GET = json_decode(file_get_contents('php://input'), true);
        $_POST = json_decode(file_get_contents('php://input'), true);
        
    }*/
    
   
    public function terms_post()
    {
		$this->response(array("status"=>"success", "link" => base_url().'terms'), 200);	
	}
     
    public function terms_gallery_post()
    {
		$this->response(array("status"=>"success", "link" => base_url().'terms-gallery'), 200);	
	}
	 
    public function user_exists($user_id){ 
		 $cond = array("user_id" => $user_id,"status"=>"active");
		 $exist_users = $this->users_model->getAllUsers($cond);	
		 if(count($exist_users)>0) {
			 return 1;
		 }
		 else{
			 return 0;
		 }
	}
   
    function registerUser_post()
    {
        /*$segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();
        }*/
        
        $postData = $_POST;
       //print_r($postData);die;
        $emailArr = $this->common_model->select("email_address",TB_USERS,array("email_address"=>trim($postData["email_address"])));
        if(count($emailArr) > 0)
        {
            $this->response(array("status"=>"error", "msg"=> "User is already exist with this email."), 200);
        }else{
            $date = date("Y-m-d H:i:s");
            $user_token = md5(uniqid(rand(), true));
            $activation_key  = md5(uniqid(mt_rand(), true));
            $insertArr = array(
                                "password"      =>md5(trim($postData["password"])),
                                //"user_token"    =>$user_token,
                                "c_name"    =>trim($postData["c_name"]),                                
                                "email_address" =>trim($postData["email_address"]),
                                "contact_number"=>trim($postData["contact_number"]),  
                                "user_type"     =>'NONOPEL',
                                "registered_date"=> $date,
                                "modified_date"  => $date,
                                "status" => 'inactive',
                                "resetkey" => $activation_key
                            );
            
            
            $user = $this->common_model->insert(TB_USERS,$insertArr);
        }
        if($user)
        {
			$hostname = $this->config->item('hostname');
			$username = $postData["c_name"];
			$emailaddress = $postData["email_address"]; 
			$password = trim($postData["password"]); 
			
			
			  
			 $config['mailtype'] ='html';
			 $config['charset'] ='iso-8859-1';
			 $this->email->initialize($config);
			 $from  = EMAIL_FROM; 
		  
					// More headers
		    $this->messageBody  = email_header();
			$this->messageBody  .= "Hello $username,<br/><br/>
									     You are registered successfully on Opel360<br/>
									     Below are the login credentials<br/>
									     Email address : $emailaddress<br/>
									     Password : $password<br/>
										 Click the below link to activate your account.<br/>
										<a href=".$hostname."verifyaccount/?key=".$activation_key."&email=".$emailaddress.">".$hostname."verifyaccount/?key=".$activation_key."&email=".$emailaddress."</a>
										 <br/><br/>If link is not working properly then copy and paste the link in browser<br/>
										";
				 
				 
			 $this->messageBody  .= email_footer();
			 $this->email->from($from, $from);
			 $this->email->to("$emailaddress");

			 $this->email->subject('OPEL360 - Login details');
			 $this->email->message($this->messageBody);	
				 
			 $this->email->send();
				
				 
			
            $this->response(array("status"=>"success","userid"=>$user, "user_token"=>$user_token, "msg"=> "You have been registered successfully. Activation link is sent to your email account."), 200);    
        }else{
            $this->response(array("status"=>"error", "msg"=> "Please try again."), 200);
        }
    }

	
	
	
	public function edit_profile_post()
	{
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		if($postData["user_id"]!='') {
				
			$cond = array("user_id" => $postData["user_id"],"user_type" => "NONOPEL");
			$exist_users = $this->users_model->getAllUsers($cond);	
			if(count($exist_users)>0) {
				
				$emailArr = $this->common_model->select("email_address",TB_USERS,array("email_address"=>trim($postData["email"]),"user_id !=" => $postData["user_id"]));
				if(count($emailArr) > 0)
				{
					$this->response(array("status"=>"error", "msg"=> "User is already exist with this email."), 200);exit;
				}
				
				$cond = array("user_id" => $postData["user_id"]); 
				$this->common_model->update(TB_USERS,$cond,array("c_name" => $postData["fname"],"contact_number" => $postData["contact_no"],"email_address" => $postData["email"]));
				
				//Get Updated data of user
				$cond = array("user_id" => $postData["user_id"]);
				$users = $this->users_model->getAllUsers($cond);
				
				$this->response(array("status"=>"success", "msg"=> "Your profile has been updated successfully.","user_data"=>$users), 200);
			}
			else {
				$this->response(array("status"=>"error", "msg"=> "User does not exist."), 400);exit;
			}
		}
		else {
			$this->response(array("status"=>"error", "msg"=> "User id is required"), 400);exit;
		}
	}
	
	public function get_user_details_post()
	{
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		if($postData["user_id"]!='') {
				
			$cond = array("user_id" => $postData["user_id"],"user_type" => "NONOPEL");
			$users = $this->users_model->getAllUsers($cond);
			$this->response(array("status"=>"success", "user_details"=> $users), 400);exit;
		}
	}

	

    public function login_post()
    {
        /*$segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();    
        }
        */
        
        $postData = $_POST; 
        //print_r($postData ); die;
        /*if(trim($postData["email_address"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }else if(trim($postData["password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter password."), 200);
        }*/
    	$userArr = $this->common_model->select("c_name,email_address,contact_number,user_type,user_id,status",TB_USERS,array("email_address"=>trim($postData["email_address"]),"password"=>md5(trim($postData["password"]))));
    	
    	if(count($userArr)>0){
			
			if($userArr[0]['status'] == 'active'){
				 $user_token = md5(uniqid(rand(), true));
				
				//if($segment != IS_WEB){
					set_user_logged_in($userArr[0]);
				//}
					$cond = array("email_address"=>$postData["email_address"],"password"=>md5(trim($postData["password"])));
					$userData = $this->users_model->getAllUsers($cond);
					 
					$this->session->set_userdata("auth_opeluser", $userData[0]);
					//Update device token
					
					if(isset($postData["device_token"]) && $postData["device_token"] != ""){
						$this->common_model->update(TB_USERS,array("user_id"=>$userData[0]['id']),array('device_token'=>$postData["device_token"]));
					}
					elseif(isset($postData["device_id_android"]) && $postData["device_id_android"] != ""){
						$this->common_model->update(TB_USERS,array("user_id"=>$userData[0]['id']),array('device_id_android'=>$postData["device_id_android"]));
					}
					
					
					$this->response(array("status"=>"success","user_token"=>$user_token ,"msg"=> "You have successfully login.","user"=> $userArr[0]), 200); exit;
			}
			else{
				
				$this->response(array("status"=>"error", "msg"=> "Your account is inactive."), 200);exit;
			}
    		
    	}else{
    		$this->response(array("status"=>"error", "msg"=> "Email or password is incorrect."), 200);
    }
  }
  
  
   public function loginweb_post()
    {
        /*$segment = $this->uri->segment(3);
        if($segment != IS_WEB){
            $this->loadGlobalVariables();    
        }
        */
        
        $postData = $_POST; 
        //print_r($postData ); die;
        /*if(trim($postData["email_address"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }else if(trim($postData["password"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter password."), 200);
        }*/
    	$userArr = $this->common_model->select("c_name,email_address,contact_number,user_type,user_id,status",TB_USERS,array("email_address"=>trim($postData["email_address"]),"password"=>md5(trim($postData["password"]))));
    	
    	if(count($userArr)>0){
			
			
			/*if($userArr[0]['user_type'] != 'ADMIN'){*/
					if($userArr[0]['status'] == 'active'){
						 $user_token = md5(uniqid(rand(), true));
						
						//if($segment != IS_WEB){
							set_user_logged_in($userArr[0]);
						//}
							$cond = array("email_address"=>$postData["email_address"],"password"=>md5(trim($postData["password"])));
							$userData = $this->users_model->getAllUsers($cond);
							 
							$this->session->set_userdata("auth_opeluser", $userData[0]);
							
							$this->response(array("status"=>"success","user_token"=>$user_token ,"msg"=> "You have successfully login.","user"=> $userArr[0]), 200); exit;
					}
					else{
						$email_address = $postData["email_address"]; 
						$this->response(array("status"=>"error", "msg"=> "Your account is inactive. please contact your admin"), 200);exit;
					}
			/*}
			else{
				$this->response(array("status"=>"error", "msg"=> "You are not an opel/non-opel user."), 200);exit;
			}*/
				
		}
		else{
			$this->response(array("status"=>"error", "msg"=> "Email or password is incorrect."), 200);exit;
		}
		
  }

    
	function forgot_password_post()
	{
		/*if($this->uri->segment(3) != IS_WEB){
            $this->loadGlobalVariables();    
        }*/
        
        $postData = $_POST;
		if(trim($postData["email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }
		else
		{
			
			$email=$postData['email'];
			$userdata=$this->common_model->select("user_id,c_name",TB_USERS,array("email_address"=>trim($email)));
			if(!count($userdata))
			{
				//echo "Please enter correct email, this email does not exist.";
				//exit;
				$this->response(array("status"=>"error", "msg"=> "Please enter correct email address, this email address does not exist."), 200);
			}
			else
			{
				$userId=$userdata[0]['user_id'];
				$characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$randstring = '';
				for ($i = 0; $i < 8; $i++) {
					$randstring.= $characters[rand(0, strlen($characters))];
				}
								
				$this->common_model->update(TB_USERS,array("user_id"=>$userdata[0]['user_id']),array('password'=>md5($randstring)));
				
				//echo "Your password has been sent to your Email. please check your email.";
				//exit;
				
									 $hostname = $this->config->item('hostname');
									 $usrname = $userdata[0]['c_name'];
									 $config['mailtype'] ='html';
									 $config['charset'] ='iso-8859-1';
									 $this->email->initialize($config);
									 $from  = EMAIL_FROM; 
									 //$this->messageBody  .= email_header();
									 $this->messageBody  = email_header();
									 $this->messageBody  .= "Hello ".$usrname." 
															<br/><br/>Please refer this password.  
															<br/>Password: " . $randstring ."
															 ";
									
									 $this->messageBody  .= email_footer();
									 //echo $this->messageBody;
									 //die;
									 $this->email->from($from, $from);
									 $this->email->to("$email");

									 $this->email->subject('Your new Password');
									 $this->email->message($this->messageBody);	
										 
									 $this->email->send();
									$user_token = md5(uniqid(rand(), true));

				$this->response(array("status"=>"success", "user_token" => $user_token , "msg" => "Your password has been sent to your email address. please check your email address."), 200);exit;
			}
		}
	}
    
    function change_password_post(){
			$postData = $_POST;
			//print_r($postData);die;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
			$cond_check = array('password' => md5($postData['old_pwd']),'user_id' => $postData['user_id']);
			$currentUser = $this->login_model->validOpelUser(TB_USERS,'password',$cond_check);
			 
			if(md5($postData['old_pwd']) == md5($postData['new_pwd'])){
				echo json_encode(array('status' => 'error','message' => 'Old password and new password should not be same.')); exit;
			}
			if(count($currentUser)==0){
				echo json_encode(array('status' => 'error','message' => 'You have entered incorrect old password.')); exit;
			}
			
			$cond = array("user_id" => $postData['user_id']);
			$updateData = array("password" => md5($postData['new_pwd']));
			$result = $this->common_model->update(TB_USERS,$cond,$updateData);
			if($result)
			{
				echo json_encode(array('status' => 'success','message' => 'Password has been updated successfully.')); exit;
			}
			else
			{
				echo json_encode(array('status' => 'error','message' => 'Error occured during updating Password.')); exit;
			} 
	}
	
    function user_delete()
    {
        //$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function services_list_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$cond = array();
		$costs = $this->services_model->getAllServicesCost($cond);
		
		
		
		if(!empty($costs)){
			
			$i=0;
			foreach($costs as $ct):
				$costs[$i]['service_cost'] = "S$".$costs[$i]['service_cost'];
				$i++;
			endforeach;
			
			
			$this->response(array("status"=>"success","services"=> $costs), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Services Available","services"=> ""), 200); exit;
		}
	}
	
	function services_exclusive_list_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$exl_data = array();
		
		$models = $this->services_model->getAllCatalogModelInServiceExclusive();
		
		if(!empty($models)){ 
			
			$i=0;
			foreach($models as $model):
				 
				$exl_data[$i]  = array(
					"model_id" => $model['model_id'],
					"model_name" => $model['m_name']
				);
				
				$cond = array( "opl_service_exclusive.model_id" =>$model['model_id'] );
				//print_r($models);die;
				$costs = $this->services_model->getAllServicesExclusiveCostByModel($cond);
				
				if(!empty($costs)){
					
					$j=0;
					foreach($costs as $ct):
					
						$exl_data[$i]['service_exlusive'][$j] = array(
							'se_id' => $ct['id'],
							'year'  => ordinal($ct['year'])." Year",
							'cost' => "S$".$ct['cost'],
							'milage' => $ct['milage']
							);
						$j++;
					endforeach; 
				}
				$i++;
			endforeach;
			
			$this->response(array("status"=>"success","services"=> $exl_data), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Services Available","services"=> ""), 200); exit;
		}
	}
	
	/*function survey_list_post(){
		$cond = array();
		$costs = $this->services_model->getAllServicesCost($cond);
		if(!empty($costs)){
			$this->response(array("status"=>"success","survey"=> $costs), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Services Available","services"=> ""), 200); exit;
		}
	}*/
    
    /*private function ordinal($number) {
		$ends = array('th','st','nd','rd','th','th','th','th','th','th');
		if ((($number % 100) >= 11) && (($number%100) <= 13))
			return $number. 'th';
		else
			return $number. $ends[$number % 10];
	}*/
	
    function all_car_models_post(){
		
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$cond = array();
		
		$models = $this->catalog_model->getAllCatalogModel($cond);
		
		$join = array(TB_CAR_CATALOG => TB_ACCESSORIES.".model_id = ".TB_CAR_CATALOG.".model_id",TB_ACCESSORIES_TYPE => TB_ACCESSORIES.".accessories_name = ".TB_ACCESSORIES_TYPE.".acc_type_id");
		$accessories = $this->accessories_model->getAllAccessories($cond,$join);
		
		if(!empty($models) || !empty($accessories)){
			
			$i=0;
			if(!empty($accessories)){
				foreach($accessories as $acc):
					$accessories[$i]['accessories_pic'] = $this->config->item('hostname')."/images/Accessories/".$acc['accessories_pic'];
					$i++;
				endforeach;
			}
			else{
				$accessories = "";
			}
			$models_data = array();
			if(!empty($models)){
				$i=0;
				$models_data['models'] = $models;
				foreach($models as $model):
						$cond_m = array("model_id"=>$model['id']);
						$car_gallery = $this->catalog_model->getCarCatalogModelGalleryById($cond_m);	
						$j=0;
						
						
						foreach($car_gallery as $gal):
							
							$models_data['models'][$i]['models_images'][$j] = $this->config->item('hostname')."car_gallery/".$gal['img_file'];
							$j++;
							
						endforeach;
						//print_r($models_data['models']);
				$i++;			
				endforeach;
			}
			 
			
			$this->response(array("status"=>"success","car_models"=> $models_data,"car_accessories"=> $accessories), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Models Available","car_models"=> ""), 200); exit;
		}
	}
	
	
	function get_all_accessories_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$cond_model = array();
		$models = $this->catalog_model->getAllCatalogModel($cond_model);
		
		//print_r($models);die;
		
		if(!empty($models)){
			$i =0;
			foreach($models as $mod):
			$accessories_data[$i]  = array(
					"model_name" => $mod['m_name'],
					"model_id" => $mod['id']
			);
			
			
			$cond = array(TB_CAR_CATALOG.".model_id" => $mod['id']);
			$join = array(TB_CAR_CATALOG => TB_ACCESSORIES.".model_id = ".TB_CAR_CATALOG.".model_id");
			$accessories = $this->accessories_model->getAllAccessories($cond,$join);
			//print_r($accessories);
			//echo $this->db->last_query();die;
			if(!empty($accessories)){
				$j=0;
				foreach($accessories as $acc):
					
					$accessories_data[$i]['accessories_details'][$j] = array(
								'accessories_id' => $acc['id'],
								'accessories_pic' => $this->config->item('hostname')."/images/Accessories/".$acc['accessories_pic'],
								'accessories_desc'=>$acc['accessories_desc']
							);			
							 
					$j++; 
				endforeach;
			}
			else{
				$accessories_data[$i]['accessories_details'] =array();
			}
			
			 
			$i++;
			endforeach;
			$this->response(array("status"=>"success","car_accessories"=> $accessories_data), 200); exit;
		}
		else{
				$this->response(array("status"=>"success","msg" => "No Accessories Available","car_accessories"=> ""), 200); exit;
		}
		
	}
	
	function getOpelUserVehicleList_post(){
		
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		
		$cond = array("user_id" => $_POST['user_id']); 
		$vehicle_details = $this->users_model->getCustomerVehicleNoByIdService($cond);
		
		if(!empty($vehicle_details)){ 
			$this->response(array("status"=>"success","vehicle_details"=> $vehicle_details), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Records Available","vehicle_details"=> ""), 200); exit;
		}
		
	}
	
	function CheckAlphineService($postdata,$service){
	    
	  $curl = curl_init();  
	  //echo "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx/".$service."";die;
	  curl_setopt($curl, CURLOPT_POST, 1); //Choosing the POST method
	  curl_setopt($curl, CURLOPT_URL, "http://webmail.alpinemotors.sg/AG_WS_Test/AlpineCalendarService.asmx/".$service."");  // Set the url path we want to call
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  // Make it so the data coming back is put into a string
	  curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0); 
	 
	  curl_setopt($curl, CURLOPT_POSTFIELDS ,$postdata);  // Insert the data
		
	  // Send the request 
	  $result = curl_exec($curl); 
	   
	  
	  //print_r($result);
	  // Free up the resources $curl is using 
	  curl_close($curl); 
	 
	  return $result;
	  //echo json_encode(array($result)); exit;
	}
	
	
	
	function ServiceHistory_post(){
		
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$lat = $postData['lat'];
		$long = $postData['long'];
		$message = $postData['message'];
		$cond = array("user_id" => $_POST['user_id']); 
		$users_details = $this->users_model->getUserById($cond);
		
		/*vehicle details*/
		$html = "<table border='1' style='width:600px;border-collapse: collapse;'>
<tbody>
<tr>

<td height='35' style='background:rgb(85, 85, 85); color:#fff; text-align:center;' class='title' colspan='2'><b>Emergency Assist Request Details</b></td>
</tr>";
		if(!empty($users_details)){
			
			$cond_veh = array("registration_no" => $_POST['vehicle_no'],"user_id" => $_POST['user_id']); 
			$vehicle_details = $this->users_model->getCustomerVehicleNoById($cond_veh);
			
			$html .= "<tr>
						<td height='35'>Message</td>
						<td height='35'>".$message."</td>
						</tr>
					  <tr>
					  <tr>
						<td height='35'>Vehicle Registration Number </td>
						<td height='35'>".$_POST['vehicle_no']."</td>
						</tr>
					  <tr>";
			
			if($vehicle_details){
				foreach($vehicle_details as $veh):
					$veh_id = $veh['id'];
					$cond_order = array("user_id" => $_POST['user_id'],"vehicle_id" => $veh_id);
					$html .= "<tr>
						<td height='35'>Customer Code </td>
						<td height='35'>".$veh['customer_code']."</td>
						</tr>
					  <tr>";
					$vehicle_service = $this->users_model->getLastServiceOrder($cond_order);
					foreach($vehicle_service as $service): 
						$html .= "<tr>
									<td height='35'>Service Order </td>
									<td height='35'>".$service['service_order']."</td>
									</tr>
								  <tr>"; 
						$html .= "<tr>
									<td height='35'>Last Service Transaction Date </td>
									<td height='35'>".date('Y-m-d',strtotime($service['transaction_date']))."</td>
									</tr>
								  <tr>"; 
						$html .= "<tr>
									<td height='35'>Vehicle Mileage </td>
									<td height='35'>".$service['vehicle_mileage']."</td>
									</tr>
								  <tr>";
						$cond_order = array("user_id" => $_POST['user_id'], "s_order_id" => $service['id']);
						$vehicle_service_proccess = $this->users_model->getCustomerVehicleServiceOrderProcessById($cond_order);
						foreach($vehicle_service_proccess as $service_pro):	
						
								$description .= $service_pro['process_description'].",";
								
							
						endforeach;
					endforeach;
					$array_veh[] = array(
						'registration_no' => $veh['registration_no'],
						'registration_date' => date('Y-m-d',strtotime($veh['registration_date'])),
						'chassis_number' => ($veh['chassis_number']?$veh['chassis_number']:""),
						'engine_number' => ($veh['engine_number']?$veh['engine_number']:""),
						'customer_code' => ($veh['customer_code']?$veh['customer_code']:""),
						'service_order' => ($vehicle_service[0]['service_order']?$vehicle_service[0]['service_order']:""),
						'transaction_date' => ($vehicle_service[0]['transaction_date']?date('Y-m-d',strtotime($vehicle_service[0]['transaction_date'])):""),
						'vehicle_mileage' => ($vehicle_service[0]['vehicle_mileage']?$vehicle_service[0]['vehicle_mileage']:""),
						'process_description' => $description
					);
				endforeach; 
			}
			$html .= "<tr>
						<td height='35'>Process Description </td>
						<td height='35'>".$description."</td>
						</tr>
					  <tr>";
			 $html .="<tr>
						 <td height='35'>Location</td>
						 <td height='35'><a href='http://maps.google.com/maps?q=".$lat.",".$long."'>Click here</a> to see the location</td>
					  </tr>
			 </table>";
			//echo $html;die;
			//print_r($users_details);die;
			 $hostname = $this->config->item('hostname');
			 $usrname = $users_details[0]['c_name'];
			 $config['mailtype'] ='html';
			 $config['charset'] ='iso-8859-1';
			 $this->email->initialize($config);
			 $from  = EMAIL_FROM; 
			 $to = EMAIL_TO;
			 //$this->messageBody  .= email_header();
			 $this->messageBody  = email_header();
			 $this->messageBody  .= "Hello Admin,<br/><br/>$html";
			
			 $this->messageBody  .= email_footer();
			 //echo $this->messageBody;
			 //die;
			 $this->email->from($from, $from);
			 $this->email->to("$to");
			 //$this->email->to("srt.tayade54@gmail.com");

			 $this->email->subject('Emergency Assist Request');
			 $this->email->message($this->messageBody);	
				 
			 $res = $this->email->send();
			 
			 $this->response(array("status"=>"success","customer_details"=>$users_details,"vehicle_details"=> $array_veh), 200); exit; 	
		}
		 
	}
	
	function myOpel_post(){
		
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		
		$cond = array("user_id" => $_POST['user_id']); 
		$users_details = $this->users_model->getUserById($cond);
		
		/*vehicle details*/
		$array_veh = array(); 
		if(!empty($users_details)){
			
			
			$cond_veh = array("user_id" => $_POST['user_id']); 
			$vehicle_details = $this->users_model->getCustomerVehicleNoById($cond_veh);
			if($vehicle_details){
				
				foreach($vehicle_details as $veh):
					$veh_id = $veh['id'];
					$cond_order = array("user_id" => $_POST['user_id'],"vehicle_id" => $veh_id);
					$vehicle_service = $this->users_model->getLastServiceOrder($cond_order);
					//foreach($vehicle_service as $service):
						
						$cond_order = array("user_id" => $_POST['user_id'], "s_order_id" => $vehicle_service[0]['id']);
						$description = "";
						$vehicle_service_proccess = $this->users_model->getCustomerVehicleServiceOrderProcessById($cond_order);
						if($vehicle_service_proccess){
							foreach($vehicle_service_proccess as $service_pro):	
								$description .=  $service_pro['process_description'].",";
							endforeach;
						}
					//endforeach;
					$array_veh[] = array(
						'registration_no' => $veh['registration_no'],
						'registration_date' => date('Y-m-d',strtotime($veh['registration_date'])),
						'chassis_number' => ($veh['chassis_number']?$veh['chassis_number']:""),
						'engine_number' => ($veh['engine_number']?$veh['engine_number']:""),
						'customer_code' => ($veh['customer_code']?$veh['customer_code']:""),
						'service_order' => ($vehicle_service[0]['service_order']?$vehicle_service[0]['service_order']:""),
						'transaction_date' => ($vehicle_service[0]['transaction_date']?date('Y-m-d',strtotime($vehicle_service[0]['transaction_date'])):""),
						'vehicle_mileage' => ($vehicle_service[0]['vehicle_mileage']?$vehicle_service[0]['vehicle_mileage']:""),
						'process_description' => $description
					);
				endforeach;
				
				//print_r($array_veh);
			}
			
			
		}
		$this->response(array("status"=>"success","vehicle_details"=> $array_veh), 200); exit; 
	} 
	
	function news_and_events_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$feed_url = 'http://media.opel.com/content/media/intl/en/opel/news/jcr:content/righttabsocialmedia/rss.newsrss.html';
		
		$content = file_get_contents($feed_url);
		 //print_r($content);die;
		$rss_feeds = new SimpleXmlElement($content);
		$data_news = array();
		foreach($rss_feeds->channel->item as $feed):
		 
			$data_news[]  = array(
					'link'=>trim((string)$feed->link),
					'title'=>trim((string)$feed->title),
					'description'=>trim((string)$feed->description)
			);
			
		endforeach; 
		
		if($postData['user_type']=='OPEL') {
			
			$cond_upcoming = array("status" => "Upcoming");
			$upcoming_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_upcoming);
			
			$cond_running = array("status" => "Running");
			$running_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_running);
		 }
		 else{
			$cond_upcoming = array("status" => "Upcoming");
			$upcoming_events = $this->events_model->getAllNonOpelUsersUpcomingEvents($cond_upcoming);
			
			$cond_running = array("status" => "Running");
			$running_events = $this->events_model->getAllNonOpelUsersUpcomingEvents($cond_running);
		 }
		
		
		//$cond_upcoming = array("status" => "Upcoming");
		//$upcoming_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_upcoming);
		//$upcoming_events = $this->events_model->getAllUpcomingEvents($cond_upcoming);
		
		 // echo $this->db->last_query();die;
		$data_upcoming = array();
		 
		foreach($upcoming_events as $upcoming_event) {
			 $event_name = $upcoming_event['event_name'];
			 if($upcoming_event['event_pic']!=""){
				$event_img = base_url().'images/Events/'.$upcoming_event['event_pic'];
			 }
			 else{
				$event_img = base_url().'images/event.jpg';	
			 }
			
			 $data_upcoming[] = array(
						'event_name'=>$event_name,
						'images'=>$event_img,
						'start_date'=>$upcoming_event['start_date'],
						'end_date' => $upcoming_event['end_date'],
						'event_description' => $upcoming_event['event_description'],
					);
		}
		
		//$cond_running = array("status" => "Running");
		//$running_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_running);
		//$running_events = $this->events_model->getAllRunningEvents($cond_running);
		
		$data_running  = array();
		foreach($running_events as $running_event) {
			 $event_name = $running_event['event_name'];
			 if($running_event['event_pic'] != ""){
				$event_img = base_url().'images/Events/'.$running_event['event_pic'];
			 }
			 else{
				$event_img = base_url().'images/event.jpg';	
			 }
			 
			
			 $data_running[] = array(
						'event_name'=>$event_name,
						'images'=>$event_img,
						'start_date'=>$running_event['start_date'],
						'end_date' => $running_event['end_date'],
						'event_description' => $running_event['event_description'],
					);
		}
		
		if($data_news || $data_upcoming || $data_running){ 
			
			$this->response(array("status"=>"success","news"=> $data_news,"events_upcoming"=> $data_upcoming,"events_running"=> $data_running), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Records Available","data_news"=> "","events"=> ""), 200); exit;
		}
		
	}
	
	function car_owner_events_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		
		 if($postData['user_type']=='OPEL') {
			$cond_upcoming = array("status" => "Upcoming");
			$upcoming_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_upcoming);
			
			$cond_running = array("status" => "Running");
			$running_events = $this->events_model->getAllOpelUsersUpcomingEvents($cond_running);
		 }
		 else{
			$cond_upcoming = array("status" => "Upcoming");
			$upcoming_events = $this->events_model->getAllNonOpelUsersUpcomingEvents($cond_upcoming);
			
			$cond_running = array("status" => "Running");
			$running_events = $this->events_model->getAllNonOpelUsersUpcomingEvents($cond_running);
		 }
		
			// echo $this->db->last_query();die;
				$data_upcoming = array();
		 
				foreach($upcoming_events as $upcoming_event) {
					 $event_name = $upcoming_event['event_name'];
					 if($upcoming_event['event_pic']!=""){
						$event_img = base_url().'images/Events/'.$upcoming_event['event_pic'];
					 }
					 else{
						$event_img = base_url().'images/event.jpg';	
					 }
					
					 $data_upcoming[] = array(
								'event_name'=>$event_name,
								'images'=>$event_img,
								'start_date'=>$upcoming_event['start_date'],
								'end_date' => $upcoming_event['end_date'],
								'event_description' => $upcoming_event['event_description'],
							);
				}
				
				
				
				
				$data_running  = array();
				foreach($running_events as $running_event) {
					 $event_name = $running_event['event_name'];
					 if($running_event['event_pic'] != ""){
						$event_img = base_url().'images/Events/'.$running_event['event_pic'];
					 }
					 else{
						$event_img = base_url().'images/event.jpg';	
					 }
					 
					
					 $data_running[] = array(
								'event_name'=>$event_name,
								'images'=>$event_img,
								'start_date'=>$running_event['start_date'],
								'end_date' => $running_event['end_date'],
								'event_description' => $running_event['event_description'],
							);
				}
		 
		if($data_upcoming || $data_running){ 
			
			$this->response(array("status"=>"success","events_upcoming"=> $data_upcoming,"events_running"=> $data_running), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Records Available","events"=> ""), 200); exit;
		} 
	
		
	}
	
	
	
	function survey_questions_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$user_id = $postData['user_id'];
		 
		 //get last serviced date
		 $cond1 = array("user_id" => $user_id);
		 $serviced_users = $this->survey_model->getAllCurrentVehicleServicedUserslastdate($cond1);
		
		 $last_serviced_date = $serviced_users[0]['transaction_date'];
		 $last_serviced_date_user = date('Y-m-d', strtotime($last_serviced_date)); 
		
		 $data =array();
		$data_r = array();
		$data_w = array();
		if($postData['user_type'] == 'OPEL' || $postData['user_type'] == 'NONOPEL' || $postData['user_type'] == 'ADMIN'){
			
			 $cond_sur = array("survey_type" => 'general survey');  
			 $all_survey = $this->survey_model->getGeneralSurveyInBetween();
			 //echo $this->db->last_query();die;
			  $data = [];	
			 if($all_survey){
				$i=0; 
			   
				foreach($all_survey as $ser):
						
					//$ser_id .= $ser['survey_id'].","; 
					
				
				$cond_servey = array("fk_survey_id" => $ser['survey_id']);
				$All_survey_questions = $this->survey_model->getallSurveyQuestionBySurveyId($cond_servey,$user_id);
				//echo $this->db->last_query();//die;
				//echo "<pre>";print_r($All_survey_questions);
				if(count($All_survey_questions)>0) {
					$data[$i] = array(
								"survey_id" => $ser['survey_id'],
								"survey_name" => $ser['survey_name']
					); 
					//$k=0;
					$j=0;
					//for($j=0;$j<count($All_survey_questions); $j++) {
						foreach($All_survey_questions as $question):
						$questions = $question['question'];
						$questions_id = $question['id'];
						
						$data[$i]['questions'][$j] = array(
								"questions" => $questions,
								"question_id" => $questions_id
						);

						$j++;
						
					endforeach;
				}
				$i++;
				endforeach; 
				
			}
			$data_r['survey_genaral'] = $data;
			//print_r($data_r);die;
		} 
		
		
		if($postData['user_type'] == 'OPEL'){  	
				$survey_count = $this->survey_model->getSurveyCountInBetween($last_serviced_date_user);
				//echo $this->db->last_query();die;
					if($survey_count[0]['cnt']>0) {
						//Get survey details.  
					$data_w =[];	
					$survey_details1 = $this->survey_model->getSurveyInBetween($last_serviced_date_user);
					//echo $this->db->last_query();die;
					//print_r($survey_details1); die; 
					$p=0;
					//echo $p;
						foreach($survey_details1 as $workshop):
							
							/*for($i=0;$i<=count($survey_details1); $i++) {
								if($survey_details1[$i]['survey_id']!=""){
									$survey_id1  .= $survey_details1[$i]['survey_id'].",";
								}
							}*/
							
							
							//print_r($survey_id);
								//Get all survey question
							//$cond_servey1 = $survey_id1;
							
							$cond_workshop = array("fk_survey_id"=>$workshop['survey_id']);
							
							$All_survey_questions = $this->survey_model->getallSurveyQuestionBySurveyId($cond_workshop,$user_id);
							//echo $this->db->last_query();die;
							//print_r($All_survey_questions);die;
							if(count($All_survey_questions)>0) {
								
								$data_w[$p] = array(
										"survey_id" => $workshop['survey_id'],
										"survey_name" => $workshop['survey_name']
								); 
								
								$k=0;  
									foreach($All_survey_questions as $question):

										$questions_w = $question['question'];
										$questions_id_w = $question['id'];
	 
										$data_w[$p]['questions'][$k] = array(
												"questions" => $questions_w,
												"question_id" => $questions_id_w
										);
										$k++; 
									endforeach; 
							}	
							$p++;
						endforeach;
						
						 
						$data_r['survey_workshop'] = $data_w;
						//print_r($data_r);die;
			}
		}
		//die;
		//print_r($data);
		//die;
		$this->response(array("status"=>"success","survey_details"=>$data_r), 200); exit;
	}
	
	function survey_ratings_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$user_id = $postData['user_id'];
		//print_r($postData['rating_array']);die;
		$data_rating = json_decode($postData['rating_array']);
		//print_r($data_rating);die;
		if($data_rating != ""){
			
			if(count($data_rating)>0){ 
				$i=0;
				foreach($data_rating as $row):
				$s_id = $row->survey_id; 
				$question_id = $row->question_id; 
				$rating = $row->ratingStar;
				//$ratings = array();
				$cond = array("question_id"=>$question_id,"user_id"=>$user_id);
				
				$res = $this->survey_model->getSurveyRatingById($cond);
				
				if(count($res)>0){
					$ratings[] = array(
						's_id' => $s_id,
						'qid' => $question_id,
						'user_id' => $user_id,
						'rating' => 'already done rating'
					);
				}
				else{ 
					
					$cond_q = array("question_id"=>$question_id);
					$res_quest = $this->survey_model->getallSurveyQuestion($cond_q);
						
					$insertId = $this->common_model->insert(TB_SURVEY_USER_RATING,array('s_id' => $s_id,"question_id" => $question_id,"user_id" => $user_id ,"rating" => $rating,"comments" => $postData['comments']));
					 
					if($insertId){
						if($res_quest){
							foreach($res_quest as $srv):
								$cond_s = array("sid" => $srv['fk_survey_id'],"user_id"=>$user_id);
								$res_ganed = $this->survey_model->getSurveyRewardsDetails($cond_s); 
								
								$cond_s = array("survey_id" => $srv['fk_survey_id']);
								$res_srv = $this->survey_model->getSurveyById($cond_s); 
								
								//print_r(count($res_ganed));
								if(count($res_ganed)==0){
									$this->common_model->insert(TB_USER_GAINED_REWARDS,array("sid" => $res_srv[0]['id'],"user_id" => $user_id,"gained_rewards"=>$res_srv[0]['award_points'],"date_modified"=>date('Y-m-d H:i:s')));	
								}
							endforeach;
						}
					}
					 
					 $ratings[] = array(
					 	"survey_id" => $survey_id,
						'qid' => $question_id,
						'user_id' => $user_id,
						'rating' => $rating
					);
				}
				
								
				endforeach;
				echo json_encode(array("status"=>$ratings, "msg" => "Thank you for your rating")); exit;
			}
		}
		else{
			echo json_encode(array("status"=>"failed","msg" => "Please give a rating to survey")); exit;
		}
		//die;
	}
	
	
	function my_rewards_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$user_id = $postData['user_id'];
		//$cond = array(TB_SURVEY_USER_RATING.".user_id"=>$postData['user_id']);
		//$join = array(TB_SURVEY_QUESTIONS => TB_SURVEY.".survey_id = ".TB_SURVEY_QUESTIONS.".fk_survey_id",TB_SURVEY_USER_RATING => TB_SURVEY_QUESTIONS.".question_id = ".TB_SURVEY_USER_RATING.".question_id");
		$cond = array("user_id"=>$postData['user_id']);
		$join = array();
		$rewards = $this->survey_model->getSurveyRewardsDetails($cond,$join);
		
		//print_r($data);die;
		
		$reward_data = array();
		$total_reward = 0;
		if(!empty($rewards)){
			$i =0;
			foreach($rewards as $rew): 
			$reward_data[$i]  = array(
					"user_id" => $rew['user_id'],
					"total_rewards" => $rew['tot_rewards']
			);
			$total_reward = $rew['tot_rewards'];
			$cond_details = array("user_id" => $user_id);
			$rewards_details = $this->survey_model->getUsersUsedReward($cond_details);
			//print_r($models);die;
			$total_available_reward =0;
			$available_reward=0;
			if(!empty($rewards_details)){
				$j=0;
				 
				foreach($rewards_details as $detail):	
					$reward_data[$i]['rewards_details'][$j] = array(
								'rewards_used' => $detail['rewards_used'],
								'used_for' => $detail['used_for'],
								'date_modified' => $detail['date_modified'],
								 
							);			
							$available_reward +=  $detail['rewards_used'];
						$j++;
					
				endforeach;
				$total_available_reward = $total_reward - $available_reward;	
				 
			}		
			$i++;
			endforeach;
			$this->response(array("status"=>"success",'currency_type'=>'S$',"total_available_rewards"=>$total_available_reward,"rewards"=> $reward_data), 200); exit;
		}
		else{
			$this->response(array("status"=>"success","msg" => "No Data Available","rewards"=> ""), 200); exit;
		}
	}
	
	function get_gallery_user_post(){
			$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
			$user_id = $postData['user_id'];
			if($user_id != ""){
				$cond = array(TB_COMMUNITY.".user_id"=>$user_id);
				$join = array(TB_USERS => TB_COMMUNITY.".user_id=".TB_USERS.".user_id");
			}
			
			$gallery = $this->gallery_model->getCarCatalogModelGalleryByUserId($cond,$join);
			//print_r($gallery);
			//echo $this->db->last_query();die;
			if(!empty($gallery)){
				$i=0;
				foreach($gallery as $gal):
				
				    $cond_total = array("vehicle_id" => $gal['id']);
				    $count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);
				    
				    $cond_gallery = array("vote_by" => $user_id, "vehicle_id" => $gal['id']);
				
					$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
				    //echo "cnt_vote=".$count_vote[0]['cnt'];
				    if($count_vote[0]['cnt']==0){
						$status = 'dislike';
					}
					else{
						$status = 'like';
					}
				    
				    //print_r($count_total);
				    $gallery[$i]['status_vote'] = $status;
				    $gallery[$i]['like_count'] = $count_total[0]['cnt'];
				    $gallery[$i]['user_id'] = $gal['user_id'];
				    $gallery[$i]['model_id'] = $gal['id'];
					$gallery[$i]['img_name'] = $this->config->item('hostname')."/community_gallery/".$gal['img_name'];
							 
					$i++; 
				endforeach;
				$this->response(array("status"=>"success","gallery"=> $gallery), 200); exit;			
			}
			else{
					$this->response(array("status"=>"success","msg" => "No Data Available","gallery"=> ""), 200); exit;
			}
	}
	
	function get_gallery_post(){
			$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
			$user_id = $postData['user_id'];
			if($user_id != ""){
				$cond = array();
			}
			else{
				$cond = array();
			}
			$join = array(TB_USERS => TB_COMMUNITY.".user_id=".TB_USERS.".user_id");
			$gallery = $this->gallery_model->getCarCatalogModelGalleryByUserId($cond,$join);
			//print_r($gallery);
			//echo $this->db->last_query();die;
			if(!empty($gallery)){
				$i=0;
				foreach($gallery as $gal):
				
				    $cond_total = array("vehicle_id" => $gal['id']);
				    $count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);
				    
				    $cond_gallery = array("vote_by" => $user_id, "vehicle_id" => $gal['id']);
				
					$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
				    //echo "cnt_vote=".$count_vote[0]['cnt'];
				    if($count_vote[0]['cnt']==0){
						$status = 'dislike';
					}
					else{
						$status = 'like';
					}
				    
				    //print_r($count_total);
				    $gallery[$i]['status_vote'] = $status;
				    $gallery[$i]['like_count'] = $count_total[0]['cnt'];
				    $gallery[$i]['user_id'] = $gal['user_id'];
				    $gallery[$i]['model_id'] = $gal['id'];
					$gallery[$i]['img_name'] = $this->config->item('hostname')."/community_gallery/".$gal['img_name'];
							 
					$i++; 
				endforeach;
				$this->response(array("status"=>"success","gallery"=> $gallery), 200); exit;			
			}
			else{
					$this->response(array("status"=>"success","msg" => "No Data Available","gallery"=> ""), 200); exit;
			}
	}
	
	function get_pdf_post(){
		$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		$cond_model = array();
		$models = $this->catalog_model->getAllCatalogModel($cond_model);
		
		//print_r($models);die;
		
		if(!empty($models)){
			$i =0;
			foreach($models as $mod):
			$pdf_data[$i]  = array(
					"model_name" => $mod['m_name'],
					"model_id" => $mod['id']
			);
			
			
			$cond = array("model_id" => $mod['id']);
			$pdf_details = $this->pdf_model->getPDFById($cond);
			//print_r($accessories);
			//echo $this->db->last_query();die;
			if(!empty($pdf_details)){
				$j=0;
				foreach($pdf_details as $pdf):
					
					$pdf_data[$i]['pdf_data'][$j] = array(
								'p_name' => $pdf['p_name'],
								'accessories_pic' => $this->config->item('hostname')."/images/PDF/".$pdf['p_file']
							);			
							 
					$j++; 
				endforeach;
			}
			else{
				$pdf_data[$i]['pdf_data']= array();	
			}
			$i++;
			endforeach;
			$this->response(array("status"=>"success","pdf_manuals"=> $pdf_data), 200); exit;
		}
		else{
				$this->response(array("status"=>"success","msg" => "No PDF Manuals Available","pdf_manuals"=> ""), 200); exit;
		}
	}
	
	function upload_community_photos_post(){
		$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		$user_id = $postData['user_id'];
		if($user_id !=""){
			//print_r($_FILES);
			if ( !empty( $_FILES ) ) {
				$CI = & get_instance();
				$CI->load->library('upload');
				$config=array();
				$config['upload_path'] = $this->communityimagepath;
				$config['allowed_types'] = 'jpg|JPG|png|PNG|jpeg|JPEG';//only images.you can set more
				$config['max_size'] = 1024*10;//10 mb you can increase more.Make sure your php.ini has congifured same or more value like this
				$config['min_width'] = 100;//if file do not replace.create new file
				//$config['remove_spaces'] = true;//remove sapces from file
				//you can set more config like height width for images
				//'min_width'		=> 100
				
				//$uploaded_files=array();
				foreach ($_FILES as $key => $value)
				{
					if(strlen($value['name'])>0)
					{
						$ext = pathinfo($value['name'], PATHINFO_EXTENSION);
						$filename  = $user_id."_".uniqid().".".$ext;
						
						$config['file_name'] = $filename;
						//echo '<Pre>';print_r($config);
						$CI->upload->initialize($config);
						if ($CI->upload->do_upload($key))
						{
							$path_to_image = $this->communityimagepath ."/". $filename;
							$new_img_path  = $this->communityimagepath_thumb.'/'.$filename;	
							$config1['image_library'] = 'gd2';
							$config1['source_image'] = $path_to_image;
							$config1['new_image'] = $new_img_path;
							$config1['create_thumb'] = TRUE;
							$config1['maintain_ratio'] = TRUE;
							$config1['height'] = 360;
							$config1['width'] = 360;
							
							$this->image_lib->initialize($config1);
							$this->image_lib->resize();
							$expl_file = explode('.',$filename);
				   
							$file_thumb =  $expl_file[0]."_thumb.".$ext;
							
							$insertId = $this->common_model->insert(TB_COMMUNITY,array("img_name" => $filename,"user_id" => $user_id,"img_thumb" => $file_thumb,"date_modified" => date('Y-m-d H:i:s')));
							if($insertId != "") {
								$uploaded_files[$key]=array("status"=>true,"actual_file"=>$filename,"info"=>$CI->upload->data());
								
							}
						}
						else
						{
							$uploaded_files[$key]=array("status"=>false,"actual_file"=>$filename,"message"=>$value['name'].': '.$CI->upload->display_errors());
						}

					}

				}
				echo json_encode(array("status"=>"success","uploaded_files"=> $uploaded_files));exit;
			}
			else{
				echo json_encode(array("msg"=>"Please upload at least one files.","uploaded_files"=> ""));exit;
			}
		}
		else{
			echo json_encode(array("msg"=>"user Id can not be empty","uploaded_files"=> ""));exit;
		}
	} 
	
	
	function vote_to_community_post(){
		$postData = $_POST;
			if($this->user_exists($postData["uid"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		$id = $postData['id'];
		$cond_image = array("com_gal_id" => $id);
		//echo $this->db->last_query();die; 
		$gallery_image = $this->gallery_model->getCarCatalogModelGalleryById($cond_image);
		if(count($gallery_image)==0){
			$this->response(array("status"=>2,"msg"=>"The car image does not exists."), 200);exit;
		}

		$user_id = $postData['uid'];
		$post_by = $postData['post_by'];
		$model_id = $postData['model_id'];
		if($user_id !=""){
				$cond_total = array("vehicle_id" => $postData['model_id']);
				$count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);
				$cond_gallery = array("vote_by" => $post_by, "vehicle_id" => $postData['model_id']);
				
				$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
				
				$cond_user = array("com_gal_id" => $id);
				//echo $this->db->last_query();die; 
				$gallery = $this->gallery_model->getCarCatalogModelGalleryById($cond_user);
				if($count_vote[0]['cnt']==0){
						$status = 'dislike';
					}
					else{
						$status = 'like';
					}
				if($gallery){
					$i=0;
					foreach($gallery as $gal): 
				    //print_r($count_total);
				    $gallery[$i]['status_vote'] = $status;
				    $gallery[$i]['like_count'] = $count_total[0]['cnt'];
				    $gallery[$i]['user_id'] = $gal['user_id'];
				    $gallery[$i]['model_id'] = $gal['id'];
					$gallery[$i]['img_name'] = $this->config->item('hostname')."/community_gallery/".$gal['img_name'];
							 
					$i++; 
					endforeach;
				}
				
				if($postData['vote'] == 'like'){
					if($count_vote[0]['cnt']>0){ 
						echo json_encode(array("status"=>"success","heart_cnt"=>$count_total[0]['cnt'],"status" => "like","msg"=>"Already vote up","gallery"=>$gallery));exit;
					}
					else{
						$insertId = $this->common_model->insert(TB_COMMUNITY_LIKE,array("user_id" => $user_id,"vote_by" => $post_by,"vehicle_id" =>$model_id));
						
						$cond_total = array("vehicle_id" => $postData['model_id']);
						$count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);
						$cond_gallery = array("vehicle_id" => $model_id);
						$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
						$gallery = $this->gallery_model->getCarCatalogModelGalleryById($cond_user);
						if($count_vote[0]['cnt']==0){
								$status = 'dislike';
							}
							else{
								$status = 'like';
							}
						if($gallery){
							$i=0;
							foreach($gallery as $gal): 
							//print_r($count_total);
							$gallery[$i]['status_vote'] = $status;
							$gallery[$i]['like_count'] = $count_total[0]['cnt'];
							$gallery[$i]['user_id'] = $gal['user_id'];
							$gallery[$i]['model_id'] = $gal['id'];
							$gallery[$i]['img_name'] = $this->config->item('hostname')."/community_gallery/".$gal['img_name'];
									 
							$i++; 
							endforeach;
						}
						echo json_encode(array("status" => "success","id"=>$model_id,"status" => "like","heart_cnt"=>$count_total[0]['cnt'],"msg"=>"Vote up","gallery"=>$gallery));exit;
					}
				}
				if($postData['vote'] == 'dislike'){
					if($count_vote[0]['cnt']>0){
						$insertId = $this->common_model->delete(TB_COMMUNITY_LIKE,array("user_id" => $user_id,"vote_by" => $post_by,"vehicle_id" =>$model_id));
						
						$cond_total = array("vehicle_id" => $postData['model_id']);
						$count_total = $this->gallery_model->getComGalleryVoteCount($cond_total);
						$cond_gallery = array("vehicle_id" => $model_id);
						$count_vote = $this->gallery_model->getComGalleryVoteCount($cond_gallery);
						$gallery = $this->gallery_model->getCarCatalogModelGalleryById($cond_user);
						if($count_vote[0]['cnt']==0){
							$status = 'dislike';
						}
						else{
							$status = 'like';
						}
						if($gallery){
							$i=0;
							foreach($gallery as $gal): 
							//print_r($count_total);
							$gallery[$i]['status_vote'] = $status;
							$gallery[$i]['like_count'] = $count_total[0]['cnt'];
							$gallery[$i]['user_id'] = $gal['user_id'];
							$gallery[$i]['model_id'] = $gal['id'];
							$gallery[$i]['img_name'] = $this->config->item('hostname')."/community_gallery/".$gal['img_name'];
									 
							$i++; 
							endforeach;
						}
						echo json_encode(array("status" => "success","id"=>$model_id,"status" => "dislike","heart_cnt"=>$count_total[0]['cnt'],"msg"=>"Vote down","gallery"=>$gallery));exit;
						
					}
					else{
						echo json_encode(array("status" => "success","heart_cnt"=>$count_total[0]['cnt'],"status" => "dislike","msg"=>"Already vote down","gallery"=>$gallery));exit;
					}
				}
		}
		else{
			echo json_encode(array("msg"=>"user Id can not be empty"));exit;
		}
	} 
	
	function delete_community_gallery_post(){
		$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		$user_id = $postData['user_id'];
		$img_id = $postData['id'];
		$cond = array("user_id" => $user_id, "com_gal_id" => $img_id);
		$com_gallery = $this->banner_model->getCommunityGallery($cond);
		$cond = array("com_gal_id" => $img_id);
		@unlink($this->communityimagepath.'/'.$com_gallery["0"]["img_name"]);
		@unlink($this->communityimagepath_thumb.'/'.$com_gallery["0"]["img_thumb"]);
		$res = $this->common_model->delete(TB_COMMUNITY,$cond);
		if($res){
			echo json_encode(array("status"=>"success","msg"=>"Image deleted successfully"));exit;
		}
		else{
			echo json_encode(array("status"=>"failed","msg"=>"Invalid image or Image is deleted already"));exit;
		}
	}
	
	function delete_community_gallery_user_post(){
		$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		//$id = $postData['id'];
		//$user_id = $postData['user_id'];
		$data_community = json_decode($postData['community_array']);
		//print_r($data_community);
		if($data_community != ""){
			
			if(count($data_community)>0){ 
				$i=0;
				foreach($data_community as $row):
				
					$cond_gallery = array("user_id" => $row->user_id, "com_gal_id" => $row->id);
					$com_gallery = $this->banner_model->getCommunityGallery($cond_gallery); 
					@unlink($this->communityimagepath.'/'.$com_gallery["0"]["img_name"]);
					@unlink($this->communityimagepath_thumb.'/'.$com_gallery["0"]["img_thumb"]);
					$cond = array("com_gal_id" => $row->id);
					$res = $this->common_model->delete(TB_COMMUNITY,$cond);
					$i++;
				endforeach;
				if($res){
						echo json_encode(array("status"=>"success","msg"=> $i." Image(s) has been deleted successfully"));exit;
				}
				else{
					echo json_encode(array("status"=>"failed","msg"=>"Invalid image or Image is deleted already"));exit;
				}
			}
			else{
					echo json_encode(array("status"=>"failed","msg"=>"Invalid parameters to delete records"));exit;
			}
		}
	}
	
	function doAPICall($apiCall, $data = array(), $method = 'GET')
	{
		// Variables
		
		//$baseUrl = 'https://opel360.sg/ticketsystem';
		$baseUrl = 'http://autogermany.com.sg/support';
		$apiToken = 'ebCVwM9StQM7tGfTOPl&eQ78kEvKeSn3';
	 
		// Start cURL
		$c = curl_init();
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER,0); 
		curl_setopt($c, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($c, CURLOPT_USERPWD, $apiToken . ":changeme");//ChangeMe!74
		
		// Start building the URL
		$apiCall = $baseUrl . $apiCall;
		
		
		// Check what type of API call we are making
		if ($method == 'GET') {
	 
			// Add the array of data to the URL
			if (is_array($data)) {
				$apiCall .= "?";
				foreach ($data AS $key => $value) {
					if (isset($value)) {
						if (is_array($value)) {
	                        // In case we have an array for a value, add each item of array
	                        foreach ($value as $arrayKey => $arrayValue) {
	                            $apiCall .= $key . '[' . $arrayKey . ']' . "=" . $arrayValue . "&";
	                        }
	                    } else {
	                        $apiCall .= $key . "=" . $value . "&";
	                    }
					}
				}
	 
				// Remove the final &
				$apiCall = rtrim($apiCall, '&');
			}
	 
		} else if ($method == 'PUT' || $method == 'DELETE') {
	 
			// PUT and DELETE require an $id variable to be appended to the URL
			if (isset($data['id'])) {
				$apiCall .= "/" . $data['id'];
			}
			
			
			
			// Setup the remainder of the cURL request
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($c, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: ' . $method));
		} else {
	 
			// Setup the remainder of the cURL request
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
	 
		}
		// Set the URL
		curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($c, CURLOPT_URL, $apiCall);
	 
		// Execute the API call and return the response
		$result = curl_exec($c);
		//print_r($result); exit;
		curl_close($c);
		// Return the results of the API call
		
		return $result;
	 
	}
	
	function upgrade_trade_post() {
		$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		$customfield = array("1" => $postData['current_model'],"2" => $postData['year_purchased'],"3" => $postData['model_interested'],"4" => $postData['action'],"5" => $postData['name'],"6" => $postData['existing_model'],"7" => $postData['text_msg'],"8"=>$postData["mobile_number"]);
		$table .= email_header();	
		$table .= '<br/><table border="1" cellpadding="2" cellspacing="2" class="table table-bordered" id="services_tb">
					<thead>
						<tr>							
							<th colspan="2">Upgrade/Trade Details</th>							
						</tr>
					</thead>
					<tbody>
					<tr>
							<td><strong>Name</td>
							<td class="text-center">'.($postData['name']?$postData['name']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Mobile Number</td>
							<td class="text-center">'.($postData['mobile_number']?$postData['mobile_number']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Address</td>
							<td class="text-center">'.($postData['address']?$postData['address']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Current Make/Model</td>
							<td class="text-center">'.($postData['current_model']?$postData['current_model']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Email Address</td>
							<td class="text-center">'.($postData['email']?$postData['email']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Model Interested</td>
							<td class="text-center">'.($postData['model_interested']?$postData['model_interested']:":").'</td> 
					</tr>					
					<tr>
							<td><strong>Action</td>
							<td class="text-center">'.($postData['action']?$postData['action']:":").'</td> 
					</tr> 
					<tr>
							<td><strong>Year Purchased</td>
							<td class="text-center">'.($postData['year_purchased']?$postData['year_purchased']:":").'</td> 
					</tr>
					<tr>
							<td><strong>General Comments</td>
							<td class="text-center">'.($postData['text_msg']?$postData['text_msg']:":").'</td> 
					</tr>
					</tbody>
				</table>';
			$table .= email_footer();	
			$pdata = array("user_firstname" => $postData['name'] , "user_email" => $postData['email'], "department" => 1, "status" => 1, "priority" => 3,"subject" => 'Upgrade/Trade In',"customfield" => $customfield, "text" => "Hello, <br/><br/>Upgrade/trade is requested from a ".$postData['name'].". <br/>Details are below<br/><br/>".$table."<br/>So please confirmed it. <br/><br/>Thanks</br>","send_user_email"=>1);
			//print_r($pdata);die;
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'GET');
			
			echo $result;exit;
	}
	
	function request_test_drive_post(){
		$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		$customfield = array("1" => $postData['model'],"2" => $postData['title'],"3" => $postData['name'],"4" => $postData['mobno'],"5" => $postData['address'],"6" => $postData['email'],"7" => $postData['existing_model'],"8" => $postData['year_purchased'],"9" => $postData['general_com']);
			$table .= email_header();
			$table .= '<br/><table border="1" cellpadding="2" cellspacing="2" class="table table-bordered" id="services_tb">
					<thead>
						<tr>							
							<th colspan="2">Test Drive Details</th>							
						</tr>
					</thead>
					<tbody>
					<tr>
							<td><strong>Name</td>
							<td class="text-center">'.($postData['name']?$postData['name']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Email Address</td>
							<td class="text-center">'.($postData['email']?$postData['email']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Model</td>
							<td class="text-center">'.($postData['model']?$postData['model']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Title</td>
							<td class="text-center">'.($postData['title']?$postData['title']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Mobile No.</td>
							<td class="text-center">'.($postData['mobno']?$postData['mobno']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Address</td>
							<td class="text-center">'.($postData['address']?$postData['address']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Existing Model</td>
							<td class="text-center">'.($postData['existing_model']?$postData['existing_model']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Year Purchased</td>
							<td class="text-center">'.($postData['year_purchased']?$postData['year_purchased']:":").'</td> 
					</tr>
					<tr>
							<td><strong>General Comments</td>
							<td class="text-center">'.($postData['general_com']?$postData['general_com']:":").'</td> 
					</tr>
					</tbody>
				</table>';
				$table .= email_footer();	
			$pdata = array( "user_firstname" => $postData['name'] , "user_email" => $postData['email'], "department" => 1, "status" => 1, "priority" => 3,"subject" => 'Request a Test Drive',"customfield" => $customfield, "text" => "Hello, <br/>Test drive is requested from a ".$postData['name'].". <br/>Details are below<br/>".$table."<br/> So please confirmed it. <br/>Thanks</br>","send_user_email"=>1);
			//print_r($pdata);die;
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'GET');
			
			echo $result;exit;
	}
	
	function feedback_post(){
		$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
		$customfield = array("1" => $postData['department'],"2" => $postData['text_msg']);
			$table .= email_header();
			$table .= '<br/><table border="1" cellpadding="2" cellspacing="2" class="table table-bordered" id="services_tb">
					<thead>
						<tr>							
							<th colspan="2">Feedback Details</th>							
						</tr>
					</thead>
					<tbody>
					<tr>
							<td><strong>Name</td>
							<td class="text-center">'.($postData['name']?$postData['name']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Email Address</td>
							<td class="text-center">'.($postData['email']?$postData['email']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Department</td>
							<td class="text-center">'.($postData['sel_department']?$postData['sel_department']:":").'</td> 
					</tr>
					<tr>
							<td><strong>Comments</td>
							<td class="text-center">'.($postData['text_msg']?$postData['text_msg']:":").'</td> 
					</tr>
					</tbody>
				</table>';
			$table .= email_footer();	
			$pdata = array( "user_firstname" => $postData['name'] , "user_email" => $postData['email'], "department" => 1, "status" => 1, "priority" => 3,"subject" => 'Feedback',"customfield" => $customfield, "text" => "Hello, <br/><br/>Feedback from a ".$postData['name'].".<br/> Details are below<br/>".$table."<br/><br/>Thanks</br>","send_user_email"=>1);
			//print_r($pdata);die;
			$result = $this->doAPICall('/api/ticket/ticket', $pdata, 'PUT');
			echo "m here";print_r($result);die;
			echo $result;exit;
	}
	
	function vehicle_current_booking_post(){
			$postData = $_POST;
			if($this->user_exists($postData["user_id"])==0){
				$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
			}
			$html_booking_info = "";
			$first = date('Y-m-d',strtotime('-3 days'));
			$last = date('Y-m-d',strtotime('+30 days'));
			$POST_EXIST['key'] = API_KEY;
			$POST_EXIST['coyId'] = 'OPEL';
			$POST_EXIST['docId'] = 'WSB';
			$POST_EXIST['deptId'] = 'ASC';
			$POST_EXIST['startWeek'] = $first.'T00:00:00.000';
			$POST_EXIST['endWeek'] = $last.'T00:00:00.000';
			$POST_EXIST['appVehNo'] = $postData['registration_no'];
			
			$join = array(TB_VEHICLE_DETAILS => TB_VEHICLE_OWNER_DETAILS.".customer_id = ".TB_VEHICLE_DETAILS.".customer_id");
			$cond_veh = array("registration_no" => $postData['registration_no']);
			$custInfo = $this->users_model->getCustomerInfo($cond_veh,$join); 
			$POST['key'] = API_KEY;
			$POST['cust_code']  = $custInfo[0]['cust_code'];
			$POST['veh_no']  =$postData['registration_no']; 
			$POST['doc_type'] = 'WSB';
			$POST['dept_code'] = 'ASC'; 
			//print_r($POST);
			$checkBooking = $this->soapRequest('GetValidity',$POST);
			
			//print_r($POST_EXIST);die;
			
			$currentBooking = $this->soapRequest('GetCurrentVehicleBooking',$POST_EXIST);
			//echo "<pre>";print_r($checkBooking);die; 
			//echo $checkBooking['result_soap']['GetValidityResult'];
			//die;
			if($currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram'] != "" && 
			$checkBooking['result_soap']['GetValidityResult'] != ""){
				$revArr = $currentBooking['result_soap']['GetCurrentVehicleBookingResult']['diffgram']['NewDataSet']['Table'];
				//echo "<pre>";print_r($revArr);die;
				//echo "<pre>";print_r($revArr);die;
				//$revArr = array_reverse($respBook);
				
				//echo "<pre>";print_r($revArr);die;
				
				//echo $revArr[0]['START_DATE'];
				//echo sizeof($revArr);die;
				if($revArr[0] && !empty($revArr)){
					//echo "test1";
					if(isset($revArr[0]['START_DATE'])){
						$cur_booking_flag=0;
						$vehicle_received_date = substr($revArr[0]['START_DATE'],0,10);
						$vehicle_received_time = substr($revArr[0]['START_DATE'],11,16); 
						
						if($vehicle_received_date >= $first){ 
							$vehicle_received_time = explode("+",$vehicle_received_time);
							$vehicle_rec_time = $vehicle_received_time[0];
							$cur_booking_flag=1;
						}
					}
					else{
						$cur_booking_flag=0;
						$vehicle_received_date = substr($revArr[0]['VEH_RECV_DATE'],0,10);
						$vehicle_received_time = substr($revArr[0]['VEH_RECV_DATE'],11,16);
						if($vehicle_received_date >= $first){
							
							$vehicle_received_time = explode("+",$vehicle_received_time);
							$vehicle_rec_time = $vehicle_received_time[0];
							$cur_booking_flag=1;
						}
					}
					if($cur_booking_flag == 1){
						$ref_no = $revArr[0]['RUN_NO'];
						$html_booking_info = array("booking_date_and_time"=>$vehicle_received_date." at ".$vehicle_rec_time,"reference_no"=>$ref_no);
						echo json_encode(array("status"=>"success", "current_booking"=> $html_booking_info)); exit;
						
					}
					else{
						$html_booking_info = "No records available for current booking.";
						echo json_encode(array("status"=>"success", "current_booking"=> $html_booking_info)); exit;
					} 
				}
				else{
					//echo "test2";die;
					if(isset($revArr['START_DATE'])){ 
						$cur_booking_flag=0;
						$vehicle_received_date = substr($revArr['START_DATE'],0,10);
						$vehicle_received_time = substr($revArr['START_DATE'],11,16);
						if($vehicle_received_date >= $first){
							$vehicle_received_time = explode("+",$vehicle_received_time);
							$vehicle_rec_time = $vehicle_received_time[0];
							$cur_booking_flag=1;
						}
					}
					else{
						//echo "test2";die;
						$cur_booking_flag=0;
							
						$vehicle_received_date = substr($revArr['VEH_RECV_DATE'],0,10);
						$vehicle_received_time = substr($revArr['VEH_RECV_DATE'],11,16);
						
						if($vehicle_received_date >= $first){
							
							$vehicle_received_time = explode("+",$vehicle_received_time);
							$vehicle_rec_time = $vehicle_received_time[0];
							$cur_booking_flag=1;
						}
					}
					if($cur_booking_flag == 1){
						$ref_no = $revArr['RUN_NO'];
						$html_booking_info = array("booking_date_and_time"=>$vehicle_received_date." at ".$vehicle_rec_time,"reference_no"=>$ref_no);
						echo json_encode(array("status"=>"success", "current_booking"=> $html_booking_info)); exit;
						
					}
					else{
						$html_booking_info = "No records available for current booking.";
						echo json_encode(array("status"=>"success", "current_booking"=> $html_booking_info)); exit;
					} 
				}				
				//echo $html_booking_info .= "Latest Booking Info</b>&nbsp;:<br/><div class='book_dt'>Booking Date and Time : $vehicle_received_date at $vehicle_rec_time</div><div class='book_dt'>Reference Number : $ref_no</div></div>";
				//die;
			}
		else{
			$html_booking_info = "No records available for current booking.";
		}
		echo json_encode(array("status"=>"success","current_booking"=> $html_booking_info));exit;
		
	}
	
	
	function soapRequest($service, $params = array()){
		 require_once('library/lib/nusoap.php'); //includes nusoap
		
		$client = new nusoap_client(API_SERVICE_URL,true);

		$error = $client->getError();
		if ($error) {
			echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
		}

		$result = $client->call($service, array($params),array("SOAPAction"=>"http://www.starvisionit.com/webservices/"));

		if ($client->fault) {
			echo "<h2>Fault</h2><pre>";
			print_r($result);
			echo "</pre>";
		}
		else {
			$error = $client->getError();
			if ($error) {
				echo "<h2>Error</h2><pre>" . $error . "</pre>";
			}
			else {
					return array("result_soap"=>$result);
			}
		}
         
	}
	
	function send_booking_email_post(){
		
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}
		$config['mailtype'] ='html';
		$config['charset'] ='iso-8859-1';
		$this->email->initialize($config);
		 $from  = EMAIL_FROM; 
		 
		 $user_id = $postData['user_id'];
		 $c_name = $postData['c_name'];
		 $cond = array("user_id" => $user_id);
		 $userdata = $this->users_model->getAllUsers($cond);
		 $vehicle_received_date = $postData['received_date'];
		 $vehicle_received_time = $postData['received_time'];
		 $vechiel_rec_date = $vehicle_received_date." At ".$vehicle_received_time;
		 $ref_no = $postData['ref_no'];
		 $veh_no = $postData['veh_no'];
		 $remarks = $postData['remarks'];
		 $milage = $postData['booking_milage']."K";
		 
		 if(trim($c_name) != ""){
			$uname = $c_name;
		 }else{
			$uname = "User";
		 }
		 $useremail = $userdata[0]['email_address'];
		 //$useremail = 'mat@mdsm-consulting.com';
		
		 //$useremail = 'car1@mdsm-consulting.com';
		// $useremail = 'swapnil.t@exceptionaire.co';
		 //$useremail = 'vishal932544@gmail.com'; 
		if($postData['book_action'] == "I"){
			
			 $this->messageBody .= email_header();
			 $this->messageBody  .= "Hello $uname,<br/><br/>
				Your OPEL service booking has been done.<br/>
				Your booking details are - <br/><br/>
				Vehicle Received Date And Time : $vechiel_rec_date<br/>
				Reference Number :  $ref_no<br/>
				Vehicle No : $veh_no<br/>
				Mileage : $milage<br/>
				";
			$this->messageBody .= email_footer();     
			//echo $this->messageBody;
			//die;
			
			 $this->email->from($from, 'OPEL360');
			 $this->email->to("$useremail");
			// $this->email->cc("$useremail_cc");
			 $this->email->subject('OPEL360 - New Booking');
			 $this->email->message($this->messageBody);	
				 
			 if ( $this->email->send()){
					$mail_status = "Booking details are sent to your registered email address";
			 }else{
					$mail_status = "Booking has been done but email can not be send.";
			 }
			 
			echo json_encode(array("status"=>"success","message"=> $mail_status));exit;
			
		}
		elseif($postData['book_action'] == "U"){
			 
			 $this->messageBody .= email_header();
			 $this->messageBody  .= "Hello $uname,<br/><br/>
				Your OPEL service booking has been changed.<br/>
				Your booking details are - <br/>
				Vehicle Received Date And Time : $vechiel_rec_date<br/>
				Reference Number :  $ref_no<br/>
				Vehicle No : $veh_no<br/>
				Mileage : $milage<br/>
				";
			$this->messageBody .= email_footer();     
			//echo $this->messageBody;
			//die;
			
			 $this->email->from($from, 'OPEL360');
			 
			 //$useremail = trim((string)$xmlCust->GetCust->CUST_EMAIL);
			 
			 $this->email->to("$useremail");

			 $this->email->subject('OPEL360 - Booking Changed');
			 $this->email->message($this->messageBody);	
				 
			 if ( $this->email->send()){
					$mail_status = "Booking details are sent to your registered email address";
			 }else{
					$mail_status = "Booking has been changed but email can not be send.";
			 }
			 echo json_encode(array("status"=>"success","message"=> $mail_status));exit;
		}
		elseif($postData['book_action'] == "C"){
			 
			 $this->messageBody .= email_header();
			 $this->messageBody  .= "Hello $uname,<br/><br/>
									You have cancelled your OPEL service booking<br/>
									Cancellation details are - 
									Reference Number :  $ref_no<br/>
									Vehicle No : $veh_no<br/>
									";
			$this->messageBody .= email_footer();     
			//echo $this->messageBody;
			//die;
			
			 $this->email->from($from, 'OPEL360');
			 
			 //$useremail = trim((string)$xmlCust->GetCust->CUST_EMAIL);
			 
			 $this->email->to("$useremail");

			 $this->email->subject('OPEL360 - Booking Cancelled');
			 $this->email->message($this->messageBody);	
				 
			 if ( $this->email->send()){
					$mail_status = "The booking has been cancelled and Cancellation details are sent to your registered email address";
			 }else{
					$mail_status = "Booking has been cancelled but email can not be send.";
			 }
			 echo json_encode(array("status"=>"success","message"=> $mail_status));exit;
		}
		
	}

	function get_booking_id_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}

		$booking_id = $this->common_model->selectQueryLimit('booking_id',TB_RENT_CAR_BOOKING,array('user_id' => $postData['user_id']),array('rent_car_booking_id' => 'DESC'),array(),array(),1);
		
		if(empty($booking_id[0]['booking_id'])){
			$booking_id = "RENTAL_".$postData['user_id']."_1";	
		}else{
			$explode_booking_id = explode('_', $booking_id[0]['booking_id']);
			$incr_cntr = $explode_booking_id[2]+1;
			$booking_id = "RENTAL_".$postdata['user_id']."_".$incr_cntr;
		}

		$insert_id = $this->db->insert(TB_RENT_CAR_BOOKING,array('booking_id' => $booking_id,'user_id' => $postData['user_id']));
		if(!empty($insert_id)){
			echo json_encode(array('status' => 'success','booking_id' => $booking_id));exit;	
		}else{
			echo json_encode(array('status' => 'error','msg' => 'Booking ID Not Generated.','booking_id' => ''));exit;	
		}
		
	}

	function update_booking_id_post(){
		$postData = $_POST;
		if($this->user_exists($postData["user_id"])==0){
			$this->response(array("status"=>0,"msg"=>USER_EXIST_MSG), 200);exit;
		}

		$update_data['customer_code'] = $postData['CustomerCode'];
		$update_data['booking_number'] = $postData['BookingNo'];
		$update_data['booking_added_date'] = date('Y-m-d H:i:s');

		$update_count = $this->common_model->update(TB_RENT_CAR_BOOKING,array('booking_id' => $postData['booking_id']),$update_data);
		//echo $this->db->last_query();exit;
		if($update_count){
			$from_mail = EMAIL_FROM;
			$subject = 'OPEL360 - New Booking';
			$message = "Hello ".$postData['first_name']." ".$postData['last_name'].",<br/><br/>
			Your OPEL Car booking has been done.<br/>
			Your booking details are - <br/><br/>
			Pickup Location : ".$postData['pickup_location']."<br>
			Pickup Date And Time : ".$postData['BookingDateFrom']."<br/>
			Dropoff Location : ".$postData['dropoff_location']."<br>
			Dropoff Date And Time : ".$postData['BookingDateTo']."<br/>
			Booking Number :  ".$update_data['booking_number']."<br/>
			Car Model : ".$postData['model_name']."<br/>
			";
			$to_mail = $postData['user_email'];
			//echo $message;exit;
			$status = $this->sendMail($from_mail,$subject,$message,$to_mail);
			if($status){
				echo json_encode(array("status"=>"success","message"=>"Booking Done Successfully.Please Check Mail for more details."));exit;
			}else{
				echo json_encode(array("status"=>"failed","message"=>"Mail not send successfully."));exit;
			}

		}else{
			echo json_encode(array("status"=>"failed","message"=>"Booking Records Not Updated Successfully.Please Try Again."));exit;
		}
	}

	function sendMail($from_mail='',$subject='',$messageBody='',$to_mail=''){
		$config['mailtype'] ='html';
		$config['charset'] ='iso-8859-1';
		$this->email->initialize($config);

		$messageBody .= email_header();
		//$messageBody  .= 

		$this->messageBody .= email_footer();     
		//echo $this->messageBody;
		//die;

		$this->email->from($from_mail, 'OPEL360');
		$this->email->to($to_mail);
		// $this->email->cc("$useremail_cc");
		$this->email->subject($subject);
		$this->email->message($this->messageBody);	

		if ($this->email->send()){
			return true;
		}else{
			return false;
		}
	}
}
?>
