<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model("login_model");
		$this->load->model("common_model");
		$this->load->model("master_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		//$this->load->library("richtexteditor"); 
		$_REQUEST = json_decode(file_get_contents('php://input'), true);
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'forum_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;http://192.168.100.14/opel/banner/getBanners
		}
	}

	public function getForum()
	{
		
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				$this->load->view("administrator/manage_forum",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}

	public function list_forum() // List forum
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					$postData["searchBy"] = array('topic');
					$cond = array('super_admin'=>0);
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("topic");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "created";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}					

				$arrayColumn = array("topic" => "topic");
		        $arrayStatus = array();
		        $arrayColumnOrder = array("created"=>"created");
		        //$like = array();
		        $conds = array();
		        $select = TB_DISCUSSION_FORUM . ".*,
		       (SELECT COUNT(*) FROM " . TB_FORUM_TOPIC_DISCUSSION . "
		        WHERE post_topic_id = " . TB_DISCUSSION_FORUM . ".topic_id AND post_status = 'active') AS postCount,
		            (SELECT COUNT(*) FROM " . TB_FORUM_TOPIC_VIEWS . "
		        WHERE view_topic_id = " . TB_DISCUSSION_FORUM . ".topic_id) AS viewCount";
		        $table_name = TB_DISCUSSION_FORUM;
		        $join = array(
		            TB_FORUM_TOPIC_DISCUSSION => TB_FORUM_TOPIC_DISCUSSION . '.post_topic_id=' . TB_DISCUSSION_FORUM . '.topic_id',
		            TB_FORUM_TOPIC_VIEWS => TB_FORUM_TOPIC_VIEWS . '.view_topic_id=' . TB_DISCUSSION_FORUM . '.topic_id');
		        $default_order_column = "created";

		        $group_by = TB_DISCUSSION_FORUM . '.topic_id';
		        $users = $this->common_model->getRowsFrontPerPage($select,$table_name,$conds,$like,array($order_column => $postData["order"]),$postData["start"],$join,$group_by);
		        
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer acti_set" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="15%" onclick="changePaginate(0,\'topic\',\''.$corder[0].'\')">Topics</th>
									<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'viewCount\',\''.$corder[1].'\')">Views</th>
									<th class="'.$css[2].'" width="10%" onclick="changePaginate(0,\'postCount\',\''.$corder[2].'\')">Posts</th>
									<th class="text-center" width="50%">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($users)>0)
					{
							if(count($users) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($users as $user) {  
								
								if($user["status"] == "active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "active";
								}
						$topicId = $this->encrypt->encode($user["topic_id"]);
								$tid = strtr($topicId, '+/', '-_');
						$table .= '<tr id="row_'.$user["topic_id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($user["topic_id"]).'" name="check" id="check" class="chk"></td>

								<td class="text-left"><a href=" ' . base_url() . 'Forum/forum_topic_comments?topic_id=' . $tid . '">' .substr($user['topic'],0,100) . ' </a></td> 

								
								<td>'.$user["viewCount"].'</td>
								<td>'.($user["postCount"]?$user["postCount"]:"N/A").'</td>
								<td class="text-center">';
								
								$table .='<a title="View details" alt="viewdetails" id="view" class="view" href=" ' . base_url() . 'Forum/forum_topic_comments?topic_id=' . $tid . '">View Details</a>';
								$table .='| <div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["topic_id"]).'\',\'active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["topic_id"]).'\',\'inactive\')" class="'.$status_inactive.'">Inactive</button>
								  </div>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($users)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($users),'paginate' => $paginate)); exit;
				
			}
		}
	}


	public function change_topic_status(){

		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["topicId"] != "" && $postData["status"] != "")
				{  					
					if($postData["status"] == "active"){
						$status_update = "active";
					}
					else if($postData["status"] == "inactive"){
						$status_update = "inactive";
					}
					
					$cond = array("topic_id" => $this->encrypt->decode($postData["topicId"]));
					//$users = $this->users_model->getAllUsers($cond);

				$arrayColumn = array("topic");
		        $arrayStatus = array();
		        $arrayColumnOrder = array("created"=>"created");
		        //$like = array();
		        $conds = array();
		        $select = TB_DISCUSSION_FORUM . ".*,
		       (SELECT COUNT(*) FROM " . TB_FORUM_TOPIC_DISCUSSION . "
		        WHERE post_topic_id = " . TB_DISCUSSION_FORUM . ".topic_id) AS postCount,
		            (SELECT COUNT(*) FROM " . TB_FORUM_TOPIC_VIEWS . "
		        WHERE view_topic_id = " . TB_DISCUSSION_FORUM . ".topic_id) AS viewCount";
		        $table_name = TB_DISCUSSION_FORUM;
		        $join = array(
		            TB_FORUM_TOPIC_DISCUSSION => TB_FORUM_TOPIC_DISCUSSION . '.post_topic_id=' . TB_DISCUSSION_FORUM . '.topic_id',
		            TB_FORUM_TOPIC_VIEWS => TB_FORUM_TOPIC_VIEWS . '.view_topic_id=' . TB_DISCUSSION_FORUM . '.topic_id');
		        $default_order_column = "created";

		        $group_by = TB_DISCUSSION_FORUM . '.topic_id';
		        $users = $this->common_model->getRowsFrontPerPage($select,$table_name,$cond,$like,array($arrayColumn => 'DESC'),'',$join,$group_by);

					// if(count($users) != 0)
					// { 
						$updateArr = $this->common_model->update(TB_DISCUSSION_FORUM,$cond,array("status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "active";
								}
							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["topic_id"].'"></td>
									<td class="text-center">'.$users["0"]["topic"].'</td> 
									<td>'.$users["0"]["viewCount"].'</td>
									<td>'.$users["0"]["postCount"].'</td>
									<td class="text-center">
										';
										if($user["user_type"] != 'OPEL'){
											$row .='<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["topic_id"].'" href="javascript:void(0)">View Details</a>';
										}
										$row .='| <div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$postData["topic_id"].'\',\'active\')" class="'.$status_active.'" title="'.$status_text.'">Active</button>
									<button onclick="changeStatus(\''.$postData["topic_id"].'\',\'inactive\')" class="'.$status_inactive.'" title="'.$status_text.'">Inactive</button>
								    </div>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["topicId"]), "msg" => "Topic status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "Topic status cannot be change")); exit;
						}
					// }
				}
			}
		}
	}


	public function delete_topic() // Delete topic from listing
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("topic_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_DISCUSSION_FORUM,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}

	public function getForumDetailsById() // Get Single User
	{
		if(is_ajax_request())
		{ 			
				$postData = $this->input->post(); 			
				
				$topic_id = $this->encrypt->decode($postData["topic_id"]);

				$table_name = TB_DISCUSSION_FORUM;
		        $select = TB_USERS.'.user_nickname,' . TB_DISCUSSION_FORUM . ".*,";
		        $join = array(TB_USERS => TB_USERS . '.user_id=' . TB_DISCUSSION_FORUM . '.dis_user_id',
		        	TB_FORUM_TOPIC_DISCUSSION => TB_FORUM_TOPIC_DISCUSSION . '.post_topic_id=' . TB_DISCUSSION_FORUM . '.topic_id'
		            
		        );
		        		      
		        $where = array("topic_id" => $topic_id,"post_status"=>"'active'");		       
		        
		       	$topicData = $this->master_model->getMaster($table_name,$where,$join,'desc','topic_id',$select,'', '','');

				$createdDate = date("jS F, Y h:i:s A", strtotime($topicData[0]['created'])? strtotime($topicData[0]['created']):'');
				// echo "<pre>";print_r($createdDate);die;
				$html  ="";				
				
					$html .= '<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Topic Name</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$topicData[0]['topic'].'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Description</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$topicData[0]['message'].'</strong>
									  </div>
									</div>
								  </div>
								 
							</div>
							<div class="section row mbn">
								  
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Posted by</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($topicData[0]['user_nickname']?$topicData[0]['user_nickname']:"-").'</strong>
									  </div>
									</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Date</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.$createdDate.'</strong>
									  </div>
									</div>
								  </div>
							</div>';
				
				echo json_encode(array("user_details" => $html));exit;
			}
		}

		/* Topic listing end */

		/* Topic discussion listing start */
	public function forum_topic_comments()
	{		
		
		$topic_id = strtr($_GET["topic_id"], '-_', '+/');
		$topicId = $this->encrypt->decode($topic_id);

		$table_name = TB_DISCUSSION_FORUM;
        $select = TB_USERS.'.user_nickname,' . TB_DISCUSSION_FORUM . ".*,";
        $join = array(TB_USERS => TB_USERS . '.user_id=' . TB_DISCUSSION_FORUM . '.dis_user_id');
        		      
        $where = array("topic_id" => $topicId,TB_DISCUSSION_FORUM.".status"=>"'active'");       
        
       	$data['forum'] = $this->master_model->getMaster($table_name,$where,$join,'desc','topic_id',$select,'', '','');	      
				
		$data['topic_id'] = $topic_id;
		$this->load->view("administrator/manage_forum_topic",$data);
	}

	public function topic_list() // List forum
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					$postData["searchBy"] = array('user_nickname','post_message');
					$topicId = $this->encrypt->decode($postData['topic_id']);
					
					$cond = array('super_admin'=>0);
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("post_nickname");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "created";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					

				$arrayColumn = array("user_nickname" => "user_nickname","post_message" => "post_message");
		        $arrayStatus = array();
		        $arrayColumnOrder = array("created_post"=>"created_post");

		        $conds = array("topic_id"=>$topicId);
		        $select = TB_USERS.".user_nickname,".TB_DISCUSSION_FORUM . ".*,post_id,post_message,post_nickname,post_status,created_post";
		        $table_name = TB_FORUM_TOPIC_DISCUSSION;
		        $join = array(TB_USERS => TB_USERS . '.user_id=' . TB_FORUM_TOPIC_DISCUSSION . '.post_user_id',
		            TB_DISCUSSION_FORUM => TB_DISCUSSION_FORUM . '.topic_id=' . TB_FORUM_TOPIC_DISCUSSION . '.post_topic_id');
		        $default_order_column = "created_post";

		        $group_by = TB_DISCUSSION_FORUM . '.topic_id';
		        $users = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array($order_column => $postData["order"]),$postData["start"],$join);
		       // echo $this->db->last_query();die;
		       
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer acti_set" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="15%" onclick="changePaginate(0,\'post_message\',\''.$corder[0].'\')">Comments</th>
									<th class="'.$css[1].'" width="20%" onclick="changePaginate(0,\'post_nickname\',\''.$corder[1].'\')">Commented by</th>
									<th class="'.$css[2].'" width="10%" onclick="changePaginate(0,\'created_post\',\''.$corder[2].'\')">Date</th>
									<th class="text-center" width="50%">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($users)>0)
					{
							if(count($users) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($users as $user) {  
								
								if($user["post_status"] == "active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "active";
								}
						$postId = $this->encrypt->encode($user["post_id"]);
								$tid = strtr($postId, '+/', '-_');
						$table .= '<tr id="row_'.$user["post_id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($user["post_id"]).'" name="check" id="check" class="chk"></td>

								<td class="text-left">' . $user['post_message'] . ' </a></td> 

								
								<td>'.$user["user_nickname"].'</td>
								<td>'.($user["created_post"]?$user["created_post"]:"N/A").'</td>
								<td class="text-center">';
																
								$table .='<div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["post_id"]).'\',\'active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$this->encrypt->encode($user["post_id"]).'\',\'inactive\')" class="'.$status_inactive.'">Inactive</button>
								  </div>
								</td>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($users)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getUserList");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($users),'paginate' => $paginate)); exit;
				
			}
		}
	}

	public function change_comment_status(){ 
		
		$topicId = strtr($_POST['topicId'], '-_', '+/');
		$topic_id = $this->encrypt->decode($topicId);

		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();  
				//print_r($postData);die;
				if($postData["postID"] != "" && $postData["status"] != "")
				{  
					
					if($postData["status"] == "active"){
						$status_update = "active";
					}
					else if($postData["status"] == "inactive"){
						$status_update = "inactive";
					}

					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("post_nickname");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "created";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}

					$post_id = strtr($_POST['postID'], '-_', '+/');
					$post_id = $this->encrypt->decode($post_id);
					$cond = array("post_id" =>$post_id);
					//$users = $this->users_model->getAllUsers($cond);

				$arrayColumn = array("post_nickname" => "post_nickname");
		        $arrayStatus = array();
		        $arrayColumnOrder = array("created_post"=>"created_post");
		        //$like = array();
		        $conds = array("topic_id"=>$topic_id);
		        $select = TB_DISCUSSION_FORUM . ".*,post_id,post_message,post_nickname,created_post";
		        $table_name = TB_DISCUSSION_FORUM;
		        $join = array(
		            TB_FORUM_TOPIC_DISCUSSION => TB_FORUM_TOPIC_DISCUSSION . '.post_topic_id=' . TB_DISCUSSION_FORUM . '.topic_id');
		        

		        $group_by = TB_DISCUSSION_FORUM . '.topic_id';
		        $users = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array($order_column => $postData["order"]),$postData["start"],$join);
		        					
					// if(count($users) != 0)
					// { 
						$updateArr = $this->common_model->update(TB_FORUM_TOPIC_DISCUSSION,$cond,array("post_status" => $status_update));
						if($updateArr)
						{
							
							if($postData["status"] == "active"){
									$status_text = "Make Inactive";
									$status_active = "btn btn-xs btn-success";
									$status_inactive = "btn btn-xs  active btn-default";
									$status = "inactive";
								}
								else{
									$status_text = "Make Active";
									$status_active = "btn btn-xs  active btn-default";
									$status_inactive = "btn btn-xs btn-danger";
									$status = "active";
								}
							 
							$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["post_id"].'"></td>
									<td class="text-center">'.$users["0"]["post_message"].'</td> 
									<td>'.$users["0"]["post_nickname"].'</td>
									<td>'.$users["0"]["created_post"].'</td>
									<td class="text-center">
										';
										
										$row .='<div class="btn-group btn-toggle"> 
									<button onclick="changeStatus(\''.$postData["post_id"].'\',\'active\')" class="'.$status_active.'">Active</button>
									<button onclick="changeStatus(\''.$postData["post_id"].'\',\'inactive\')" class="'.$status_inactive.'">Inactive</button>
								    </div>
									</td>';
									//echo $this->encrypt->decode($postData["userId"]);die;
							
							echo json_encode(array("status" => 1,"action"=> "add","row" => $row,"id"=>$this->encrypt->decode($postData["postID"]), "msg" => "Post Status changed successfully")); exit;
						}
						else
						{
							echo json_encode(array("status" => 2,"msg" => "Post Status cannot be change")); exit;
						}
					// }
				}
			}
		}
	}

	public function delete_comment() // Delete comments from listing
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("post_id" => $this->encrypt->decode($postData["ids"][$i]));
					
					$isdelete = $this->common_model->delete(TB_FORUM_TOPIC_DISCUSSION,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}

}