<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Survey extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("users_model");
		$this->load->model("survey_model");
		$this->load->model("login_model");
		$this->load->model("master_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->helper("email_template");
		$this->load->library("ValidateFile");
		$this->pdf_file_path =  realpath('images/PDF');
		date_default_timezone_set('Asia/Kolkata');
		//$this->load->library("richtexteditor"); 
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0',false);
		header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
			$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'survey_management');
			if($res_permission == 1){
				return 1;
			}
			else{
				return 0;
			}
		}
		else{
			return 1;
		}
	}
	
	public function getSurvey()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				$this->load->view("administrator/survey",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getSurveyById() // Get Survey data by id for edit
	{
		if(is_ajax_request())
		{ 
			
			$postData = $this->input->post(); 
				//print_r($postData);die;
			$cond = array("survey_id" => $this->encrypt->decode($postData["id"]));
			$pdf = $this->survey_model->getSurveyById($cond);

				//Get all survey question
			$cond = array("fk_survey_id" => $this->encrypt->decode($postData["id"]));
			$questions = $this->survey_model->getallSurveyQuestion($cond);

			if(count($questions) > 0)
			{
				$html = "";
				$i=0;
				foreach($questions as $question) {

					$label="";
					if($i==0){
						$label = '<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Question to ask<span class="validationerror">*</span></label>';
						$add_or_remove = 'add_more';
						$circle = '<i aria-hidden="true" class="fa fa-plus-circle"></i>';
					}
					else{
						$label = '<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first"></label>';
						$add_or_remove = 'remove_div';
						$circle = '<i aria-hidden="true" class="fa fa-minus-circle"></i>';
					}						

					$selectedrat = '';
					$selecteddec='';
					$selectedmc='';
					$selectedyn='';

					$option_a = '';
					$option_b = '';
					$option_c = '';
					$option_d = '';
					$multichoiceans = '';

					$selectA = '';
					$selectB = '';
					$selectC = '';
					$selectD = '';

					if($question['question_type'] == 'ratings')
					{
						$selectedrat = 'selected="selected"';
					}
					if($question['question_type'] == 'discriptive')
					{
						$selecteddec = 'selected="selected"';
					}
					if($question['question_type'] == 'Yes/No')
					{
						$selectedyn = 'selected="selected"';
					}
					if($question['question_type'] == 'Multiple Choice')
					{						
						$option_a = $question['option_A'];
						$option_b = $question['option_B'];
						$option_c = $question['option_C'];
						$option_d = $question['option_D'];
						$multichoiceans = $question['answer'];
						$selectedmc = 'selected="selected"';

						if ($multichoiceans == "A")
						{
							$selectA = 'selected="selected"';
						}else{
							$selectA = '';
						}

						if ($multichoiceans == "B"){
							$selectB = 'selected="selected"';
						}else{
							$selectB = '';
						}

						if ($multichoiceans == "C"){
							$selectC = 'selected="selected"';
						}else{
							$selectC = '';
						}

						if ($multichoiceans == "D"){
							$selectD = 'selected="selected"';
						}else{
							$selectD = '';
						}

					} 

					$survey_question = $question['question'];
					$survey_question_id .= $question['id'].",";

					$html .='<div class="add_new_row">
					<div class="section row mbn">			
						<div class="col-sm-6">
							<div class="form-group ">
								<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Question to ask<span class="validationerror">*</span></label>
								<div class="col-lg-8 text_inpu_new first_que"> 

									<textarea type="text" id="survey_question'.$i.'" name="survey_question[]" class="form-control input-sm" placeholder="Question to Ask">'.$survey_question.'</textarea>
									<span id="QuestionInfo'.$i.'"  class="text-danger marg"></span>
									<input type="hidden" name="question_id'.$i.'" value="'.$question['id'].'" />
								</div> 
							</div>
						</div>
						<div class="add_new_column">
							<div class="col-sm-6">

								<div class="form-group">
									<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Select Survey Type<span class="validationerror">*</span></label>
									<div class="col-lg-8 text_inpu_new add_btn">
										<select id="question_type'.$i.'" class="form-control qtype" name="question_type[]">
											<option value="">Select question type</option>
											<option value="ratings"';
											$html.=$selectedrat;
											$html.='>1 – 5 questions with star ratings </option>

											<option value="discriptive"';
											$html.= $selecteddec;
											$html.='>Free text </option>

											<option value="Yes/No"';
											$html.= $selectedyn;
											$html.='>Yes/No </option>

											<option value="Multiple Choice"';
											$html.= $selectedmc;
											$html.='>Multiple Choice </option>
										</select>
										<span id="Questiontypeinfo'.$i.'" class="text-danger marg"></span>



									</div>
								</div>

								<div class="form-group" id="multipleoption'.$i.'">
									<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">A.<span class="validationerror">*</span></label>
									<div class="col-lg-8 text_inpu_new">					
										<input type="text" id="multiple_choice'.$i.'" name="multiple_choice[]" class="form-control input-sm" placeholder="Option A." value="'.$option_a.'">
									</div>
									<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">B.<span class="validationerror">*</span></label>
									<div class="col-lg-8 text_inpu_new">					
										<input type="text" id="multiple_choice'.$i.'" name="multiple_choice[]" class="form-control input-sm" placeholder="Option B." value="'.$option_b.'">
									</div>
									<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">C.<span class="validationerror">*</span></label>
									<div class="col-lg-8 text_inpu_new">					
										<input type="text" id="multiple_choice'.$i.'" name="multiple_choice[]" class="form-control input-sm" placeholder="Option C." value="'.$option_c.'">
									</div>
									<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">D.<span class="validationerror">*</span></label>
									<div class="col-lg-8 text_inpu_new">					
										<input type="text" id="multiple_choice'.$i.'" name="multiple_choice[]" class="form-control input-sm" placeholder="Option D." value="'.$option_d.'">
									</div>
									<label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Answer<span class="validationerror">*</span></label>
									<div class="col-lg-8 text_inpu_new">					
										<select id="multiChoice_ans'.$i.'" class="form-control" name="multiChoice_ans[]">
											<option value="">Select answer</option>
											<option value="A" ';
											$html.= $selectA;
											$html.='>A </option>

											<option value="B" ';
											$html.= $selectB;
											$html.='>B </option>

											<option value="C" ';
											$html.= $selectC;
											$html.='>C </option>

											<option value="D" ';
											$html.= $selectD;
											$html.='>D </option>
										</select>
									</div>
								</div>

							</div>
						</div>
						<span for="inputStandard" class="col-lg-2 pn mt5 control-label categorytitle add_btn_icon2"><a data-qno='.$i.' data-id'.$i.'="'.$question['id'].'" href="javascript:void(0);" id ="'.$add_or_remove.'" class="">'.$circle.'
							<input type="hidden" name="question_id'.$i.'" value="'.$question['id'].'" />
						</a></span>		</div>
					</div>';
					$i++;

				}
			}else{

			}
			echo json_encode(array("survey" =>$pdf[0],"questions" => $html,"qid"=>$survey_question_id,"total_questions"=>$i));exit;
			
		}
	}
	
	
	function send_survey_notification($device_token,$msg_survey) {
		
		$deviceToken = $device_token;
		//print_r($deviceToken);die;
		//$passphrase = '123456';
		$passphrase = '123456';
		$message = $msg_survey;

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'O_360_DEV.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		//print_r($fp);
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		//echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
		$body['aps'] = array('title'=>'Opel Survey','alert' => $message,'badge'=> 'Increment','sound' => 'chime');
		$payload = json_encode($body);
		for($i=0;$i<count($deviceToken);$i++){
			$devtoken =  trim($deviceToken[$i]);
			$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $devtoken)) . pack('n', strlen($payload)) . $payload;
			$result = fwrite($fp, $msg, strlen($msg));
		}
		fclose($fp);
		// Send it to the server
		
		return $result; die;
		
	}
	function send_survey_notification_android($device_token = array(),$msg) {
		//print_r($device_token);
		//AIzaSyB7RyOObaLVFygZHGWh4iSIE5cvALzNrnc
		define('API_ACCESS_KEY', 'AIzaSyD51ZoV_nwPsMzb7yX0AxVePy5TiQ2g9RA');

		$message = $msg;
		$registrationIds = $device_token;
						//print_r($registrationIds);die;
						/*$registrationIds = array(
							"flECike3rY8:APA91bF1btDqwXuyW3yEwUSKjumxaUoySfanhDvWEJ5GqueGwV6Obpg6TaGAIBcZmVXiTQVRnPW8Q7TgTpjrUxTQhPIX4Gzt2TAWdKkQwRu7GSt94Y4_h3xNPlrGVHtMA1iJ-LMtd3T2"
							//"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth",
							//"e2N0UCrhW4c:APA91bEKSkqcevv8HM7z6H22D_gbkikNm1dWmpxcOA5H4YKxuF4q2BbILGsbDPfw9VDMtHmxcqlVCHSDFW309VyOkSzoeVUIL3lU3bIt95XmpdeEuCd1cxdppjMCifyobjK2FMubAbth"
							);*/
						// prep the bundle

						//print_r($registrationIds);die;
							$msg = array
							(
								'title'=>'Opel Survey',
								'message' 	=> $message,
								);
							$fields = array
							(
								'registration_ids' 	=> $registrationIds,
								'data'			=> $msg
								);

							$headers = array
							(
								'Authorization: key=' . API_ACCESS_KEY,
								'Content-Type: application/json'
								);

						//print_r(json_encode( $fields )) ;die;
							$ch = curl_init();
							curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
							curl_setopt( $ch,CURLOPT_POST, true );
							curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
							curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
							curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
							$result = curl_exec($ch );
							curl_close( $ch );
							return  $result;die;

						}

	public function save_survey() // Add/Edit Survey Questions 
	{
		$data = array_map('array_filter', $_POST);
		$data = array_filter($data);
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				if($postData["survey_id"] == "")
				{
								//date_default_timezone_set('Asia/Singapore');
					$created_date = date('Y-m-d H:i:s');
					$insertId = $this->common_model->insert(TB_SURVEY,array("survey_from_date" => $postData["start_date"],"survey_to_date" => $postData["date_to"],"survey_type" => $postData["survey"],"survey_name" => $postData["survey_name"],"award_points" => $postData["award_points"],"date_modified"=>$created_date));
					$reward_points  = $postData["award_points"];
					if($insertId)
					{

						$date = date('Y-m-d');
									//get users

						if($postData["survey"] == "workshop survey"){

							$cond1 = array("transaction_date" => $date);
							$users = $this->survey_model->getAllCurrentVehicleServicedUsers($cond1);
						}
						else{
							$cond1 = array("user_type"=>"NONOPEL","user_type"=>"OPEL");
							$users = $this->survey_model->getAllUsersForSurvey($cond1);
										//print_r($users);die;
						}

									//$emailaddress = array();
						$survey_id = $insertId;

						for($i=0;$i<count($users);$i++) {

							if($postData["survey"] =="workshop survey"){
								$user_id = $users[$i]['user_id'];
							} 	
							else{
								$user_id = $users[$i]['user_id'];
							}
										//Get Email id's
							$cond = array("user_id"=>$user_id);
							$allusers = $this->survey_model->getAllUsers($cond);
										//echo "<pre>";print_r($allusers[0]['device_id_android']);
							$emailaddress = $allusers[0]['email_address'];
							if($allusers[0]['device_token']!=""){
								$device_token[] = trim($allusers[0]['device_token']);

							}

							if($allusers[0]['device_id_android'] != ""){
								$device_id[] = trim($allusers[0]['device_id_android']);
							}

						}

						$k = 0;
						$count = count($postData['survey_question']); 
						foreach($postData['survey_question'] as $question) {
							if($data['question_type'][$k]=='Multiple Choice')
							{
								if($data['multiple_choice'.$k][0] !='')
									{	$data['multiple_choice'] = explode(',', $data['multiple_choice'.$k][0]);

								$options_A = $data['multiple_choice'][0];
								$options_B = $data['multiple_choice'][1];
								$options_C = $data['multiple_choice'][2];
								$options_D = $data['multiple_choice'][3];
								$answer = $data['multiple_choice'][4];
							}
						}

						if($data['question_type'][$k] == 'discriptive')
						{
							$options_A = '';
							$options_B = '';
							$options_C = '';
							$options_D = '';					
							$answer = '';
						}
						if($data['question_type'][$k] == 'ratings')
						{
							$options_A = '';
							$options_B = '';
							$options_C = '';
							$options_D = '';					
							$answer = '';
						}

						if($data['question_type'][$k] == 'Yes/No')
						{
							$options_A = '';
							$options_B = '';
							$options_C = '';
							$options_D = '';					
							$answer = '';
						}			


						$insertsurvey = $this->common_model->insert(TB_SURVEY_QUESTIONS,array("question_type" => $data["question_type"][$k],"question" => $question,"option_A"=>$options_A,"option_B"=>$options_B,"option_C"=>$options_C,"option_D"=>$options_D,"answer"=>$answer,"fk_survey_id" => $insertId));
						$k++;
					}

					$cond_sur = array("survey_id" => $insertId);  
					$all_survey = $this->common_model->select("*",TB_SURVEY,$cond_sur);
					$data = [];	
					if($all_survey){
						$i=0;
						foreach($all_survey as $ser):

							$cond_servey = array("fk_survey_id" => $ser['survey_id']);
						$All_survey_questions = $this->survey_model->getallSurveyQuestionBySurveyId($cond_servey,$user_id);


						if(count($All_survey_questions)>0) {

							$data[$i] = array(
								"survey_id" => $ser['survey_id'],
								"survey_name" => $ser['survey_name']
								); 

							$j=0;

							foreach($All_survey_questions as $question):
								$questions = $question['question'];
							$questions_id = $question['id'];

							$questions_type = $question['question_type'];

							if($questions_type == 'Multiple Choice')
							{
								$options_A = $question['option_A'];
								$options_B = $question['option_B'];
								$options_C = $question['option_C'];
								$options_D = $question['option_D'];
								$answer = $question['answer'];
								$data[$i]['questions'][$j] = array(
									"questions" => $questions,
									"questions_type" => $questions_type,
									"option_A" => $options_A,
									"option_B" => $options_B,
									"option_C" => $options_C,
									"option_D" => $options_D,
									"answer" => $answer,
									"question_id" => $questions_id
									);
							}
							if($questions_type == 'Yes/No')
							{
								$data[$i]['questions'][$j] = array(
									"questions" => $questions,
									"questions_type" => $questions_type,
									"question_id" => $questions_id
									);
							}
							if($questions_type == 'discriptive' || $questions_type == 'ratings')
							{
								$data[$i]['questions'][$j] = array(
									"questions" => $questions,
									"questions_type" => $questions_type,		
									"question_id" => $questions_id
									);
							}

							$j++;

							endforeach;
						}
						else{
							$data[$i] = array(

								);
						}
						$i++;
						endforeach;
					}
					$b = array_values(array_filter($data));
					$data_r['survey_genaral'] = $b;

					/* sujit added code for push notification */
					$msgforNotification = 'Grab a chance to answer the latest Survey added by our team to win exciting offers at our Store.';
					$userDevice = $this->common_model->select("user_id,c_name,last_name,user_type,device_id,device_type",TB_USERS,array("status"=>"active","device_type !="=>""));

					$nId = mt_rand(10000000, 99999999);
					if(count($userDevice)>0){
						foreach($userDevice as $userinfo){
							if($userinfo['device_id']!="" && ($userinfo['device_type'] == "android" || $userinfo['device_type'] == "Android")){
								$registrationIds = array($userinfo['device_id']);
								$msg = array(
									'message' 	=> $msgforNotification,
									'type'	=> 'Survey',
									'survey_details' => $data_r['survey_genaral'][0],
									'action_by' =>'',
									'action_by_profile_pic' =>'',
									'action' => '',
									'nId' => $nId,
									'action_time' => trim(date('Y-m-d H:i:s'))
									);
								$notify = $this->sendPushnotificationAndroid($registrationIds,$msg,$data_r['survey_genaral']); 

							}
							if($userinfo['device_id']!="" && ($userinfo['device_type']=="ios" || $userinfo['device_type']=="Ios")){
								$message = $msgforNotification;
												// $message = array(
												// 	'message' 	=> $msgforNotification,
													// 'type'	=> 'Survey',
													// 'survey_details' => $data_r['survey_genaral'][0],
													// 'action_by' =>'',
													// 'action_by_profile_pic' =>'',
													// 'action' => '',
													//'nId' => $nId
													// 'action_time' => trim(date('Y-m-d H:i:s'))
												//);
								$userDevice = $this->common_model->select("badgecount",TB_NOTIFICATION,array("device_token" =>$userinfo['device_id'],"status" =>'0'));

								$badgecount = $userDevice[0]["badgecount"]+1;
								$cond1 = array("device_token" => $userinfo['device_id']);
								$updatecontest = $this->common_model->update(TB_NOTIFICATION,$cond1,array("badgecount" => $badgecount));


								$notify = $this->sendPushnotificationIos($userinfo['device_id'],$message,$data_r['survey_genaral'],$nId,$badgecount);
							}


						}

						$userID = $this->users_model->getUserByIdAll();
						foreach ($userID as $key => $value) {
							$this->common_model->insert(TB_NOTIFICATION,array("user_id" => $value['id'],"nId" => $nId,"json_response"=>json_encode($data_r['survey_genaral'][0]),"user_type" => '2',"device_token"=>$value['device_id'],"notification_type" => 'Survey',"created_date" => trim(date('Y-m-d H:i:s'))));
						}

					}
					/* end code */ 

					echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Survey has been added successfully.</div>')); exit;
				}
				else
				{
					echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
				}

			}
			else
			{
						// echo "<pre>";print_r($_POST);
				$created_date = date('Y-m-d H:i:s');
				$cond1 = array("survey_id" => $this->encrypt->decode($postData["survey_id"]));

				$updateArr = $this->common_model->update(TB_SURVEY,$cond1,array("survey_from_date" => $postData["start_date"],"survey_to_date" => $postData["date_to"],"survey_type" => $postData["survey"],"survey_name" => $postData["survey_name"],"award_points" => $postData["award_points"],"date_modified"=>$created_date));


				$cond = array("fk_survey_id" => $this->encrypt->decode($postData["survey_id"]));
				$isdelete = $this->common_model->delete(TB_SURVEY_QUESTIONS,$cond);

				foreach ($_POST['survey_question'] as $key => $value)
				{
					$mcq=array();
					if($_POST['question_type'][$key] == "Multiple Choice")
					{
						$kcnt = $key * 4;
						for($k=1;$k<=4;$k++)
						{
							$mcq[] = $_POST['multiple_choice'][$kcnt];
							$kcnt++;
						}
					}

					if($_POST['question_type'][$key] == "Multiple Choice")
					{
						$opt_A = $mcq[0];
						$opt_B = $mcq[1];
						$opt_C = $mcq[2];
						$opt_D = $mcq[3];
						$ans = $_POST['multiChoice_ans'][$key];
					}else
					{
						$opt_A = '';
						$opt_B = '';
						$opt_C = '';
						$opt_D = '';
						$ans = '';
					}

					$updtArr=array(
						"question_type"=>$_POST['question_type'][$key],
						"question"=>$_POST['survey_question'][$key],
						"fk_survey_id" => $this->encrypt->decode($postData["survey_id"]),
						"option_A"=>$opt_A,
						"option_B"=>$opt_B,
						"option_C"=>$opt_C,
						"option_D"=>$opt_D,
						"answer"=>$ans,
						);
							// echo "<pre>";print_r($updtArr);
					$insertsurvey = $this->common_model->insert(TB_SURVEY_QUESTIONS,$updtArr);
				}

			$cond1 = array("user_type"=>"NONOPEL","user_type"=>"OPEL");
			$users = $this->survey_model->getAllUsersForSurvey($cond1);


			for($i=0;$i<count($users);$i++) {

				if($postData["survey"] =="workshop survey"){
					$user_id = $users[$i]['user_id'];
				} 	
				else{
					$user_id = $users[$i]['user_id'];
				}
										//Get Email id's
				$cond = array("user_id"=>$user_id);
				$allusers = $this->survey_model->getAllUsers($cond);
										//echo "<pre>";print_r($allusers[0]['device_id_android']);
				$emailaddress = $allusers[0]['email_address'];
				if($allusers[0]['device_token']!=""){
					$device_token[] = trim($allusers[0]['device_token']);

				}

				if($allusers[0]['device_id_android'] != ""){
					$device_id[] = trim($allusers[0]['device_id_android']);
				}										

			}
			$serveyID = $this->encrypt->decode($postData["survey_id"]);
			$cond_sur = array("survey_id" => $serveyID);  
			$all_survey = $this->common_model->select("*",TB_SURVEY,$cond_sur);

			$data = [];	
			if($all_survey){
				$i=0;
				foreach($all_survey as $ser):

					$cond_servey = array("fk_survey_id" => $ser['survey_id']);
				$All_survey_questions = $this->survey_model->getallSurveyQuestionBySurveyId($cond_servey,$user_id);


				if(count($All_survey_questions)>0) {

					$data[$i] = array(
						"survey_id" => $ser['survey_id'],
						"survey_name" => $ser['survey_name']
						); 

					$j=0;

					foreach($All_survey_questions as $question):
						$questions = $question['question'];
					$questions_id = $question['id'];

					$questions_type = $question['question_type'];

					if($questions_type == 'Multiple Choice')
					{
						$options_A = $question['option_A'];
						$options_B = $question['option_B'];
						$options_C = $question['option_C'];
						$options_D = $question['option_D'];
						$answer = $question['answer'];
						$data[$i]['questions'][$j] = array(
							"questions" => $questions,
							"questions_type" => $questions_type,
							"option_A" => $options_A,
							"option_B" => $options_B,
							"option_C" => $options_C,
							"option_D" => $options_D,
							"answer" => $answer,
							"question_id" => $questions_id
							);
					}
					if($questions_type == 'Yes/No')
					{
													// $options_A = $question['option_A'];
													// $options_B = $question['option_B'];
													// $answer = $question['answer'];
						$data[$i]['questions'][$j] = array(
							"questions" => $questions,
							"questions_type" => $questions_type,
														// "option_A" => $options_A,
														// "option_B" => $options_B,
														// "answer" => $answer,
							"question_id" => $questions_id
							);
					}
					if($questions_type == 'discriptive' || $questions_type == 'ratings')
					{
						$data[$i]['questions'][$j] = array(
							"questions" => $questions,
							"questions_type" => $questions_type,		
							"question_id" => $questions_id
							);
					}

					$j++;

					endforeach;
				}
				else{
					$data[$i] = array(

						);
				}
				$i++;
				endforeach;
			}
			$b = array_values(array_filter($data));
			$data_r['survey_genaral'] = $b;
										// echo "<pre>";print_r($data_r['survey_genaral']);die;					
			/* sujit added code for push notification */
			$msgforNotification = 'Grab a chance to answer the latest Survey added by our team to win exciting offers at our Store.';
			$userDevice = $this->common_model->select("user_id,c_name,last_name,user_type,device_id,device_type",TB_USERS,array("status"=>"active","device_type !="=>""));

			$nId = mt_rand(10000000, 99999999);
			if(count($userDevice)>0){
				foreach($userDevice as $userinfo){
					if($userinfo['device_id']!="" && ($userinfo['device_type'] == "android" || $userinfo['device_type'] == "Android")){
						$registrationIds = array($userinfo['device_id']);
						$msg = array(
							'message' 	=> $msgforNotification,
							'type'	=> 'Survey',
							'survey_details' => $data_r['survey_genaral'][0],
							'action_by' =>'',
							'action_by_profile_pic' =>'',
							'action' => '',
							'nId' => $nId,
							'action_time' => trim(date('Y-m-d H:i:s'))
							);
						$notify = $this->sendPushnotificationAndroid($registrationIds,$msg,$data_r['survey_genaral']); 

					}
					if($userinfo['device_id']!="" && ($userinfo['device_type']=="ios" || $userinfo['device_type']=="Ios")){
						$message = $msgforNotification;
												// $message = array(
												// 	'message' 	=> $msgforNotification,													
												// 	'nId' => $nId
												// );
						$userDevice = $this->common_model->select("badgecount",TB_NOTIFICATION,array("device_token" =>$userinfo['device_id'],"status" =>'0'));

						$badgecount = $userDevice[0]["badgecount"]+1;
						$cond1 = array("device_token" => $userinfo['device_id']);
						$updatecontest = $this->common_model->update(TB_NOTIFICATION,$cond1,array("badgecount" => $badgecount));


						$notify = $this->sendPushnotificationIos($userinfo['device_id'],$message,$data_r['survey_genaral'],$nId,$badgecount);												
					}


				}
				$userID = $this->users_model->getUserByIdAll();
				foreach ($userID as $key => $value) {
					$this->common_model->insert(TB_NOTIFICATION,array("user_id" => $value['id'],"nId" => $nId,"json_response"=>json_encode($data_r['survey_genaral'][0]),"user_type" => '2',"device_token"=>$value['device_id'],"notification_type" => 'Survey',"created_date" => trim(date('Y-m-d H:i:s'))));
				}

			}
			/* end code */ 

							//die;
							// $row = '
							// 	<td class="text-center"  width="5%"><input type="checkbox" value="'.$postData["survey_id"].'" name="check" id="check" class="chk"></td>
							// 	<td class="text-center"  width="25%">'.$postData["survey_name"].'</td>
							// 	<td class="text-center"  width="25%">'.$postData["survey"].'</td>

							// 	<td class="text-center"  width="25%">'.$postData["start_date"].'</td>
							// 	<td class="text-center"  width="25%">'.$postData["date_to"].'</td>
							// 	<td class="text-center">
							// 		<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["survey_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							// 	</td>';
							//echo $this->encrypt->decode($postData["userId"]);die;
			echo json_encode(array("status" => 1,"action" => "modify","id"=>$this->encrypt->decode($postData["survey_id"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Survey has been updated successfully.</div>')); exit;

		} 
	}
	else
	{
		echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
	}
}
exit;
}



	public function list_survey() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
				$postData = $this->input->post();

				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$count = $this->survey_model->getSurveyCount($cond,$like);
					//echo $this->db->last_query(); die;
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }

				$orderColumns = array("survey_name","survey_type","award_points","survey_from_date","survey_to_date");

				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "survey_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}

				foreach($orderColumns AS $k => $v)
				{

					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}

				$survey = $this->survey_model->getSurveyPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);

				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
					$table .= '<thead>
					<tr>
						<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
						<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'survey_name\',\''.$corder[0].'\')">Survey Name</th>
						<th class="text-center '.$css["1"].'" width="25%" onclick="changePaginate(0,\'survey_type\',\''.$corder[1].'\')">Survey Type</th>						
						<th class="text-center '.$css["2"].'" width="15%" onclick="changePaginate(0,\'survey_from_date\',\''.$corder[2].'\')">From Date</th>
						<th class="text-center '.$css["3"].'" width="15%" onclick="changePaginate(0,\'survey_to_date\',\''.$corder[2].'\')">To Date</th>
						<th class="text-center" width="15%">Action</th>
					</tr>
				</thead>
				<tbody>';
				}
				if(count($survey)>0)
				{
					if(count($survey) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
					foreach($survey as $row) {  

						$table .= '<tr id="row_'.$row["id"].'">
						<td class="text-center"  width="5%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
						<td class="text-center"  width="25%">'.$row["survey_name"].'</td>
						<td class="text-center"  width="25%">'.$row["survey_type"].'</td>
						<td class="text-center"  width="25%">'.$row["survey_from_date"].'</td>
						<td class="text-center"  width="25%">'.$row["survey_to_date"].'</td>
						<td class="text-center">
							<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($row["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
						</td>
					</tr> 
				</tr>
				';
			}
		}
		if($postData["start"] == 0)
		{
			if(count($survey)==0)
			{
				$table .= '<tr id=""><td class="text-center" colspan="7">No Records Found</td></tr>';
			}
			$table .= '</tbody>
		</table>';
	}
	$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					// $this->pagination->initialize($config);
	$to = $postData["start"]+PER_PAGE_OPTION;
	if($to > $count[0]["cnt"])
	{
		$to = $count[0]["cnt"];
		$paginate = ($to).",".$order_column.",".$postData["order"];
	}
	else
	{
		$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
	}

	echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($survey),'paginate' => $paginate)); exit;

}
}
}


public function validateCatalogModel($postData)
{   

	return array("status" => 1);
}


	public function delete_survey() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("survey_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_SURVEY,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	public function delete_survey_question() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 
				$isdelete = 0;
				$cond = array("question_id" => $postData["id"]);
				$isdelete = $this->common_model->delete(TB_SURVEY_QUESTIONS,$cond);

				echo "success";exit;
			}
		}
	}
	
	
	
	public function reward_survey()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				/*$where="1 group by survey_id";
				$join = array(
				'opl_users' => "opl_users.user_id = opl_survey_userrating.user_id | inner",
				'opl_survey_questions' => "opl_survey_questions.question_id = opl_survey_userrating.question_id | inner",
				'opl_survey' => "opl_survey.survey_id = opl_survey_questions.fk_survey_id | inner"
				);
				$data['reward_data'] = $this->master_model->getMaster('opl_survey_userrating',$where,$join);	
					*/		
				$this->load->view("administrator/reward_details",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
		
	}
	
	public function rewardsurvey()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
				$postData = $this->input->post();

				$cond = array();
				$like = array();
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					for($x=0; $x<count($postData["searchBy"]); $x++)
					{
						if($postData["searchBy"][$x] != "")
						{
							$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
						} 
					}
				}
				$join = array(
					TB_USERS => TB_USERS.".user_id = ".TB_USER_GAINED_REWARDS.".user_id"

					);
				$count = $this->survey_model->getSurveyRewardsCount($cond,$like,$join);
					//echo $this->db->last_query(); die;
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }

				$orderColumns = array("c_name","email_address","award_points","used_points");

				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "gid";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}

				foreach($orderColumns AS $k => $v)
				{

					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}

				$join = array(
					TB_USERS => TB_USERS.".user_id = ".TB_USER_GAINED_REWARDS.".user_id"

					);

				$survey = $this->survey_model->getRewardSurveyDetails($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);

				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
					$table .= '<thead>
					<tr>
						<th class="text-center" width="5%">Sr.No</th>
						<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'c_name\',\''.$corder[0].'\')">User Name</th>
						<th class="text-center '.$css["1"].'" width="25%" onclick="changePaginate(1,\'email_address\',\''.$corder[1].'\')">Email Address</th>
						<th class="text-center width="15%" ">Total Reward Points</th>
						<th class="text-center width="15%"">Available Rewards</th>
						<th class="text-center" width="15%">Action</th>
					</tr>
				</thead>
				<tbody>';
				}
				if(count($survey)>0)
				{
					if(count($survey) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
					$cnt=1;
					$used_rewards=0;
					foreach($survey as $row) {  
						$cond1 = array("user_id" => $row['id']);
						$rewards = $this->survey_model->getUsedReward($cond1);

						if(sizeof($rewards)==0)
						{
							$used_rewards=$row['award_points'];
						}
						else
						{
							$used_rewards=$row['award_points']-$rewards[0]["reward"]; 
						}
						$table .= '<tr id="row_'.$row["id"].'">
						<td class="text-center"  width="5%">'.$cnt.'</td>
						<td class="text-center"  width="25%">'.($row["c_name"]?$row["c_name"]:"-").'</td>
						<td class="text-center"  width="25%">'.$row["email_address"].'</td>
						<td class="text-center"  width="25%">'.$row["award_points"].'</td>
						<td class="text-center"  width="25%">'.$used_rewards.'</td>
						<td class="text-center">
							<a title="Deduct" alt="Deduct Rewards" id="edit" class="editPayment" data-adward="'.$row["award_points"].'" data-sname="'.$row["survey_name"].'" data-name="'.$row["c_name"].'" data-id="'.$this->encrypt->encode($row["id"]).'" data-option="'.$this->encrypt->encode($row["survey_id"]).'" href="javascript:void(0)"><i class="fa fa-minus"></i></a> | 
							<a title="View Rewards" alt="View" id="view" class="editPayment" data-sname="'.$row["survey_name"].'" data-id="'.$this->encrypt->encode($row["id"]).'" data-option="'.$this->encrypt->encode($row["survey_id"]).'" href="javascript:void(0)"><i class="fa fa-eye"></i></a>
						</td>
					</tr> 
				</tr>
				';
				$cnt++;
			}
		}
		if($postData["start"] == 0)
		{
			if(count($survey)==0)
			{
				$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
			}
			$table .= '</tbody>
		</table>';
	}
	$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
	$this->pagination->initialize($config);
	$to = $postData["start"]+PER_PAGE_OPTION;
	if($to > $count[0]["cnt"])
	{
		$to = $count[0]["cnt"];
		$paginate = ($to).",".$order_column.",".$postData["order"];
	}
	else
	{
		$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
	}

	echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($survey),'paginate' => $paginate)); exit;
}
}
}

public function getRewardDetails()
{
	$postData = $this->input->post(); 				 
	$cond = array("user_id" => $this->encrypt->decode($postData['uid']));
	$reward = $this->survey_model->getUsersUsedReward($cond);


	$user_details = $this->survey_model->getAllUsers($cond);
	$table = '<table class="table table-striped">
	<thead>
		<tr>
			<th>Sr.No</th> 
			<th>Reward Points</th>
			<th>Reason</th>
			<th>Date and Time</th>
		</tr>
	</thead> <tbody>';

	$i=1;
	if(count($reward)>0){
		foreach($reward as $value)
		{
			$table .= '<tr>
			<td>'.$i.'</td> 
			<td>'.$value["rewards_used"].'</td>
			<td>'.$value["used_for"].'</td>
			<td>'.$value["date_modified"].'</td>
		</tr>';
		$i++;
	}
}
else{
	$table .= '<tr><td colspan="5" align="center"> No Records Available</td></tr>';
}
$table .= '</tbody></table>';
echo json_encode(array('table' => $table,'username'=>$user_details[0]['c_name']));exit;
}


public function getSurveyCommentsDetails()
{
	$postData = $this->input->post(); 				 
	$cond = array("s_id" => $this->encrypt->decode($postData['sid']));
	$reward = $this->survey_model->getUsersUsedReward($cond);


	$user_details = $this->survey_model->getAllUsers($cond);
	$table = '<table class="table table-striped">
	<thead>
		<tr>
			<th>Sr.No</th> 
			<th>Survey Name</th>
			<th>Ratings</th>
			<th>Username</th>
			<th>Comments</th>
			<th>Date and Time</th>
		</tr>
	</thead> <tbody>';

	$i=1;
	if(count($reward)>0){
		foreach($reward as $value)
		{
			$table .= '<tr>
			<td>'.$i.'</td> 
			<td>'.$value["rewards_used"].'</td>
			<td>'.$value["used_for"].'</td>
			<td>'.$value["date_modified"].'</td>
		</tr>';
		$i++;
	}
}
else{
	$table .= '<tr><td colspan="5" align="center"> No Records Available</td></tr>';
}
$table .= '</tbody></table>';
echo json_encode(array('table' => $table,'username'=>$user_details[0]['c_name']));exit;
}

public function save_used_reward()  
{
	if(is_ajax_request())
	{
		if(is_user_logged_in())
		{
			$postData = $this->input->post();
				//date_default_timezone_set('Asia/Singapore');
			$created_date = date('Y-m-d H:i:s');
			$save_data=array(
				"user_id" => $this->encrypt->decode($postData["user_id"]) ,
				"rewards_used" => $postData["reward_point"],
				"used_for" => $postData["reson"],
				"date_modified"=>$created_date
				);
				//print_r($save_data); die;
			$total_adward=$postData["total_adward"];
			$cond1 = array("user_id" => $this->encrypt->decode($postData["user_id"]));
			$rewards = $this->survey_model->getUsedReward($cond1);
			if(sizeof($rewards)==0)
			{
				$used_rewards=$total_adward;
			}
			else
			{
				$used_rewards=$total_adward-$rewards[0]["reward"]; 
			}

			if($used_rewards>=$postData["reward_point"])
			{
				$insertId = $this->common_model->insert(TB_SURVEY_USED_REWARD,$save_data);
			}	
			else
			{
				echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, You do not have sufficiant available rewards!!.</div>')); exit;
			}							
			echo json_encode(array("status" => 1,"available_rewards"=>$used_rewards,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Rewards point has been deducted successfully.</div>')); exit;
		}
		else
		{
			echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
		}
	}
	else
	{
		echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
	}
}

public function avd($device_token = array(),$msg_survey = "test") {
	$deviceToken= array('05652e54aed11a0586cd0f13922294e373aceada5fea9dcc109ac2e4de0c707e');
	$passphrase = '123456';
	$message = $msg_survey;

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'O_360_DEV.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			// Open a connection to the APNS server
	$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err,$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			//print_r($fp);
	if (!$fp)
		exit("Failed to connect: $err $errstr" . PHP_EOL);
			//echo 'Connected to APNS' . PHP_EOL;

			// Create the payload body
	$body['aps'] = array('title'=>'Opel Survey','alert' => $message,'badge'=> 'Increment','sound' => 'chime');

			// Encode the payload as JSON
	$payload = json_encode($body);

			// Build the binary notification
			//print_r($deviceToken);

	for($i=0;$i<count($deviceToken);$i++){
		$devtoken =  trim($deviceToken[$i]);
		$msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $devtoken)) . pack('n', strlen($payload)) . $payload;
		$result = fwrite($fp, $msg, strlen($msg));
	}

	print_r($result); echo "#####";
			// Send it to the server
			//$result = fwrite($fp, $msg, strlen($msg));
			exit;//echo PHP_EOL; exit;
			
		}

		public function sendPushnotificationAndroid($registrationIds,$msg,$survey_genaral) {

		// define( 'API_ACCESS_KEY', 'AIzaSyAqILqwjh8c2ulVLyDHw_nLrtsmCQFjXAY' );
			define( 'API_ACCESS_KEY', 'AIzaSyCZ8fos7svfalo3LnLV9aTGvpiyiE44dIY' );


			$fields = array
			(
				'registration_ids' 	=> $registrationIds,
				'data'			=> $msg,
				'survey_details'=> $survey_genaral
				);

			$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
				);

			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );
		//print_r($result); echo "sujit"; die;
			return $result;		

		}

		public function sendPushnotificationIos($deviceToken,$message,$survey_genaral,$nId,$badgecount) { 
			$passphrase = 'Opel1234';
			// Put your alert message here:
			//$message = 'A push notification has been sent!';
			////////////////////////////////////////////////////////////////////////////////
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'Oepl_APNS_Certificates.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			
			$NotificationCount = $this->common_model->NotificationCount();
			
			$body['aps'] = array(
				'alert' => array(
					'body' => $message,
					'flag' => 'survey',
					'survey_details' => $survey_genaral,
					'nId' => $nId,
					//'user_type' => $user_type,
					'action-loc-key' => 'Opel360 App',
					),
				'badge' => $badgecount,
				'sound' => 'oven.caf',
				);
			// Encode the payload as JSON
			$payload = json_encode($body);
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			socket_close($fp);
			fclose($fp);
			//$this->response(array("result"=>$result,"ddd"=>"sss"), 200);exit;
			return $result;
		}


	}
	?>
