<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accessories extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model("accessories_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->model("catalog_model");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->load->library("image_lib");
		$this->accessories_file_path =  realpath('images/Accessories');
		$this->accessories_file_path_thumb =  realpath('images/Accessories/thumb');
		//$this->load->library("richtexteditor"); 
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission(4);
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	
	public function getAccessories()
	{ 
		
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				
				
				//GET ALL MODELS
				$cond = array();
				$catalog = $this->catalog_model->getAllCatalogModel($cond);
				foreach($catalog as $catalog_value) {
					
					$model_list .= '<option value="'.$catalog_value['id'].'">'.$catalog_value['model_name'].'</option>';
					
				}
				$data['models'] = $model_list;
				$cond = array();
				$acctype = $this->accessories_model->getAccessoriesTypeById($cond);
				foreach($acctype as $acc) {
					
					$acctype_list .= '<option value="'.$acc['id'].'">'.$acc['acc_type'].'</option>';
					
				}
				$data['acctype'] = $acctype_list;
				
				$this->load->view("administrator/accessories",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getAccessoriesById() // Get Single PDF
	{
		if(is_ajax_request())
		{ 
			
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("accessories_id" => $this->encrypt->decode($postData["id"]));
				$pdf = $this->accessories_model->getAccessoriesById($cond);
				echo json_encode($pdf[0]);exit;
			
		}
	}
	
	
	
	//Validate File
	
	public function validateManualfile($postData,$files)
	{
	
		if($files!=""){ 
		$count = count($files['name']);
		if (isset($files['name'][0]) && $files['name'][0] != "") { 
			$img_info = getimagesize($_FILES['accessories_file']['tmp_name']);
			
			$image_width = $img_info[0];
			$image_height = $img_info[1];
			
			
			if($image_width <=100  || $image_height <=100) { 
				$error = "The image width and height should be greater than 100 pixel";
				return array("status" => 0,"error" => $error); exit;
			}
			
			
            $config = array(
                'upload_path' => $this->accessories_file_path,
                'allowed_types' => 'gif|jpg|png|jpeg',
                'max_width' => '6000',
                'max_height' => '5000'
            );
            
        // load Upload library
           $this->load->library('upload', $config);
           $check_upload = $this->upload->do_upload('accessories_file');
 
			if($check_upload){
				$uniqid = date('Y-m-d-H-i-s') . '_' . uniqid();
				$ext = pathinfo($_FILES['accessories_file']['name'], PATHINFO_EXTENSION); 
				$img_thumb = $uniqid.'.'.$ext;		
				$uploaded_manual = $this->upload->data('accessories_file'); 
				
				$path_to_image = $uploaded_manual['full_path'];
				$new_img_path  = $this->accessories_file_path_thumb.'/'.$img_thumb;
				//echo $this->accessories_file_path_thumb.'/'.$uploaded_manual['file_name'];die;
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = $path_to_image;
				$config1['new_image'] = $new_img_path;
				$config1['create_thumb'] = TRUE;
				$config1['create_thumb'] = TRUE;
				$config1['thumb_marker'] = '';
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 300;
				$config1['height'] = 300;
			    $this->image_lib->initialize($config1);
			    $this->image_lib->resize();
			
				$uploaded_manual = $this->upload->data('accessories_file'); 
				return array("status" => 1,"count" => $count,"data_flat" => $uploaded_manual,"thumb"=>$img_thumb); die;
			 }
			 else { 
				$error = $this->upload->display_errors();
				return array("status" => 0,"error" => $error); 
				 
			 }
		}
			 
		else {
				return array("status" => 1,"is_file"=>0);
		}
	}
	else{
				return array("status" => 1,"is_file"=>0);
	}
	}
	
	
	 
	public function save_accessories() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();
					
					$FILEDATA = $_FILES['accessories_file'];
					
					if($postData["manual_id"] == "")
					{   
						$isValid = $this->validateManualfile($postData,$FILEDATA);
						if($isValid["status"] == 1)
						{ 
								//date_default_timezone_set('Asia/Singapore');
								$created_date = date('Y-m-d H:i:s');
								$insertId = $this->common_model->insert(TB_ACCESSORIES,array("accessories_name" => $postData["txt_accessories_name"],"accessories_pic" => $isValid['data_flat']['file_name'],"accessosries_thumb"=>$isValid['thumb'],"model_id" => $postData["model"],"accessories_desc" => $postData["accessories_manual_desc"],"date_modified" => $created_date));
								
								if($insertId)
								{    
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Accessories has been added successfully.</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValid['error'].'</div>')); exit;
						} 
						 
					}
					else
					{  
						//echo $_FILES['accessories_file']['name'];die;
						
							
						$isValid = $this->validateManualfile($postData,$FILEDATA);
						$file_name = $isValid['data_flat']['file_name'];
						
						
						//print_r($isValid);die;
						if($isValid["status"] == 1)
						{ 
							$cond1 = array("accessories_id" => $this->encrypt->decode($postData["manual_id"]));
								//date_default_timezone_set('Asia/Singapore');
							$updated_date = date('Y-m-d H:i:s');
							$updateArr = $this->common_model->update(TB_ACCESSORIES,$cond1,array("accessories_name" => $postData["txt_accessories_name"],"model_id" => $postData["model"],"accessories_desc" => $postData["accessories_manual_desc"],"date_modified" => $updated_date));
							
							if(isset($_FILES['accessories_file']['name']) && $_FILES['accessories_file']['name']!="") {
								$this->common_model->update(TB_ACCESSORIES,$cond1,array("accessosries_thumb"=>$isValid['thumb'], "accessories_pic" => $file_name));
							}
							
							
							$acctype = $this->accessories_model->getAccesseriesGallery($cond1); 
							//print_r($acctype);die;
							$cond_acc = array("model_id" => $postData["model"]);
							$catalog = $this->catalog_model->getCatalogModelById($cond_acc); 
							
							$row = '<td class="text-center" width="5%"><input type="checkbox" value="'.$postData["manual_id"].'" name="check" id="check" class="chk"></td>
							<td class="text-center"  width="25%">'.$catalog[0]["model_name"].'</td>
							<td class="text-center"  width="25%"><img width="30%" src="'.base_url().'images/Accessories/'.$acctype[0]['accessories_pic'].'"/></a></td>
							<td class="text-center"  width="25%">'.$acctype[0]["accessories_desc"].'</td>
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["manual_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>';
							//echo $this->encrypt->decode($postData["userId"]);die;
							echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["manual_id"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Accessories has been updated successfully.</div>')); exit;
							
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValid['error'].'</div>')); exit;
						}
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_accessories() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					//print_r($postData);die;
					$cond = array();
					$like = array();
					$join = array(TB_CAR_CATALOG => TB_ACCESSORIES.".model_id = ".TB_CAR_CATALOG.".model_id");
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->accessories_model->getaccessoriesCount($cond,$like,$join);
					//echo $this->db->last_query();die;
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("model_name","accessories_pic","accessories_desc");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "accessories_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$accessories = $this->accessories_model->getaccessoriesPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like,$join);
					//print_r($accessories);
					//echo $this->db->last_query();die;
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'model_name\',\''.$corder[0].'\')">Model name</th>
									<th class="text-center '.$css["1"].'" width="25%" onclick="changePaginate(0,\'accessories_pic\',\''.$corder[1].'\')">Accessories image</th>
									<th class="text-center '.$css["2"].'" width="25%" onclick="changePaginate(0,\'accessories_desc\',\''.$corder[2].'\')">Accessories Description</th>
									<th class="text-center" width="15%">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($accessories)>0)
					{
							if(count($accessories) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($accessories as $row) {  
								 
								 
								$table .= '<tr id="row_'.$row["id"].'">
											<td class="text-center"  width="5%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
											<td class="text-center"  width="25%">'.$row["model_name"].'</td>
											<td class="text-center"  width="25%"><img width="30%" src="'.base_url().'images/Accessories/'.$row["accessories_pic"].'"/></td>
											<td class="text-center"  width="25%">'.$row["accessories_desc"].'</td>
											<td class="text-center">
												<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($row["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
											</td>
										  </tr> 
										  </tr>
										  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($accessories)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="5">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($pdf),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	
	
	
	public function validateCatalogModel($postData)
	{   
		
		return array("status" => 1);
	}
	
	
	public function delete_accessories() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("accessories_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_ACCESSORIES,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	public function getAccessoriesType()
	{ 
		
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				
				$this->load->view("administrator/accessories_type",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getAccessoriesTypeById() // Get Single PDF
	{
		if(is_ajax_request())
		{ 
			
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("acc_type_id" => $this->encrypt->decode($postData["id"]));
				$pdf = $this->accessories_model->getAccessoriesTypeById($cond);
				echo json_encode($pdf[0]);exit;
			
		}
	}
	
	public function save_accessories_type() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();
					 
					if($postData["acctype_id"] == "")
					{   
						$isValid = $this->validateAccessoriesType($postData);
						if($isValid["status"] == 1)
						{ 
								//date_default_timezone_set('Asia/Singapore');
								$created_date = date('Y-m-d H:i:s');
								$insertId = $this->common_model->insert(TB_ACCESSORIES_TYPE,array("acc_type" => $postData["txt_accessories_name"],"date_modified" => $created_date));
								
								if($insertId)
								{    
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Accessories Type has been added successfully.</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValid['error'].'</div>')); exit;
						} 
						 
					}
					else
					{  
						$isValid = $this->validateAccessoriesType($postData);
												
						if($isValid["status"] == 1)
						{ 
							$cond1 = array("acc_type_id" => $this->encrypt->decode($postData["acctype_id"]));
								//date_default_timezone_set('Asia/Singapore');
							$updated_date = date('Y-m-d H:i:s');
							$updateArr = $this->common_model->update(TB_ACCESSORIES_TYPE,$cond1,array("acc_type" => $postData["txt_accessories_name"],"date_modified" => $updated_date));
							
							$row = '<td class="text-center" width="5%"><input type="checkbox" value="'.$postData["acctype_id"].'" name="check" id="check" class="chk"></td>
							<td class="text-center"  width="25%">'.$postData["txt_accessories_name"].'</td>
							<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["acctype_id"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								
							</td>';
							//echo $this->encrypt->decode($postData["userId"]);die;
							echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["acctype_id"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Accessories type has been updated successfully.</div>')); exit;
							
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValid['error'].'</div>')); exit;
						}
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_accessories_type() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					//print_r($postData);die;
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->accessories_model->getaccessoriesTypeCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("acc_type");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "acc_type_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$accessories = $this->accessories_model->getaccessoriesTypePerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//print_r($accessories);
					//echo $this->db->last_query();die;
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="25%" onclick="changePaginate(0,\'acc_type\',\''.$corder[0].'\')">Accessories Type</th>
									<th class="text-center" width="15%">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($accessories)>0)
					{
							if(count($accessories) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($accessories as $row) {  
								 
						$table .= '<tr id="row_'.$row["id"].'">
								<td class="text-center"  width="5%"><input type="checkbox" value="'.$this->encrypt->encode($row["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center"  width="25%">'.$row["acc_type"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($row["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr> 
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($accessories)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="3">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					$this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($pdf),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	
	
	
	public function validateAccessoriesType($postData)
	{   
		
		return array("status" => 1);
	}
	
	
	public function delete_accessories_type() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("acc_type_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_ACCESSORIES_TYPE,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
}
?>
