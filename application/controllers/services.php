<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("services_model");
		$this->load->model("login_model");
		$this->load->library("form_validation");
		$this->load->model("catalog_model");
		$this->load->library("email");
		$this->load->helper("email_template");
		$this->load->library("ValidateFile");
		
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission($userdata['user_id'],'service_cost_management');
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	
	public function getServicesCost()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				
				//GET ALL MODELS
				$cond = array();
				$catalog = $this->catalog_model->getAllCatalogModel($cond);
				foreach($catalog as $x => $catalog_value) {
					
					$model_list .= '<option value="'.$catalog_value['id'].'">'.$catalog_value['model_name'].'</option>';
					
				}
				$data['models'] = $model_list;
				$this->load->view("administrator/services_cost",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getServicesCostById() // Get Single User
	{
		if(is_ajax_request())
		{  
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("s_id" => $this->encrypt->decode($postData["id"]));
				$cost = $this->services_model->getServicesCostById($cond);
				//print_r($users);die;
				echo json_encode($cost[0]);exit;
			
		}
	}
	
	public function save_services_cost() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();  
						//print_r($postData);die;
					if($postData["sId"] == "")
					{   
						$isValid = $this->validateServiceCost($postData);
						//print_r($isValid);die;
						if($isValid["status"] == 1)
						{ 
							$cond = array("service_name"=>$postData["txt_service_name"],TB_SERVICE_COST.".model_id" => $postData["model"]);
							$costs = $this->services_model->getAllServicesCost($cond);
							$data["costs"] = $costs;
							
							if(count($costs) == 0)
							{
								$insertId = $this->common_model->insert(TB_SERVICE_COST,array("service_name" => $postData["txt_service_name"], "service_cost" => $postData["txt_service_cost"],"model_id" => $postData["model"],"date_modified" => date('Y-m-d H:i:s')));
								if($insertId)
								{	  
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Services cost has been added successfully.</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This service has been already available.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);
						} 
						 
					}
					else
					{ 
						$isValid = $this->validateServiceCost($postData);
						if($isValid["status"] == 1)
						{
							$cond = array("s_id !=" => $this->encrypt->decode($postData["sId"]), "service_name"=>$postData["txt_service_name"],TB_SERVICE_COST.".model_id" => $postData["model"]);
							$cond1 = array("s_id" => $this->encrypt->decode($postData["sId"]));
							$costs = $this->services_model->getAllServicesCost($cond);
							$userbyid = $this->services_model->getServicesCostById($cond1); 
							
							$data["costs"] = $costs; 
							//echo count($users);die;
							if(count($costs) == 0)
							{  
								 
									$updateArr = $this->common_model->update(TB_SERVICE_COST,$cond1,array("service_name" => $postData["txt_service_name"], "service_cost" => $postData["txt_service_cost"],"model_id" => $postData["model"],"date_modified" => date('Y-m-d H:i:s')));
									$cond_model = array("model_id" => $row['model_id']);
									$model_name = $this->catalog_model->getAllCatalogModel($cond_model);
									$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["sId"].'"></td>
											<td class="text-center">'.$model_name[0]['model_name'].'</td>
											<td class="text-center">'.$postData["txt_service_name"].'</td>
											<td>$'.$postData["txt_service_cost"].'</td>
											<td class="text-center">
												<a title="Edit" alt="Edit" id="edit" class="" data-option="'.$postData["sId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
											</td>';
											//echo $this->encrypt->decode($postData["userId"]);die;
									echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["sId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Service cost has been updated successfully.</div>')); exit;
								
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This services has been already taken.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);exit;
						}
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_service_cost() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->services_model->getServicesCostCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("model_name","service_name","service_cost");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "s_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$costs = $this->services_model->getServicesCostPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//echo $this->db->last_query();die;
					
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'model_name\',\''.$corder[0].'\')">Model Name</th>
									<th class="text-center '.$css[1].'" width="20%" onclick="changePaginate(0,\'service_name\',\''.$corder[1].'\')">Services Name</th>
									<th class="text-center '.$css[2].'" onclick="changePaginate(0,\'service_cost\',\''.$corder[2].'\')">Services Cost</th>
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($costs)>0)
					{
							if(count($costs) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($costs as $cost) {  
								
								 
								
						$table .= '<tr id="row_'.$cost["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cost["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center">'.$cost["model_name"].'</td>
								<td class="text-center">'.$cost["service_name"].'</td>
								<td class="text-center">S$'.$cost["service_cost"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cost["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($costs)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="5">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($costs),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	public function validateServiceCost($postData)
	{   
		return array("status" => 1);
	}
	
	
	public function delete_service_cost() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("s_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_SERVICE_COST,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
	
	/*
	 * service exclusive
	 * 
	 * */
	
	public function getServicesExclusiveCost()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				
				//GET ALL MODELS
				$cond = array();
				$catalog = $this->catalog_model->getAllCatalogModel($cond);
				foreach($catalog as $x => $catalog_value) {
					
					$model_list .= '<option value="'.$catalog_value['id'].'">'.$catalog_value['model_name'].'</option>';
					
				}
				$data['models'] = $model_list;
				$this->load->view("administrator/services_cost_exclusive",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}
	
	
	public function getServicesExclusiveCostById() // Get Single User
	{
		if(is_ajax_request())
		{  
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("se_id" => $this->encrypt->decode($postData["id"]));
				$cost = $this->services_model->getServicesExclusiveCostById($cond);
				//print_r($users);die;
				echo json_encode($cost[0]);exit;
			
		}
	}
	
	public function save_services_exclusive() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
					$postData = $this->input->post();  
					//	print_r($postData);die;
					if($postData["sId"] == "")
					{   
						$isValid = $this->validateServiceExclusive($postData);
						//print_r($isValid);die;
						if($isValid["status"] == 1)
						{ 
							$cond = array("year" => $postData["year"],"milage"=>$postData["txt_milage"],TB_SERVICE_EXCLUSIVE.".model_id" => $postData["model"]);
							$costs = $this->services_model->getAllServicesExclusiveCost($cond);
							$data["costs"] = $costs;
							
							if(count($costs) == 0)
							{
								$insertId = $this->common_model->insert(TB_SERVICE_EXCLUSIVE,array("milage" => $postData["txt_milage"],"year" => $postData["year"], "cost" => $postData["txt_service_cost"],"model_id" => $postData["model"],"date_modified" => date('Y-m-d H:i:s')));
								if($insertId)
								{	  
									echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Services exclusive has been added successfully.</div>')); exit;
								}
								else
								{
									echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
								}
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This service exclusive has been already available.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);
						} 
						 
					}
					else
					{ 
						
						$isValid = $this->validateServiceExclusive($postData);
						if($isValid["status"] == 1)
						{
							$cond = array("se_id !=" => $this->encrypt->decode($postData["sId"]), "year" => $postData["year"],"milage"=>$postData["txt_milage"],TB_SERVICE_EXCLUSIVE.".model_id" => $postData["model"]);
							$cond1 = array("se_id" => $this->encrypt->decode($postData["sId"]));
							$costs = $this->services_model->getAllServicesExclusiveCost($cond);
							//$userbyid = $this->services_model->getServicesExclusiveCostById($cond1); 
							//echo "test";die;
							 
							
							$data["costs"] = $costs; 
							//echo count($users);die;
							if(count($costs) == 0)
							{  
								 
									$updateArr = $this->common_model->update(TB_SERVICE_EXCLUSIVE,$cond1,array("year" => $postData["year"],"milage" => $postData["txt_milage"], "cost" => $postData["txt_service_cost"],"model_id" => $postData["model"],"date_modified" => date('Y-m-d H:i:s')));
									$cond_model = array("model_id" => $postData["model"]);
									$model_name = $this->catalog_model->getAllCatalogModel($cond_model);
									$row = '<td class="text-center"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["sId"].'"></td>
											<td class="text-center">'.$model_name[0]['model_name'].'</td>
											<td class="text-center">'.ordinal($postData["year"])." Year".'</td>
											<td class="text-center">'.number_format($postData["txt_milage"]).'</td>
											<td class="text-center">S$'.$postData["txt_service_cost"].'</td>
											<td class="text-center">
												<a title="Edit" alt="Edit" id="edit" class="" data-option="'.$postData["sId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
											</td>';
											//echo $this->encrypt->decode($postData["userId"]);die;
									echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["sId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Service exclusive has been updated successfully.</div>')); exit;
								
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This services exclusive has been already taken.</div>')); exit;
							}
						}
						else
						{
							echo json_encode($isValid);exit;
						}
						
					} 
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	public function list_service_exclusive() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				
					$postData = $this->input->post();
					
					$cond = array();
					$like = array();
					if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
					{
						for($x=0; $x<count($postData["searchBy"]); $x++)
						{
							if($postData["searchBy"][$x] != "")
							{
								$like[$postData["searchBy"][$x]] = trim($postData["search"]); 
							} 
						}
					}
					$count = $this->services_model->getServicesExclusiveCostCount($cond,$like);
					//echo $this->db->last_query();
					if(!isset($postData["start"])){ $postData["start"] = 0; }
					if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
					
					$orderColumns = array("model_name","year","milage","cost");
					
					if(!in_array($postData["column"],$orderColumns))
					{
						$order_column = "se_id";
						$postData["order"] == "DESC";
					}
					else
					{
						$order_column = $postData["column"];
					}
					$orderArr = array("ASC","asc","DESC","desc");
					if(!in_array($postData["order"],$orderArr))
					{
						$postData["order"] = "ASC";
					}
					
					foreach($orderColumns AS $k => $v)
					{
						
						if($postData["column"] != $v)
						{
							$corder[$k] = "DESC";
							$css[$k] = "sorting";
						}
						else
						{
							if($postData["order"] == "ASC")
							{
								$css[$k] = "sorting_desc";
								$corder[$k] = "DESC";
							}
							else
							{
								$css[$k] = "sorting_asc";
								$corder[$k] = "ASC";
							}
						}
					}
					
					$costs = $this->services_model->getServicesExclusiveCostPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
					//echo $this->db->last_query();die;
					
					
					$links = "";
					$table = "";
					if($postData["start"] == 0)
					{
					$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
							$table .= '<thead>
								  <tr>
									<th class="text-center" width="5%"><input type="checkbox" id="selecctall"></th>
									<th class="text-center '.$css["0"].'" width="20%" onclick="changePaginate(0,\'model_name\',\''.$corder[0].'\')">Model Name</th>
									<th class="text-center '.$css["1"].'" width="20%" onclick="changePaginate(0,\'year\',\''.$corder[1].'\')">Year</th>
									<th class="text-center '.$css[2].'" width="20%" onclick="changePaginate(0,\'milage\',\''.$corder[2].'\')">Mileage(In number)</th>
									<th class="text-center '.$css[3].'" onclick="changePaginate(0,\'cost\',\''.$corder[3].'\')">Services Exclusive Cost</th>
									<th class="text-center">Action</th>
								  </tr>
								</thead>
								<tbody>';
					}
					if(count($costs)>0)
					{
							if(count($costs) == 1) { $start = $postData["start"] - PER_PAGE_OPTION; }else{$start = $postData["start"];}
							foreach($costs as $cost) {  
								
								 
								
						$table .= '<tr id="row_'.$cost["id"].'">
								<td class="text-center" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cost["id"]).'" name="check" id="check" class="chk"></td>
								<td class="text-center">'.$cost["model_name"].'</td>
								<td class="text-center">'.ordinal($cost["year"])." Year".'</td>
								<td class="text-center">'.number_format($cost["milage"]).'</td>
								<td class="text-center">S$'.$cost["cost"].'</td>
								<td class="text-center">
									<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$this->encrypt->encode($cost["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								</td>
							  </tr>
							  </tr>
							  ';
							}
					}
					if($postData["start"] == 0)
					{
							if(count($costs)==0)
							{
								$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
							}
							$table .= '</tbody>
							</table>';
					}
					$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "");
					// $this->pagination->initialize($config);
					$to = $postData["start"]+PER_PAGE_OPTION;
					if($to > $count[0]["cnt"])
					{
						$to = $count[0]["cnt"];
						$paginate = ($to).",".$order_column.",".$postData["order"];
					}
					else
					{
						$paginate = ($postData["start"]+PER_PAGE_OPTION).",".$order_column.",".$postData["order"];
					}
					
					echo json_encode(array('table' => $table,'limit'=>PER_PAGE_OPTION,'start' => $postData["start"],'totalrec' => count($costs),'paginate' => $paginate)); exit;
				
			}
		}
	}
	
	public function validateServiceExclusive($postData)
	{   
		return array("status" => 1);
	}
	
	
	public function delete_service_exclusive() // Delete Tax Authorities
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$arrDelete = array();
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					$isdelete = 0;
					$cond = array("se_id" => $this->encrypt->decode($postData["ids"][$i]));
					$isdelete = $this->common_model->delete(TB_SERVICE_EXCLUSIVE,$cond);
					if($isdelete)
					{
						$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
					}
				}
				echo json_encode(array("ids" => $arrDelete));exit;
			}
		}
	}
	
}
?>
