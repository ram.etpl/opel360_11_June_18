<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("events_model");
		$this->load->model("login_model");
		$this->load->model("users_model");
		$this->load->library("form_validation");
		$this->load->library("email");
		$this->load->library("ValidateFile");
		$this->load->helper('download');
        $this->load->library('excel');
		$this->eventimagepath = realpath('images/Events');
		date_default_timezone_set('Asia/Singapore');
		if(!is_user_logged_in())
		{ 
			redirect('logout'); exit;
		}
		$this->no_cache();
	}
	
	protected function no_cache()
	{
		  header('Cache-Control: no-store, no-cache, must-revalidate');
		  header('Cache-Control: post-check=0, pre-check=0',false);
		  header('Pragma: no-cache'); 
	}
	
	public function checkAdminPermission(){
		$userdata = $this->session->userdata("auth_user");
		if($userdata['user_type'] == 'ADMIN'){
				$res_permission = $this->login_model->getModulePermission(2);
				if($res_permission == 1){
					return 1;
				}
				else{
					return 0;
				}
		}
		else{
			return 1;
		}
	}
	
	
	public function getEvents()
	{
		if(!is_user_logged_in())
		{
			redirect(); exit;
		}
		else
		{   
			if($this->checkAdminPermission()){
				
				$this->load->view("administrator/events",$data);
			}
			else{
				$this->load->view("administrator/access_denied",$data);
			}
			
		}
	}

	
	//UPLOAD EVENT PICTURE
	
	public function validateEventPic($files)
	{
	
		$count = count($files['name']);		
		
		if (isset($files['file_event_pic']['name']) && $files['file_event_pic']['name'] != "") {
						
			$img_info = getimagesize($files['file_event_pic']['tmp_name']);
			$image_width = $img_info[0];
			$image_height = $img_info[1];
			$ext = $this->getExtension($files['file_event_pic']['name']);
			$valid_formats = array("jpg", "png", "jpeg", "PNG", "JPG", "JPEG");
            if (in_array($ext, $valid_formats)) {
            	if($image_width <=100  || $image_height <=100) { 
					$error = "The image width and height should be greater than 100 pixel";
					return array("status" => 0,"error" => $error); exit;
				}
				$time = time();
				$newimagename = 'eventpic'.$time;
				$filename=$files['file_event_pic']['name'];
				$extension=end(explode(".", $filename));
				$file_name= $newimagename;
				
	            $config = array(
	                'upload_path' => $this->eventimagepath,
	                'allowed_types' => 'jpg|JPG|png|PNG|jpeg|JPEG',
	                'max_width' => '6000',
	                'max_height' => '5000'
	                
	            );
	            
	        // load Upload library
	           $this->load->library('upload', $config);
	           $check_upload = $this->upload->do_upload('file_event_pic',$file_name);
	 			
				if($check_upload){
	 
					$uploaded_manual = $this->upload->data('file_event_pic',$file_name);
					//echo "<pre>";print_r($uploaded_manual );die; 
					return array("status" => 1,"count" => $count,"data_flat" => $uploaded_manual); die;
				 }
				 else {
					$error = $this->upload->display_errors();
					return array("status" => 0,"error" => $error); 
					 
				 }
            } else {
            	$error = "please select Jpg,Jpeg and png etc files";
				return array("status" => 0,"error" => $error); exit;
            }

			


		 }
			 
		else {
				return array("status" => 1,"is_file"=>0);
			}
	}

    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }

        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return strtolower($ext);
    }
	
	
	//LIST EVENTS
	
	public function list_events() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				$cond = array();
				$like = array();
				
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					$like[$postData["searchBy"]] = trim($postData["search"]); 
				
				}
				else if($postData["fromdate"])
				{	
					$cond['start_date >='] = trim(date('Y-m-d',strtotime($postData["fromdate"])));
					$cond['end_date <='] = trim(date('Y-m-d',strtotime($postData["todate"])));
				}

				
				$count = $this->events_model->getEventsCount($cond,$like);
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("event_name","start_date","end_date");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "event_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$contest = $this->events_model->getEventsListPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-left" width="5%"><input type="checkbox" id="selecctall"></th>
								<th class="text-left '.$css["0"].'" width="20%" onclick="changePaginate(0,\'event_name\',\''.$corder[0].'\')">Name</th>
								<th class="text-left '.$css[1].'" width="15%" onclick="changePaginate(0,\'start_date\',\''.$corder[1].'\')">Start Date</th>
								<th class="text-left '.$css[2].'" width="15%" onclick="changePaginate(0,\'end_date\',\''.$corder[2].'\')">End Date</th>
								<th class="text-left '.$css[2].'" width="10%">Type</th>
								<th class="text-left" width="10%">Status</th>
								<th class="text-center" width="15%">Action</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($contest)>0)
				{
						if(count($contest) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						foreach($contest as $cont) { 
						
						if($cont["status"] == "Upcoming"){
								$status_text = "<span class='label label-warning'>Upcoming</span>";
								$edit_td = '<td class="text-center">
								<a title="View details" alt="viewdetails" id="view" class="view" data-option="'.$this->encrypt->encode($cont["id"]).'" href="javascript:void(0)">View Details</a>
							| <a title="Edit" alt="Edit" id="edit" class="" data-option="'.$this->encrypt->encode($cont["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>';
						}
						else if($cont["status"] == "Running"){
							$status_text = "<span class='label label-success'>Running</span>";
							$edit_td = '<td class="text-center"><a title="View details" alt="viewdetails" id="view" class="view" data-option="'.$this->encrypt->encode($cont["id"]).'" href="javascript:void(0)">View Details</a>
							| N/A</td>';
						}
						else if($cont["status"] == "Closed"){
							$status_text = "<span class='label label-danger'>Closed</span>";
							$edit_td = '<td class="text-center"><a title="View details" alt="viewdetails" id="view" class="view" data-option="'.$this->encrypt->encode($cont["id"]).'" href="javascript:void(0)">View Details</a>
							| N/A</td>';
						}
						
					$table .= '<tr id="row_'.$cont["id"].'">
							<td class="text-left" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cont["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-left">'.ucwords($cont["event_name"]).'</td> 
							<td class="text-left">'.($cont["start_date"]?date("d/m/Y",strtotime($cont["start_date"])):"N/A").'</td>
							<td class="text-left">'.($cont["end_date"]?date("d/m/Y",strtotime($cont["end_date"])):"N/A").'
							<input type="hidden" id="type" class="type" name="type" value="'.$cont["type"].'">
							</td>
							<td class="text-left">'.($cont["type"]?$cont["type"]:"N/A").'</td>
							<td class="text-left">'.$status_text.'</td>
								
							'.$edit_td.'
						  </tr>';
						}
				}
				if($postData["start"] == 0)
				{
						if(count($contest)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getContestList");
				//$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE,'start' => $postData["start"],'totalrec' => count($contest),'paginate' => $paginate)); exit;
			}
		}
	}
	
	//LIST EVENTS
	
	public function getEventsHistory()
		{
			if(!is_user_logged_in())
			{
				redirect(); exit;
			}
			else
			{   
				if($this->checkAdminPermission()){
					
					$this->load->view("administrator/events_history",$data);
				}
				else{
					$this->load->view("administrator/access_denied",$data);
				}
				
			}
		}


	public function list_events_history() // List users
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{ 
				$postData = $this->input->post();
				$cond = array();
				$like = array();
				
				if($postData["search"] != "" && count($postData["searchBy"]) >= 0)
				{
					$like[$postData["searchBy"]] = trim($postData["search"]); 
				
				}
				else if($postData["fromdate"])
				{	
					$cond['start_date >='] = trim(date('Y-m-d',strtotime($postData["fromdate"])));
					$cond['end_date <='] = trim(date('Y-m-d',strtotime($postData["todate"])));
				}

				
				$count = $this->events_model->getEventsCount($cond,$like);
				if(!isset($postData["start"])){ $postData["start"] = 0; }
				if(!isset($postData["order"])){ $postData["order"] = "ASC"; }
				
				$orderColumns = array("event_name","start_date","end_date");
				
				if(!in_array($postData["column"],$orderColumns))
				{
					$order_column = "event_id";
					$postData["order"] == "DESC";
				}
				else
				{
					$order_column = $postData["column"];
				}
				$orderArr = array("ASC","asc","DESC","desc");
				if(!in_array($postData["order"],$orderArr))
				{
					$postData["order"] = "ASC";
				}
				
				foreach($orderColumns AS $k => $v)
				{
					
					if($postData["column"] != $v)
					{
						$corder[$k] = "DESC";
						$css[$k] = "sorting";
					}
 					else
					{
						if($postData["order"] == "ASC")
						{
							$css[$k] = "sorting_desc";
							$corder[$k] = "DESC";
						}
						else
						{
							$css[$k] = "sorting_asc";
							$corder[$k] = "ASC";
						}
					}
				}
				$contest = $this->events_model->getEventsListPerPage($cond,$postData["start"],array($order_column => $postData["order"]),$like);
				$rsvp_user = $this->common_model->select("*",TB_RSVP_USER,'');
				//$contest = array_merge($eventData,$rsvp_user);
				//echo "<pre>";print_r($contest);die;
				$links = "";
				$table = "";
				if($postData["start"] == 0)
				{
				$table = '<table class="table table-striped table-hover dataTable no-footer" width="100%">';
						$table .= '<thead>
							  <tr>
								<th class="text-left" width="1%"><input type="checkbox" id="selecctall"></th>
								<th class="text-left '.$css["0"].'" width="10%" onclick="changePaginate(0,\'event_name\',\''.$corder[0].'\')">Event Name</th>
								<th class="text-left '.$css[1].'" width="10%" onclick="changePaginate(0,\'start_date\',\''.$corder[1].'\')">Event Start Date</th>
								<th class="text-left '.$css[2].'" width="10%" onclick="changePaginate(0,\'end_date\',\''.$corder[2].'\')">Event End Date</th>					
								<th class="text-left" width="10%">Status</th>
								<th class="text-left" width="9%">User Type</th>
								<th class="text-center" width="10%">Name</th>
								<th class="text-center" width="10%">Email</th>
								<th class="text-center" width="10%">Mobile</th>
								<th class="text-center" width="10%">Car Registration Number</th>
							  </tr>
							</thead>
							<tbody>';
				}
				if(count($contest)>0)
				{
						if(count($contest) == 1) { $start = $postData["start"] - PER_PAGE; }else{$start = $postData["start"];}
						$t = array();
						foreach($contest as $cont) { 
						$cond = array("event_id"=>$cont['id']);
						$rsvp_user = $this->common_model->select("*",TB_RSVP_USER,$cond);
						$t = $rsvp_user;
						//echo "<pre>";print_r(count($rsvp_user)); //die;
						foreach ($rsvp_user as $key => $value) {
							//echo "<pre>";print_r($value);
						
						//echo "<pre>";print_r($rsvp_user); //die;
						if($cont["status"] == "Upcoming"){
								$status_text = "<span class='label label-warning'>Upcoming</span>";
								$edit_td = '<td class="text-center">
								<a title="Edit" alt="Edit" id="edit" class="" data-option="'.$this->encrypt->encode($cont["id"]).'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
							</td>';
						}
						else if($cont["status"] == "Running"){
							$status_text = "<span class='label label-success'>Running</span>";
							$edit_td = '<td class="text-center">N/A</td>';
						}
						else if($cont["status"] == "Closed"){
							$status_text = "<span class='label label-danger'>Closed</span>";
							$edit_td = '<td class="text-center">N/A</td>';
						}
						if($cont["id"] == $value["event_id"])
						{


					$table .= '<tr id="row_'.$cont["id"].'">
							<td class="text-left" width="10%"><input type="checkbox" value="'.$this->encrypt->encode($cont["id"]).'" name="check" id="check" class="chk"></td>
							<td class="text-left">'.ucwords($cont["event_name"]).'</td> 
							<td class="text-left">'.($cont["start_date"]?date("d/m/Y",strtotime($cont["start_date"])):"N/A").'</td>
							<td class="text-left">'.($cont["end_date"]?date("d/m/Y",strtotime($cont["end_date"])):"N/A").'</td>
							<td class="text-left">'.$status_text.'</td>
							<td class="text-left">'.$value["user_type"].'</td>	
							<td class="text-left">'.$value["name"].'</td>
							<td class="text-left">'.$value["email"].'</td>
							<td class="text-left">'.$value["contact_number"].'</td>
							<td class="text-left">'.$value["car_registration_no"].'</td>
						  </tr>';
						}
							}
					} //die;
				}
				// echo "<pre>";print_r(count($t));die;
				if($postData["start"] == 0)
				{
						if(count($contest)==0 || count($t)==0)
						{
							$table .= '<tr id=""><td class="text-center" colspan="6">No Records Found</td></tr>';
						}
						$table .= '</tbody>
						</table>';
				}
				$config = get_pagination_config($postData["start"], $count[0]["cnt"], $order_column, $postData["order"], "getContestList");
				//$this->pagination->initialize($config);
				$to = $postData["start"]+PER_PAGE;
				if($to > $count[0]["cnt"])
				{
					$to = $count[0]["cnt"];
					$paginate = ($to).",".$order_column.",".$postData["order"];
				}
				else
				{
					$paginate = ($postData["start"]+PER_PAGE).",".$order_column.",".$postData["order"];
				}
				
				echo json_encode(array('table' => $table,'limit'=>PER_PAGE,'start' => $postData["start"],'totalrec' => count($contest),'paginate' => $paginate)); exit;
			}
		}
	}
	
	//ADD/EDIT EVENTS
	
	public function save_event() // Add/Edit Payment Type 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();


				if($postData["contestId"] == "")
				{
					$isValid = $this->validateEvent($postData);
					//print_r($isValid);die;
					if($isValid["status"] == 1)
					{
						$isValidPic = $this->validateEventPic($_FILES);

						if($isValidPic["status"] == 1)
						{
						$cond = array("event_name" => $postData["txt_title"]);
						$join = array();
						$contests = $this->events_model->getAllEvents($cond,$join);
						$data["contest"] = $contests;
						
						if(count($contests) == 0)
						{
							$date = date('Y-m-d'); 
							$status = "Upcoming";
							if($date == date('Y-m-d',strtotime($postData["txt_from_date"]))){
								$status = "Running";
							}
							else if($date > date('Y-m-d',strtotime($postData["txt_from_date"]))){
								$status = "Upcoming";
							}
							
							$file_name = $isValidPic['data_flat']['file_name'];
							
							$event_shown = implode(",",$postData["event_shown"]);
							
							if($postData["rsvp"] !='')
							{
								$rsvp = 1;
							}else{
								$rsvp = '';
							}

							/* sujit added code for push notification */
							if($postData["event_promo"]=="Event"){
								$msgforNotification="A new Event has been added by admin Team, Check it out here.";
								$notflag = "Event";
							}else{
								$msgforNotification="A new Promotion has been added by admin Team, Check it out here.";
								$notflag = "Promotion";
							}
							if($event_shown==0){
								$condUser=array("user_type"=>"OPEL","status"=>"active","device_type !="=>"");
							}elseif($event_shown==1){
								$condUser=array("user_type"=>"NONOPEL","status"=>"active","device_type !="=>"");
							}else{
								$condUser=array("status"=>"active","device_type !="=>"");
							}
							/* end code */
							$insertId = $this->common_model->insert(TB_EVENTS,array("start_date" => $postData["txt_from_date"], "end_date" => $postData["txt_to_date"],"event_name" => $postData["txt_title"],"event_description" => $postData["txt_description"],"status"=> $status,"event_pic"=>$file_name,"event_show_to" => $event_shown,"type" => $postData["event_promo"],"is_rsvp" => $rsvp,"date_modified" => date('Y-m-d H:i:s')));
							

							if($insertId)
							{
								/* sujit added code for push notification */
								$userDevice = $this->common_model->select("user_id,c_name,last_name,user_type,device_id,device_type",TB_USERS,$condUser);
								$condEvent=array("event_id"=>$insertId);
								$curentEvent = $this->common_model->select("*",TB_EVENTS,$condEvent);
								// echo "<pre>";print_r($curentEvent);die;
								$nId = mt_rand(10000000, 99999999);
								if(count($userDevice)>0){
									foreach($userDevice as $userinfo){
										if($userinfo['device_id']!="" && ($userinfo['device_type'] == "android" || $userinfo['device_type'] == "Android")){
											$registrationIds = array($userinfo['device_id']);
											$eventpromo1	= $curentEvent[0];
											if($eventpromo1['event_pic']!=""){
												$event_img = base_url().'images/Events/'.$eventpromo1['event_pic'];
											 }
											 else{
												$event_img = base_url().'images/event.jpg';
											 }
											$eventpromo = array(
												'event_id' 	=> $eventpromo1['event_id'],
												'event_name' =>$eventpromo1['event_name'],
												'event_pic' =>$event_img,
												'event_description' => $eventpromo1['event_description'],
												'start_date' => $eventpromo1['start_date'],
												'end_date' => $eventpromo1['end_date'],
												'is_rsvp' => $eventpromo1['is_rsvp'],
												'type' => $eventpromo1['type'],
												'nId' => $nId
											);
											$msg = array(
												'message' 	=> $msgforNotification,
												'type'	=> $notflag,
												'registration_ids' 	=> $registrationIds,
												'data'			=> $msg,
												'eventpromo'	=> $eventpromo,												
												'action_by' =>'',
												'action_by_profile_pic' =>'',
												'action' => '',
												'action_time' => trim(date('Y-m-d H:i:s'))
											);
											 // echo "<pre>";print_r($msg);die;
											$notify = $this->sendPushnotificationAndroid($registrationIds,$msg,$notflag,$eventpromo); 
										
										}
										if($userinfo['device_id']!="" && ($userinfo['device_type']=="ios" || $userinfo['device_type']=="Ios")){
											$message = $msgforNotification;
											$eventpromo1	= $curentEvent[0];
											if($eventpromo1['event_pic']!=""){
												$event_img = base_url().'images/Events/'.$eventpromo1['event_pic'];
											 }
											 else{
												$event_img = base_url().'images/event.jpg';
											 }
											  
											$eventpromo = array(
												'event_id' 	=> $eventpromo1['event_id'],
												'event_name' =>$eventpromo1['event_name'],
												'event_pic' =>$event_img,
												'event_description' => $eventpromo1['event_description'],
												'start_date' => $eventpromo1['start_date'],
												'end_date' => $eventpromo1['end_date'],
												'is_rsvp' => $eventpromo1['is_rsvp'],
												'type' => $eventpromo1['type'],
												'nId' => $nId
												
											);
											$userDevice = $this->common_model->select("badgecount",TB_NOTIFICATION,array("device_token" =>$userinfo['device_id'],"status" =>'0'));
												
											    $badgecount = $userDevice[0]["badgecount"]+1;
											    $cond1 = array("device_token" => $userinfo['device_id']);
											    $updatecontest = $this->common_model->update(TB_NOTIFICATION,$cond1,array("badgecount" => $badgecount));
											    
											$notify = $this->sendPushnotificationIos($userinfo['device_id'],$message,$notflag,$eventpromo,$badgecount);
										}
										
									}
									if($event_shown==0)
									{
										$cond = array("user_type"=>'OPEL');
										$userID = $this->users_model->getUserById($cond);
									}
									if($event_shown==1)
									{
										$cond = array("user_type"=>'NONOPEL');
										$userID = $this->users_model->getUserById($cond);
										
									}
									if($event_shown==2)
									{
										$userID = $this->users_model->getUserByIdAll();										
									}

																			
									foreach ($userID as $key => $value) {
										$this->common_model->insert(TB_NOTIFICATION,array("user_id" => $value['id'],"nId" => $nId,"event_id" => $eventpromo['event_id'],"json_response"=>json_encode($eventpromo),"user_type" => $event_shown,"device_token"=>$value['device_id'],"notification_type" => $eventpromo1['type'],"created_date" => trim(date('Y-m-d H:i:s'))));
									}
								} 
								/* end code */
								$arrContest = array();
								$contest_id = $this->encrypt->encode($insertId);
								$cid = str_replace(array('+', '/', '='), array('-', '_', '~'), $contest_id); 

								echo json_encode(array("status" => 1,"action" => "add", "msg" => '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Event has been added successfully.</div>')); exit;
							}
							else
							{
								echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>Opps, there is problem please try again.</div>')); exit;
							}
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This event title has been already available.</div>')); exit;
						}
					}
					else {
						echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValidPic['error'].'</div>')); exit;
					}
				}
					else
					{
						echo json_encode($isValid);
					} 
					 
				}
				else
				{ 
					
					
					if(count($_FILES)>0 && $_FILES['file_event_pic']['name']!="") {
						$isValidPic = $this->validateEventPic($_FILES);
						$file_name = $isValidPic['data_flat']['file_name'];
					}
					else {
						$isValidPic["status"] =1;
						$file_name = $postData["hidden_file_event_pic"];
					}
					
					if($isValidPic["status"] == 1)
					{
						
						$cond = array("event_name" => $postData["txt_title"],"event_id !=" => $this->encrypt->decode($postData["contestId"]));
						$join = array();
						$contests = $this->events_model->getAllEvents($cond,$join);
						$data["contest"] = $contests;
						
						if(count($contests) == 0)
						{
							$date = date('Y-m-d'); 
							$status = "Upcoming";
							if($date == date('Y-m-d',strtotime($postData["txt_from_date"]))){
								$status = "Running";
							}
							else if($date > date('Y-m-d',strtotime($postData["txt_from_date"]))){
								$status = "Upcoming";
							}
							
							
							$event_shown = implode(",",$postData["event_shown"]);
							/* sujit added code for push notification */
							if($postData["event_promo"]=="Event"){
								$msgforNotification="A new Event has been added by admin Team, Check it out here.";
								$notflag = "Event";
							}else{
								$msgforNotification="A new Promotion has been added by admin Team, Check it out here.";
								$notflag = "Promotion";
							}
							if($event_shown==0){
								$condUser=array("user_type"=>"OPEL","status"=>"active","device_type !="=>"");
							}elseif($event_shown==1){
								$condUser=array("user_type"=>"NONOPEL","status"=>"active","device_type !="=>"");
							}else{
								$condUser=array("status"=>"active","device_type !="=>"");
							}
							/* end code */
							$cond1 = array("event_id" => $this->encrypt->decode($postData["contestId"]));
							
							if(isset($postData["txt_from_date"]) && isset($postData["txt_from_date"])) {
								$updatecontest = $this->common_model->update(TB_EVENTS,$cond1,array("start_date" => $postData["txt_from_date"], "end_date" => $postData["txt_to_date"]));
							}
							//echo "<pre>";print_r($postData["rsvp"]);die;
							if($postData["rsvp"] != '')
							{
								$rsvp = 1;
							}else{
								$rsvp = '';
							}
							//echo "<pre>";print_r($rsvp);die;
							$updatecontest = $this->common_model->update(TB_EVENTS,$cond1,array("event_name" => $postData["txt_title"],"event_description" => $postData["txt_description"],"status"=> $status,"event_pic"=>$file_name,"event_show_to" => $event_shown,"type" => $postData["event_promo"],"is_rsvp" => $rsvp,"date_modified" => date('Y-m-d H:i:s')));


							$curentEvent = $this->common_model->select("*",TB_EVENTS,$cond1);
							/* sujit added code for push notification */
							$userDevice = $this->common_model->select("user_id,c_name,last_name,user_type,device_id,device_type",TB_USERS,$condUser);
							$nId = mt_rand(10000000, 99999999);
							if(count($userDevice)>0){
								foreach($userDevice as $userinfo){
									if($userinfo['device_id']!="" && ($userinfo['device_type'] == "android" || $userinfo['device_type'] == "Android")){
										$registrationIds = array($userinfo['device_id']);
										$eventpromo1 = $curentEvent[0];
										if($eventpromo1['event_pic']!=""){
											$event_img = base_url().'images/Events/'.$eventpromo1['event_pic'];
										 }
										 else{
											$event_img = base_url().'images/event.jpg';
										 }
				 						$eventpromo = array(
											'event_id' 	=> $eventpromo1['event_id'],
											'event_name' =>$eventpromo1['event_name'],
											'event_pic' =>$event_img,
											'event_description' => $eventpromo1['event_description'],
											'start_date' => $eventpromo1['start_date'],
											'end_date' => $eventpromo1['end_date'],
											'is_rsvp' => $eventpromo1['is_rsvp'],
											'type' => $eventpromo1['type'],
											'nId' => $nId
										);

										$msg = array(
											'message' 	=> $msgforNotification,
											'type'	=> $notflag,
											'registration_ids' 	=> $registrationIds,
											'data'			=> $msg,
											'eventpromo'	=> $eventpromo,
											'action_by' =>'',
											'action_by_profile_pic' =>'',
											'action' => '',
											'action_time' => trim(date('Y-m-d H:i:s'))
										);
										$notify = $this->sendPushnotificationAndroid($registrationIds,$msg,$notflag,$eventpromo); 
										
									}
									if($userinfo['device_id']!="" && ($userinfo['device_type']=="ios" || $userinfo['device_type']=="Ios")){
										$message = $msgforNotification;
										$eventpromo1 = $curentEvent[0];
										if($eventpromo1['event_pic']!=""){
											$event_img = base_url().'images/Events/'.$eventpromo1['event_pic'];
										 }
										 else{
											$event_img = base_url().'images/event.jpg';
										 }
										$eventpromo = array(
											'event_id' 	=> $eventpromo1['event_id'],
											'event_name' =>$eventpromo1['event_name'],
											'event_pic' =>$event_img,
											'event_description' => $eventpromo1['event_description'],
											'start_date' => $eventpromo1['start_date'],
											'end_date' => $eventpromo1['end_date'],
											'is_rsvp' => $eventpromo1['is_rsvp'],
											'type' => $eventpromo1['type'],
											'nId' => $nId
										);
										$userDevice = $this->common_model->select("badgecount",TB_NOTIFICATION,array("device_token" =>$userinfo['device_id'],"status" =>'0'));
												
											    $badgecount = $userDevice[0]["badgecount"]+1;
											    $cond1 = array("device_token" => $userinfo['device_id']);
											    $updatecontest = $this->common_model->update(TB_NOTIFICATION,$cond1,array("badgecount" => $badgecount));
											    
										$notify = $this->sendPushnotificationIos($userinfo['device_id'],$message,$notflag,$eventpromo,$badgecount);
									}
								}

								if($event_shown==0)
									{
										$cond = array("user_type"=>'OPEL');
										$userID = $this->users_model->getUserById($cond);
									}
									if($event_shown==1)
									{
										$cond = array("user_type"=>'NONOPEL');
										$userID = $this->users_model->getUserById($cond);
										
									}
									if($event_shown==2)
									{
										$userID = $this->users_model->getUserByIdAll();										
									}
									
									foreach ($userID as $key => $value) {
										$this->common_model->insert(TB_NOTIFICATION,array("user_id" => $value['id'],"nId" => $nId,"json_response"=>json_encode($eventpromo),"user_type" => $event_shown,"device_token"=>$value['device_id'],"notification_type" => $eventpromo1['type'],"created_date" => trim(date('Y-m-d H:i:s'))));
									}
								
							}
							/* end code */
							
								// $cond2 = array("event_id" => $this->encrypt->decode($postData["contestId"]));
								// $cont = $this->events_model->getAllEvents($cond2); 
								
								// if($cont[0]["status"] == "Upcoming"){
								// $status_text = "<span class='label label-warning'>Upcoming</span>";
								// }
								// else if($cont[0]["status"] == "Running"){
								// 	$status_text = "<span class='label label-success'>Running</span>";
								// }
								// else if($cont[0]["status"] == "Closed"){
								// 	$status_text = "<span class='label label-danger'>Closed</span>";
								// }
																 
								// $row = '<td class="text-left"><input type="checkbox" class="chk" id="check" name="check" value="'.$postData["contestId"].'"></td>
								// 		<td class="text-left">'.ucwords($postData["txt_title"]).'</td>
								// 		<td class="text-left">'.($cont[0]["start_date"]?date("d/m/Y",strtotime($cont[0]["start_date"])):"N/A").'</td>
								// 		<td class="text-left">'.($cont[0]["end_date"]?date("d/m/Y",strtotime($cont[0]["end_date"])):"N/A").'</td>
								// 		<td>'.$status_text.'</td>
								// 		<td class="text-center">
								// 		<a title="View details" alt="viewdetails" id="view" class="view" data-option="'.$postData["contestId"].'" href="javascript:void(0)">View Details</a> |
								// 		<a title="Edit" alt="Edit" id="edit" class="editPayment" data-option="'.$postData["contestId"].'" href="javascript:void(0)"><i class="fa fa-edit"></i></a>
								// 		</td>';
										//echo $this->encrypt->decode($postData["userId"]);die;
								// echo json_encode(array("status" => 1,"action" => "modify","row" => $row,"id"=>$this->encrypt->decode($postData["contestId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>The event has been updated successfully.</div>')); exit;
							echo json_encode(array("status" => 1,"action" => "modify","id"=>$this->encrypt->decode($postData["contestId"]), "msg" => '<div class="alert  alert-success"><button data-dismiss="alert" class="close" type="button">×</button>The event has been updated successfully.</div>')); exit;
							
						}
						else
						{
							echo json_encode(array("status" => 2,"action" => "modify", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>This event title has been already available.</div>')); exit;
						}
					}
					else {
						echo json_encode(array("status" => 2,"action" => "add", "msg" => '<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'.$isValidPic['error'].'</div>')); exit;
					}
				}
			}
			else
			{
				echo json_encode(array("status" => 0, "msg"=>"Redirect to login page.")); exit;
			}
		}
	}
	
	
	public function delete_event() // Delete Event
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{

				
				$postData = $this->input->post();
				$arrDelete = array();

				$ids = array();
				
				for($i = 0; $i < count($postData["ids"]); $i++)
				{
					
					$cond1 = array("event_id"=>$this->encrypt->decode($postData["ids"][$i]));

					$ids = $ids . $this->encrypt->decode($postData["ids"][$i]).',';


					$rsvp_user = $this->common_model->select("*",TB_RSVP_USER,$cond1);


					$conduser = array("user_id"=>$rsvp_user[0]['user_id']);
					$userDevice = $this->common_model->select("user_id,c_name,last_name,user_type,device_id,device_type",TB_USERS,$conduser);


					
					$condEvent = array("event_id"=>$this->encrypt->decode($postData["ids"][$i]));
					$curentEvent = $this->common_model->select("*",TB_EVENTS,$condEvent);

					// echo "<pre>";print_r($curentEvent);die;
					$msgforNotification="A Event has been deleted by admin Team, Check it out here.";
							$nId = mt_rand(10000000, 99999999);

							$delete = $this->common_model->delete(TB_NOTIFICATION,array("event_id" =>$this->encrypt->decode($postData["ids"][$i])));

							$cond = array("event_id" => $this->encrypt->decode($postData["ids"][$i]));
							$isdelete = $this->common_model->delete(TB_EVENTS,$cond);

							if($isdelete)
							{
								$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
							}


							if(count($userDevice)>0){
								
								foreach($userDevice as $userinfo){
									if($userinfo['device_id']!="" && ($userinfo['device_type'] == "android" || $userinfo['device_type'] == "Android")){
										$registrationIds = array($userinfo['device_id']);
										$eventpromo1 = $curentEvent[0];
										if($eventpromo1['event_pic']!=""){
											$event_img = base_url().'images/Events/'.$eventpromo1['event_pic'];
										 }
										 else{
											$event_img = base_url().'images/event.jpg';
										 }
				 						$eventpromo = array(
											'event_id' 	=> $eventpromo1['event_id'],
											'event_name' =>$eventpromo1['event_name'],
											'event_pic' =>$event_img,
											'event_description' => $eventpromo1['event_description'],
											'start_date' => $eventpromo1['start_date'],
											'end_date' => $eventpromo1['end_date'],
											'is_rsvp' => $eventpromo1['is_rsvp'],
											'type' => $eventpromo1['type'],
											'nId' => $nId
										);

										$msg = array(
											'message' 	=> $msgforNotification,
											'type'	=> $eventpromo1['type'],
											'registration_ids' 	=> $registrationIds,
											'data'			=> $msg,
											'eventpromo'	=> $eventpromo,
											'action_by' =>'',
											'action_by_profile_pic' =>'',
											'action' => '',
											'action_time' => trim(date('Y-m-d H:i:s'))
										);
										$notify = $this->sendPushnotificationAndroid($registrationIds,$msg,$notflag,$eventpromo); 
										
									}
									if($userinfo['device_id']!="" && ($userinfo['device_type']=="ios" || $userinfo['device_type']=="Ios")){
										$message = $msgforNotification;
										$eventpromo1 = $curentEvent[0];


										if($eventpromo1['event_pic']!=""){
											$event_img = base_url().'images/Events/'.$eventpromo1['event_pic'];
										 }
										 else{
											$event_img = base_url().'images/event.jpg';
										 }


										 
										$eventpromo = array(
											'event_id' 	=> $eventpromo1['event_id'],
											'event_name' =>$eventpromo1['event_name'],
											'event_pic' =>$event_img,
											'event_description' => $eventpromo1['event_description'],
											'start_date' => $eventpromo1['start_date'],
											'end_date' => $eventpromo1['end_date'],
											'is_rsvp' => $eventpromo1['is_rsvp'],
											'type' => $eventpromo1['type'],
											'nId' => $nId
										);


										$data = array(
											 	"end_date" => $eventpromo1['end_date'],
											 	'event_description' => $eventpromo1['event_description'],
												'start_date' => $eventpromo1['start_date'],
												'event_id' 	=> $eventpromo1['event_id'],
												'event_name' =>$eventpromo1['event_name'],
												'event_pic' =>$event_img,
												'is_rsvp' => $eventpromo1['is_rsvp'],
												'type' => $eventpromo1['type'],
												'nId' => $nId
											 

											  
										);


										$insertData = array(


											"user_id" => $userinfo['user_id'],
											"nId" => $nId,
											"event_id" => $eventpromo['event_id'],
											"json_response" => json_encode($data),
											"notification_type" => 'Event Deleted',
											'user_type' => 2,
											'created_date' => date('Y-m-d H:i:s')
										);



										
										$delete = $this->common_model->delete(TB_NOTIFICATION,array("event_id" =>$eventpromo['event_id']));

										$insert = $this->common_model->insert(TB_NOTIFICATION,$insertData);
										
										//print_r($this->db->last_query()); 


										$userDevice = $this->common_model->select("badgecount",TB_NOTIFICATION,array("device_token" =>$userinfo['device_id'],"status" =>'0'));
												
									    $badgecount = $userDevice[0]["badgecount"]+1;
									    $cond1 = array("device_token" => $userinfo['device_id']);
									    $updatecontest = $this->common_model->update(TB_NOTIFICATION,$cond1,array("badgecount" => $badgecount));

									    	    
										$notify = $this->sendPushnotificationIos($userinfo['device_id'],$message,$notflag,$eventpromo,$badgecount);
									}
								}
								//echo $this->db->last_query(); 
								
								$isdelete = 0;
								$cond = array("event_id" => $this->encrypt->decode($postData["ids"][$i]));
								$isdelete = $this->common_model->delete(TB_EVENTS,$cond);
								if($isdelete)
								{
									$arrDelete[] = $this->encrypt->decode($postData["ids"][$i]);
								}
								
							}




				}


				echo json_encode(array("ids" => $arrDelete)); die;
			}
		}
	}
	
	public function delete_event_pic() // Delete event pic
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$cond = array("event_id" => $postData["id"]);
				$contest = $this->events_model->getContestById($cond); 
				//print_r($contest);die;
				unlink($this->eventimagepath."/".$contest[0]['event_pic']);
				$data_update = array("event_pic"=>"");
				$isupdate = $this->common_model->update(TB_EVENTS,$cond,$data_update);
				
				if($isupdate){
					echo json_encode(array("status" => "success")); die;
				}
			}
		}
	}
	
	
	
	
	public function getEventById() // Get Single User
	{
		if(is_ajax_request())
		{ 
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("event_id" => $this->encrypt->decode($postData["id"]));
				$contest = $this->events_model->getContestById($cond);
				echo json_encode(array("contest" => $contest[0])); die;
			} 
		}
	}

	public function getEventByIdDetials() // Get Single User details
	{
		if(is_ajax_request())
		{ 
			if(is_user_logged_in())
			{
				$postData = $this->input->post(); 
				//print_r($postData);die;
				$cond = array("event_id" => $this->encrypt->decode($postData["id"]));
				$contest = $this->events_model->getContestById($cond);
				/* echo "<pre>";print_r($cond);
				 echo "<pre>";print_r($contest);die;die;*/
				$html  ="";
				
				$start_date = date("d/m/Y", strtotime($contest[0]['start_date']));
				$end_date = date("d/m/Y", strtotime($contest[0]['end_date']));
				if($contest[0]['event_show_to'] == 2)
				{
					$event_show_to = 'All users';
				}
				if($contest[0]['event_show_to'] == 1)
				{
					$event_show_to = 'Non Opel users';
				}
				if($contest[0]['event_show_to'] == 0)
				{
					$event_show_to = 'Opel users';
				}
				if($contest[0]['event_pic'] != ""){
					$event_img = base_url().'images/Events/'.$contest[0]['event_pic'];
				 }
				 else{
					$event_img = base_url().'images/event.jpg';
				 }
				if($contest[0]['type'] == 'Event') 
				{
					$type = "Event";
				} 
				else
				{
					$type = "Promotion";
				} 

					$html .= '<div class="section row mbn">
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">'.$type.' Name</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$contest[0]['event_name'].'</strong>
									  </div>
									</div>
								  </div>
								  <div class="col-sm-12">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-2 pn mt5 control-label new_first">Description</label>
									  <div class="col-lg-8 text_inpu_new"> 
											<strong>'.$contest[0]['event_description'].'</strong>
									  </div>
									</div>
								  </div>
								 
							</div>
							<div class="section row mbn">
								  
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">Start Date</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($start_date?$start_date:"-").'</strong>
									  </div>
									</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">End Date</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.$end_date.'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">'.$type.' Status</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.($contest[0]['status']?$contest[0]['status']:"-").'</strong>
									  </div>
									</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">'.$type.' show to</label>
									  <div class="col-lg-8 text_inpu_new">
											<strong>'.$event_show_to .'</strong>
									  </div>
									</div>
								  </div>
							</div>
							<div class="section row mbn">
								  
								  <div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first">'.$type.' Picture</label>
									  <div class="col-lg-8 text_inpu_new">
											<img src="'.$event_img.'" width="250px" height="150px"/>
									  </div>
									</div>
									</div>
									<div class="col-sm-6">
									<div class="form-group">
									  <label for="inputStandard" class="col-lg-4 pn mt5 control-label new_first"></label>
									  <div class="col-lg-8 text_inpu_new">
											
									  </div>
									</div>
								  </div>
							</div>';

				echo json_encode(array("user_details" => $html,"types"=>$type)); die;
			} 
		}
	}
	
	
	public function validateEvent($postData)
	{   
		return array("status" => 1);
	}


	 function rsvpReport() {
        error_reporting(E_ALL);
        $this->excel->setActiveSheetIndex(0);
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:K1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        // $this->excel->getActiveSheet()->getStyle('A4:J4')->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle('A4:J4')->getFont()->setSize(12);
        // $this->excel->getActiveSheet()->getStyle('A4:J4')->getFill()->getStartColor()->setARGB('#333');

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
        for ($col = ord('A'); $col <= ord('K'); $col++) { //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
            //change the font size
            $this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(20);
            $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
            $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }
        
        //name the worksheet
            //$this->excel->getActiveSheet()->setTitle('Interviews attended Reports');
            $this->excel->getActiveSheet()->setCellValue('A1', ' Rsvp User Excel Sheet');
        
        //retrive contries table data
            // $result = $this->common_model->select("event_id,event_name,event_description,start_date,end_date",TB_RSVP_USER,'');
		$result = $this->common_model->selectQuery("rsvp_id,event_id,user_type,name,email,contact_number,car_registration_no",TB_RSVP_USER,array(),array('rsvp_id' => 'DESC'));
		//echo "<pre>";print_r($result);die;
        if (!empty($result)) {

                    $this->excel->getActiveSheet()->getStyle('A3')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
                    $this->excel->getActiveSheet()->getStyle('A3')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
                    $this->excel->getActiveSheet()->getStyle('A3')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
                    $this->excel->getActiveSheet()->getStyle('A3')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);

                    $this->excel->getActiveSheet()->getRowDimension(3)->setRowHeight(20);
                    $this->excel->getActiveSheet()->setCellValue('A3', 'Sr. No.');
                    $this->excel->getActiveSheet()->setCellValue('B3', 'Event Title');
                    $this->excel->getActiveSheet()->setCellValue('C3', 'From');
                    $this->excel->getActiveSheet()->setCellValue('D3', 'To');
                    $this->excel->getActiveSheet()->setCellValue('E3', 'Status');
                    $this->excel->getActiveSheet()->setCellValue('F3', 'User Type ');
                    
                    $this->excel->getActiveSheet()->setCellValue('G3', 'Event Description');
                    $this->excel->getActiveSheet()->setCellValue('H3', 'name');
                    $this->excel->getActiveSheet()->setCellValue('I3', 'Email');
                    $this->excel->getActiveSheet()->setCellValue('J3', 'Contact number');
                    $this->excel->getActiveSheet()->setCellValue('K3', 'Car Registration No.');
                    
            $i = 4;
            $k = 1;
            $j = $i + 0;
            foreach ($result as $job) {
                 	
                    //$candi = 1;
                    $candcount = $j;
                     // $countcandi = count($job['candi_data']) + 3;
                    $cond1 = array('event_id'=>$job['event_id']);
        			$rsvpdata = $this->common_model->selectQuery("event_id,event_name,event_description,start_date,end_date,status",TB_EVENTS,$cond1,array('event_id' => 'DESC'));
                    //echo "<pre>";print_r($rsvpdata);die;
                   if(!(empty($rsvpdata))){

                     // foreach ($rsvpdata as $key => $value) {
                     	$test = array_filter($rsvpdata);
                      if($rsvpdata[0]['event_id'] == $job['event_id'])
                      {
                        $this->excel->getActiveSheet()->getRowDimension($candcount)->setRowHeight(20);
                        $this->excel->getActiveSheet()->setCellValue('A' . $candcount, $k);
                        $this->excel->getActiveSheet()->setCellValue('B' . $candcount, $rsvpdata[0]['event_name']);
                        $this->excel->getActiveSheet()->setCellValue('C' . $candcount, $rsvpdata[0]['start_date']);
                        $this->excel->getActiveSheet()->setCellValue('D' . $candcount, $rsvpdata[0]['end_date']);
                        $this->excel->getActiveSheet()->setCellValue('E' . $candcount, $rsvpdata[0]['status']);
                        $this->excel->getActiveSheet()->setCellValue('F' . $candcount, $job['user_type']);
                        $this->excel->getActiveSheet()->setCellValue('G' . $candcount, $rsvpdata[0]['event_description']);
                        $this->excel->getActiveSheet()->setCellValue('H' . $candcount, $job['name']);
                        $this->excel->getActiveSheet()->setCellValue('I' . $candcount, $job['email']);
                        $this->excel->getActiveSheet()->setCellValue('J' . $candcount, $job['contact_number']);
                        $this->excel->getActiveSheet()->setCellValue('K' . $candcount, $job['car_registration_no']);
                      }
                      $k++;
                       // $i = $i + $countcandi;
                    	$j = $j + 1;
                        
                }
               
            }

            ob_start();
            $filename = "Report.xls"; //save our workbook as this file name
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            ob_end_clean();
            $objWriter->save('php://output');
            // redirect("/jobseeker/reports/","Refresh");            
        }
    }

    public function getrsvpresult($event_id)
    {
    	$cond1 = array('event_id'=>$event_id);
        $rsvpdata = $this->common_model->select("event_id,event_name,event_description,start_date,end_date",TB_EVENTS,$cond);
    	$t = array();
    	// foreach ($rsvpdata as $key => $value) {
    	// 	$t[] = $value;
    	// }
    	return $rsvpdata;
    }

    public function sendPushnotificationAndroid($registrationIds,$msg,$flag,$eventpromo) {
    
		// define( 'API_ACCESS_KEY', 'AIzaSyAqILqwjh8c2ulVLyDHw_nLrtsmCQFjXAY' );
		define( 'API_ACCESS_KEY', 'AIzaSyCZ8fos7svfalo3LnLV9aTGvpiyiE44dIY' );

		$fields = array
		(	
			'flag'	=> $flag,
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg,
			'eventpromo'	=> $eventpromo
		);
 
		$headers = array
		(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);
 
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		return $result;		

	}
    
    public function sendPushnotificationIos($deviceToken,$message,$notflag,$eventpromo,$badgecount) { 
			$passphrase = 'Opel1234';
			// Put your alert message here:
			//$message = 'A push notification has been sent!';
			////////////////////////////////////////////////////////////////////////////////
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'Oepl_APNS_Certificates.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			
			// Open a connection to the APNS server
			$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			
			$notificationCount = $this->common_model->NotificationCount();
			// foreach ($notificationCount as $key => $value) {
				$body['aps'] = array(
				'alert' => array(
					'body' => $message,
					'flag' => $notflag,
					'eventpromo' => $eventpromo,
					//'booking_id' => $book_id,
					//'user_type' => $user_type,
					'action-loc-key' => 'Opel360 App',
				),
				'badge' => $badgecount,
				'sound' => 'oven.caf',
				);
			// }
			
			// Encode the payload as JSON
			$payload = json_encode($body);
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			socket_close($fp);
			fclose($fp);
			//$this->response(array("result"=>$result,"ddd"=>"sss"), 200);exit;
			return $result;
	}

}
?>
